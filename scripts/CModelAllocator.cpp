
#include "CModelAllocator.h"
#include "nsfunc.h"
#include "Cd3d.h"
#include "XFileFuncs.h"
#include "NSAssert.h"
#include "XFileFormat.h"


namespace ns_model {

	CModel* CModelAllocator::GetModelFromXFile(const char *_Name)
	{
		using namespace nslib;
		using namespace ns_types;

		size_t __count, __vertex, __index, __square = 0;
		char* __texnames = new char[eSizeofDesc];
		__texnames[0] = eNull;
		__texnames[eDescLength] = eNull;
		char* __ptexnames = __texnames;

		const char __slash = '/';
		const char __header = 'H';
		const char __space = 0x20;
		const char __child = '{';
		const char __parent = '}';
		const char __temp = 't';
		const char __frame = 'F';
		const char __mesh = 'M';
		const char __animation = 'A';
		const char* __coords = "MeshT";
		const char* __material = "MeshM";
		const char* __normals = "MeshN";
		const char* __transform = "FrameT";
		const char* __binarycode = "bzip";
		const char __dupli = 'V';
		const char __xskin = 'X';
		const char __quote = '"';
		const char __effect = 'E';
		const char __string = 'S';
		const char __floats = 'F';

		LPMODELHEADER __modelHeader	= new MODELHEADER();
		LPANIMEHEADER __animeHeader = 0;
		LPSKINEDMESHHEADER __skinHeader = 0;
		LPEFFECTHEADER __fxHeader = 0;

		//LoadFile
		CIFStream ifs(_Name);

		// xof
		char* __src = ifs._getl();
		if (nslib::findstr(__src, __binarycode)) {
			ifs.close();
			delete __modelHeader;
			//return NULL;
			GetModelFromXFileAsBinary(_Name);
		}

		// header
		__src = ifs.getl();
		if (*__src == __space) {
			ifs._ignorel(5);
		}
		// knot.x
		else {
			if (*__src == __slash) { // comment (compatible with triplane.x, heli.x)
				ifs.ignorelc(__slash);
				__src = ifs.getl();
			}
			// template
			if (*__src == __temp) {
				do {
					ifs.ignorel(__parent); // compatible with triplane.x
				} while (*(__src = ifs.getl()) == __temp); // getls: triplane.x ('A`)
				// header: lobby, tiger, triplane
				if (*__src == __header) {
					ifs.ignorels(__parent); // compatible with triplane.x
				}
			}
		}
		do {// while(__src = ifs.getl())
			if (*__src == __parent) {
			}
			else if (*__src == __frame) {
				++ __modelHeader->nFrame;
				__src = ifs.getls();
				if (!strictlystrstr(__src, __transform)) // triplane.x
					continue;
				// FrameTransformMatrix
				ifs.ignorels(__parent); // parent:Frame
			}
			else if (*__src == __mesh) {
				++ __modelHeader->nMesh;
				__modelHeader->nVertex += __vertex = stoi(ifs._getl());
				ifs._ignorel(__vertex);
				__modelHeader->nIndex += __index = stoi(ifs.getl());
				for (size_t i=__index; i; --i) {
					if (stoi(ifs._getl()) == 4) {
						++ __modelHeader->nSquare;
					}
				}

				// child, data, parent の間に空行が無いこと前提
				while (__src = ifs.getls()) { // parent:mesh, getls:triplane.x
					if (*__src == __parent) { // mesh
						break;
					}
					else if (nslib::strictlystrstr(__src, __material)) {
						__modelHeader->nMaterial += __count = stoi(ifs._getl());
						ifs._ignorel(__index + 1);
						// Material
						for (size_t i=0; i<__count; ++i) {
							++ __modelHeader->nMaterial;
							ifs.getl(); // child: Material
							ifs._ignorel(4);
							// TextureFilename
							__src = ifs.getls(); // child:Filename, parent:Material
							if (*__src != __parent) {
								if (!nslib::findstr(__texnames, __src = ifs.getls()+1, __quote)) {
									*__ptexnames = *__src;
									for (; *__src!=__quote; *++__ptexnames = *++__src); // 15step
									*__ptexnames = __slash; // delimiter
									*(++ __ptexnames) = 0;	// terminated
									++ __modelHeader->nTexture;
								}
								__src = ifs.getls(2);// parent:Material
							}
						}
						if (*__src == __effect) { // child:EffectInstance
							if (__fxHeader == NULL) __fxHeader = new EFFECTHEADER;
							ifs._ignorel();
							while (*(__src = ifs.getls()) != __parent) {
								++ __modelHeader->nEffect;
								__src += 11;
								if (*__src == __string) { // EffectParamString
									++ __fxHeader->fxtx;
									__src = ifs.getls(2);
									if (!nslib::findstr(__texnames, ++__src, eQuote)) {
										*__ptexnames = *__src;
										for (; *__src!=__quote; *++__ptexnames = *++__src) { // 15step
											if (*__src == eBackSlash)
												++ __src;
										}
										*__ptexnames = __slash; // delimiter
										*(++ __ptexnames) = 0;	// terminated
										++ __modelHeader->nTexture;
									}
									ifs._ignorel();
								}
								else if (*__src == __floats) { // Floats
									switch(stoi(ifs._getl(2))) {
										case 1:
											++ __fxHeader->fxfloat1;
											break;
										case 2:
										case 3:
											NS_ASSERT_FAILURE(STR(Undefined EffectParamFloats));
										case 4:
											++ __fxHeader->fxfloat4;
											break;
									}
									ifs._ignorel(2);
								}
								else {
									NS_ASSERT_FAILURE(STR(Undefined EffectParam));
									ifs.ignorels(__parent);
								}
							}
							ifs._ignorel();
						}
						ifs._ignorel(); // parent:MaterialList -> while
					}
					else if (strictlystrstr(__src, __coords)) {
						ifs._ignorel(__vertex + 2);
					}
					else if (*__src == __dupli) {
						ifs._ignorel(__vertex + 3);
					}
					else if (strictlystrstr(__src, __normals)) {
						ifs._ignorel(stoi(ifs._getl()));	// furea.x
						ifs.getl();
						ifs._ignorel(__index + 1);
					}
					else if (*__src == __xskin) {
						++ __modelHeader->nSkinedMesh;
						__skinHeader = new SKINEDMESHHEADER();
						__skinHeader->nBoneAll += __count = stoi(ifs._getl(3));
						__skinHeader->nVertexAll += __vertex;
						ifs._ignorel();
						size_t total = 0;
						for (; __count; --__count) {
							size_t weight = stoi(ifs._getl(4));
							total += weight;
							ifs._ignorel(weight*2+2);
						}
						__skinHeader->nWeightAll += total;
					}
					else {
						ifs.ignorels(__parent);
					}
				}
				continue;
			}
			else if (*__src == __animation) {
				++ __modelHeader->nAnimationSet;
				if (!__animeHeader)
					__animeHeader = new ANIMEHEADER();

				while (*ifs.getls() != __parent) {	// child:Animation, parent:ASet
					++ __animeHeader->nAnimation;
					if (*(__src = ifs.getls()) == __child) {	// name, child:AnimationKey
						ifs.ignorel();	// child:AnimationKey
					}
					do {
						switch (stoi(ifs._getl())) {
							case eAnimationQuatanion:
								++ __animeHeader->nQuat;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nQuatList += __count;
								__animeHeader->nMatList += __count;
								break;
							case eAnimationScale:
								++ __animeHeader->nScale;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nVecList += __count;
								break;
							case eAnimationSlide:
								++ __animeHeader->nSlide;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nVecList += __count;
								break;
							case eAnimationMatrix:
								++ __animeHeader->nMat;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nMatList += __count;
						}
						ifs._ignorel(__count+1); // parent:AnimetionKey
						if (*(__src = ifs.getls()) == __child) { // name, child:AKey, parent:Animation
							ifs._ignorel(); // parent:Animation
							break;
						}
					} while (*__src != __parent); // child:AKey, parent:Animation
				}
			}
		} while(__src = ifs.getls());
		__modelHeader->nIndex += __modelHeader->nSquare;
		if (__modelHeader->nTexture == 1) *--__ptexnames = 0;
		LPTEXTUREHEADER __texHeader = CreateTexture(__modelHeader, __texnames, _Name);
		CModel* __model;
		if (__modelHeader->nSkinedMesh) {
			CSkinedMeshModel* __skinedModel = new CSkinedMeshModel(__modelHeader, __animeHeader, __skinHeader, __texHeader, __fxHeader);
			if (!Allocate(__skinedModel)) {
				NS_ASSERT_FAILURE(Allocate::CSkinedMeshModel);
				__skinedModel->Release();
				return NULL;
			}
			if (!LoadData(__skinedModel, ifs, __texnames)) {
				NS_ASSERT_FAILURE(LoadData::CSkinedMeshModel);
				__skinedModel->Release();
				return NULL;
			}
			Scalling(__skinedModel);
			__model = __skinedModel;
		}
		else {
			__model = new CModel(__modelHeader, __animeHeader, __texHeader, __fxHeader);
			if (!Allocate(__model)) {
				NS_ASSERT_FAILURE(Allocate::CModel);
				__model->Release();
				return NULL;
			}
			if (!LoadData(__model, ifs, __texnames)) {
				NS_ASSERT_FAILURE(LoadData::CModel);
				__model->Release();
				return NULL;
			}
			Scalling(__model);
		}

		return __model;
	}


	CModel* CModelAllocator::GetModelFromXFileAsBinary(const char *_Name)
	{
		//D3DXLoadMeshFromX(_Name, 0, pDevice, NULL, &pMat, NULL, &cMat, &pRawMesh);
		nsgl::CXFileAsBinary bnry(_Name);
		bnry.Road();
		FILE* fp;
		fopen_s(&fp, _Name, "rb");

		// Large temp buffer
		nsgl::stXHeader h;

		fread(&h, sizeof(h), 1, fp);

		return NULL;
	}


	LPTEXTUREHEADER CModelAllocator::CreateTexture
		(LPMODELHEADER _HeaderModel, char* _texNames, const char* _Name)
	{
		using namespace ns_types;
		char_t *pass = _HeaderModel->szPass;
		nslib::GetFilePass(pass, _Name);
		size_t __textures = _HeaderModel->nTexture;
		LPTEXTUREHEADER __texHeader = NULL;
		if (__textures == 1) {
			__texHeader = new TEXTUREHEADER();
			mbstowcs_s(NULL, __texHeader->szTexName, eSizeofName, _texNames, eSizeofName);
			ns_d3d::Cd3d::Instance().CreateTexture(__texHeader->szTexName, &__texHeader->pTexture, pass);
		}
		else if (__textures) {
			__texHeader = new TEXTUREHEADER[__textures]();
			LPTEXTUREHEADER __postex = __texHeader;
			char_t __temptexnames[eSizeofDesc],
				*__ptmp = __temptexnames, *__pout = __texHeader->szTexName;
			mbstowcs_s(NULL, __temptexnames, eSizeofDesc, _texNames, eSizeofDesc);
			for (size_t i=0; i<__textures; ++i, __pout = (++__postex)->szTexName, ++__ptmp) {
				*__pout = *__ptmp;
				for (; *__ptmp!=eSlash; *++__pout = *++__ptmp);
				*__pout = 0;
				ns_d3d::Cd3d::Instance().CreateTexture(__postex->szTexName, &__postex->pTexture, pass);
			}
		}
		return __texHeader;
	}


	bool CModelAllocator::Allocate(ns_model::CModel *_Model)
	{
		LPMODELHEADER		pModelHead	= _Model->m_pModelHead;
		LPANIMEHEADER		pAnimeHead	= _Model->m_pAnimeHead;

		// Create the vertex buffer.
		if ( FAILED( ns_d3d::Cd3d::Instance().Getd3dDevice()->CreateVertexBuffer( static_cast<UINT>(pModelHead->nVertex * sizeof( CUSTOMVERTEX )), 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &_Model->m_pVB, NULL )) )
			return	false;

		// Fill the index buffer.
		if ( FAILED( ns_d3d::Cd3d::Instance().Getd3dDevice()->CreateIndexBuffer( static_cast<UINT>(pModelHead->nIndex * 3 * sizeof( word_t )), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &_Model->m_pIB, NULL )) )
			return	false;

		try	{
			if ( pModelHead->nFrame ) {
				_Model->m_pFrameList	= new FRAMEHEADER[ pModelHead->nFrame ];
			}
			_Model->m_pMeshList		= new MESHHEADER[ pModelHead->nMesh ];
			if ( pModelHead->nMaterial ) {
				_Model->m_pMtrlList		= new MATERIAL[ pModelHead->nMaterial ];
			}
			if ( pModelHead->nEffect ) {
				LPEFFECTHEADER __fx = _Model->m_pEffectHead;
				__fx->fxInstance = new EFFECTINSTANCE[pModelHead->nEffect];
				__fx->fxString = new EFFECTSTRING[__fx->fxtx];
				__fx->fxFloats1 = new EFFECTFLOAT1[__fx->fxfloat1];
				__fx->fxFloats4 = new EFFECTFLOAT4[__fx->fxfloat4];
			}

			if ( pModelHead->nAnimationSet ) {
				pAnimeHead->pAnimeSetList	= new ANIMESET[ pModelHead->nAnimationSet ];
				pAnimeHead->pAnimeList	= new ANIMENODE[ pAnimeHead->nAnimation ];
				pAnimeHead->pMatAnime		= new MATRIX[ pAnimeHead->nAnimation ];
				pAnimeHead->pnTimeList	= new size_t[ pAnimeHead->nTimeList ];

				if ( pAnimeHead->nQuat ) {
					pAnimeHead->pQuatList	= new ANIMEQUATHEADER[ pAnimeHead->nQuat ];
				}
				if ( pAnimeHead->nScale ) {
					pAnimeHead->pScaleList	= new ANIMESCALEHEADER[ pAnimeHead->nScale ];
				}
				if ( pAnimeHead->nSlide ) {
					pAnimeHead->pSlideList	= new ANIMESLIDEHEADER[ pAnimeHead->nSlide ];
				}
				if ( pAnimeHead->nMat ) {
					pAnimeHead->pMatList	= new ANIMEMATHEADER[ pAnimeHead->nMat ];
				}
				if ( pAnimeHead->nQuatList ) {
					pAnimeHead->pvQuatList	= new VECTOR4[ pAnimeHead->nQuatList ];
				}
				if ( pAnimeHead->nMatList ) {
					pAnimeHead->pmatList	= new MATRIX[ pAnimeHead->nMatList ];
				}
				if ( pAnimeHead->nVecList ) {
					pAnimeHead->pvList		= new VECTOR3[ pAnimeHead->nVecList ];
				}
			}
		}
		catch ( std::bad_alloc )
		{
			return false;
		}
		return true;
	}


	bool CModelAllocator::Allocate(CSkinedMeshModel *_Model)
	{
		LPMODELHEADER		pModelHead	= _Model->m_pModelHead;
		LPANIMEHEADER		pAnimeHead	= _Model->m_pAnimeHead;
		LPSKINEDMESHHEADER	pSkinHead	= _Model->m_pSkinHead;

		// Create the vertex buffer.
		if ( FAILED( ns_d3d::Cd3d::Instance().Getd3dDevice()->CreateVertexBuffer( static_cast<UINT>(pModelHead->nVertex * sizeof( CUSTOMVERTEX )), 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &_Model->m_pVB, NULL )) )
			return	false;

		// Fill the index buffer.
		if ( FAILED( ns_d3d::Cd3d::Instance().Getd3dDevice()->CreateIndexBuffer( static_cast<UINT>(pModelHead->nIndex * 3 * sizeof( word_t )), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &_Model->m_pIB, NULL )) )
			return	false;

		try	{
			if ( pModelHead->nFrame ) {
				_Model->m_pFrameList = new FRAMEHEADER[ pModelHead->nFrame ];
			}
			_Model->m_pMeshList = new MESHHEADER[ pModelHead->nMesh ];
			if ( pModelHead->nMaterial ) {
				_Model->m_pMtrlList = new MATERIAL[ pModelHead->nMaterial ];
			}

			if ( pModelHead->nAnimationSet ) {
				pAnimeHead->pAnimeSetList = new ANIMESET[ pModelHead->nAnimationSet ];
				pAnimeHead->pAnimeList = new ANIMENODE[ pAnimeHead->nAnimation ];
				pAnimeHead->pMatAnime = new MATRIX[ pAnimeHead->nAnimation ];
				pAnimeHead->pnTimeList = new size_t[ pAnimeHead->nTimeList ];

				if ( pAnimeHead->nQuat ) {
					pAnimeHead->pQuatList = new ANIMEQUATHEADER[ pAnimeHead->nQuat ];
				}
				if ( pAnimeHead->nScale ) {
					pAnimeHead->pScaleList = new ANIMESCALEHEADER[ pAnimeHead->nScale ];
				}
				if ( pAnimeHead->nSlide ) {
					pAnimeHead->pSlideList = new ANIMESLIDEHEADER[ pAnimeHead->nSlide ];
				}
				if ( pAnimeHead->nMat ) {
					pAnimeHead->pMatList = new ANIMEMATHEADER[ pAnimeHead->nMat ];
				}
				if ( pAnimeHead->nQuatList ) {
					pAnimeHead->pvQuatList = new VECTOR4[ pAnimeHead->nQuatList ];
				}
				if ( pAnimeHead->nMatList ) {
					pAnimeHead->pmatList = new MATRIX[ pAnimeHead->nMatList ];
				}
				if ( pAnimeHead->nVecList ) {
					pAnimeHead->pvList = new VECTOR3[ pAnimeHead->nVecList ];
				}
			}

			/////////////////////////////////////
			//	Allocate Skined Mesh Pool.
			_Model->m_pSkinList = new SKINEDMESHNODE[ pModelHead->nSkinedMesh ];
			pSkinHead->pbWeight = new bool[ pSkinHead->nVertexAll ];
			pSkinHead->pnVertexList = new size_t[ pSkinHead->nWeightAll ];
			pSkinHead->pfWeightList = new float[ pSkinHead->nWeightAll ];
			pSkinHead->pSkinVers = new SKINEDMESHNODEVERTEX[ pSkinHead->nVertexAll ];
			pSkinHead->pBoneList = new SKINEDMESHBONE[ pSkinHead->nBoneAll ];
		}
		catch ( std::bad_alloc )
		{
			return false;
		}
		return true;
	}

	bool CModelAllocator::LoadData(CModel *_Model, nslib::CIFStream& ifs, char* _Texnames)
	{
		using namespace nslib;
		using namespace ns_types;
		// Lock vertex buffer
		if( _Model->m_pVB->Lock( 0, 0, reinterpret_cast<void**>( &_Model->m_pVertices ), 0 ) < 0 )
			return	false;

		// Lock index buffer
		if( _Model->m_pIB->Lock( 0, 0, reinterpret_cast<void**>( &_Model->m_pwIndices ), 0 ) < 0 )
			return	false;

		size_t __count, __vertex, __index;
		char* __ptexnames = _Texnames;

		const char __header = 'H';
		const char __temp = 't';
		const char __frame = 'F';
		const char __mesh = 'M';
		const char __animation = 'A';
		const char* __coords = "MeshT";
		const char* __material = "MeshM";
		const char* __normals = "MeshN";
		const char* __transform = "FrameT";
		const char __dupli = 'V';
		const char __xskin = 'X';
		const char __effect = 'E';
		const char __string = 'S';
		const char __floats = 'F';

		LPMODELHEADER __modelHeader = _Model->m_pModelHead;
		LPFRAMEHEADER __posFrame = _Model->m_pFrameList, __prevFrame = NULL,
			__firstFrame = __posFrame;
		LPMESHHEADER __posMesh = _Model->m_pMeshList;
		LPTEXTUREHEADER __posTex = _Model->m_pTexList;
		LPCUSTOMVERTEX __customVers = _Model->m_pVertices,
			__firstVtx = __customVers, __polygon[3], __customVersSub;
		word_t* __wordIndices = _Model->m_pwIndices;
		LPMATERIAL __posMtrl = _Model->m_pMtrlList;
		word_t __offsetvtx = 0;
		size_t __textures = __modelHeader->nTexture, __frames = __modelHeader->nFrame;

		LPANIMEHEADER __animeHeader = _Model->m_pAnimeHead;
		LPANIMENODE __animeNodes;
		LPANIMESET __animeSets;
		LPMATRIX __posMtx, __posTransMtx;
		LPANIMEQUATHEADER __animeQuats;
		LPANIMESCALEHEADER __animeScales;
		LPANIMESLIDEHEADER __animeSlides;
		LPANIMEMATHEADER __animeMats;
		LPVECTOR4 __posQuat;
		LPVECTOR3 __posVec;
		size_t *__posTime;
		if (__animeHeader != NULL) {
			__animeNodes = __animeHeader->pAnimeList;
			__animeSets = __animeHeader->pAnimeSetList;
			__posMtx = __animeHeader->pmatList;
			__animeQuats = __animeHeader->pQuatList;
			__posQuat = __animeHeader->pvQuatList;
			__animeScales = __animeHeader->pScaleList;
			__posVec = __animeHeader->pvList;
			__animeSlides = __animeHeader->pSlideList;
			__animeMats = __animeHeader->pMatList;
			__posTime = __animeHeader->pnTimeList;
			__posTransMtx = __animeHeader->pMatAnime;
		}

		LPEFFECTHEADER __fxHeader = _Model->m_pEffectHead;
		LPEFFECTINSTANCE __fxInst;
		LPEFFECTSTRING __fxTx;
		LPEFFECTFLOAT1 __fxFloat1;
		LPEFFECTFLOAT4 __fxFloat4;
		if (__fxHeader != NULL) {
			__fxInst = __fxHeader->fxInstance;
			__fxTx = __fxHeader->fxString;
			__fxFloat1 = __fxHeader->fxFloats1;
			__fxFloat4 = __fxHeader->fxFloats4;
		}

		ifs.seekbegin();
		// xof
		ifs._ignorel();
		char* __src = ifs.getl();
		// header
		if (*__src == eSpace) {
			ifs._ignorel(5);
		}
		// knot.x
		else {
			if (*__src == eSlash) { // comment (compatible with triplane.x, heli.x)
				ifs.ignorelc(eSlash);
				__src = ifs.getl();
			}
			// template
			if (*__src == __temp) {
				do {
					ifs.ignorel(eParent); // compatible with triplane.x
				} while (*(__src = ifs.getl()) == __temp); // getls: triplane.x ('A`)
				// header: lobby, tiger, triplane
				if (*__src == __header) {
					ifs.ignorels(eParent); // compatible with triplane.x
				}
			}
		}
		do {// while(__src = ifs.getl())
			if (*__src == eParent) {
				__prevFrame = __prevFrame->pPrev;
			}
			else if (*__src == __frame) {
				GetNameT(__posFrame->szFrameName, __src+6, eSpace);
				__src = ifs.getls();
				if (!nslib::strictlystrstr(__src, __transform)) // triplane.x
					continue;
				// FrameTransformMatrix
				D3DXMATRIX* mat = &__posFrame->matTrans;
				GetMatrixFromStream(ifs, mat);
				ifs._ignorel();
				//ifs.ignorels(__parent); // parent:Matrix

				__posFrame->pPrev = __prevFrame;
				__prevFrame = __posFrame;
				++__posFrame;
			}
			else if (*__src == __mesh) {
				__posMesh->nVertexNum = __vertex = stoi(ifs._getl());
				__customVersSub = __customVers;	// for TextureCoords
				VECTOR3* vec = &__customVers->position;
				for (size_t i=__vertex; i; --i, vec=&(++__customVers)->position) {
					__src = ifs._getl();
					vec->x = stof32(&__src);
					vec->y = stof32(&__src);
					vec->z = stof32(__src);
					__customVers->normal = VECTOR3(.0f, .0f, .0f);
				}
				__index = stoi(ifs.getl());
				size_t offsetpolygon = 0;
				for (size_t i=__index; i; --i, ++__wordIndices) {
					VECTOR3 vec;
					switch (stoi(&(__src = ifs.getls())))
					{
					case 4:
						{
							word_t* buf = __wordIndices;
							*__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
							VectorCross(&vec,
								&(__polygon[1]->position - __polygon[0]->position),
								&(__polygon[2]->position - __polygon[0]->position));
							__polygon[0]->normal += vec;
							__polygon[1]->normal += vec;
							__polygon[2]->normal += vec;
							*++__wordIndices = *buf;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = *(buf+2);
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
							++ offsetpolygon;
							break;
						}
					case 3:
						{
							*__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
						}
					}
					VectorCross(&vec,
						&(__polygon[1]->position - __polygon[0]->position),
						&(__polygon[2]->position - __polygon[0]->position));
					__polygon[0]->normal += vec;
					__polygon[1]->normal += vec;
					__polygon[2]->normal += vec;
				}
				__offsetvtx += (word_t)__posMesh->nVertexNum;
				__posMesh->nPolygonNum = __index + offsetpolygon;

				// child, data, parent の間に空行が無いこと前提
				while (__src = ifs.getls()) { // parent:mesh, getls:triplane.x
					if (*__src == eParent) { // mesh
						break;
					}
					else if (nslib::strictlystrstr(__src, __material)) {
						__posMesh->pMtrl = __posMtrl;
						__posMesh->nMtrlNum = __count = atoi(ifs._getl());
						ifs._ignorel(__index + 1);
						// Material
						for (; __count; --__count, ++__posMtrl) {
							ifs.getl(); // child: Material
							GetMaterialFromStream(ifs, __posMtrl);
							// TextureFilename
							__src = ifs.getls(); // child:Filename, parent:Material
							if (*__src != eParent) {
								if (__textures == 1) {
									__posMesh->pTexture = __posTex;
									__src = ifs.getls(3);
								} else {
									__src = ifs.getls()+1;
									char* __s = __src;
									size_t tex = 0;
									do {
										if (*__ptexnames != *__s) {
											while (*++__ptexnames != eSlash);
											++ tex;
											continue;
										}
										while (*++__ptexnames == *++__s && *__s != eQuote);
										if (*__s == eQuote) break;
										else if (*__ptexnames == eSlash) {
											++ tex;
										} else {
											while (*++__ptexnames != eSlash);
											++ tex;
										}
										__s = __src;
									} while ((*++__ptexnames) != 0);
									__ptexnames = _Texnames;
									__posMesh->pTexture = __posTex + tex;
									__src = ifs.getls(2);// parent:Material
								}
							}
						}
						if (*__src == __effect) { // child:EffectInstance
							__posMesh->pEffect = __fxInst;
							__src = ifs.getls();
							GetNameT(__fxInst->name, ++__src, eQuote);
							if (FAILED(ns_d3d::Cd3d::_Inst().CreateEffect(__modelHeader->szPass, __fxInst->name, &__fxInst->d3dEffect))) {
								NS_ASSERT_FAILURE(CreateEffect);
							}
							LPD3DXEFFECT effect = __fxInst->d3dEffect;
							__fxInst->hWorld = effect->GetParameterBySemantic(NULL, "WORLD");
							__fxInst->hView = effect->GetParameterBySemantic(NULL, "VIEW");
							__fxInst->hProj = effect->GetParameterBySemantic(NULL, "PROJECTION");
							while (*(__src = ifs.getls()) != eParent) {
								__src += 11;
								if (*__src == __string) { // EffectParamString
									++ __fxInst->numtx;
									__fxInst->string = __fxTx;
									__src = ifs.getls();
									GetNameT(__fxTx->name, ++__src, eQuote);

									if (__textures == 1) {
										__fxTx->texture = __posTex;
										ifs._ignorel(2);
									} else {
										__src = ifs.getls()+1;
										char* __s = __src;
										size_t tex = 0;
										do {
											if (*__ptexnames != *__s) {
												while (*++__ptexnames != eSlash);
												++ tex;
												continue;
											}
											while (*++__ptexnames == *++__s && *__s != eQuote) {
												if (*__s == eBackSlash)
													++ __s;
											}
											if (*__s == eQuote) break;
											else if (*__ptexnames == eSlash) {
												++ tex;
											} else {
												while (*++__ptexnames != eSlash);
												++ tex;
											}
											__s = __src;
										} while ((*++__ptexnames) != 0);
										__ptexnames = _Texnames;
										__fxTx->texture = __posTex + tex;
										ifs._ignorel();// parent:EffectParamString
									}

									char annotation[eSizeofName];
									wcstombs_s(NULL, annotation, eNameLength, __fxTx->name, eNameLength);
									__fxTx->handle = effect->GetParameterByName(NULL, annotation);
									effect->SetTexture(__fxTx->handle, __fxTx->texture->pTexture);
									++ __fxTx;
								}
								else if (*__src == __floats) { // Floats
									__src = ifs.getls();
									char annotation[eSizeofName];
									GetNameWithMBS(annotation, ++__src, eQuote);
									__src = ifs._getl();
									int count = stoi(__src);
									if (count == 1) {
										++ __fxInst->numfloat1;
										__fxInst->float1 = __fxFloat1;
										__fxFloat1->handle = effect->GetParameterByName(NULL, annotation);
										mbstowcs_s(NULL, __fxFloat1->name, eNameLength, annotation, eNameLength);
										__src = ifs._getl();
										__fxFloat1->param = stof32(__src);
										HRESULT hr = effect->SetFloat(__fxFloat1->handle, __fxFloat1->param);
										++ __fxFloat1;
									}
									else if (count == 4) {
										++ __fxInst->numfloat4;
										__fxInst->float4 = __fxFloat4;
										__fxFloat4->handle = effect->GetParameterByName(NULL, annotation);
										mbstowcs_s(NULL, __fxFloat4->name, eNameLength, annotation, eNameLength);
										__src = ifs.getls();
										__fxFloat4->param.x = stof32(&__src);
										__fxFloat4->param.y = stof32(&__src);
										__fxFloat4->param.z = stof32(&__src);
										__fxFloat4->param.w = stof32(__src);
										HRESULT hr = effect->SetVector(__fxFloat4->handle, &__fxFloat4->param);
										++ __fxFloat4;
									}
									else {
										MessageBox(NULL, STR(EffectParamFloats: Unknown Value), STR(ERROR), MB_OK);
										ifs._ignorel();
									}
									ifs._ignorel();
								}
								else {
										MessageBox(NULL, STR(EffectInstance: Unknown Value), STR(ERROR), MB_OK);
									ifs.ignorels(eParent);
								}
							}
							ifs._ignorel();
							++ __fxInst;
						}
						ifs._ignorel(); // parent:MaterialList -> while
					}
					else if (nslib::strictlystrstr(__src, __coords)) {
						ifs._ignorel();
						__customVers = __customVersSub;
						for (size_t i=__vertex; i; --i, ++__customVers) {
							__src = ifs._getl();
							__customVers->texcoord.x = stof32(&__src);
							__customVers->texcoord.y = stof32(__src);
						}
						ifs._ignorel();
					}
					else if (*__src == __dupli) {
						ifs._ignorel(__vertex + 3);
					}
					else if (nslib::strictlystrstr(__src, __normals)) {
						__src = ifs._getl();
						__count = stoi(__src);
						ifs._ignorel(__count);
						ifs.getl();
						//NS_ASSERT(__index != stoi(ifs._getl()), STR(Mesh Normals));
						ifs._ignorel(__index + 1);
					}
					else {
						ifs.ignorels(eParent);
					}
				}

				if (__frames) {
					__posMesh->pPrev = __prevFrame;
				}
				++ __posMesh;
				continue;
			}
			else if (*__src == __animation) {
				size_t animation = 0;
				__animeSets->pAnimeList = __animeNodes;
				while (*ifs.getls() != eParent) {	// child:Animation, parent:ASet
					if (*(__src = ifs.getls()) == eChild) {	// name, child:AnimationKey
						ns_model::GetNameT(__animeNodes->szTargetFrame, ++__src, eParent);
						ifs.ignorel();	// child:AnimationKey
					}
					do {
						switch (stoi(ifs._getl())) {
							case eAnimationQuatanion:
								__animeNodes->pQuat = __animeQuats;
								__animeQuats->nTimeNum = __count = stoi(ifs._getl());
								__animeQuats->pnTimeList = __posTime;
								__animeQuats->pvQuatList = __posQuat;
								__animeQuats->pmatQuatList = __posMtx;
								for (; __count; --__count, ++__posQuat, ++__posTime, ++__posMtx) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posQuat->w = stof32(&++__src);
									__posQuat->x = stof32(&__src);
									__posQuat->y = stof32(&__src);
									__posQuat->z = stof32(__src);
									ConvertQuatToMat(__posMtx, __posQuat);
								}
								++ __animeQuats;
								break;
							case eAnimationScale:
								__animeNodes->pScale = __animeScales;
								__animeScales->nTimeNum = __count = stoi(ifs._getl());
								__animeScales->pnTimeList = __posTime;
								__animeScales->pvScaleList = __posVec;
								for (; __count; --__count, ++__posTime, ++__posVec) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posVec->x = stof32(&++__src);
									__posVec->y = stof32(&__src);
									__posVec->z = stof32(__src);
								}
								++ __animeScales;
								break;
							case eAnimationSlide:
								__animeNodes->pSlide = __animeSlides;
								__animeSlides->nTimeNum = __count = stoi(ifs._getl());
								__animeSlides->pnTimeList = __posTime;
								__animeSlides->pvSlideList = __posVec;
								for (; __count; --__count, ++__posTime, ++__posVec) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posVec->x = stof32(&++__src);
									__posVec->y = stof32(&__src);
									__posVec->z = stof32(__src);
								}
								++ __animeSlides;
								break;
							case eAnimationMatrix:
								__animeNodes->pMat = __animeMats;
								__animeMats->nTimeNum = __count = stoi(ifs._getl());
								__animeMats->pnTimeList = __posTime;
								__animeMats->pmatList = __posMtx;
								for (; __count; --__count, ++__posTime, ++__posMtx) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posMtx->_11 = stof32(&++__src);
									__posMtx->_12 = stof32(&__src);
									__posMtx->_13 = stof32(&__src);
									__posMtx->_14 = stof32(&__src);
									__posMtx->_21 = stof32(&__src);
									__posMtx->_22 = stof32(&__src);
									__posMtx->_23 = stof32(&__src);
									__posMtx->_24 = stof32(&__src);
									__posMtx->_31 = stof32(&__src);
									__posMtx->_32 = stof32(&__src);
									__posMtx->_33 = stof32(&__src);
									__posMtx->_34 = stof32(&__src);
									__posMtx->_41 = stof32(&__src);
									__posMtx->_42 = stof32(&__src);
									__posMtx->_43 = stof32(&__src);
									__posMtx->_44 = stof32(__src);
								}
								++ __animeMats;
						}
						ifs._ignorel();
						if (*(__src = ifs.getls()) == eChild) { // name, child:AKey, parent:Animation
							ns_model::GetNameT(__animeNodes->szTargetFrame, *++__src == eSpace ? ++__src : __src, eSpace); // tiny
							ifs._ignorel(); // parent:Animation
							break;
						}
					} while (*__src != eParent); // child:AKey, parent:Animation
					LinkFrame(__animeNodes->szTargetFrame, &__animeNodes->pTargetFrame, __firstFrame, __frames);
					__animeNodes->pTargetFrame->pmatAnim = __posTransMtx;
					++ __posTransMtx;
					++ __animeNodes;
					++ animation;
				}
				__animeSets->nMaxTime = *--__posTime;	// add last frame
				__animeSets->nAnimation = animation;
				++ __animeSets;
			}
		} while(__src = ifs.getls());

		ifs.close();
		return true;
	}

	bool CModelAllocator::LoadData(CSkinedMeshModel *_Model, nslib::CIFStream& ifs, char* _Texnames)
	{
		using namespace nslib;
		using namespace ns_types;
		// Lock vertex buffer
		if( _Model->m_pVB->Lock( 0, 0, reinterpret_cast<void**>( &_Model->m_pVertices ), 0 ) < 0 )
			return	false;

		// Lock index buffer
		if( _Model->m_pIB->Lock( 0, 0, reinterpret_cast<void**>( &_Model->m_pwIndices ), 0 ) < 0 )
			return	false;

		size_t __count, __vertex, __index;
		char* __ptexnames = _Texnames;

		const char __header = 'H';
		const char __temp = 't';
		const char __frame = 'F';
		const char __mesh = 'M';
		const char __animation = 'A';
		const char* __coords = "MeshT";
		const char* __material = "MeshM";
		const char* __normals = "MeshN";
		const char* __transform = "FrameT";
		const char __dupli = 'V';
		const char __xskin = 'X';
		const char __effect = 'E';
		const char __string = 'S';
		const char __floats = 'F';

		LPMODELHEADER __modelHeader = _Model->m_pModelHead;
		LPFRAMEHEADER __posFrame = _Model->m_pFrameList, __prevFrame = NULL,
			__firstFrame = __posFrame;
		LPMESHHEADER __posMesh = _Model->m_pMeshList;
		LPTEXTUREHEADER __posTex = _Model->m_pTexList;
		LPCUSTOMVERTEX __customVers = _Model->m_pVertices,
			__firstVtx = __customVers, __polygon[3], __customVersSub;
		word_t* __wordIndices = _Model->m_pwIndices;
		LPMATERIAL __posMtrl = _Model->m_pMtrlList;
		word_t __offsetvtx = 0;
		size_t __textures = __modelHeader->nTexture, __frames = __modelHeader->nFrame;

		LPANIMEHEADER __animeHeader = _Model->m_pAnimeHead;
		LPANIMENODE __animeNodes;
		LPANIMESET __animeSets;
		LPMATRIX __posMtx, __posTransMtx;
		LPANIMEQUATHEADER __animeQuats;
		LPANIMESCALEHEADER __animeScales;
		LPANIMESLIDEHEADER __animeSlides;
		LPANIMEMATHEADER __animeMats;
		LPVECTOR4 __posQuat;
		LPVECTOR3 __posVec;
		size_t *__posTime;
		if (__animeHeader != NULL) {
			__animeNodes = __animeHeader->pAnimeList;
			__animeSets = __animeHeader->pAnimeSetList;
			__posMtx = __animeHeader->pmatList;
			__animeQuats = __animeHeader->pQuatList;
			__posQuat = __animeHeader->pvQuatList;
			__animeScales = __animeHeader->pScaleList;
			__posVec = __animeHeader->pvList;
			__animeSlides = __animeHeader->pSlideList;
			__animeMats = __animeHeader->pMatList;
			__posTime = __animeHeader->pnTimeList;
			__posTransMtx = __animeHeader->pMatAnime;
		}

		size_t __offsetBoneVtx = 0, __offsetSkinVtx = 0;
		LPSKINEDMESHHEADER __skinHeader = _Model->m_pSkinHead;
		LPSKINEDMESHNODE __posSkin = _Model->m_pSkinList;
		LPSKINEDMESHBONE __posBone = __skinHeader->pBoneList;

		ifs.seekbegin();
		// xof
		ifs._ignorel();
		char* __src = ifs.getl();
		// header
		if (*__src == eSpace) {
			ifs._ignorel(5);
		}
		// knot.x
		else {
			if (*__src == eSlash) { // comment (compatible with triplane.x, heli.x)
				ifs.ignorelc(eSlash);
				__src = ifs.getl();
			}
			// template
			if (*__src == __temp) {
				do {
					ifs.ignorel(eParent); // compatible with triplane.x
				} while (*(__src = ifs.getl()) == __temp); // getls: triplane.x ('A`)
				// header: lobby, tiger, triplane
				if (*__src == __header) {
					ifs.ignorels(eParent); // compatible with triplane.x
				}
			}
		}
		do {// while(__src = ifs.getl())
			if (*__src == eParent) {
				__prevFrame = __prevFrame->pPrev;
			}
			else if (*__src == __frame) {
				GetNameT(__posFrame->szFrameName, __src+6, eSpace);
				__src = ifs.getls();
				if (!nslib::strictlystrstr(__src, __transform)) // triplane.x
					continue;
				// FrameTransformMatrix
				D3DXMATRIX* mat = &__posFrame->matTrans;
				GetMatrixFromStream(ifs, mat);
				ifs._ignorel();
				//ifs.ignorels(__parent); // parent:Matrix

				__posFrame->pPrev = __prevFrame;
				__prevFrame = __posFrame;
				++__posFrame;
			}
			else if (*__src == __mesh) {
				__posMesh->nVertexNum = __vertex = stoi(ifs._getl());
				__customVersSub = __customVers;	// for TextureCoords
				VECTOR3* vec = &__customVers->position;
				for (size_t i=__vertex; i; --i, vec=&(++__customVers)->position) {
					__src = ifs._getl();
					vec->x = stof32(&__src);
					vec->y = stof32(&__src);
					vec->z = stof32(__src);
					__customVers->normal = VECTOR3(.0f, .0f, .0f);
				}
				__index = stoi(ifs.getl());
				size_t offsetpolygon = 0;
				for (size_t i=__index; i; --i, ++__wordIndices) {
					VECTOR3 vec;
					switch (stoi(&(__src = ifs.getls())))
					{
					case 4:
						{
							word_t* buf = __wordIndices;
							*__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
							VectorCross(&vec,
								&(__polygon[1]->position - __polygon[0]->position),
								&(__polygon[2]->position - __polygon[0]->position));
							__polygon[0]->normal += vec;
							__polygon[1]->normal += vec;
							__polygon[2]->normal += vec;
							*++__wordIndices = *buf;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = *(buf+2);
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
							++ offsetpolygon;
							break;
						}
					case 3:
						{
							*__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
						}
					}
					VectorCross(&vec,
						&(__polygon[1]->position - __polygon[0]->position),
						&(__polygon[2]->position - __polygon[0]->position));
					__polygon[0]->normal += vec;
					__polygon[1]->normal += vec;
					__polygon[2]->normal += vec;
				}
				__offsetvtx += (word_t)__posMesh->nVertexNum;
				__posMesh->nPolygonNum = __index + offsetpolygon;

				// child, data, parent の間に空行が無いこと前提
				while (__src = ifs.getls()) { // parent:mesh, getls:triplane.x
					if (*__src == eParent) { // mesh
						break;
					}
					else if (nslib::strictlystrstr(__src, __material)) {
						__posMesh->pMtrl = __posMtrl;
						__posMesh->nMtrlNum = __count = atoi(ifs._getl());
						ifs._ignorel(__index + 1);
						// Material
						for (; __count; --__count, ++__posMtrl) {
							ifs.getl(); // child: Material
							GetMaterialFromStream(ifs, __posMtrl);
							// TextureFilename
							__src = ifs.getls(); // child:Filename, parent:Material
							if (*__src != eParent) {
								if (__textures == 1) {
									__posMesh->pTexture = __posTex;
									__src = ifs.getls(3);
								} else {
									__src = ifs.getls()+1;
									char* __s = __src;
									size_t tex = 0;
									do {
										if (*__ptexnames != *__s) {
											while (*++__ptexnames != eSlash);
											++ tex;
											continue;
										}
										while (*++__ptexnames == *++__s && *__s != eQuote);
										if (*__s == eQuote) break;
										else if (*__ptexnames == eSlash) {
											++ tex;
										} else {
											while (*++__ptexnames != eSlash);
											++ tex;
										}
										__s = __src;
									} while ((*++__ptexnames) != 0);
									__ptexnames = _Texnames;
									__posMesh->pTexture = __posTex + tex;
									__src = ifs.getls(2);// parent:Material
								}
							}
						}
						if (*__src == __effect) { // child:EffectInstance
							ifs._ignorel();
							while (*(__src = ifs.getls()) != eParent) {
								__src += 11;
								if (*__src == __string) { // EffectParamString
									ifs._ignorel(3);
								}
								else if (*__src == __floats) { // Floats
									ifs._ignorel(4);
								}
								else {
									ifs.ignorels(eParent);
								}
							}
							ifs._ignorel();
						}
						ifs._ignorel(); // parent:MaterialList -> while
					}
					else if (nslib::strictlystrstr(__src, __coords)) {
						ifs._ignorel();
						__customVers = __customVersSub;
						for (size_t i=__vertex; i; --i, ++__customVers) {
							__src = ifs._getl();
							__customVers->texcoord.x = stof32(&__src);
							__customVers->texcoord.y = stof32(__src);
						}
						ifs._ignorel();
					}
					else if (*__src == __dupli) {
						ifs._ignorel(__vertex + 3);
					}
					else if (nslib::strictlystrstr(__src, __normals)) {
						//__src = ifs._getl();
						//NS_ASSERT(__vertex != stoi(__src), STR(Mesh Normals));
						ifs._ignorel(stoi(ifs._getl()));
						ifs.getl();
						ifs._ignorel(__index + 1);
					}
					else if (*__src == __xskin) {
						__posMesh->bSkinMesh = true;
						__posMesh->pSkinMesh = __posSkin;
						__posSkin->nVertexNum = __vertex;
						__posSkin->pnVertexList = __skinHeader->pnVertexList + __offsetBoneVtx;
						__posSkin->pfWeightList = __skinHeader->pfWeightList + __offsetBoneVtx;
						__posSkin->pBoneList = __posBone;
						__posSkin->nVertexPos = static_cast<size_t>(__offsetvtx - __posMesh->nVertexNum);
						__posSkin->pSkinVers = __skinHeader->pSkinVers + __offsetSkinVtx;
						__posSkin->pbWeight = __skinHeader->pbWeight + __offsetSkinVtx;
						__posSkin->nMaxWeightPerVertex = stoi(ifs._getl());
						__posSkin->nMaxWeightPerFace = stoi(ifs._getl());
						__posSkin->nBones = __count = stoi(ifs._getl());

						LPSKINEDMESHNODEVERTEX nodevtx = __posSkin->pSkinVers, firstnode = nodevtx;
						__customVers = __customVersSub;
						for (size_t i=__vertex; i; --i, ++nodevtx, ++__customVers) {
							nodevtx->vPosition = __customVers->position;
							nodevtx->vNormal = __customVers->normal;
							nodevtx->vTexture = __customVers->texcoord;
						}

						size_t offset = 0;
						for (size_t i=0; i<__count; ++i, ++__posBone) {
							__src = ifs.getls(4);
							GetNameT(__posBone->szBoneName, ++__src, eQuote);

							// vertex
							size_t vtx = atoi(ifs._getl());
							__posSkin->nBoneVertexAll += __posBone->nVertex = vtx;
							size_t *posv = __posBone->pnVertexList = __posSkin->pnVertexList + offset,
								*subv = posv;
							float *posw = __posBone->pfWeightList = __posSkin->pfWeightList + offset;
							offset += vtx;
							for (size_t j=0; j<vtx; ++j, ++posv) {
								*posv = stoi(ifs._getl());
							}

							// weight
							for (size_t j=0; j<vtx; ++j, ++subv, ++posw) {
								nodevtx = firstnode + *subv;
								// bone no.
								BrendingIndex(&nodevtx->dwBoneIndex, i, nodevtx->nBlendNum);
								// weight no.
								BrendingIndex(&nodevtx->dwWeightIndex, j, nodevtx->nBlendNum);
								++ nodevtx->nBlendNum;
								*posw = stof32(ifs._getl());
							}
							LPMATRIX mtx = &__posBone->matBone;
							__src = ifs._getl();
							mtx->_11 = stof32(&__src);
							mtx->_12 = stof32(&__src);
							mtx->_13 = stof32(&__src);
							mtx->_14 = stof32(&__src);
							mtx->_21 = stof32(&__src);
							mtx->_22 = stof32(&__src);
							mtx->_23 = stof32(&__src);
							mtx->_24 = stof32(&__src);
							mtx->_31 = stof32(&__src);
							mtx->_32 = stof32(&__src);
							mtx->_33 = stof32(&__src);
							mtx->_34 = stof32(&__src);
							mtx->_41 = stof32(&__src);
							mtx->_42 = stof32(&__src);
							mtx->_43 = stof32(&__src);
							mtx->_44 = stof32(__src);
							LinkFrame(__posBone->szBoneName, &__posBone->pLinkFrame, __firstFrame, __frames);
						}
						++ __posSkin;
						__offsetBoneVtx += __posSkin->nBoneVertexAll;
						__offsetSkinVtx += __vertex;
						ifs._ignorel();
					}
					else {
						ifs.ignorels(eParent);
					}
				}

				if (__frames) {
					__posMesh->pPrev = __prevFrame;
				}
				++ __posMesh;
				continue;
			}
			else if (*__src == __animation) {
				size_t animation = 0;
				__animeSets->pAnimeList = __animeNodes;
				while (*ifs.getls() != eParent) {	// child:Animation, parent:ASet
					if (*(__src = ifs.getls()) == eChild) {	// name, child:AnimationKey
						ns_model::GetNameT(__animeNodes->szTargetFrame, *++__src == eSpace ? ++__src : __src, eParent);
						ifs.ignorel();	// child:AnimationKey
					}
					do {
						switch (stoi(ifs._getl())) {
							case eAnimationQuatanion:
								__animeNodes->pQuat = __animeQuats;
								__animeQuats->nTimeNum = __count = stoi(ifs._getl());
								__animeQuats->pnTimeList = __posTime;
								__animeQuats->pvQuatList = __posQuat;
								__animeQuats->pmatQuatList = __posMtx;
								for (; __count; --__count, ++__posQuat, ++__posTime, ++__posMtx) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posQuat->w = stof32(&++__src);
									__posQuat->x = stof32(&__src);
									__posQuat->y = stof32(&__src);
									__posQuat->z = stof32(__src);
									ConvertQuatToMat(__posMtx, __posQuat);
								}
								++ __animeQuats;
								break;
							case eAnimationScale:
								__animeNodes->pScale = __animeScales;
								__animeScales->nTimeNum = __count = stoi(ifs._getl());
								__animeScales->pnTimeList = __posTime;
								__animeScales->pvScaleList = __posVec;
								for (; __count; --__count, ++__posTime, ++__posVec) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posVec->x = stof32(&++__src);
									__posVec->y = stof32(&__src);
									__posVec->z = stof32(__src);
								}
								++ __animeScales;
								break;
							case eAnimationSlide:
								__animeNodes->pSlide = __animeSlides;
								__animeSlides->nTimeNum = __count = stoi(ifs._getl());
								__animeSlides->pnTimeList = __posTime;
								__animeSlides->pvSlideList = __posVec;
								for (; __count; --__count, ++__posTime, ++__posVec) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posVec->x = stof32(&++__src);
									__posVec->y = stof32(&__src);
									__posVec->z = stof32(__src);
								}
								++ __animeSlides;
								break;
							case eAnimationMatrix:
								__animeNodes->pMat = __animeMats;
								__animeMats->nTimeNum = __count = stoi(ifs._getl());
								__animeMats->pnTimeList = __posTime;
								__animeMats->pmatList = __posMtx;
								for (; __count; --__count, ++__posTime, ++__posMtx) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posMtx->_11 = stof32(&++__src);
									__posMtx->_12 = stof32(&__src);
									__posMtx->_13 = stof32(&__src);
									__posMtx->_14 = stof32(&__src);
									__posMtx->_21 = stof32(&__src);
									__posMtx->_22 = stof32(&__src);
									__posMtx->_23 = stof32(&__src);
									__posMtx->_24 = stof32(&__src);
									__posMtx->_31 = stof32(&__src);
									__posMtx->_32 = stof32(&__src);
									__posMtx->_33 = stof32(&__src);
									__posMtx->_34 = stof32(&__src);
									__posMtx->_41 = stof32(&__src);
									__posMtx->_42 = stof32(&__src);
									__posMtx->_43 = stof32(&__src);
									__posMtx->_44 = stof32(__src);
								}
								++ __animeMats;
						}
						ifs._ignorel();
						if (*(__src = ifs.getls()) == eChild) { // name, child:AKey, parent:Animation
							ns_model::GetNameT(__animeNodes->szTargetFrame, *++__src == eSpace ? ++__src : __src, eSpace); // tiny
							ifs._ignorel(); // parent:Animation
							break;
						}
					} while (*__src != eParent); // child:AKey, parent:Animation
					LinkFrame(__animeNodes->szTargetFrame, &__animeNodes->pTargetFrame, __firstFrame, __frames);
					__animeNodes->pTargetFrame->pmatAnim = __posTransMtx;
					++ __posTransMtx;
					++ __animeNodes;
					++ animation;
				}
				__animeSets->nMaxTime = *--__posTime;	// add last frame
				__animeSets->nAnimation = animation;
				++ __animeSets;
			}
		} while(__src = ifs.getls());

		ifs.close();
		return true;
	}


	//void CModelAllocator::CreateEffect(LPEFFECTINSTANCE effect)
	//{
	//	LPD3DXEFFECT d3dEffect = effect->d3dEffect;
	//	if (FAILED(ns_d3d::Cd3d::_Inst().CreateEffect(effect->name, d3dEffect))) {
	//		return;
	//	}
	//}


	void CModelAllocator::Scalling(CModel* _Model)
	{
		LPMODELHEADER header = _Model->m_pModelHead;
		size_t	nVertex,
			nMesh	= header->nMesh,
			nFrame	= header->nFrame;
		float	size	= 0, ratio = 0,
			max_x = .0f, max_y = .0f, max_z = .0f,
			min_x = 63.0f, min_y = 63.0f, min_z = 63.0f;
		MATRIX	mtx;
		LPCUSTOMVERTEX	pVertexPos	= _Model->m_pVertices;
		LPMESHHEADER	pMeshPos	= _Model->m_pMeshList;

		if (header->nAnimationSet > 0) {
			_Model->Animation(0);
		}
		if (nFrame)
		{
			VECTOR3	vTemp;
			for (size_t iMesh=0; iMesh<nMesh; ++iMesh, ++pMeshPos)
			{
				// メッシュの変換行列の構築
				TransMatrixComplete(&mtx, pMeshPos->pPrev);
				nVertex	= pMeshPos->nVertexNum;
				for (size_t jVers=0; jVers<nVertex; ++jVers, ++pVertexPos)
				{
					VectorIdiot(&pVertexPos->normal);
					VecxMat(&vTemp, &pVertexPos->position, &mtx);
					if (vTemp.x > max_x)
						max_x = vTemp.x;
					if (vTemp.y > max_y)
						max_y = vTemp.y;
					if (vTemp.z > max_z)
						max_z = vTemp.z;
					if (vTemp.x < min_x)
						min_x = vTemp.x;
					if (vTemp.y < min_y)
						min_y = vTemp.y;
					if (vTemp.z < min_z)
						min_z = vTemp.z;
				}
			}
		}
		else
		{
			LPVECTOR3 vTemp;
			for (size_t iMesh=0; iMesh<nMesh; ++iMesh, ++pMeshPos)
			{
				nVertex	= pMeshPos->nVertexNum;
				for (size_t jVers=0; jVers<nVertex; ++jVers, ++pVertexPos)
				{
					vTemp = &pVertexPos->position;
					VectorIdiot(&pVertexPos->normal);
					if (vTemp->x > max_x)
						max_x = vTemp->x;
					if (vTemp->y > max_y)
						max_y = vTemp->y;
					if (vTemp->z > max_z)
						max_z = vTemp->z;
					if (vTemp->x < min_x)
						min_x = vTemp->x;
					if (vTemp->y < min_y)
						min_y = vTemp->y;
					if (vTemp->z < min_z)
						min_z = vTemp->z;
				}
			}

			// 実空間での頂点の最大ベクトルを求める
			//nOffsetVertex += pMeshPos->nVertexNum;
			//nVertex	= pMeshPos->nVertexNum;
			//for (size_t jVers=0; jVers<nVertex; ++jVers, ++pVertexPos)
			//{
			//	VectorIdiot(&pVertexPos->normal);
			//	if (nFrame)
			//	{
			//		VecxMat(&vTemp, &pVertexPos->position, &mtx);
			//		vTemp.x	= sqrtf(vTemp.x * vTemp.x);
			//		vTemp.y	= sqrtf(vTemp.y * vTemp.y);
			//		vTemp.z	= sqrtf(vTemp.z * vTemp.z);

			//		if (vTemp.x > fMaximumVertex)
			//			fMaximumVertex = vTemp.x;
			//		if (vTemp.y > fMaximumVertex)
			//			fMaximumVertex = vTemp.y;
			//		if (vTemp.z > fMaximumVertex)
			//			fMaximumVertex = vTemp.z;
			//	}
			//	else
			//	{
			//		if ((vTemp.x = sqrtf(pVertexPos->position.x * pVertexPos->position.x)) > fMaximumVertex)
			//			fMaximumVertex = vTemp.x;
			//		if ((vTemp.y = sqrtf(pVertexPos->position.y * pVertexPos->position.y)) > fMaximumVertex)
			//			fMaximumVertex = vTemp.y;
			//		if ((vTemp.z = sqrtf(pVertexPos->position.z * pVertexPos->position.z)) > fMaximumVertex)
			//			fMaximumVertex = vTemp.z;
			//	}
			//}
		}

		size = max_x - min_x;
		if (size < (max_y - min_y)) size = max_y - min_y;
		if (size < (max_z - min_z)) size = max_z - min_z;

		ratio	= ns_d3d::OBJECT_SIZE / size; // OBJECT_SIZE nsd3d9.cpp
		//MatrixIdentity( &model->m_matScale );
		LPMATRIX mat = &_Model->m_matScale;
		//mat->_11 = ratio;
		//mat->_22 = ratio;
		//mat->_33 = ratio;
		mat->_41 = -(max_x + min_x) / 2.0f;
		mat->_42 = -(max_y + min_y) / 2.0f;
		mat->_43 = -(max_z + min_z) / 2.0f;
		header->vRadius.x = (max_x - min_x) / 2.0f;
		header->vRadius.y = (max_y - min_y) / 4.0f;
		header->vRadius.z = (max_z - min_z) / 2.0f;
		//if ((mat->_41 = -(plus_x + minus_x) / 2.0f) > .0f)
		//	mat->_41 = .0f;
		//if ((mat->_42 = -(plus_y + minus_y) / 4.0f) > .0f)
		//	mat->_42 = .0f;	// 右手系
		//if ((mat->_43 = -(plus_z + minus_z) / 2.0f) > .0f)
		//	mat->_43 = .0f;
	}


	void CModelAllocator::Scalling(CSkinedMeshModel* _Model)
	{
		LPMODELHEADER header = _Model->m_pModelHead;
		size_t	nVertex,
			nMesh	= header->nMesh,
			nFrame	= header->nFrame;
		float	fMaximumVertex	= 0, fRatio = 0,
			plus_x = .0f, plus_y = .0f, plus_z = .0f,
			minus_x = 63.0f, minus_y = 63.0f, minus_z = 63.0f;
		VECTOR3	vTemp;
		MATRIX	mtx;
		LPCUSTOMVERTEX	pVertexPos	= _Model->m_pVertices;
		LPMESHHEADER	pMeshPos	= _Model->m_pMeshList;

		if (header->nAnimationSet > 0) {
			_Model->Animation(0);
		}
		if (nFrame)
		{
			for (size_t iMesh=0; iMesh<nMesh; ++iMesh, ++pMeshPos)
			{
				// メッシュの変換行列の構築
				TransMatrixComplete(&mtx, pMeshPos->pPrev);
				//ns_d3d::Cd3d::_Inst().ViewMatrix();
				//ns_d3d::Cd3d::_Inst().MatrixWorldToViewPort(&mtx);
				nVertex	= pMeshPos->nVertexNum;
				for (size_t jVers=0; jVers<nVertex; ++jVers, ++pVertexPos)
				{
					VectorIdiot(&pVertexPos->normal);
					VecxMat(&vTemp, &pVertexPos->position, &mtx);
					if (vTemp.x > plus_x)
						plus_x = vTemp.x;
					if (vTemp.y > plus_y)
						plus_y = vTemp.y;
					if (vTemp.z > plus_z)
						plus_z = vTemp.z;
					if (vTemp.x < minus_x)
						minus_x = vTemp.x;
					if (vTemp.y < minus_y)
						minus_y = vTemp.y;
					if (vTemp.z < minus_z)
						minus_z = vTemp.z;

					//vTemp.x	= sqrtf(vTemp.x * vTemp.x);
					//vTemp.y	= sqrtf(vTemp.y * vTemp.y);
					//vTemp.z	= sqrtf(vTemp.z * vTemp.z);
					//if (vTemp.x > fMaximumVertex)
					//	fMaximumVertex = vTemp.x;
					//if (vTemp.y > fMaximumVertex)
					//	fMaximumVertex = vTemp.y;
					//if (vTemp.z > fMaximumVertex)
					//	fMaximumVertex = vTemp.z;
				}
			}
			vTemp.x = plus_x > -.0f*minus_x ? plus_x : -.0f*minus_x;
			vTemp.y = plus_y > -.0f*minus_y ? plus_y : -.0f*minus_y;
			vTemp.z = plus_z > -.0f*minus_z ? plus_z : -.0f*minus_z;
			fMaximumVertex = vTemp.x > vTemp.y ? vTemp.x : vTemp.y;
			if (fMaximumVertex < vTemp.z)
				fMaximumVertex = vTemp.z;
			header->vRadius.x = vTemp.x;
			header->vRadius.y = vTemp.z;
		}
		else
		{
			for (size_t iMesh=0; iMesh<nMesh; ++iMesh, ++pMeshPos)
			{
				nVertex	= pMeshPos->nVertexNum;
				for (size_t jVers=0; jVers<nVertex; ++jVers, ++pVertexPos)
				{
					VectorIdiot(&pVertexPos->normal);
					if (vTemp.x > plus_x)
						plus_x = vTemp.x;
					if (vTemp.y > plus_y)
						plus_y = vTemp.y;
					if (vTemp.z > plus_z)
						plus_z = vTemp.z;
					if (vTemp.x < minus_x)
						minus_x = vTemp.x;
					if (vTemp.y < minus_y)
						minus_y = vTemp.y;
					if (vTemp.z < minus_z)
						minus_z = vTemp.z;

					if ((vTemp.x = sqrtf(pVertexPos->position.x * pVertexPos->position.x)) > fMaximumVertex)
						fMaximumVertex = vTemp.x;
					if ((vTemp.y = sqrtf(pVertexPos->position.y * pVertexPos->position.y)) > fMaximumVertex)
						fMaximumVertex = vTemp.y;
					if ((vTemp.z = sqrtf(pVertexPos->position.z * pVertexPos->position.z)) > fMaximumVertex)
						fMaximumVertex = vTemp.z;
				}
			}
		}

		fRatio	= ns_d3d::OBJECT_SIZE / fMaximumVertex; // OBJECT_SIZE nsd3d9.cpp
		//MatrixIdentity( &model->m_matScale );
		LPMATRIX mat = &_Model->m_matScale;
		mat->_11 = fRatio;
		mat->_22 = fRatio;
		mat->_33 = fRatio;
		if ((mat->_41 = -(plus_x + minus_x) / 2.0f) > .0f)
			mat->_41 = .0f;
		if ((mat->_42 = -(plus_y + minus_y) / 4.0f) > .0f)
			mat->_42 = .0f;	// 右手系
		if ((mat->_43 = -(plus_z + minus_z) / 2.0f) > .0f)
			mat->_43 = .0f;
		header->vRadius.x = plus_x + minus_x / 2.0f;
		header->vRadius.y = plus_y + minus_y / 2.0f;
		header->vRadius.z = plus_z + minus_z / 2.0f;
	}


	void CModelAllocator::CreateNSModel(CModel* model)
	{
		//FILE fp = fopen(model->m_pModelHead->szName, "w");
		// frame matrix, index_child
		// texture
		// material
		// skinmesh
		// mesh vertex, index, tex, 
	}

}