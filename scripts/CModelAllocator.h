

#pragma once
#ifndef _NS_C_MODEL_ALLOCATOR_H_
#define _NS_C_MODEL_ALLOCATOR_H_

#include "CModel.h"
#include "CFStream.h"

namespace ns_model {

	class CModelAllocator
	{
	public:
		static CModel* GetModelFromXFile(const char* _Name);
		static CModel* GetModelFromXFileAsBinary(const char* _Name);

	private:
		static LPTEXTUREHEADER CreateTexture
			(LPMODELHEADER _HeaderModel, char* _texNames, const char* _Name);
		static bool Allocate(CModel*);
		static bool Allocate(CSkinedMeshModel*);
		static bool LoadData (CModel *_Model, nslib::CIFStream& ifs, char* _Texnames);
		static bool LoadData (CSkinedMeshModel *_Model, nslib::CIFStream& ifs, char* _Texnames);
		static void Scalling(CModel*);
		static void Scalling(CSkinedMeshModel*);
		static void CreateEffect(LPEFFECTINSTANCE);
		static void CreateNSModel(CModel*);
	};

}

#endif // _NS_C_MODEL_ALLOCATOR_H_