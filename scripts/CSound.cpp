
#include "CSound.h"

namespace nsl {

	CSound::CSound()
		: _name(NULL)
		, _wave(NULL)
		, _pos_wave(NULL)
		, _len_wave()
		, _num_playbuf()
		, _tbl_buf()
		, _tbl_wavehdr()
		, _wave_fmtex()
		, _wave_out()
		, _bufsize()
		, _last_wave(NULL)
	{
	}

	CSound::~CSound()
	{
		if (_wave) {
			free(_wave);
		}
		for (size_t i=0; i<eWaveBufferSize; ++i) {
			if (_tbl_buf[i]) {
				free(_tbl_buf[i]);
			}
		}
		if (_wave_out) {
			waveOutClose(_wave_out);
		}
	}

	void CSound::LoadWave(const char *name)
	{
		char szChankID[5];
		char szFileType[5];
		char szFact[5];
		unsigned int unDataSize, unChankSize;
		unsigned char* lpWaveData;
		memset(szChankID, 0, sizeof(szChankID));
		memset(szFileType, 0, sizeof(szFileType));

		// ファイルを開く
		FILE* fp;
		fopen_s(&fp, name, "rb");
		if (!fp) {
			MessageBox(NULL, L"fail", L"FOpen Error", MB_ICONERROR);
			return;
		}

		// RIFF チャンクを読み込む
		fread(szChankID, 1, 4, fp);
		if (!strcmp( szChankID, "RIFF"))		// RIFF形式か
		{
			fread(&unDataSize, 1, sizeof(unsigned int), fp);
			fread(szFileType, 1, 4, fp);
			fread(szChankID, 1, 4, fp);

			if (strcmp(szChankID, "fmt"))	// フォーマットのサイズの値か
				fread(&unChankSize, 1, sizeof(unsigned int), fp);
		}

		// WAVEFORMAT構造体を読み込む
		fread(&_wave_fmtex, 1, sizeof(WAVEFORMATEX), fp);
		fseek(fp, unChankSize - sizeof(WAVEFORMATEX), SEEK_CUR);

		// WAVEデータを読み込む
		fread(szChankID, 1, 4, fp);
		while (strcmp( szChankID, "data"))	// dataチャンクまで飛ばす
		{
			fread(&unChankSize, 1, sizeof(unsigned int), fp);
			fread(szFact, 1, unChankSize, fp);
			fread(szChankID, 1, 4, fp);
		}

		if (!strcmp(szChankID, "data"))
		{
			fread(&_len_wave, 1, sizeof(unsigned int), fp);
			lpWaveData = (unsigned char*)malloc(_len_wave);
			fread(lpWaveData, 1, _len_wave, fp);
		}

		_wave_fmtex.wBitsPerSample = _wave_fmtex.nBlockAlign * 8 / _wave_fmtex.nChannels;
		_bufsize = _wave_fmtex.nAvgBytesPerSec * 4;	// WAVEBUFFER_LENGTH
		_last_wave = lpWaveData + _len_wave;
		free(_wave);
		_wave = lpWaveData;

		fclose(fp);
	}

	void CSound::PlayWave(HWND hWnd)
	{
		ns_types::char_t error[ns_types::eNameLength];
		/*---------------------------------------------------------------------------*
			WAVEデータをチェックする
		*---------------------------------------------------------------------------*/
		// 再生するWAVEデータのフォーマットを指定し、再生デバイスをオープンできるかをチェック
		MMRESULT mmr = waveOutOpen(NULL, WAVE_MAPPER, &_wave_fmtex, 0, 0, WAVE_FORMAT_QUERY);
		if (mmr != MMSYSERR_NOERROR)
		{
			waveOutGetErrorText(mmr, error, ns_types::eNameLength);
			MessageBox(NULL, error, L"DeviceOpenCheck", MB_ICONERROR);
			return;
		}


		/*---------------------------------------------------------------------------*
			再生デバイスをオープンする
		*---------------------------------------------------------------------------*/
		// 再生するWAVEデータのフォーマットを指定し、再生デバイスをオープンできるかをチェック
		if (!_wave_out)
		{
			MMRESULT mmr = waveOutOpen(&_wave_out, WAVE_MAPPER, &_wave_fmtex, (DWORD)hWnd, 0, CALLBACK_WINDOW);
			if (mmr != MMSYSERR_NOERROR)
			{
				waveOutGetErrorText(mmr, error, ns_types::eNameLength);
				MessageBox(NULL, error, L"WaveOpen", MB_ICONERROR);
				return;
			}
		}


		/*---------------------------------------------------------------------------*
			バッファのアロケート
		*---------------------------------------------------------------------------*/
		// WAVEバッファを動的に確保し、ゼロクリア
		for (int i=0; i<eWaveBufferSize; ++i)
		{
			if (_tbl_buf[i]) {
				free(_tbl_buf[i]);
			}
			_tbl_buf[i] = (unsigned char*)malloc(_bufsize);
			memset((void*)_tbl_buf[i], 0, sizeof(BYTE) * _bufsize);
		}


		/*---------------------------------------------------------------------------*
			WAVEデータをセットし、WAVEヘッダを準備
		*---------------------------------------------------------------------------*/
		// WAVEバッファに音データを書き込む
		_pos_wave = _wave;	// 現在の読み取り位置をリセット.
		int num_buffer = 0;
		for (int i=0; i<eWaveBufferSize; ++i, ++num_buffer)
		{
			if (_pos_wave < _last_wave)
			{
				int length = 0;
				// WAVEデータ
				if (_pos_wave + _bufsize <= _last_wave)
				{
					// WAVEバッファのサイズ以上WAVEデータが残っているので全て書き込む
					length = _bufsize;
				}
				else
				{
					// WAVEバッファ分のデータがないので、書き込むサイズを計算する.
					length = (int)(_last_wave - _pos_wave);
				}

				memcpy((void*)_tbl_buf[i], (const void*)_pos_wave, length);
				_pos_wave += length;

				// WAVEヘッダ
				_tbl_wavehdr[i].lpData = (char*)_tbl_buf[i];
				_tbl_wavehdr[i].dwBufferLength = length;
				_tbl_wavehdr[i].dwBytesRecorded = 0;
				_tbl_wavehdr[i].dwUser = 0;
				_tbl_wavehdr[i].dwFlags = 0;
				_tbl_wavehdr[i].dwLoops = 1;
				_tbl_wavehdr[i].lpNext = NULL;
				_tbl_wavehdr[i].reserved = 0;
			}
		}


		/*---------------------------------------------------------------------------*
			バッファをサウンドデバイスに送る準備
		*---------------------------------------------------------------------------*/
		// この関数呼び出しで WAVEHDR構造体とバッファはディスクにスワップアウトされなくなる
		for (int i=0; i<num_buffer; ++i)
		{
			waveOutPrepareHeader(_wave_out, &_tbl_wavehdr[i], sizeof(WAVEHDR));
			++_num_playbuf;
		}


		/*---------------------------------------------------------------------------*
			バッファを再生デバイスに書き込む
		*---------------------------------------------------------------------------*/
		for (int i=0; i<num_buffer; ++i)
		{
			waveOutWrite(_wave_out, &_tbl_wavehdr[i], sizeof(WAVEHDR));
		}
	}

}