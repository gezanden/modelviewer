//-----------------------------------------------------------------------------
// File: main.cpp
//
// Desc: 
//
// Copyright (c) 2008 Naoki Shimoyamada. All rights reserved.
//-----------------------------------------------------------------------------
// including ns_win::CHwnd;
#include "CHwnd.h"
// including ns_model::ModelMgr;
#include "GameEngine.h"
#include "SceneBuilder.h"
#include <D3D11.h>

const D3D11_INPUT_ELEMENT_DESC layout[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "WEIGHTS", 0, DXGI_FORMAT_R8G8B8A8_UNORM, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "BONES", 0, DXGI_FORMAT_R8G8B8A8_UINT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "NORMAL", 0, DXGI_FORMAT_R8G8B8A8_UNORM, 0, 20, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD", 0, DXGI_FORMAT_R16G16_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TANGENT", 0, DXGI_FORMAT_R8G8B8A8_UNORM, 0, 28, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "BINORMAL", 0, DXGI_FORMAT_R8G8B8A8_UNORM, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
};


//-----------------------------------------------------------------------------
// Name: MsgProcMain()
// Desc: The main window's message handler
//-----------------------------------------------------------------------------
LRESULT WINAPI MsgProcMain(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}

	//case WM_MOUSEWHEEL:
	//	{
	//		short sDelta = static_cast<short>(HIWORD(wParam));
	//		float fAngle = static_cast<float>(sDelta / 2400.0f);
	//	} break;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}


bool Initialize( const HINSTANCE hInst )
{
	if (!ns_win::CHwnd::Instance().GenerateWindow( ns_win::eMAIN_WINDOW, hInst ))
		return false;
	if (!ns_win::CHwnd::Instance().Generated3dDevice( ns_win::eMAIN_WINDOW ))
		return false;
	//ModelMgr::Instance().RegisterModel("G:\\works\\leet\\nsXFile_S\\new_x_file\\tiny\\tiny_4anim.x");
	//ModelMgr::Instance().RegisterModel("G:\\works\\leet\\nsXFile_S\\x_file\\furea.x");
	//ModelMgr::Instance().RegisterModelAsPlayer("L:\\works\\leet\\nsXFile_S\\x_file\\goblin.x", 0.3f, 0.0f);
	//ModelMgr::Instance().RegisterModel("G:\\works\\leet\\nsXFile_S\\x_file\\giant_00.x");
	//ModelMgr::Instance().RegisterModel("G:\\works\\leet\\nsXFile_S\\x_file\\wolf_00.x");
	//ModelMgr::Instance().RegisterModel("G:\\works\\leet\\nsXFile_S\\x_file\\ninjya.x");
	ModelMgr::Instance().RegisterModelAsPlayer(".\\resources\\dwarfwitheffectinstance.x", 1.0f, 1.0f);
	WorldOrg::Instance().RegisterLand(".\\resources\\cube_on_plane.x");
	//ModelMgr::Instance().RegisterModel("L:\\works\\leet\\nsXFile_S\\new_x_file\\PRT Demo\\wall_with_pillars.x");
	//ModelMgr::Instance().RegisterModel("G:\\works\\leet\\nsXFile_S\\new_x_file\\Dwarf\\dwarf.x");
	//ModelMgr::Instance().RegisterModel("C:\\Programs\\Microsoft DirectX SDK (March 2009)\\Samples\\Media\\Dwarf\\dwarf.sdkmesh");
	//ModelMgr::Instance().RegisterId(0, 0);
	//ModelMgr::Instance().RegisterModelAsPlayer(".\\data\\dwarfwitheffectinstance.x");
	//WorldOrg::Instance().RegisterLand(".\\data\\cube_on_plane.x");
	nsgl::CSceneBuilder::Instance().LoadSound(".\\resources\\Screams Male 02.wav");

	nsgl::CGameEngine::Instance().EnableControl();

	//FILE* fp = NULL;
	//fopen_s(&fp, "H:\\Program Files (x86)\\Microsoft DirectX SDK (June 2010)\\Samples\\Media\\BatHead\\BatHead", "r");
	return true;
}


//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: The application's entry point
//-----------------------------------------------------------------------------
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR lpCmdLine, INT )
{
//#ifdef MY_DEBUG_MODE
//	// メモリリークを検出させる
//	int tmpFlag = _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG );
//	tmpFlag |= _CRTDBG_LEAK_CHECK_DF;
//	_CrtSetDbgFlag( tmpFlag );
//#endif
//
//    // Register the window class
//	if ( !nsCreateWndClassEx( hInst, MsgProcMain, MAIN_CLASS_NAME, 0x777777 ) )
//		return 0;
//
//    // Create the application's window
//	int nLen = ( int )strlen( lpCmdLine );
//	if ( nLen ) {
//		if ( *lpCmdLine == 0x22 ) lpCmdLine++;
//		char *pAna;
//		if ( ( pAna = strchr( lpCmdLine, 0x22 ) ) ) *pAna = 0;
//		strncpy( g_szFileName, lpCmdLine, 100 );
//	}
//	//char szCaption[250];
//	//wsprintf( szCaption, "コナミスクール ＸＬｏａｄ Source [%s]", g_szFileName );
//    HWND	hWndMain = CreateWindow( MAIN_CLASS_NAME, "NS X FILE VIEWER",
//                              WS_OVERLAPPEDWINDOW, 100, 100, 1024, 768,
//                              GetDesktopWindow(), NULL, hInst, NULL );
//
//	// Initialize OpenFileName
//	nsOpenFileInit( hWndMain );
//
//    // Show the window
//    ShowWindow( hWndMain, SW_SHOWDEFAULT );
//    UpdateWindow( hWndMain );

	if (!Initialize( hInst ))
		return 0;

    // Enter the message loop
    MSG msg;
    ZeroMemory( &msg, sizeof(msg) );

	while( msg.message!=WM_QUIT )
	{
		if( PeekMessage( &msg, NULL, 0U, 0U, PM_NOREMOVE ) )
		{
			if (!GetMessage(&msg, NULL, 0, 0))
			{
				break;
			}
			else
			{
				TranslateMessage( &msg );
				DispatchMessage( &msg );
			}
		}
		else
		{
			nsgl::CGameEngine::_Inst().Refresh();
		}
	}

    //UnregisterClass( MAIN_CLASS_NAME, hInst );
    return 0;
}