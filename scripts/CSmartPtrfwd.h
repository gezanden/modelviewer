//
//	CSmatrPtr.h
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once

//--------------------------------------------------------
//	Include
//--------------------------------------------------------
#include	"CSmartPtr.h"


//--------------------------------------------------------
//	Template class
//--------------------------------------------------------
template <typename T>
class ns_smartptr::CSmartPtr;
