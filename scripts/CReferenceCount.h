//
//	CReferenceCount.h
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once

//--------------------------------------------------------
//	Include
//--------------------------------------------------------
#include	"nstypes.h"
#include	"nsnew.h"
#include	"nsstring.h"
//#include	<stdlib.h>	// realloc
//#include	<malloc.h>	// realloc
#include	<assert.h>


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_refcount
{

	//--------------------------------------------------------
	//	Templates
	//--------------------------------------------------------
	template <typename T>
	class CReferenceCount;

	template <>
	class CReferenceCount< char_t >;

	template <typename T>
	struct SCreateRefCountObj;

	//////////////////////////////////////////////////////////
	//	name:	SRefAllocator
	//
	//	desc:	CReferenceCount を動的確保する関数オブジェクト
	//
	template <typename T>
	struct SRefAllocator
	{
		struct SAllocate;
		struct SDeallocate;
		SAllocate	Allocate;
		SDeallocate	Deallocate;
	};

	template <typename T>
	struct SRefAllocator<T>::SAllocate
	{
		CReferenceCount<T>* operator ()();
		CReferenceCount<T>* operator ()( CReferenceCount<T>& );
	};

	template <typename T>
	struct SRefAllocator<T>::SDeallocate
	{
		void operator ()();
	};

	template <typename T>
	CReferenceCount<T>* SRefAllocator<T>::SAllocate::operator ()()
	{
		CReferenceCount<T>* pObj;
		try
		{
			pObj	= new CReferenceCount<T>();
		}
		catch( std::bad_alloc )
		{
			return	false;
		}
		return	pObj;
	}

	template <typename T>
	CReferenceCount<T>* SRefAllocator<T>::SAllocate::operator ()( CReferenceCount<T>& rhs )
	{
		CReferenceCount<T>* pObj;
		try
		{
			pObj	= new CReferenceCount<T>( rhs );
		}
		catch( std::bad_alloc )
		{
			return	false;
		}
		return	pObj;
	}


	//////////////////////////////////////////////////////////
	//	name:	CReferenceCount
	//
	//	member:	m_pData		- データのポインタ
	//			m_nCount	- 参照カウンタ
	//			m_nSize		- 要素数
	//			m_eNewType	- new の種類
	//
	//	Reference documents:
	//			・著: Bjarne Stroustrup 訳: 株式会社ロングテール/長尾 高
	//				『プログラミング言語 C++ 第３版』 ASCII Addison Wesley P347, P385, P665
	//
	template <typename T>
	class CReferenceCount
	{
		typedef	CReferenceCount		_MyClass;
	public:
		CReferenceCount()
			: m_pData( 0 )
			, m_nCount( 0 )
			, m_nSize( 0 )
			, m_nBuf( 0 )
			, m_eNewType( eNewOnce )
		{}

		CReferenceCount( _MyClass& rhs )	// 参照カウントをインクリメントして初期化するので const ではない
			: m_pData( rhs.m_pData )
			, m_nCount( ++ rhs.m_nCount )
			, m_nSize( rhs.m_nSize )
			, m_nBuf( rhs.m_nBuf )
			, m_eNewType( rhs.m_eNewType )
		{}

		~CReferenceCount()
		{
			if ( m_nCount != 0 )
				Erase();
		}

		void Release()
		{
			delete	this;
		}

		bool Check( size_t index )
		{
			return	( m_nSize > index );
		}

		void Erase();

		T* Insert( const T* );
		T* Insert( const T*, const size_t );

		void Append( const T* );

	private:
		T* Get() const
		{
			return	m_pData;
		}

		CReferenceCount( const T* );
		CReferenceCount& operator =( const _MyClass& );

		enum eNEWTYPES
		{
			eNEWONCE,
			eNEWARREY,
		};

		T*			m_pData;
		size_t		m_nCount;
		size_t		m_nSize;
		size_t		m_nBuf;
		eNEWTYPES	m_eNewType;
	};

	template <typename T>
	void CReferenceCount<T>::Erase()
	{
		if ( !m_nCount )
			return;
		else if ( !(-- m_nCount) )
		{
			if ( m_eNewType == eNEWONCE )
				delete	m_pData;
			else
				ns_new::DeleteArreyObj( m_pData, m_nSize );
		}
	}

	template <typename T>
	T* CReferenceCount<T>::Insert( const T* Data )
	{
		if ( m_pData )
			Erase();
		m_pData		= new T( *Data );
		m_nCount	= 1;
		m_eNewType	= eNEWONCE;
		return	m_pData;
	}

	template <typename T>
	T* CReferenceCount<T>::Insert( const T* Data, const size_t size )
	{
		if ( m_pData )
			Erase();
		m_pData		= ns_new::NewArreyObj( Data, size );
		m_nCount	= 1;
		m_eNewType	= eNEWARREY;
		return	m_pData;
	}


	/////////////////////////////////////////////////////////
	//	name:	CReferenceCount
	//
	//	desc:	CReferenceCount の char_t 特化バージョン
	//
	template <>
	class CReferenceCount< char_t >
	{
		typedef CReferenceCount		_MyClass;
	public:
		CReferenceCount()
			: m_pData( 0 )
			, m_nCount( 0 )
			, m_nSize( 0 )
			, m_nBuf( 0 )
		{}

		CReferenceCount( _MyClass& rhs )	// 参照カウントをインクリメントして初期化するので const ではない
			: m_pData( rhs.m_pData )
			, m_nCount( ++ rhs.m_nCount )
			, m_nSize( rhs.m_nSize )
			, m_nBuf( rhs.m_nBuf )
		{}

		~CReferenceCount()
		{
			if ( m_nCount != 0 )
				Erase();
		}

		void Release()
		{
			delete this;
		}

		bool Check( size_t index )
		{
			return	( m_nSize > index );
		}

		void Erase()
		{
			if ( !(-- m_nCount) )
				delete [] m_pData;
		}

		char_t* Insert( const char_t* );
		void Append( const char_t* );

	private:
		char_t* Get() const
		{
			return	m_pData;
		}

		CReferenceCount( const char_t* );
		_MyClass& operator =( const _MyClass& );


		char_t*		m_pData;
		size_t		m_nCount;
		size_t		m_nSize;
		size_t		m_nBuf;
	};

	char_t* CReferenceCount<char_t>::Insert( const char_t* Data )
	{
		if ( m_pData )
			Erase();
		m_nSize		= strlen_t( Data ) + 1;
		m_nBuf		= m_nSize + ( sizeof( w64_t ) - m_nSize % sizeof( w64_t ) );
		m_pData		= new char_t[ m_nBuf ];
		memcpy_t( m_pData, m_nBuf, Data, m_nSize );
		m_pData[ m_nSize-1 ]	= '\0';
		m_nCount	= 1;
		return	m_pData;
	}

	void CReferenceCount<char_t>::Append( const char_t* Data )
	{
		size_t	size	= strlen_t( Data );
		if ( m_nBuf > (m_nSize + size) )
		{
			memcpy_t( &m_pData[ m_nSize-1 ], m_nBuf, Data, size );
		}
		else
		{
			size_t nBuf	= m_nSize;
			m_nSize		= m_nSize + size;
			m_nBuf		= m_nSize + ( sizeof( w64_t ) - m_nSize % sizeof( w64_t ) );
			char_t*	pBuf	= new char_t[ m_nBuf ];
			memcpy_t( pBuf, m_nBuf, m_pData, nBuf );
			memcpy_t( &pBuf[nBuf], size, Data, size );
		}

		m_pData[ m_nSize-1 ]	= '\0';
	}

}	// napespace: ns_CReferenceCount
