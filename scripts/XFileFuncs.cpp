
//--------------------------------------------------------------------
//	Includes
//--------------------------------------------------------------------
#include	"XFileFuncs.h"
#include	"CModelInit.h"
#include	"nsfunc.h"
#include	"Cd3d.h"
#include	<fstream>



//--------------------------------------------------------------------
//	Namespace
//--------------------------------------------------------------------
namespace ns_model
{
	using ns_types::eLineLength;


	//--------------------------------------------------------------------
	//	Name:	VectorIdiot
	//	Desc:	ベクトルの単位化
	//--------------------------------------------------------------------
	void VectorIdiot( LPVECTOR3 Vec )
	{
		float		fUnit;

		fUnit	= sqrtf( Vec->x*Vec->x + Vec->y*Vec->y + Vec->z*Vec->z );

		Vec->x	= Vec->x / fUnit;
		Vec->y	= Vec->y / fUnit;
		Vec->z	= Vec->z / fUnit;
	}




	// --------------------------------------------------------------------
	//	Name:	VectorCross
	//	Desc:	ベクトルの外積を求める
	// --------------------------------------------------------------------
	void VectorCross( LPVECTOR3 Out, LPVECTOR3 V1, LPVECTOR3 V2 )
	{
		Out->x	= V1->y * V2->z - V2->y * V1->z;
		Out->y	= V1->z * V2->x - V2->z * V1->x;
		Out->z	= V1->x * V2->y - V2->x * V1->y;

		VectorIdiot( Out );
	}

	void VectorCross( float Out, LPVECTOR2 V1, LPVECTOR2 V2 )
	{
		Out	= V1->x * V2->y - V2->x * V1->y;
	}

	float VectorDot( LPVECTOR3 _V1, LPVECTOR3 _V2)
	{
		return _V1->x*_V2->x + _V1->y*_V2->y + _V1->z*_V2->z;
	}

	float VectorDot( LPVECTOR4 _V1, LPVECTOR4 _V2)
	{
		return _V1->w*_V2->w + _V1->x*_V2->x + _V1->y*_V2->y + _V1->z*_V2->z;
	}



	// --------------------------------------------------------------------
	//	Name:	MatrixIdentity
	//	Desc:	単位行列 (Identity Matrix)
	// --------------------------------------------------------------------
	void MatrixIdentity( LPMATRIX matrix )
	{
		matrix->_11	= 1.0f;	matrix->_12	=  .0f;	matrix->_13 =  .0f;	matrix->_14 =  .0f;
		matrix->_21	=  .0f;	matrix->_22	= 1.0f;	matrix->_23 =  .0f;	matrix->_24 =  .0f;
		matrix->_31	=  .0f;	matrix->_32	=  .0f;	matrix->_33 = 1.0f;	matrix->_34 =  .0f;
		matrix->_41	=  .0f;	matrix->_42	=  .0f;	matrix->_43 =  .0f;	matrix->_44 = 1.0f;
	}



	// --------------------------------------------------------------------
	//	Name:	ConvertQuatToMat
	//	Desc:	クォータニオン行列を求める
	// --------------------------------------------------------------------
	void ConvertQuatToMat( LPMATRIX Out, LPVECTOR4 Quat )
	{
		D3DXMatrixIdentity( Out );

		Out->_11 = 1 - 2 * ( Quat->y * Quat->y + Quat->z * Quat->z );
		Out->_12 = 2 * ( Quat->x * Quat->y - Quat->w * Quat->z );
		Out->_13 = 2 * ( Quat->x * Quat->z + Quat->w * Quat->y );

		Out->_21 = 2 * ( Quat->x * Quat->y + Quat->w * Quat->z );
		Out->_22 = 1 - 2 * ( Quat->x * Quat->x + Quat->z * Quat->z );
		Out->_23 = 2 * ( Quat->y * Quat->z - Quat->w * Quat->x );

		Out->_31 = 2 * ( Quat->x * Quat->z - Quat->w * Quat->y );
		Out->_32 = 2 * ( Quat->y * Quat->z + Quat->w * Quat->x );
		Out->_33 = 1 - 2 * ( Quat->x * Quat->x + Quat->y * Quat->y );
	}


	// ---------------------------------------------------------------------
	//	Name:	GetName
	//	Desc:	XFileの各種名前を取得する
	// ---------------------------------------------------------------------
	void GetName( char_t* Out, const char_t* Str )
	{
		for ( ; *Str != 0x00; ++Out, ++Str )
			*Out	= *Str;
		*Out = '\0';
	}

	void GetName( char_t* Out, size_t number )
	{
		const char_t pNoName[] = STR(NoName);
		sprintf_t( Out, ns_types::eSizeofName, STR(%s%03d\n), pNoName, number );
		++ number;
	}


	// -----------------------------------------------------------------------
	//	Name:	TransMatrixComplete
	//	Desc:	親子関係にそって変換行列を掛け合わせる
	// -----------------------------------------------------------------------
	void TransMatrixComplete( LPMATRIX Out, LPFRAMEHEADER Frame )
	{
		*Out = Frame->pmatAnim != NULL ? *Frame->pmatAnim : Frame->matTrans;

		LPFRAMEHEADER pos = Frame, prev;
		while ( prev = pos->pPrev )
		{
			prev->pmatAnim ?
				D3DXMatrixMultiply( Out, Out, prev->pmatAnim )
				: D3DXMatrixMultiply( Out, Out, &prev->matTrans );
			pos = prev;
		}
	}


	// -----------------------------------------------------------------------
	//	Name:	TransMatrixCompleteTest
	//	Desc:	親子関係にそって変換行列を掛け合わせる
	// -----------------------------------------------------------------------
	void TransMatrixCompleteTest( LPMATRIX Out, LPFRAMEHEADER Frame )
	{
		if ( !Frame )
		{
			MatrixIdentity( Out );
			return;
		}
		*Out = Frame->matTrans;
		while ( Frame->pPrev )
		{
			D3DXMatrixMultiply( Out, Out, &Frame->pPrev->matTrans );
			Frame = Frame->pPrev;
		}
	}


	// -----------------------------------------------------------------------
	//	Name:	nsTransMatrixLord
	//	Desc:	X file から行列を読み込む
	// -----------------------------------------------------------------------
	void FileLoadMatrix( LPMATRIX Out, char_t* Str )
	{
		char_t	szCanma[]	= L",";
		for ( size_t i=0; i<4; ++i )
			for ( size_t j=0; j<4; ++j )
			{
				Out->m[i][j]	= static_cast< float >( atof_t( strtok_t( NULL, szCanma, &Str ) ));
			}
	}



	// ------------------------------------------------------------------------
	//	Name:	nsMatMulVec
	//	Desc:	行列×ベクトル
	// ------------------------------------------------------------------------
	void VecxMat( LPVECTOR3 Out, LPVECTOR3 Vec, LPMATRIX Mat )
	{
		float	x	= Vec->x;
		float	y	= Vec->y;
		float	z	= Vec->z;
		Out->x		= x * Mat->_11 + y * Mat->_21 + z * Mat->_31 + Mat->_41;
		Out->y		= x * Mat->_12 + y * Mat->_22 + z * Mat->_32 + Mat->_42;
		Out->z		= x * Mat->_13 + y * Mat->_23 + z * Mat->_33 + Mat->_43;
	}




	// ------------------------------------------------------------------------
	//	Name:	FileLoadPass
	//	Desc:	メニューから開いたファイルのパスの\を二つにする
	// ------------------------------------------------------------------------
	u_long FileLoadPass( char_t* Out, char_t* Load )
	{
		char_t*	pBuf	= Load;
		char_t*	pPos, *pFirst = Out;
		while ( ( pPos = strchr_t( pBuf, L'\\' ) ) )
		{
			for ( ; pBuf<pPos; ++Out, ++pBuf )
				*Out		= *pBuf;
			*( Out ++ )	= L'\\';
			*( Out ++ )	= L'\\';
			++ pBuf;
		}

		while ( *pBuf )
			*( Out ++ ) = *( pBuf ++ );
		return (u_long)(Out - pFirst);
	}



	//-------------------------------------------------------------------------
	//	Name:	BrendingIndex
	//	Desc:	スキンメッシュの頂点ブレンディング用のインデックスを代入
	//-------------------------------------------------------------------------
	void BrendingIndex( dword_t* Indices, size_t index, size_t num )
	{
		switch ( num )
		{
		case 0:
			{
				if ( !( *Indices & 0x000000ff ) )
					*Indices	|= index;
			} break;

		case 1:
			{
				if ( !( *Indices & 0x0000ff00 ) )
					*Indices	|= ( index << 8 );
			} break;

		case 2:
			{
				if ( !( *Indices & 0x00ff0000 ) )
					*Indices	|= ( index << 16 );
			} break;

		case 3:
			{
				if ( !( *Indices & 0xff000000 ) )
					*Indices	|= ( index << 24 );
			} break;
		}
	}




	//-------------------------------------------------------------------------
	//	Name:	BubbleSort()
	//	Desc:	マテリアルのインデックスが小さい順にバブルソート
	//			- メッシュのマテリアルが複数あるとき
	//-------------------------------------------------------------------------
	void BubbleSort( word_t* Mtrl, word_t* Indices, size_t index )
	{
		bool	bSort;
		word_t	wBuf;
		word_t*	pwIndexPos[2];

		for ( size_t iIndex=0; iIndex<index-1; ++iIndex )
		{
			bSort	= false;

			for ( size_t jDecl=index-1; jDecl>iIndex; --jDecl )
			{
				if ( Mtrl[ jDecl ] < Mtrl[ jDecl-1 ] )
				{
					pwIndexPos[0]	= &Indices[ jDecl * 3 ];
					pwIndexPos[1]	= pwIndexPos[0] - 3;

					for ( size_t k=0; k<3; ++k )
					{
						wBuf					= pwIndexPos[ 0 ][ k ];
						pwIndexPos[ 0 ][ k ]	= pwIndexPos[ 1 ][ k ];
						pwIndexPos[ 1 ][ k ]	= wBuf;
					}

					wBuf			= Mtrl[ jDecl ];
					Mtrl[ jDecl ]	= Mtrl[ jDecl-1 ];
					Mtrl[ jDecl-1 ]	= wBuf;

					if ( !bSort )
						bSort	= true;
				}
			}

			if ( !bSort )
				break;
		}
	}


	void GetMatrixFromStream(nslib::CIFStream& _Ifs, D3DXMATRIX* _Mtx)
	{
		char* __src = _Ifs._getl();
		_Mtx->_11 = nslib::stof32(&__src);
		_Mtx->_12 = nslib::stof32(&__src);
		_Mtx->_13 = nslib::stof32(&__src);
		_Mtx->_14 = nslib::stof32(&__src);
		if (*__src == ns_types::eLineFeed) {
			_Mtx->_21 = nslib::stof32(&(__src = _Ifs._getl()));
			_Mtx->_22 = nslib::stof32(&__src);
			_Mtx->_23 = nslib::stof32(&__src);
			_Mtx->_24 = nslib::stof32(__src);
			__src = _Ifs._getl();
			_Mtx->_31 = nslib::stof32(&__src);
			_Mtx->_32 = nslib::stof32(&__src);
			_Mtx->_33 = nslib::stof32(&__src);
			_Mtx->_34 = nslib::stof32(__src);
			_Mtx->_41 = nslib::stof32(&(__src = _Ifs._getl()));
			_Mtx->_42 = nslib::stof32(&__src);
			_Mtx->_43 = nslib::stof32(&__src);
			_Mtx->_44 = nslib::stof32(__src);
		}
		else {
			_Mtx->_21 = nslib::stof32(&__src);
			_Mtx->_22 = nslib::stof32(&__src);
			_Mtx->_23 = nslib::stof32(&__src);
			_Mtx->_24 = nslib::stof32(&__src);
			_Mtx->_31 = nslib::stof32(&__src);
			_Mtx->_32 = nslib::stof32(&__src);
			_Mtx->_33 = nslib::stof32(&__src);
			_Mtx->_34 = nslib::stof32(&__src);
			_Mtx->_41 = nslib::stof32(&__src);
			_Mtx->_42 = nslib::stof32(&__src);
			_Mtx->_43 = nslib::stof32(&__src);
			_Mtx->_44 = nslib::stof32(__src);
		}
	}

	void GetMaterialFromStream(nslib::CIFStream& _Ifs, D3DMATERIAL9* _Mtrl)
	{
		D3DCOLORVALUE* ambient = &_Mtrl->Ambient,
			*specular = &_Mtrl->Specular,
			*emissive = &_Mtrl->Emissive;
		char* __src = _Ifs._getl();
		ambient->r = nslib::stof32(&__src);
		ambient->g = nslib::stof32(&__src);
		ambient->b = nslib::stof32(&__src);
		ambient->a = nslib::stof32(__src);
		_Mtrl->Diffuse = *ambient;
		_Mtrl->Power = nslib::stof32(_Ifs._getl());
		specular->r = nslib::stof32(&(__src = _Ifs._getl()));
		specular->g = nslib::stof32(&__src);
		specular->b = nslib::stof32(__src);
		emissive->r = nslib::stof32(&(__src = _Ifs._getl()));
		emissive->g = nslib::stof32(&__src);
		emissive->b = nslib::stof32(__src);
	}

	void GetNameWithMBS(char *_Dest, char *_Src, char _Delim)
	{
		//if (*_Src != _Delim) {
		//	while (*++_Src != _Delim);
		//}
		*_Dest = *_Src;
		for (; *_Src!=_Delim && *_Src!=NULL; *++_Dest=*++_Src);
		*_Dest = ns_types::eNull;
	}

	void GetNameWithMBSToWCS(wchar_t *_Dest, char *_Src, char _Delim)
	{
		//if (*_Src != _Delim) {
		//	while (*++_Src != _Delim);
		//}
		*_Dest = (wchar_t)(unsigned char)*_Src;
		for (; *++_Src!=_Delim && *_Src!=NULL; *++_Dest=(wchar_t)(unsigned char)*_Src);
		if (*_Dest != ns_types::eSpace)
			++_Dest;
		*_Dest = ns_types::eNull;
	}

	void GetNameWithWCS(char_t *_Dest, char_t *_Src, char_t _Delim)
	{
		//if (*_Src != _Delim) {
		//	while (*++_Src != _Delim);
		//}
		*_Dest = *_Src;
		for (; *_Src!=_Delim && *_Src!=NULL; *++_Dest=*++_Src);
		*_Dest = ns_types::eNull;
	}

	void LinkFrame(char_t *_Name, LPFRAMEHEADER *_Link, LPFRAMEHEADER _FirstFrame, size_t _NumFrame)
	{
		for (; _NumFrame; --_NumFrame, ++_FirstFrame) {
			if (nslib::strictlystrcmp(_Name, _FirstFrame->szFrameName)) {
				*_Link = _FirstFrame;
				break;
			}
		}
	}


	void JoinFilePass(char_t *_Dest, const char_t *_Path, const char_t *_Name)
	{
		char_t *file = _Dest;
		if (*_Path != NULL) {
			*file = *_Path;
			for (; *++_Path != ns_types::eNull; *++file = *_Path);
			if (wcschr(_Name, static_cast<int>(ns_types::eBackSlash))) {
				while (*--file != ns_types::eBackSlash);
			}
			*++file = *_Name;
		} else {
			if (wcschr(_Name, static_cast<int>(ns_types::eBackSlash))) {
				*file = '.';
				*++file = '\\';
				*++file = '\\';
				*++file = *_Name;
			} else {
				*file = *_Name;
			}
		}
		while (*++file != ns_types::eNull && (*file = *++_Name) != ns_types::eNull);
	}

	
	//-------------------------------------------------------------------------
	//	Name:	GetModel()
	//	Desc:	モデルインスタンス生成
	//-------------------------------------------------------------------------
	CModel* GetModel( const char* _Name )
	{
		using namespace nslib;
		using namespace ns_types;
		///////////////////////////////////// ファイルオープン /////////////////////////////////////
//#ifdef	_UNICODE
		//std::wifstream		f_in;		// ストリーム
//#else
//		std::ifstream		f_in;
//#endif

		// ファイルを開く
		//f_in.open( name, std::ios::binary );
		//if( !f_in.is_open() )
		//	return NULL;

		//char_t* line = new char_t[eLineLength+1];
		//char* data = new char[eMaximizeChunk+1];
		size_t __count, __vertex, __index, __square = 0;
		char __texnames[eSizeofDesc];
		__texnames[0] = eNull;
		__texnames[eDescLength] = eNull;
		char* __ptexnames = __texnames;

		const char __slash = '/';
		const char __header = 'H';
		const char __space = 0x20;
		const char __child = '{';
		const char __parent = '}';
		const char __temp = 't';
		const char __frame = 'F';
		const char __mesh = 'M';
		const char __animation = 'A';
		const char* __coords = "7MeshT";
		const char* __material = "MeshM";
		const char* __normals = "MeshN";
		const char* __transform = "FrameT";
		const char __dupli = 'V';
		const char __xskin = 'X';
		const char __quote = '"';

		LPMODELHEADER __modelHeader	= new MODELHEADER();
		LPANIMEHEADER __animeHeader = 0;
		LPTEXTUREHEADER __texHeader;
		LPSKINEDMESHHEADER __skinHeader;

		// C++のstreamの転送速度を調べる - 純粋関数型雑記帳
		// http://d.hatena.ne.jp/tanakh/20081223
		// ifstream vs fopen()/fread()/fclose() - GameDev.Net Discussion Forums
		// http://www.gamedev.net/community/forums/topic.asp?topic_id=121193
		// iostream メモ - IT戦記
		// http://d.hatena.ne.jp/amachang/20080928/1222604408

		//_tsetlocale(LC_CTYPE, L"");
		//FILE* fp;
		//_tfopen_s(&fp, name, L"r");
		//nslib::CIFStreamT ifs(STR(G:\\works\\leet\\nsXFile_S\\new_x_file\\Tiny\\tiny_4anim.x));
		CIFStream ifs(_Name);
		//clock_t start = clock();
		//while(f_in.read(data, eMaximizeChunk));	// 2750s, 2735s
		//while(f_in.getline(line, eLineLength));	// 3094s, 3094s
		//while(f_in.rdbuf()->sgetn(data, eMaximizeChunk));	// 2667s
		//while(_fgetts(line, eLineLength, fp));	// 46s, 47s, 579s, 594s
		//while(__src = ifs.getl());					// 609s, 610s
		//clock_t end = clock();

		// xof
		ifs._ignorel();
		char* __src = ifs.getl();
		// header
		if (*__src == __space) {
			ifs._ignorel(5);
		}
		// knot.x
		else {
			if (*__src == __slash) { // comment (compatible with triplane.x, heli.x)
				ifs.ignorelc(__slash);
				__src = ifs.getl();
			}
			// template
			if (*__src == __temp) {
				do {
					ifs.ignorel(__parent); // compatible with triplane.x
				} while (*(__src = ifs.getl()) == __temp); // getls: triplane.x ('A`)
				// header: lobby, tiger, triplane
				if (*__src == __header) {
					ifs.ignorels(__parent); // compatible with triplane.x
				}
			}
		}
		do {// while(__src = ifs.getl())
			if (*__src == __parent) {
			}
			else if (*__src == __frame) {
				++ __modelHeader->nFrame;
				__src = ifs.getls();
				if (!strictlystrstr(__src, __transform)) // triplane.x
					continue;
				// FrameTransformMatrix
				ifs.ignorels(__parent); // parent:Frame
			}
			else if (*__src == __mesh) {
				++ __modelHeader->nMesh;
				__modelHeader->nVertex += __vertex = stoi(ifs._getl());
				ifs._ignorel(__vertex);
				__modelHeader->nIndex += __index = stoi(ifs.getl());
				for (size_t i=__index; i; --i) {
					if (stoi(ifs._getl()) == 4) {
						++ __modelHeader->nSquare;
					}
				}

				// child, data, parent の間に空行が無いこと前提
				while (__src = ifs.getls()) { // parent:mesh, getls:triplane.x
					if (*__src == __parent) { // mesh
						break;
					}
					else if (nslib::strictlystrstr(__src, __material)) {
						__modelHeader->nMaterial += __count = stoi(ifs._getl());
						ifs._ignorel(__index + 1);
						// Material
						for (size_t i=0; i<__count; ++i) {
							++ __modelHeader->nMaterial;
							ifs.getl(); // child: Material
							ifs._ignorel(4);
							// TextureFilename
							__src = ifs.getls(); // child:Filename, parent:Material
							if (*__src != __parent) {
								if (!nslib::findstr(__texnames, __src = ifs.getls()+1, __quote)) {
									*__ptexnames = *__src;
									for (; *__src!=__quote; *++__ptexnames = *++__src); // 15step
									//while (*++__ptexnames = *++__src != __quote);	// 18step
									//while ((*++__ptexnames = *++__src) != __quote); // =↑*2
									*__ptexnames = __slash; // delimiter
									*(++ __ptexnames) = 0;	// terminated
									++ __modelHeader->nTexture;
								}
								ifs._ignorel(2);// parent:Material
							}
						}
						ifs._ignorel(); // parent:MaterialList -> while
					}
					else if (strictlystrstr(__src, __coords)) {
						ifs._ignorel(__vertex + 2);
					}
					else if (*__src == __dupli) {
						ifs._ignorel(__vertex + 3);
					}
					else if (strictlystrstr(__src, __normals)) {
						ifs._ignorel(stoi(ifs._getl()));	// furea.x
						ifs.getl();
						ifs._ignorel(__index + 1);
						//ifs._ignorel(__vertex + 1);
						//ifs.getl();
						//ifs._ignorel(__index + 1);
					}
					else if (*__src == __xskin) {
						++ __modelHeader->nSkinedMesh;
						__skinHeader = new SKINEDMESHHEADER();
						__skinHeader->nBoneAll += __count = stoi(ifs._getl(3));
						__skinHeader->nVertexAll += __vertex;
						ifs._ignorel();
						size_t total = 0;
						for (; __count; --__count) {
							size_t weight = stoi(ifs._getl(4));
							total += weight;
							ifs._ignorel(weight*2+2);
						}
						__skinHeader->nWeightAll += total;
					}
					else {
						ifs.ignorels(__parent);
					}
				}
				continue;
			}
			else if (*__src == __animation) {
				++ __modelHeader->nAnimationSet;
				if (!__animeHeader)
					__animeHeader = new ANIMEHEADER();

				while (*ifs.getls() != __parent) {	// child:Animation, parent:ASet
					++ __animeHeader->nAnimation;
					if (*(__src = ifs.getls()) == __child) {	// name, child:AnimationKey
						ifs._ignorel();	// child:AnimationKey
					}
					do {
						switch (stoi(ifs._getl())) {
							case eAnimationQuatanion:
								++ __animeHeader->nQuat;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nQuatList += __count;
								__animeHeader->nMatList += __count;
								break;
							case eAnimationScale:
								++ __animeHeader->nScale;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nVecList += __count;
								break;
							case eAnimationSlide:
								++ __animeHeader->nSlide;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nVecList += __count;
								break;
							case eAnimationMatrix:
								++ __animeHeader->nMat;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nMatList += __count;
						}
						ifs._ignorel(__count+1); // parent:AnimetionKey
						if (*(__src = ifs.getls()) == __child) { // name, child:AKey, parent:Animation
							ifs._ignorel(); // parent:Animation
							break;
						}
					} while (*__src != __parent); // child:AKey, parent:Animation
				}
			}
		} while(__src = ifs.getls());
		//clock_t end = clock();	// char 46ms:250 296, wchar_t 625ms:281 906

		__modelHeader->nIndex += __modelHeader->nSquare;
		char_t *pass = __modelHeader->szPass;
		nslib::GetFilePass(pass, _Name);
		size_t __textures = __modelHeader->nTexture;
		if (__textures == 1) {
			__texHeader = new TEXTUREHEADER();
			*--__ptexnames = 0;
			mbstowcs_s(NULL, __texHeader->szTexName, eSizeofName, __texnames, _TRUNCATE);
			ns_d3d::Cd3d::Instance().CreateTexture(__texHeader->szTexName, &__texHeader->pTexture, pass);
		}
		else if (__textures) {
			__texHeader = new TEXTUREHEADER[__textures]();
			LPTEXTUREHEADER __postex = __texHeader;
			char_t __temptexnames[eSizeofDesc],
				*__ptmp = __temptexnames, *__pout = __texHeader->szTexName;
			mbstowcs_s(NULL, __temptexnames, eSizeofDesc, __texnames, _TRUNCATE);
			for (size_t i=0; i<__textures; ++i, __pout = (++__postex)->szTexName) {
				*__pout = *__ptmp;
				for (; *__ptmp!=__slash; *++__pout = *++__ptmp);
				*__pout = 0;
				ns_d3d::Cd3d::Instance().CreateTexture(__postex->szTexName, &__postex->pTexture, pass);
			}
		}

		CModel* __model;
		if (__modelHeader->nSkinedMesh) {
			CSkinedMeshModel* __skinedModel = new CSkinedMeshModel(__modelHeader, __animeHeader, __skinHeader, __texHeader);
			CSkinedMeshModelInit* __initModel = new CSkinedMeshModelInit();
			if (!__initModel->CreateFromStream(__skinedModel, ifs, __texnames)) {
				__skinedModel->Release();
				__initModel->Release();
				return NULL;
			}
			__initModel->Release();
			__model = __skinedModel;
		}
		else {
			__model = new CModel(__modelHeader, __animeHeader, __texHeader);
			CModelInit* __initModel = new CModelInit();
			if (!__initModel->CreateFromStream(__model, ifs, __texnames)) {
				__model->Release();
				__initModel->Release();
				return NULL;
			}
			__initModel->Release();
		}

		return __model;
	}



	//-------------------------------------------------------------------------
	//	Name:	CreateModel()
	//	Desc:	モデルインスタンス生成
	//-------------------------------------------------------------------------
	CModel* CreateModel( const char_t* File )
	{
		///////////////////////////////////// ファイルオープン /////////////////////////////////////
#ifdef	_UNICODE
		std::wifstream		f_in;		// ストリーム
#else
		std::ifstream		f_in;
#endif

		// ファイルを開く
		f_in.open( File, std::ios::binary | std::ios::ate );
		if( !f_in )
			return NULL;

		char_t*		pData, *pBuf;	// データバッファ
		std::streamsize	s_size;		// ストリームサイズ

		// ファイルサイズ
		s_size	= f_in.tellg();

		// 先頭へシーク
		f_in.seekg( std::ios::beg );

		// メモリを確保
		pData	= new char_t[ static_cast<char_t>(s_size) ];
		pBuf	= new char_t[ static_cast<char_t>(s_size) ];

		// ファイル読み込み
		f_in.read( pData, s_size );
		memcpy_t( pBuf, static_cast<size_t>(s_size), pData, s_size );

		// ファイルを閉じる
		f_in.close();
		/////////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////// データ読み込み ///////////////////////////////////////
		char_t	szParent[]		= STR({);
		char_t	szChild[]		= STR(});
		char_t	szCoron[]		= STR(;);
		char_t	szLastToken[]	= STR(;;);
		char_t	szSubTokens[4];
		char_t*	pMainToken, *pSubToken, *pNextToken = pData;
		char_t	szFrame[]			= STR(Frame);
		char_t	szMesh[]			= STR(Mesh);
		char_t	szCoords[]			= STR(Coords);
		char_t	szMaterial[]		= STR(Material);
		char_t	szFilename[]		= STR(Filename);
		char_t	szSkin[]			= STR(Skin);
		char_t	szAnimation[]		= STR(Animation);
		LPMODELHEADER		pModelHead	= new MODELHEADER();
		memset( pModelHead, '\0', sizeof( MODELHEADER ) );
		LPANIMEHEADER		pAnimeHead	= 0;
		LPSKINEDMESHHEADER	pSkinHead	= 0;

		szSubTokens[0]	= 0x20;
		szSubTokens[1]	= 0x0d;
		szSubTokens[2]	= 0x0a;
		szSubTokens[3]	= '\0';

		// トークンループ
		while ( pMainToken = strtok_t( NULL, szChild, &pNextToken ) )
		{
			if ( !( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) ) )
			{
				// 親子関係処理
				continue;
			}

			if ( strstr_t( pSubToken, szFrame ) )
			{
				// Frame処理
				++ pModelHead->nFrame;
			}
			else if ( strstr_t( pSubToken, szMesh ) )
			{
				// Mesh処理
				++ pModelHead->nMesh;
				pMainToken	= strstr_t( pMainToken, szParent ) + 1;	// Mesh名
				pModelHead->nVertex	+= atoi_t( pMainToken );	// 頂点数
				pMainToken	= strstr_t( pMainToken, szLastToken ) + 2;
				pModelHead->nIndex	+= atoi_t( pMainToken );		// インデックス数
				pMainToken	= strstr_t( pMainToken, szLastToken ) + 2;

				do
				{
					if ( !( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) ) )
						break;
					else if ( strstr_t( pSubToken, szCoords ) )
					{
						// Texture coords処理
					}
					else if ( strstr_t( pSubToken, szMaterial ) )
					{
						size_t	nMtrl		= 0;

						// Material list処理
						pMainToken	= strstr_t( pMainToken, szParent ) + 1;	// Material名
						nMtrl		= atoi_t( pMainToken );
						pMainToken	= strstr_t( pMainToken, szParent ) + 1;

						for ( size_t iMtrl=0; iMtrl<nMtrl; ++iMtrl )
						{
							// Material処理
							++ pModelHead->nMaterial;
							if ( iMtrl > 0 )
								pMainToken	= strtok_t( NULL, szChild, &pNextToken );
							for ( size_t jMtrl=0; jMtrl<3; ++jMtrl )
								pMainToken	= strstr_t( pMainToken, szLastToken ) + 2;

							if ( ( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) )
								&& strstr_t( pSubToken, szFilename ) )
							{
								// Texture file name処理
								strtok_t( NULL, szChild, &pNextToken );
							}
						}
						pNextToken	= strstr_t( pNextToken, szChild );
					}
					else if ( strstr_t( pSubToken, szSkin ) )
					{
						size_t		nBone	= 0;
						// Skin mesh処理
						++ pModelHead->nSkinedMesh;
						if ( !pSkinHead )
						{
							pSkinHead	= new SKINEDMESHHEADER();
							memset( pSkinHead, '\0', sizeof( SKINEDMESHHEADER ) );
						}
						pMainToken			= strstr_t( pMainToken, szCoron ) + 1;
						pMainToken			= strstr_t( pMainToken, szCoron ) + 1;
						nBone				= atoi_t( pMainToken );	// ボーン数
						pSkinHead->nBoneAll	+= nBone;

						// Skin weightsループ
						for ( size_t iBone=0; iBone<nBone; ++iBone )
						{
							pMainToken				= strtok_t( NULL, szChild, &pNextToken );
							pMainToken				= strstr_t( pMainToken, szCoron ) + 1;	// 頂点数
							pSkinHead->nVertexAll	+= atoi_t( pMainToken );
						}
					}
				} while ( pMainToken = strtok_t( NULL, szChild, &pNextToken ) );
			}
			else if ( strstr_t( pSubToken, szAnimation ) )	// Animation set
			{
				size_t	nType, nTime;
				char_t	szSet[]	= L"AnimationSet";
				char_t	szKey[]	= L"Key";
				pAnimeHead		= new ANIMEHEADER();
				memset( pAnimeHead, '\0', sizeof( ANIMEHEADER ) );

				// Animation set処理
				++ pModelHead->nAnimationSet;
				pMainToken		= strstr_t( pMainToken, szParent ) + 1;
				strtok_t( NULL, szParent, &pMainToken );	// Animation

				// Animationループ
				do
				{
					++ pAnimeHead->nAnimation;
					if ( strstr_t( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ), szParent ) )
					{
						// Animation name処理
						pMainToken	= strtok_t( NULL, szChild, &pNextToken );
						pSubToken	= strtok_t( NULL, szSubTokens, &pMainToken );
					}

					// Animation keyループ
					while ( pSubToken && strstr_t( pSubToken, szKey ) )
					{
						// Animation key処理
						++ pMainToken;
						nType	= atoi_t( strtok_t( NULL, szSubTokens, &pMainToken ) );
						nTime	= atoi_t( strtok_t( NULL, szSubTokens, &pMainToken ) );
						pAnimeHead->nTimeList	+= nTime;
						switch ( nType )
						{
						case eAnimationQuatanion:
							{	// Quatanion処理
								++ pAnimeHead->nQuat;
								pAnimeHead->nQuatList	+= nTime;
								pAnimeHead->nMatList	+= nTime;
							} break;

						case eAnimationScale:
							{	// Scale処理
								++ pAnimeHead->nScale;
								pAnimeHead->nVecList	+= nTime;
							} break;

						case eAnimationSlide:
							{	// Slide処理
								++ pAnimeHead->nSlide;
								pAnimeHead->nVecList	+= nTime;
							} break;

						case eAnimationMatrix:
							{	// Matrix処理
								++ pAnimeHead->nMat;
								pAnimeHead->nMatList	+= nTime;
							} break;
						}

						pMainToken	= strtok_t( NULL, szChild, &pNextToken );
						pSubToken	= strtok_t( NULL, szSubTokens, &pMainToken );
					}

					if ( !pSubToken )
						pMainToken	= strtok_t( NULL, szParent, &pNextToken );
					else if ( strstr_t( pSubToken, szParent ) )
					{
						// Animation name処理
						pMainToken	= strtok_t( NULL, szSubTokens, &( pNextToken += 4 ) );
					}

					if ( !strstr_t( pMainToken, szAnimation ) )	// Animation
					{
						if ( ( pMainToken = strtok_t( NULL, szSubTokens, &pNextToken ) )
							&& strstr_t( pMainToken, szSet ) )
						{
							// Animation set処理
							++ pModelHead->nAnimationSet;
							strtok_t( NULL, szParent, &pNextToken );	// Animation
							strtok_t( NULL, szParent, &pNextToken );	// AnimationKey
						}
						else
							break;
					}
				} while ( pMainToken = strtok_t( NULL, szChild, &( ++pNextToken ) ) );
			}
		}

		delete [] pData;
		pData	= pBuf;

		/////////////////////////////////// モデル生成 ///////////////////////////////////////

		CModel*		pModel		= 0;
		CModelInit*	pModelInit	= 0;

		try
		{
			if ( pModelHead->nSkinedMesh )
			{
				pModel		= new CSkinedMeshModel( pModelHead, pAnimeHead, pSkinHead );
				pModelInit	= new CSkinedMeshModelInit();
			}
			else
			{
				pModel		= new CModel( pModelHead, pAnimeHead );
				pModelInit	= new CModelInit();
			}
		}
		catch ( std::bad_alloc )
		{
			if ( pModel )		pModel->Release();
			if ( pModelInit )	pModelInit->Release();
		}

		//if ( !pModelInit->Create( pModel, pData ) )
		//{
		//	pModel->Release();
		//	pModelInit->Release();
		//	return	NULL;
		//}

		pModelInit->Release();
		return	pModel;
	}


}	// namespace: ns_model
