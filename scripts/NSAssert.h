#pragma	once
#ifndef _NSASSERT_H_
#define	_NSASSERT_H_

// Including assert();
#include <cassert>
// Including char_t;
#include "nstypes.h"


#if defined (_DEBUG)
namespace ns_dbg
{
	//	Bugslayer, MSJ, February 1999
	//	http://www.microsoft.com/msj/0299/bugslayer/bugslayer0299.aspx
	//
	//	Bugslayer: SUPERASSERT Goes .NET -- MSDN Magazine, November 2005
	//	http://msdn.microsoft.com/msdnmag/issues/05/11/Bugslayer/default.aspx
	void CustomAssertFunction(const ns_types::char_t*, ...);
	bool CustomAssertFunctionEx(int, ns_types::char_t*, ns_types::char_t*, size_t, ns_types::char_t*);
	wchar_t* CharToWChar(const char*);

	//	I know the answer (it's 42) : C/C++ Compile Time Asserts
	//	http://blogs.msdn.com/abhinaba/archive/2008/10/27/c-c-compile-time-asserts.aspx
	//	Compile Time Assertion �`�⑫�`�bCode++
	//	http://ameblo.jp/woooh-p/entry-10033062583.html
	template<bool> struct STATIC_ASSERT_FAILURE;
	template<> struct STATIC_ASSERT_FAILURE<true> {};
	template<int> struct _static_assert {};

}	// ns_dbg

// debug
#define ASSERT_FORMAT(exp) L"----------------\nExp: " ## L#exp ## L"\nDesc: %s\nLine: %d\nFunction: %s\nFile://%s\n----------------\n"
#define NS_ASSERT(exp, description)	\
	if (exp == true)	\
{ ns_dbg::CustomAssertFunction(ASSERT_FORMAT(exp), description, __LINE__, ns_dbg::CharToWChar(__FUNCTION__), ns_dbg::CharToWChar(__FILE__));	\
	_asm { int 3 } }

// v1.0a (Stacktrace�p)
#define NS_ASSERTEX(exp, description)	\
	if (ns_dbg::CustomAssertFunctionEx((int)(exp), L#exp, description, __LINE__, __FILE__))	\
{ _asm { int 3 } }

#define NS_STATIC_ASSERT(exp) \
	typedef ns_dbg::_static_assert<sizeof(ns_dbg::STATIC_ASSERT_FAILURE<exp>)>	\
	JOIN(NS_STATIC_ASSERTION_FAILURE,JOIN(__LINE__,__FILE__))

#define NS_ASSERT_FAILURE(exp) \
	assert(0 && STR(exp))

#else
#define NS_ASSERT(exp, description)
#define NS_ASSERTEX(exp, description)
#define NS_STATIC_ASSERT(exp)
#define NS_ASSERT_FAILURE(exp)
#endif	// _DEBUG

#endif	// _NSASSERT_H_
