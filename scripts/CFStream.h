
#pragma once
#ifndef _NS_CFSTREAM_H_
#define _NS_CFSTREAM_H_

#include "nstypes.h"
//#include <time.h>

namespace nslib {
	using ns_types::char_t;
	using ns_types::eLineLength;

	class CIFStream {

	public:
		CIFStream() : _fp() {
			_line[0] = ns_types::eZero;
		}
		CIFStream(const char* _Filename, const char* _Mode = "r")
#pragma warning(disable:4996)
			: _fp(fopen(_Filename, _Mode))
		{
			_line[0] = ns_types::eNull;
		}
#pragma warning(default:4996)
		~CIFStream() {
			if (_fp) fclose(_fp);
		}

		errno_t open(const char* _Filename, const char* _Mode = "r") {
			return fopen_s(&_fp, _Filename, _Mode);
		}
		char* read();
		void write();
		void getc();
		// getline
		char* _getl() {
			FILE* __fp = _fp;
			return ::fgets(_line, eLineLength, __fp);
		}
		char* _getl(size_t _Row) {
			FILE* __fp = _fp;
			-- _Row;
			for (; _Row; --_Row, ::fgets(_line, eLineLength, __fp));
			return ::fgets(_line, eLineLength, __fp);
		}
		// getline + ignore 0x0a
		char* getl();
		// getline + ignore LF:0x0a, SP:0x20, TAB: 0x09
		char* getls();
		char* getls(size_t _Row);
		// _Delimiterがある場所まで飛ばす
		char* ignore(char _Delimiter);
		// ignore line
		void _ignorel() {
			FILE* __fp = _fp;
			::fgets(_line, eLineLength, __fp);
		}
		void _ignorel(size_t _Count) {
			FILE* __fp = _fp;
			for (; _Count; --_Count, ::fgets(_line, eLineLength, __fp));
			return;
		}
		// ignore + _ignorel
		void ignorel();
		void ignorel(const char _Delimiter);
		// ignorel + ignore space
		void ignorels(const char _Delimiter);
		// ignorel + ignore char
		void ignorelc(const char _Ignore);
		void seekbegin() {
			fseek(_fp, 0, SEEK_SET);
		}
		void close() {
			fclose(_fp);
		}

	private:
		CIFStream(const CIFStream&);
		CIFStream& operator = (const CIFStream&);
		char _line[ns_types::eSizeofLine];
		FILE* _fp;
	};



/*	class CWIFStream {

	public:
		CWIFStream() : _fp() {
			_line[0] = ns_types::eZero;
			//_line[ns_types::eSizeofLine] = ns_types::eZero;
		}
		//template<typename T>
		CWIFStream(const char_t* _Filename, const char_t* _Mode = STR(r))
#pragma warning(disable:4996)
			: _fp(_tfopen(_Filename, _Mode))
		{
#pragma warning(default:4996)
#ifdef _UNICODE
			_tsetlocale(LC_CTYPE, _T(""));
#endif
			_line[0] = ns_types::eZero;
			//_line[ns_types::eSizeofLine] = ns_types::eZero;
		}
		~CWIFStream() {
			if (_fp) fclose(_fp);
		}

		errno_t open(const char_t* _Filename, const char_t* _Mode = STR(r)) {
#ifdef _UNICODE
			_tsetlocale(LC_CTYPE, _T(""));
#endif
			return _tfopen_s(&_fp, _Filename, _Mode);
		}
		void read(char_t* _Str);
		void getc();
		// getline
		char_t* _getl() {
			FILE* __fp = _fp;
			return ::_fgetts(_line, eLineLength, __fp);
		}
		char_t* _getl(const size_t _Row) {
			FILE* __fp = _fp;
			for (size_t i=1; i<_Row; ++i, ::_fgetts(_line, eLineLength, __fp));
			return ::_fgetts(_line, eLineLength, __fp);
		}
		// getline + ignore 0x0a
		char_t* getl() {
			FILE* __fp = _fp;
			//char_t* __src = _line;
			char_t* __p;
			//while (__p = ::_fgetts(_line, eLineLength, _fp)) { // 594s:281 875, 610s:265 875
			while (__p = ::_fgetts(_line, eLineLength, __fp)) { // 578s:281 859, 594s:265 859
				if (*__p != 0x0a)
					break;
			}
			return __p;
			//return _fgetts(_line, eLineLength, _fp); // 265 859
			//return _fgetts(_line, eLineLength, *__fp); // 594s:265 859

			//clock_t start = clock();
			//while (_fgetts(__src, eLineLength, *__fp)); // 281 890
			//while (_fgetts(_line, eLineLength, *__fp)); // 578s:281 859
			//while (__p = _fgetts(_line, eLineLength, __fp)); // 609s:281 890
			//while (::_fgetts(_line, ns_types::eLineLength, __fp)); // 578s:265 843
			//while (_fgetts(_line, eLineLength, __fp)); // 578s:281 859
			//while (_fgetts(_line, eLineLength, _fp)); // 610s:265 875

			// 438s:256 703, 422s:281 703
			//FILE* fp;
			//_tfopen_s(&fp, L"data\\tiny_4anim.x", L"r");
			//char_t line[ns_types::eLineLength+1];
			//while (_fgetts(line, eLineLength, fp));
			//clock_t end = clock();
			//return;
		}
		// getline + ignore LF:0x0a, SP:0x20, TAB: 0x09
		char_t* getls() {
			FILE* __fp = _fp;
			char_t* __p;
			while (__p = ::_fgetts(_line, eLineLength, __fp)) {
				if (*__p == 0x20) {
					while (*(++__p) == 0x20);
					if (*__p == 0x0a)	// tank.x, tiny.x
						continue;
					return __p;
				}
				else if (*__p == 0x09) {
					while (*(++__p) == 0x09);
					//if (*__p == 0x0a)
					//	continue;
					return __p;
				}
				else if (*__p != 0x0a) {
					break;
				}
			}
			return __p;
		}
		// _Delimiterがある場所まで飛ばす
		char_t* ignore(char_t _Delimiter) {
			FILE* __fp = _fp;
			char_t* __p;
			while (__p = ::_fgetts(_line, eLineLength, __fp)) {
				if (*__p == 0x0a || !(__p = _tcschr(_line, _Delimiter)))
					continue;
				break;
			}
			return __p;
		}
		// ignore line
		void _ignorel(const size_t _Row = 1) {
			FILE* __fp = _fp;
			for (size_t i=0; i<_Row; ++i, ::_fgetts(_line, eLineLength, __fp));
			//size_t __count = 0;
			//while (++__count < _Row) {	// for と step 数変わらず
			//	::_fgetts(_line, eLineLength, __fp);
			//}
			return;// ::_fgetts(_line, eLineLength, *__fp);
		}
		// ignore + _ignorel
		void ignorel(const char_t _Delimiter) {
			FILE* __fp = _fp;
			char_t* __p;
			while (__p = ::_fgetts(_line, eLineLength, __fp)) {
				if (*__p == _Delimiter)
					break;
			}
			return;// ::_fgetts(_line, eLineLength, *__fp);
		}
		// ignorel + ignore space
		void ignorels(const char_t _Delimiter) {
			FILE* __fp = _fp;
			char_t* __p;
			while (__p = ::_fgetts(_line, eLineLength, __fp)) {
				if (*__p == 0x0a) {
					continue;
				}
				else if (*__p == 0x20) {
					for (;*__p == 0x20; ++__p);
					if (*__p == _Delimiter)
						break;
				}
				else if (*__p == _Delimiter) {
					break;
				}
			}
			return;
		}
		// ignorel + ignore char
		void ignorelc(const char_t _Ignore) {
			FILE* __fp = _fp;
			char_t* __p = 0;
			while (__p = ::_fgetts(_line, eLineLength, __fp)) {
				if (*__p != _Ignore)
					break;
			}
			return;
		}
		int atoi() {
			return ::_ttoi(_line);
		}
		float atof() {
			return static_cast<float>(::_tstof(_line));
		}
		void getword(char_t*);

	private:
		CWIFStream(const CWIFStream&);
		CWIFStream& operator = (const CWIFStream&);
		char_t _line[ns_types::eSizeofLine];
		FILE* _fp;
	};

#ifdef _UNICODE
	typedef class CWIFStream CIFStreamT;
#else
	typedef class CIFStream IFStreamT;
#endif*/

}

#endif