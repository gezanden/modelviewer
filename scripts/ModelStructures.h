//
//	ModelStructure.h
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include	"nsd3d9.h"


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_model
{
	using namespace ns_types;


	//--------------------------------------------------------
	// structures
	//--------------------------------------------------------

	/////////////////////// Model status ///////////////////////////

	// A structure for our custom vertex type. We added texture coordinates
	typedef struct _CUSTOMVERTEX_
	{
		VECTOR3	position;		// The position
		VECTOR3	normal;		// The surface normal for the vertex
		VECTOR2	texcoord;	// The texture coordinates

		_CUSTOMVERTEX_()
			: position(.0f, .0f, .0f)
			, normal(.0f, .0f, .0f)
			, texcoord(.0f, .0f)
		{
		}
	} CUSTOMVERTEX, *LPCUSTOMVERTEX;


	// A structure for our skin meshed vertex type
	typedef struct _SKINEDMESHVERTEX_
	{
		VECTOR3	vPosition;		// The position
		VECTOR3	vNormal;		// The surface normal for the vertex
		VECTOR2	vTexture;  		// The texture coordinates
		dword_t	dwWeightIndex;	// The index for blending weight
		dword_t	dwBoneIndex;	// The index for bone
		size_t	nBlendNum;		// the number of Blending

		explicit _SKINEDMESHVERTEX_()
			: vPosition()
			, vNormal()
			, vTexture()
			, dwWeightIndex()
			, dwBoneIndex()
			, nBlendNum() {}
	} SKINEDMESHNODEVERTEX, *LPSKINEDMESHNODEVERTEX;

	// A structure for save to square polygon index
	typedef struct _SQUAREPOLYGONHEADER_
	{
		size_t	nSquare;					// 現在の4 角形ポリゴン数
		size_t	nPosition;					// 現在のメッシュの4 角形ポリゴンリスト開始位置
		size_t	nOffsetPolygon;				// メッシュのポリゴンオフセット
		word_t*	pwSquare;					// 4 角形ポリゴンのインデックスリスト

		explicit _SQUAREPOLYGONHEADER_()
			: nSquare()
			, nPosition()
			, nOffsetPolygon()
			, pwSquare() {}
	} SQUAREPOLYGONHEADER, *LPSQUAREPOLYGONHEADER;


	// - TEXTURE - //
	typedef struct _TEXTUREHEADER_
	{
		char_t			szTexName[eSizeofName];	// テクスチャの名前
		LPDIRECT3DTEXTURE9      pTexture;		// テクスチャ

		explicit _TEXTUREHEADER_()
			//, szTexName()
			: pTexture()
		{ szTexName[0] = '\0'; }
	} TEXTUREHEADER, *LPTEXTUREHEADER;


	typedef struct _EFFECTSTRING_
	{
		D3DXHANDLE handle;
		char_t name[eSizeofName];
		LPTEXTUREHEADER texture;
		_EFFECTSTRING_()
			: texture()	{
			name[0] = eNull;
			name[eNameLength] = eNull;
		}
	} EFFECTSTRING, *LPEFFECTSTRING;

	typedef struct _EFFECTFLOAT1_
	{
		D3DXHANDLE handle;
		char_t name[eSizeofName];
		float param;
		_EFFECTFLOAT1_()
			: param(.0f) {
				name[0] = eNull;
				name[eNameLength] = eNull;
		}
	} EFFECTFLOAT1, *LPEFFECTFLOAT1;

	typedef struct _EFFECTFLOAT3_
	{
		D3DXHANDLE handle;
		char_t name[eSizeofName];
		VECTOR3 param;
		_EFFECTFLOAT3_()
			: param(.0f, .0f, .0f) {
				name[0] = eNull;
				name[eNameLength] = eNull;
		}
	} EFFECTFLOAT3, *LPEFFECTFLOAT3;

	typedef struct _EFFECTFLOAT4_
	{
		D3DXHANDLE handle;
		char_t name[eSizeofName];
		VECTOR4 param;
		_EFFECTFLOAT4_()
			: param(.0f, .0f, .0f, .0f) {
				name[0] = eNull;
				name[eNameLength] = eNull;
		}
	} EFFECTFLOAT4, *LPEFFECTFLOAT4;

	typedef struct _EFFECTINSTANCE_
	{
		char_t name[eSizeofName]; // .fx file
		size_t numtx;
		size_t numfloat1;
		size_t numfloat4;
		LPEFFECTSTRING string;
		LPEFFECTFLOAT1 float1;
		//LPEFFECTFLOAT3 float3;
		LPEFFECTFLOAT4 float4;
		//MATERIAL material;
		LPD3DXEFFECT d3dEffect;
		D3DXHANDLE hWorld;
		D3DXHANDLE hView;
		D3DXHANDLE hProj;
		_EFFECTINSTANCE_()
			: numtx()
			, numfloat1()
			, numfloat4()
			, string()
			, float1()
			, float4() {
				name[0] = eNull;
				name[eNameLength] = eNull;
		}
	} EFFECTINSTANCE, *LPEFFECTINSTANCE;

	typedef struct _EFFECTHEADER_
	{
		size_t fxtx;
		size_t fxfloat1;
		size_t fxfloat4;
		LPEFFECTINSTANCE fxInstance;
		LPEFFECTSTRING fxString;
		LPEFFECTFLOAT1 fxFloats1;
		LPEFFECTFLOAT4 fxFloats4;
		_EFFECTHEADER_()
			: fxtx()
			, fxfloat1()
			, fxfloat4()
			, fxInstance()
			, fxString()
			, fxFloats1()
			, fxFloats4() {
		}
	} EFFECTHEADER, *LPEFFECTHEADER;


	// - FRAME - //
	typedef struct _FRAMEHEADER_
	{
		char_t			szFrameName[eSizeofName];	// フレーム名
		MATRIX			matTrans;				// 変換行列
		LPMATRIX		pmatAnim;				// アニメーション変換行列
		_FRAMEHEADER_*	pPrev;					// 親

		explicit _FRAMEHEADER_()
			//: szFrameName()
			: matTrans()
			, pmatAnim()
			, pPrev()
		{ szFrameName[0] = '\0'; }
	} FRAMEHEADER, *LPFRAMEHEADER;

	struct _SKINEDMESHNODE_;

	// - MESH - //
	typedef struct _MESHHEADER_
	{
		size_t			nVertexNum;				// 頂点数
		size_t			nPolygonNum;			// ポリゴン数
		size_t			nMtrlNum;				// マテリアル数
		dword_t			dwMtrlIndex;			// 複数マテリアル時のポリゴンインデックス
		char_t			szMeshName[eSizeofName];	// メッシュ名
		//char_t			szTexName[eSizeofName];	// テクスチャ名
		LPMATERIAL		pMtrl;					// マテリアル情報
		LPFRAMEHEADER	pPrev;					// 親
		LPTEXTUREHEADER	pTexture;				// テクスチャ情報
		bool			bSkinMesh;				// スキンメッシュの有無
		_SKINEDMESHNODE_*	pSkinMesh;			// スキンメッシュ情報
		LPEFFECTINSTANCE pEffect;

		explicit _MESHHEADER_()
			: nVertexNum()
			, nPolygonNum()
			, nMtrlNum()
			, dwMtrlIndex()
			//, szMeshName()
			//, szTexName()
			, pMtrl()
			, pPrev()
			, pTexture()
			, bSkinMesh()
			, pSkinMesh()
			, pEffect()
		{
			szMeshName[0] = '\0';
			//szTexName[0] = '\0';
		}
	} MESHHEADER, *LPMESHHEADER;


	////////////////////////////// アニメーション /////////////////////////////////

	// - QUATERNION - //
	typedef struct _ANIMEQUATHEADER_
	{
		size_t			nTimeNum;				// コマ数
		size_t*			pnTimeList;				// コマ
		LPVECTOR4		pvQuatList;				// クォータニオン成分
		LPMATRIX		pmatQuatList;			// クォータニオン行列

		explicit _ANIMEQUATHEADER_()
			: nTimeNum()
			, pnTimeList()
			, pvQuatList()
			, pmatQuatList(){}
	} ANIMEQUATHEADER, *LPANIMEQUATHEADER;


	// - SCALING - //
	typedef struct _ANIMESCALEHEADER_
	{
		size_t			nTimeNum;				// コマ数
		size_t*			pnTimeList;				// コマ
		LPVECTOR3		pvScaleList;			// 拡大

		explicit _ANIMESCALEHEADER_()
			: nTimeNum()
			, pnTimeList()
			, pvScaleList() {}
	} ANIMESCALEHEADER, *LPANIMESCALEHEADER;


	// - MOVE - //
	typedef struct _ANIMESLIDEHEADER_
	{
		size_t			nTimeNum;				// コマ数
		size_t*			pnTimeList;				// コマ
		LPVECTOR3		pvSlideList;			// 平行移動

		explicit _ANIMESLIDEHEADER_()
			: nTimeNum()
			, pnTimeList()
			, pvSlideList() {}
	} ANIMESLIDEHEADER, *LPANIMESLIDEHEADER;


	// - MATRIX - //
	typedef struct _ANIMEMATHEADER_
	{
		size_t			nTimeNum;				// コマ数
		size_t*			pnTimeList;				// コマ
		LPMATRIX		pmatList;				// マトリクス

		explicit _ANIMEMATHEADER_()
			: nTimeNum()
			, pnTimeList()
			, pmatList() {}
	} ANIMEMATHEADER, *LPANIMEMATHEADER;


	// - ANIMATION - //
	typedef struct _ANIMENODE_
	{
		char_t			szAnimeName[eSizeofName];		// アニメ名
		char_t			szTargetFrame[eSizeofName];	// アニメさせるフレーム名
		LPFRAMEHEADER		pTargetFrame;				// ターゲットフレーム
		LPANIMEQUATHEADER	pQuat;						// クォータニオン
		LPANIMESCALEHEADER	pScale;						// 拡大
		LPANIMESLIDEHEADER	pSlide;						// 平行移動
		LPANIMEMATHEADER	pMat;						// マトリクス

		explicit _ANIMENODE_()
			//: szAnimeName()
			//, szTargetFrame()
			: pTargetFrame()
			, pQuat()
			, pScale()
			, pSlide()
			, pMat()
		{
			szAnimeName[0] = '\0';
			szTargetFrame[0] = '\0';
		}
	} ANIMENODE, *LPANIMENODE;


	// - ANIMATION SET - //
	typedef struct _ANIMESET_
	{
		char_t			szAnimeName[eSizeofName];	// アニメーション名
		size_t			nAnimation;				// アニメーション数
		size_t			nMaxTime;				// アニメーションの最終フレーム
		LPANIMENODE		pAnimeList;				// アニメーション情報リスト

		explicit _ANIMESET_()
			//: szAnimeName()
			: nAnimation()
			, nMaxTime()
			, pAnimeList()
		{ szAnimeName[0] = '\0'; }
	} ANIMESET, *LPANIMESET;


	// - RELATIONAL ANIMATION - //
	typedef struct _ANIMEHEADER_
	{
		size_t			nAnimation;				// アニメーション数
		size_t			nQuat;					// ANIMEQUAT num
		size_t			nScale;					// ANIMESCALE num
		size_t			nSlide;					// ANIMESLIDE num
		size_t			nMat;					// ANIMEMAT num
		size_t			nTimeList;				// lpnTimeListの数
		size_t			nQuatList;				// lpvQuatListの数
		size_t			nVecList;				// lpvListの数
		size_t			nMatList;				// lpmatListの数
		size_t			nThisAnime;				// 現在のアニメーションナンバー
		float			fThisTime;				// アニメーションの現在タイム
		LPANIMESET	pAnimeSetList;				// アニメーションセット情報リスト
		LPANIMENODE		pAnimeList;				// アニメーション情報リスト
		LPMATRIX		pMatAnime;				// アニメーション変換行列
		size_t*			pnTimeList;				// 全てのアニメーションのタイムリスト
		LPANIMEQUATHEADER	pQuatList;			// アニメーションクォータニオン
		LPANIMESCALEHEADER	pScaleList;			// アニメーションスケーリング
		LPANIMESLIDEHEADER	pSlideList;			// アニメーション平行移動
		LPANIMEMATHEADER	pMatList;			// アニメーションマトリクス
		LPVECTOR4		pvQuatList;				// 全てのクォータニオンアニメーションリスト
		LPMATRIX		pmatList;				// 全てのアニメーションのマトリクスリスト
		LPVECTOR3		pvList;					// 全てのアニメーションのベクトルリスト

		explicit _ANIMEHEADER_()
			: nAnimation()
			, nQuat()
			, nScale()
			, nSlide()
			, nMat()
			, nTimeList()
			, nQuatList()
			, nVecList()
			, nMatList()
			, nThisAnime()
			, fThisTime()
			, pAnimeSetList()
			, pAnimeList()
			, pMatAnime()
			, pnTimeList()
			, pQuatList()
			, pScaleList()
			, pSlideList()
			, pMatList()
			, pvQuatList()
			, pmatList()
			, pvList() {}
	} ANIMEHEADER, *LPANIMEHEADER;

	///////////////////////////////////////////////////////////////////////////////

	//////////////////////////////// スキンメッシュ ///////////////////////////////

	// - SKINWEIGHT - //
	typedef struct _SKINEDMESHBONE_
	{
		char_t			szBoneName[eSizeofName];	// このボーンの名前
		size_t			nVertex;				// このボーンが影響を与える頂点数
		size_t*			pnVertexList;			// このボーンが影響を与える頂点リスト
		float*			pfWeightList;			// このボーンが各頂点に与える影響のリスト
		MATRIX			matBone;				// メッシュ頂点をボーンのローカル座標系に変換する行列。
		MATRIX			matTrans;				// リンクするフレームの変換行列 * matBone
		LPFRAMEHEADER	pLinkFrame;				// リンクするフレーム

		explicit _SKINEDMESHBONE_()
			//: szBoneName()
			: nVertex()
			, pnVertexList()
			, pfWeightList()
			, matBone()
			, matTrans()
			, pLinkFrame()
		{ szBoneName[0] = '\0'; }
	} SKINEDMESHBONE, *LPSKINEDMESHBONE;


	// - SKINMESH - //
	typedef struct _SKINEDMESHNODE_
	{
		size_t				nVertexPos;				// このメッシュの頂点リストにおける開始位置
		size_t				nVertexNum;				// このメッシュの頂点数
		size_t				nMaxWeightPerVertex;	// 頂点に影響を与えるウェイトの最大数
		size_t				nMaxWeightPerFace;		// フェースの3頂点に影響を与えるウェイトの最大数
		size_t				nBones;					// このメッシュの頂点に影響を与えるボーンの数
		size_t				nBoneVertexAll;			// 全てのボーンにある頂点・重さのリストの合計
		bool*				pbWeight;				// ボーン影響スイッチリスト
		size_t*				pnVertexList;			// 全てのボーンの頂点インデックスリスト
		float*				pfWeightList;			// 全てのボーンの重さリスト
		LPSKINEDMESHBONE	pBoneList;				// ボーンリスト
		LPSKINEDMESHNODEVERTEX	pSkinVers;			// インデックス付頂点ブレンディング型頂点リスト

		explicit _SKINEDMESHNODE_()
			: nVertexPos()
			, nVertexNum()
			, nMaxWeightPerVertex()
			, nMaxWeightPerFace()
			, nBones()
			, nBoneVertexAll()
			, pbWeight()
			, pnVertexList()
			, pfWeightList()
			, pBoneList()
			, pSkinVers() {}
	} SKINEDMESHNODE, *LPSKINEDMESHNODE;


	// - RELATIONAL SKINMESH - //
	typedef struct _SKINEDMESHHEADER_
	{
		size_t				nBoneAll;				// モデルにある合計ボーン数
		size_t				nWeightAll;				// ウェイトリストの合計数
		size_t				nVertexAll;				// 頂点リストバッファの合計数
		bool*				pbWeight;				// スキンメッシュのボーン影響スイッチリスト
		size_t*				pnVertexList;			// スキンメッシュの頂点インデックスリスト
		float*				pfWeightList;			// スキンメッシュのボーンの重さリスト
	//	float*				pfTotalWeight;			// スキンメッシュの合計影響重量リスト
		LPSKINEDMESHNODEVERTEX	pSkinVers;			// インデックス付頂点ブレンディング型頂点リスト
		LPSKINEDMESHBONE	pBoneList;				// スキンメッシュのボーンリスト

		explicit _SKINEDMESHHEADER_()
			: nBoneAll()
			, nWeightAll()
			, nVertexAll()
			, pbWeight()
			, pnVertexList()
			, pfWeightList()
			, pSkinVers()
			, pBoneList() {}

		~_SKINEDMESHHEADER_()
		{
			if ( pbWeight )		delete[] pbWeight;
			if ( pnVertexList )	delete[] pnVertexList;
			if ( pfWeightList )	delete[] pfWeightList;
			if ( pSkinVers )	delete[] pSkinVers;
			if ( pBoneList )	delete[] pBoneList;
		}
	} SKINEDMESHHEADER, *LPSKINEDMESHHEADER;

	////////////////////////////////////////////////////////////////////////////////


	// - MODEL - //
	typedef struct _MODELHEADER_
	{
		char_t			szName[ eSizeofName ];	// モデル名
		char_t			szPass[ eSizeofName ];
		size_t			nVertex;				// 頂点数
		size_t			nIndex;					// インデックス数
		size_t			nSquare;				// 四角形
		size_t			nFrame;					// フレーム数
		size_t			nMesh;					// メッシュ数
		size_t			nMaterial;				// マテリアル数
		size_t			nAnimationSet;			// アニメーションセット数
		size_t			nTexture;				// テクスチャ数
		size_t			nEffect;
		size_t			nSkinedMesh;			// スキンメッシュ数
		VECTOR3			vRadius;

		explicit _MODELHEADER_()
			: nVertex()
			, nIndex()
			, nSquare()
			, nFrame()
			, nMesh()
			, nMaterial()
			, nAnimationSet()
			, nTexture()
			, nEffect()
			, nSkinedMesh()
			, vRadius(.0f, .0f, .0f)
		{
			*szName = NULL; szName[ns_types::eNameLength] = NULL;
			*szPass = NULL; szPass[ns_types::eNameLength] = NULL;
		}
		_MODELHEADER_( const _MODELHEADER_& rhs )
			: nVertex( rhs.nVertex )
			, nIndex( rhs.nIndex )
			, nSquare( rhs.nSquare )
			, nFrame( rhs.nFrame )
			, nMesh( rhs.nMesh )
			, nMaterial( rhs.nMaterial )
			, nAnimationSet( rhs.nAnimationSet )
			, nTexture( rhs.nTexture )
			, nEffect( rhs.nEffect )
			//, nSkinedMesh( rhs.nSkinedMesh ) { strcpy_t( szName, strlen_t(szName, eSizeofName), rhs.szName ); }
			, nSkinedMesh( rhs.nSkinedMesh ) { strcpy_t( szName, rhs.szName ); }
	} MODELHEADER, *LPMODELHEADER;


}	// namespace: ns_model
