//
//	Cd3d.cpp
//
//	Copyright (c) 2008 Naoki Shimoyamada, All Rights Reserved.
//

#include	"Cd3d.h"
#include	"CHwnd.h"
// including	ns_model::VecMulMat()
#include	"XFileFuncs.h"
// including	ns_model::CModelMgr::Instance().Renderer();
#include	"CModelMgr.h"
// including	nx_d3d::g_decl, g_vs, g_ps
#include	"nsshader.h"
#include	<zmouse.h>
// including	nsgl::CPlayerController::SetCamToword()
#include	"SceneBuilder.h"

namespace ns_d3d
{
	Cd3d::Cd3d()
		: m_texture_pass(STR(data\\\\))
		, m_handle_decl()
		, m_handle_vs()
		, m_handle_ps()
		, m_mtx_view()
		, m_mtx_proj()
		, m_cam_pos(VECTOR3(2.0f, .0f, -2.4f))
		, m_cam_trg(VECTOR3(-.015185f, 1.732f, -.007524f))
		, m_pitch(.33f) // 上下の揺れ
		, m_yaw(.0f/*-.46f*/) // 左右の揺れ
		, m_dist(20.32f) // distance
		, m_light_pos(VECTOR3(-16.0f, 8.0f, 4.0f))
		//, m_cam_pos(VECTOR3(2.0f, 1.6f, -2.4f))
		//, m_cam_trg(VECTOR3(.0f, .8f, .08f))
		//, m_pitch(.20f)
		//, m_yaw(PI/6.0f * 4.8f)
		//, m_dist(30.2f)
		//, m_light_pos(VECTOR3(-.0f, 8.0f, 32.0f))
		, m_mtx_world()
	{
		size_t	nWidth	= ns_win::CHwnd::Instance().GetWindowWidth();
		size_t	nHeight	= ns_win::CHwnd::Instance().GetWindowHeight();
		float	fovY	= D3DX_PI/4.0f;
		float	aspect	= static_cast<float>(nWidth/nHeight);
		float	z_far	= 256.0f;
		float	z_near	= .10f;
		float	h		= 1.0f/tanf(fovY * .5f);
		float	w		= h/aspect;

		// Projection Matrix
		m_mtx_proj._11 =   w;	m_mtx_proj._12 = .0f;	m_mtx_proj._13 = .0f;								m_mtx_proj._14 =  .0f;
		m_mtx_proj._21 = .0f;	m_mtx_proj._22 =   h;	m_mtx_proj._23 = .0f;								m_mtx_proj._24 =  .0f;
		m_mtx_proj._31 = .0f;	m_mtx_proj._32 = .0f;	m_mtx_proj._33 = z_far/(z_far - z_near);			m_mtx_proj._34 = 1.0f;
		m_mtx_proj._41 = .0f;	m_mtx_proj._42 = .0f;	m_mtx_proj._43 = -z_near * z_far/(z_far - z_near);	m_mtx_proj._44 =  .0f;

		D3DXMatrixIdentity(&m_mtx_world);
	}

	bool Cd3d::CreateDevice(const HWND hWnd)
	{
		D3DPRESENT_PARAMETERS	d3dParam;
		D3DCAPS9				d3dCaps;
		m_pd3d	= Direct3DCreate9( D3D_SDK_VERSION );
		if ( !m_pd3d )
			return false;

		memset( &d3dParam, 0, sizeof(D3DPRESENT_PARAMETERS) );
		if ( WINDOW_MODE )
		{
			D3DDISPLAYMODE disp;
			m_pd3d->GetAdapterDisplayMode( D3DADAPTER_DEFAULT, &disp );
			d3dParam.BackBufferFormat			= disp.Format;
			d3dParam.FullScreen_RefreshRateInHz	= 0;
			//d3dParam.PresentationInterval		= D3DPRESENT_INTERVAL_IMMEDIATE;	// Don't wait VSYNC
	//		d3dParam.PresentationInterval		= D3DPRESENT_INTERVAL_DEFAULT;		// wait VSYNC
		}
		else
		{
			d3dParam.BackBufferFormat			= D3DFMT_X8R8G8B8;
			d3dParam.FullScreen_RefreshRateInHz	= D3DPRESENT_RATE_DEFAULT;
			//d3dParam.PresentationInterval		= D3DPRESENT_INTERVAL_IMMEDIATE;	// Don't wait VSYNC
	//		d3dParam.PresentationInterval		= D3DPRESENT_INTERVAL_DEFAULT;		// wait VSYNC
		}

		// 以下共通設定
		d3dParam.Windowed				= WINDOW_MODE;
		d3dParam.BackBufferWidth		= static_cast<UINT>(ns_win::CHwnd::Instance().GetWindowWidth());
		d3dParam.BackBufferHeight		= static_cast<UINT>(ns_win::CHwnd::Instance().GetWindowHeight());
		d3dParam.SwapEffect				= D3DSWAPEFFECT_DISCARD;
		d3dParam.BackBufferCount		= 1;
		d3dParam.EnableAutoDepthStencil	= true;
		d3dParam.AutoDepthStencilFormat	= D3DFMT_D24S8;
		d3dParam.MultiSampleType		= D3DMULTISAMPLE_NONE;
		d3dParam.hDeviceWindow			= hWnd;
		d3dParam.Flags					= D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;

		m_pd3d->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &d3dCaps);

		// T&L HAL 
		if (FAILED(m_pd3d->CreateDevice(D3DADAPTER_DEFAULT,D3DDEVTYPE_HAL, d3dParam.hDeviceWindow, D3DCREATE_MIXED_VERTEXPROCESSING, &d3dParam, &m_pd3dDevice)))
	//	if (FAILED(m_pd3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, d3dParam.hDeviceWindow, D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_PUREDEVICE, &d3dParam, &m_pd3dDevice)))
		{
			// HAL
			if (FAILED(m_pd3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, d3dParam.hDeviceWindow, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dParam, &m_pd3dDevice)))
			{
				// REF
				if (FAILED(m_pd3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, d3dParam.hDeviceWindow, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dParam, &m_pd3dDevice)))
				{
					// 完全に失敗
					return false;
				}
			}
		}

		m_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );	// カリング
		m_pd3dDevice->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );		// Zバッファ
		m_pd3dDevice->SetRenderState( D3DRS_ZFUNC, D3DCMP_LESSEQUAL );	// //

		// VertexDeclaration
		m_pd3dDevice->CreateVertexDeclaration( g_decl, &m_handle_decl );

		// VertexShader
		LPD3DXBUFFER buffer = NULL, error = NULL;
		// ※static_cast<UINT>, strnlenを使うとエラーが出る。
		HRESULT hr	= D3DXAssembleShader( g_vs, (UINT)strlen(g_vs), NULL, NULL, D3DXSHADER_DEBUG, &buffer, &error );
		if (SUCCEEDED(hr))
		{
			m_pd3dDevice->CreateVertexShader( (LPDWORD)buffer->GetBufferPointer(), &m_handle_vs );
			buffer->Release();
		}
		//NS_ASSERT(D3DERR_INVALIDCALL == hr, STR(Invalid called));
		//NS_ASSERT(D3DXERR_INVALIDDATA == hr, STR(Invalid data));
		//NS_ASSERT(E_OUTOFMEMORY == hr, STR(Out of Memory));

		// PixelShader
		hr = D3DXAssembleShader( g_ps, (UINT)strlen(g_ps), NULL, NULL, D3DXSHADER_DEBUG, &buffer, &error );
		if (SUCCEEDED(hr))
		{
			m_pd3dDevice->CreatePixelShader( (LPDWORD)buffer->GetBufferPointer(), &m_handle_ps );
			buffer->Release();
		}
		//NS_ASSERT(D3DERR_INVALIDCALL == hr, STR(Invalid called));
		//NS_ASSERT(D3DXERR_INVALIDDATA == hr, STR(Invalid data));
		//NS_ASSERT(E_OUTOFMEMORY == hr, STR(Out of Memory));

		// 定数レジスタ
		m_pd3dDevice->SetVertexShaderConstantF(8, reinterpret_cast<float*>(&D3DXVECTOR4(100.0f, -.5f, 1.0f, .0f)), 1);
		m_pd3dDevice->SetVertexShaderConstantF(9, reinterpret_cast<float*>(&D3DXVECTOR4(.5f, -.5f, 1.0f, .0f)), 1);
		m_pd3dDevice->SetVertexShaderConstantF(19, reinterpret_cast<float*>(&D3DXVECTOR4(1.0f, 0.0f, 3.0f, 765.01f)), 1);

		D3DXVECTOR4 diffuse_light_color(1.0f, 1.0f, 1.0f, 1.0f);
		m_pd3dDevice->SetVertexShaderConstantF(11, reinterpret_cast<float*>(&diffuse_light_color), 1);
		D3DXVECTOR4 diffuse_material_color(.494118f, .309804f, .203922f, 1.0f);
		m_pd3dDevice->SetVertexShaderConstantF(12, reinterpret_cast<float*>(&diffuse_material_color), 1);

		D3DXVECTOR4 specular_light_color(1.0f, 1.0f, 1.0f, 1.0f);
		m_pd3dDevice->SetVertexShaderConstantF(13, reinterpret_cast<float*>(&specular_light_color), 1);
		D3DXVECTOR4 specular_material_color(.7f, .7f, .7f, 6.4f);
		m_pd3dDevice->SetVertexShaderConstantF(14, reinterpret_cast<float*>(&specular_material_color), 1);

		D3DXVECTOR4 ambient_light_color(.2f, .2f, .2f, 1.0f);
		m_pd3dDevice->SetVertexShaderConstantF(15, reinterpret_cast<float*>(&ambient_light_color), 1);
		D3DXVECTOR4 ambient_material_color(1.0f, 1.0f, 1.0f, 1.0f);
		m_pd3dDevice->SetVertexShaderConstantF(16, reinterpret_cast<float*>(&ambient_material_color), 1);

		m_pd3dDevice->SetPixelShaderConstantF(0, reinterpret_cast<float*>(&D3DXVECTOR4(.0f, .0f, .0f, .0f)), 1);
		m_pd3dDevice->SetPixelShaderConstantF(1, reinterpret_cast<float*>(&D3DXVECTOR4(.30f, .30f, .30f, .30f)), 1);
		m_pd3dDevice->SetPixelShaderConstantF(3, reinterpret_cast<float*>(&D3DXVECTOR4(.0f, .0f, .0f, 1.0f)), 1);

		// Hook
		ns_win::CHwnd::_Inst().AddWndHook(&Cd3d::CamMsgProc);

		// Effect
		return true;
	}


	void Cd3d::CreateTexture( const ns_types::char_t* name, LPDIRECT3DTEXTURE9* texture, const ns_types::char_t* pass )
	{
		//u_long length = GetCurrentDirectory(0, 0);
		//ns_types::char_t* pCurrentPass = new ns_types::char_t[length];
		//GetCurrentDirectory(length, pCurrentPass);
		ns_types::char_t szTexturePass[ns_types::eSizeofLine];
		szTexturePass[ns_types::eLineLength] = ns_types::eNull;
		//length = ns_model::FileLoadPass(szTexturePass, pCurrentPass);
		ns_model::JoinFilePass(szTexturePass, pass, name);

		HRESULT hr = D3DXCreateTextureFromFile( m_pd3dDevice, szTexturePass, texture );
		if (FAILED(hr))
		//if ( FAILED( D3DXCreateTextureFromFileEx( m_pd3dDevice, name, D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, D3DFMT_UNKNOWN, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, &texture ) ))
			texture	= NULL;
	}


	HRESULT Cd3d::CreateEffect(const ns_types::char_t* path, const ns_types::char_t* name, LPD3DXEFFECT *effect)
	{
		ns_types::char_t file[ns_types::eSizeofLine];
		ns_model::JoinFilePass(file, path, name);
		LPD3DXBUFFER error = NULL;
		HRESULT hResult = D3DXCreateEffectFromFile(this->m_pd3dDevice, file,
								NULL, NULL, /*NULL*/D3DXSHADER_USE_LEGACY_D3DX9_31_DLL,
								NULL, effect, &error);
		if (error) {
			MessageBoxA(NULL, (LPCSTR)error->GetBufferPointer(), "ERROR", MB_OK);
		}
		return hResult;
	}


	HRESULT Cd3d::BeginEffect(ns_model::LPEFFECTINSTANCE fxCont, LPMATRIX world)
	{
		LPD3DXEFFECT fx = fxCont->d3dEffect;
		fx->SetMatrix(fxCont->hWorld, world);
		fx->SetMatrix(fxCont->hView, &m_mtx_view);
		fx->SetMatrix(fxCont->hWorld, &m_mtx_proj);
		u_int pass;
		return fx->Begin(&pass, 0);
	}


	void Cd3d::SetWorld( const D3DXMATRIX* mtx ) const
	{
		D3DXMATRIX mtx_model;
		mtx_model	= m_mtx_world;
		mtx_model	*= *mtx;
		mtx_model	*= m_mtx_view;
		mtx_model	*= m_mtx_proj;
		D3DXMatrixTranspose(&mtx_model, &mtx_model);
		m_pd3dDevice->SetVertexShaderConstantF(0, reinterpret_cast<float*>(&mtx_model), 4);
	}


	void Cd3d::MatrixWorldToViewPort(LPMATRIX mtx)
	{
		*mtx *= m_mtx_view;
		*mtx *= m_mtx_proj;
	}


	void Cd3d::SetShaderHandles()
	{
		// Set Handles
		m_pd3dDevice->SetVertexShader(m_handle_vs);
		m_pd3dDevice->SetPixelShader(m_handle_ps);
		m_pd3dDevice->SetVertexDeclaration(m_handle_decl);
	}


	void Cd3d::ExecuteTest()
	{
		m_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x000000ff, 1.0f, 0);
		if (SUCCEEDED(m_pd3dDevice->BeginScene()))
		{
			ModelMgr::Instance().RLZTest();
			//ns_model::CModelMgr::_Inst().RLZTest();
		}

		m_pd3dDevice->SetIndices(NULL);
		m_pd3dDevice->EndScene();
		m_pd3dDevice->Present(NULL, NULL, NULL, NULL);
	}


	void Cd3d::ViewMatrix()
	{
		//////////////////////////////////////////
		//	View Maxrix の作成
		float	sy = sinf(m_yaw);
		float	cy = cosf(m_yaw);
		float	sp = sinf(m_pitch);
		float	cp = cosf(m_pitch);

		m_cam_pos.x	= m_dist * cp * sy + m_cam_trg.x;
		m_cam_pos.y	= m_dist * sp + m_cam_trg.y;
		m_cam_pos.z	= m_dist * cp * cy + m_cam_trg.z;

		VECTOR3	axis_x, axis_y, cam_dir;
		cam_dir	= m_cam_trg;
		cam_dir	-= m_cam_pos;
		D3DXVec3Normalize(&cam_dir, &cam_dir);
		D3DXVec3Cross(&axis_x,
			D3DX_PI/2 < m_pitch && m_pitch < D3DX_PI + (D3DX_PI/2.0f) ? &D3DXVECTOR3(.0f, -1.0f, .0f) : &D3DXVECTOR3(.0f, 1.0f, .0f),
			&cam_dir);
		D3DXVec3Normalize(&axis_x, &axis_x);
		D3DXVec3Cross(&axis_y, &cam_dir, &axis_x);

		m_mtx_view._11 = axis_x.x;	m_mtx_view._12 = axis_y.x;	m_mtx_view._13 = cam_dir.x;		m_mtx_view._14 = .0f;
		m_mtx_view._21 = axis_x.y;	m_mtx_view._22 = axis_y.y;	m_mtx_view._23 = cam_dir.y;		m_mtx_view._24 = .0f;
		m_mtx_view._31 = axis_x.z;	m_mtx_view._32 = axis_y.z;	m_mtx_view._33 = cam_dir.z;		m_mtx_view._34 = .0f;
		m_mtx_view._41 = -D3DXVec3Dot(&axis_x, &m_cam_pos);
		m_mtx_view._42 = -D3DXVec3Dot(&axis_y, &m_cam_pos);
		m_mtx_view._43 = -D3DXVec3Dot(&cam_dir, &m_cam_pos);
		m_mtx_view._44 = 1.0f;
	}


	void Cd3d::RefreshScene()
	{
		ViewMatrix();

		/////////////////////////
		//	レジスタに転送
		//	c[9]	: 定数
		//	c[10]	: ライト方向ベクトル
		//	c[11]	: ライトのディヒューズ色
		//	c[12]	: マテリアルのディヒューズ色
		//	c[13]	: ライトのスペキュラー色
		//	c[14]	: マテリアルのスペキュラー色
		//	c[15]	: ライトのアンビエント色
		//	c[16]	: マテリアルのアンビエント色
		//	c[17]	: 視点座標
		//	c[18]	: ライト座標
		m_pd3dDevice->Clear(0, NULL, D3DCLEAR_STENCIL | D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x000000ff, 1.0f, 0);
		if (SUCCEEDED(m_pd3dDevice->BeginScene()))
		{
			MATRIX	mtx_inv;
			VECTOR3	light_pos, camera_pos;
			//D3DXMatrixInverse(&mtx_inv, NULL, &m_mtx_world);	// 逆行列

			VECTOR3	light_dir;
			mtx_inv		= m_mtx_world;
			mtx_inv._41	= .0f;	mtx_inv._42 = .0f; mtx_inv._43 = .0f;
			D3DXMatrixInverse(&mtx_inv, NULL, &mtx_inv);

			ns_model::VecxMat(&light_dir, &m_light_pos, &mtx_inv);
			D3DXVec3Normalize(&light_dir, &light_dir);
			m_pd3dDevice->SetVertexShaderConstantF(10, reinterpret_cast<float*>(&light_dir), 1);

			mtx_inv	= m_mtx_world;
			D3DXMatrixInverse(&mtx_inv, NULL, &mtx_inv);
			ns_model::VecxMat(&camera_pos, &m_cam_pos, &mtx_inv);
			m_pd3dDevice->SetVertexShaderConstantF(17, reinterpret_cast<float*>(&camera_pos), 1);

			ns_model::VecxMat(&light_pos, &m_light_pos, &mtx_inv);
			m_pd3dDevice->SetVertexShaderConstantF(18, reinterpret_cast<float*>(&light_pos), 1);

			// オブジェクトの転送
			ModelMgr::Instance().Render();
			WorldOrg::Instance().Render();
			//ModelMgr::Instance().Execute();
			//ns_model::CModelMgr::_Inst().RLZ();

			// 描画終了
			//m_pd3dDevice->SetIndices(NULL);
			m_pd3dDevice->EndScene();
			m_pd3dDevice->Present(NULL, NULL, NULL, NULL);
		}
		else {
			Sleep(100);
		}
	}

	LRESULT WINAPI Cd3d::CamMsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		static int multi = 0;
		static int x = 0;
		static int y = 0;
		static float PI2 = PI * 2;

		switch (msg) {
			case WM_MOUSEMOVE:
				if (multi) {
					POINT pt;
					GetCursorPos(&pt);
					ScreenToClient(hWnd, &pt);

					switch (multi) {
						case WM_RBUTTONDOWN:
							Cd3d::Instance().m_yaw -= static_cast<float>(x - pt.x) * .01f;
							nsgl::CPlayerController::SetCamToword(Cd3d::Instance().m_yaw);
							x = pt.x;

							float *pitch = &Cd3d::Instance().m_pitch;
							float range = *pitch -= static_cast<float>(y - pt.y) * .01f;
							if (range>PI2)
								*pitch = range - PI2 * (int)(range / PI2);
							else if (range<.0f)
								*pitch = range + PI2 * ((int)(range / PI2) + 1);
							y = pt.y;
							break;
					}
				}
				break;

			case WM_RBUTTONDOWN:
				SetCapture(hWnd);
				POINT pt;
				GetCursorPos(&pt);
				ScreenToClient(hWnd, &pt);
				x = pt.x;
				y = pt.y;
				multi = msg;
				SetCursor(LoadCursor(NULL, IDC_SIZEALL));
				break;

			case WM_RBUTTONUP:
				//if (multi) {
					ReleaseCapture();
					multi = 0;
					SetCursor(LoadCursor(NULL, IDC_ARROW));
				//}
				break;

			case WM_MOUSEWHEEL:
				short delta = (short)HIWORD(wParam);
				float resize = (float)delta / 24.0f + Cd3d::_Inst().m_dist;
				if (resize < 1.0f ) Cd3d::_Inst().m_dist = 1.0f;
				else if (resize > 255.0f) Cd3d::_Inst().m_dist = 255.0f;
				else Cd3d::_Inst().m_dist = resize;
				break;
		}

		return 0;
	}

}	// ns_d3d