


#include "SceneBuilder.h"
#include "GameEngine.h"

namespace nsgl {

	size_t CPlayerController::_my_id = 0;
	LPMATRIX CPlayerController::_my_mtx = NULL;
	size_t CPlayerController::_is_inputting = 0;
	VECTOR3 CPlayerController::_input_vec(.0f, .0f, .0f);
	VECTOR3 CPlayerController::_start_vec(.0f, .0f, .0f);
	float CPlayerController::_camspeed = .05f;
	size_t CPlayerController::_jumping = 0;

	LRESULT WINAPI CPlayerController::ControlProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch(msg) {
			case WM_KEYDOWN:
				switch(wParam) {
					case 'A':
						++ _is_inputting;
						_input_vec.x = _camspeed;
						//nsgl::CGameEngine::_Inst().SetExecute(&SlideToLeft);
						break;
					case 'D':
						++ _is_inputting;
						_input_vec.x = -_camspeed;
						//nsgl::CGameEngine::_Inst().SetExecute(&SlideToRight);
						break;
					case 'W':
						++ _is_inputting;
						_input_vec.z = -_camspeed;
						//nsgl::CGameEngine::_Inst().SetExecute(&Forward);
						break;
					case 'S':
						++ _is_inputting;
						_input_vec.z = _camspeed;
						//nsgl::CGameEngine::_Inst().SetExecute(&Back);
						break;
					case VK_SPACE:
						if (_jumping == 1) {
							break;
						}
						_jumping = 1;
						WorldOrg::_Inst().Jumping();
						break;

					//case VK_LEFT:
					//	ns_d3d::Cd3d::_Inst().m_cam_trg.x -= _camspeed;
					//	break;
					//case VK_RIGHT:
					//	ns_d3d::Cd3d::_Inst().m_cam_trg.x += _camspeed;
					//	break;
				}
				if (_is_inputting > 0) {
					nsgl::CGameEngine::_Inst().SetExecute(&MoveWithCam);
				}
				break;
			case WM_KEYUP:
				switch(wParam) {
					case 'A':
						-- _is_inputting;
						if (_input_vec.x == _camspeed) {
							_input_vec.x = .0f;
						}
						break;
					case 'D':
						-- _is_inputting;
						if (_input_vec.x == -_camspeed) {
							_input_vec.x = .0f;
						}
						break;
					case 'W':
						-- _is_inputting;
						if (_input_vec.z == -_camspeed) {
							_input_vec.z = .0f;
						}
						break;
					case 'S':
						-- _is_inputting;
						if (_input_vec.z == _camspeed) {
							_input_vec.z = .0f;
						}
						break;
				}
				if (_is_inputting == 0) {
					nsgl::CGameEngine::_Inst().EmptyExecute();
				}
				break;
		}

		return MB_OK;
	}
}