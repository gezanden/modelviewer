//
//	nswin.hpp
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once

//--------------------------------------------------------
//	Liblaries
//--------------------------------------------------------
#pragma comment(lib, "winmm.lib")


//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include	<windows.h>
#include	<new>
#include	"nstypes.h"


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_win
{
	using namespace ns_types;
	//const char_t* const	MAIN_CLASS_NAME = STR(main window);

	//--------------------------------------------------------
	//	Functions
	//--------------------------------------------------------
	//bool CreateWndClassEx( HINSTANCE, WNDPROC, const char_t*, dword_t );
	bool StrCmp( const char_t*, const char_t* );
	//LRESULT WINAPI MsgProcMain( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );

}	// namespace: ns_win
