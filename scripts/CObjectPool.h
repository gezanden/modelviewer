
#pragma once
#ifndef _NS_C_OBJECTPOOL_H_
#define _NS_C_OBJECTPOOL_H_

#include "NSUtility.h"
#include "NSVector.h"

namespace nsl {
	template
	<
		typename IdentifierProduct,	// オブジェクト識別タイプ
		typename AbstractProduct,	// オブジェクトタイプ
		typename Executor = void* (*)(IdentifierProduct*, AbstractProduct*)
		//template <typename, class>
		//	class ErrorPolicy = ThrowFactoryError
	>
	class CObjectPool
	{
		//ns_lib::CFunctor<AbstractProduct*, char*> BacicalyCreator;
		typedef nsl::NSVector<IdentifierProduct> IdVector;
		typedef nsl::pair<IdVector, AbstractProduct> ValueVector;
		typedef nsl::NSVector<ValueVector> Associations;
	public:
		typedef typename Associations::iterator iterator_assoc;
		CObjectPool() : _assoc(), _file(_assoc) {
			//_file = _assoc;
		}
		size_t RegisterFile(IdentifierProduct& id, AbstractProduct& product) {
			IdVector* vid = new IdVector;
			vid->push_back(id);
			ValueVector* value = new ValueVector(*vid, product);
			size_t n = _assoc.size();
			_assoc.push_back(*value);
			//(*_assoc.front())->first->begin();
			//_file->first->begin();
			return n;
		}
		size_t RegisterFile(NSVector<IdentifierProduct>& id, AbstractProduct& product);
		size_t UnRegister(const IdentifierProduct id);
		size_t RegisterObj(size_t position, IdentifierProduct& id) {
			(*(_file + position)).first.push_back(id);
		}
		void SetExecutor(Executor func) { _func = func; }
		void Execute()
		{
			iterator_assoc &begin = _assoc.begin();
			iterator_assoc &end = _assoc.end();
			for (; begin != end; ++begin) {
				_func(*(begin->first->front()), begin->second);
			}
		}
		void TestExecute()
		{
			_func(*(_file->first->front()), _file->second);
			//(*_file)->
		}
		iterator_assoc GetIterator() const {
			return _assoc.begin();
		}
		IdentifierProduct* GetId() const {
			return _file->first->front();
		}
		AbstractProduct* GetObj() const {
			return _file->second;
		}

	private:
		Associations _assoc;
		iterator_assoc _file;
		Executor _func;
	};
}

#endif // _NS_C_OBJECTPOOL_H_