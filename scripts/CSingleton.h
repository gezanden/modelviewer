//
//	CSingleton.hpp
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once
#ifndef	_CSINGLETON_H_
#define	_CSINGLETON_H_

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include	<new>


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_singleton
{
	//--------------------------------------------------------
	//	Template class
	//--------------------------------------------------------

	//////////////////////////////////////////////////////////
	//	name:	CSingleton
	//
	//	member:	m_pSingleton	- シングルトンオブジェクトのポインタ
	//
	//	desc:	シングルトンパターンテンプレート
	//				・このクラスを継承して作ったクラスから GetSingleton/GetSingletonPtr
	//				　を呼び出すことで、シングルトンパターンを実装できる。
	//				　このテンプレートを使用するとき、派生クラスのコンストラクタは public
	//				　で実装しなければならない。
	//
	//	Reference documents:
	//			・著: Erich Gamma/Richard Helm/Ralph Johnson/John Vlissides 訳: 本位田 真一/吉田 和樹 (1999/10)
	//				『オブジェクト指向における再利用のためのデザインパターン 改訂版』 P137
	//			・編者: Mark DeLoura 監訳: 川西 裕幸 翻訳 : 狩野 智英 (2001/06)
	//				『Game Programming Gems』P36
	//
	template <typename T> class CSingleton
	{
	public:
		static T& _Inst()
		{
			return *m_pSingleton;
		}
		static T& Instance()	// 一回目の呼び出しでm_tSingletonに割り当てる
		{
			return m_pSingleton ? *m_pSingleton : *( m_pSingleton = CreateSingleton() );
		}

	protected:
		CSingleton()
//			: m_pSingleton()
		{
			//int offset = (int)(T*)1 - (int)(CSingleton <T>*)(T*)1;
			//m_tSingleton = (T*)((int)this + offset);
		}
		~CSingleton()
		{
			//m_pSingleton = NULL;
		}
		//static T& GetSingleton()	// 一回目の呼び出しでm_tSingletonに割り当てる
		//{
		//	return m_pSingleton ? *m_pSingleton : *( m_pSingleton = CreateSingleton() );
		//}
		//static T* GetSingletonPtr()
		//{
		//	return m_pSingleton ? m_pSingleton : CreateSingleton();
		//}
		static void ReleaseSingleton()
		{
			delete m_pSingleton;
			m_pSingleton = NULL;
		}

	private:
		static T* CreateSingleton()
		{
			m_pSingleton = new typename T;
			atexit(ReleaseSingleton);
			return	(m_pSingleton = new typename T);
		}

		static T* m_pSingleton;
	};

	template <typename T> T* CSingleton<T>::m_pSingleton	= 0;
}

#endif	// _CSINGLETON_H_