//
//	CModelInit.h
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once
#ifndef	_CMODELINIT_H_
#define	_CMODELINIT_H_

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include	"CModel.h"
#include	"CFStream.h"


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_model
{

	//--------------------------------------------------------
	//	Class
	//--------------------------------------------------------

	class CModelInit
	{
	public:
		explicit CModelInit() {}
		virtual ~CModelInit() {}
		virtual bool Create( CModel*, char_t*, size_t );
		virtual void Release() { delete this; }
		void Scaling( CModel* );
		virtual ns_types::err_t CreateFromStream(CModel*, nslib::CIFStream&, char*);

	private:
		CModelInit( const CModelInit& );
		template <typename T, typename U> T& operator =( const U& );
		bool Allocate( CModel* );
		bool Set( CModel*, char_t*, size_t );
		ns_types::err_t SetFromStream(CModel*, nslib::CIFStream&, char*);

//		friend CModel;
	};

	class CSkinedMeshModelInit : public CModelInit
	{
	public:
		explicit CSkinedMeshModelInit() {}
		virtual ~CSkinedMeshModelInit() {}
		virtual bool Create( CSkinedMeshModel*, char_t*, size_t );
		virtual void Release() { delete this; }
		virtual ns_types::err_t CreateFromStream(CSkinedMeshModel*, nslib::CIFStream&, char*);

	private:
		CSkinedMeshModelInit( const CSkinedMeshModelInit& );
		template <typename T, typename U> T& operator =( const U& );
		bool Allocate( CSkinedMeshModel* );
		bool Set( CSkinedMeshModel*, char_t*, size_t );
		ns_types::err_t SetFromStream(CSkinedMeshModel*, nslib::CIFStream&, char*);

//		friend CSkinedMeshModel;
	};

}

#endif	// _CMODELINIT_H_