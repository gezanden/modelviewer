//
//	CModel.cpp
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//


//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include	"CModel.h"
#include	"Cd3d.h"
#include	"XFileFuncs.h"
#include	"NSAssert.h"
//#include	<math.h>



//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_model
{


//--------------------------------------------------------
//	Name:	CSkinedMeshModel::CModel
//	Desc:	CSkinedMeshModel Constracta
//--------------------------------------------------------
	CSkinedMeshModel::CSkinedMeshModel()
		: m_nSkinedMesh()	// CModelにも同じものがある。省略可能？
		, m_pSkinList()
		, m_pSkinHead()
//		, m_pVertices()
//		, m_pwIndices()
//		, m_pVB()
//		, m_pIB()
	{}


//--------------------------------------------------------
//	Name:	CSkinedMeshModel::CModel
//	Desc:	CSkinedMeshModel Constracta 2
//--------------------------------------------------------
	CSkinedMeshModel::CSkinedMeshModel( LPMODELHEADER pModelHead, LPANIMEHEADER pAnimeHead, LPSKINEDMESHHEADER pSkinHead )
		: CModel( pModelHead, pAnimeHead )
		, m_nSkinedMesh( pModelHead->nSkinedMesh )
		, m_pSkinList()
		, m_pSkinHead( pSkinHead )
	{}


//--------------------------------------------------------
//	Name:	CSkinedMeshModel::CModel
//	Desc:	CSkinedMeshModel Constracta 3 ( Texture )
//--------------------------------------------------------
	CSkinedMeshModel::CSkinedMeshModel( LPMODELHEADER pModelHead, LPANIMEHEADER pAnimeHead, LPSKINEDMESHHEADER pSkinHead, LPTEXTUREHEADER pTexHead )
		: CModel( pModelHead, pAnimeHead, pTexHead )
		, m_nSkinedMesh( pModelHead->nSkinedMesh )
		, m_pSkinList()
		, m_pSkinHead( pSkinHead )
	{}



	CSkinedMeshModel::CSkinedMeshModel( LPMODELHEADER pModelHead,
		LPANIMEHEADER pAnimeHead, LPSKINEDMESHHEADER pSkinHead,
		LPTEXTUREHEADER pTexHead, LPEFFECTHEADER pEffect )
		: CModel( pModelHead, pAnimeHead, pTexHead, pEffect )
		, m_nSkinedMesh( pModelHead->nSkinedMesh )
		, m_pSkinList()
		, m_pSkinHead( pSkinHead )
	{}



//--------------------------------------------------------
//	Name:	CSkinedMeshModel::~CSkinedMeshModel
//	Desc:	CSkinedMeshModel Destracta
//--------------------------------------------------------
	CSkinedMeshModel::~CSkinedMeshModel()
	{
		if ( m_pSkinHead )	delete	 m_pSkinHead;
		if ( m_pSkinList )	delete[] m_pSkinList;
	};


	void CModel::RenderTest( const LPMATRIX matrix )
	{
		LPDIRECT3DDEVICE9	device	= ns_d3d::Cd3d::Instance().Getd3dDevice();

		// Light
		float		ambient	= .3f,
					diffuse	= .7f;
		VECTOR3 vec	= VECTOR3(cosf(timeGetTime()/350.0f), cosf(timeGetTime()/350.0f), sinf(timeGetTime()/350.0f));
		D3DLIGHT9	light;
		memset(&light, '\0', sizeof(D3DLIGHT9));
		light.Type		= D3DLIGHT_DIRECTIONAL;
		light.Ambient.r	= ambient;
		light.Ambient.g	= ambient;
		light.Ambient.b	= ambient;
		light.Diffuse.r	= diffuse;
		light.Diffuse.g	= diffuse;
		light.Diffuse.b	= diffuse;
		D3DXVec3Normalize(reinterpret_cast<VECTOR3*>(&light.Direction), &vec);
		light.Range		= 1000.0f;
		device->SetLight(0, &light);
		device->LightEnable(0, TRUE);
		device->SetRenderState(D3DRS_LIGHTING, TRUE);
		device->SetRenderState(D3DRS_AMBIENT, 0x00000000);

		// View
		MATRIX	mtx;
		D3DXMatrixLookAtLH(&mtx, &VECTOR3(.0f, 5.0f, -7.0f), &VECTOR3(.0f, 2.0f, .0f), &VECTOR3(.0f, 1.0f, .0f));
		device->SetTransform(D3DTS_VIEW, &mtx);

		// Projection
		D3DXMatrixPerspectiveFovLH(&mtx, ns_d3d::PI/4.0f, 1.0f, 1.0f, 1000.0f);
		device->SetTransform(D3DTS_PROJECTION, &mtx);

		// Vertex buffer contents
		device->SetStreamSource(0, m_pVB, 0, sizeof(CUSTOMVERTEX));
		device->SetIndices(m_pIB);
		device->SetFVF(D3DFVF_CUSTOMVERTEX);

		// Mesh
		size_t	nMesh	= m_pModelHead->nMesh;
		UINT	nVertex	= static_cast<UINT>(m_pModelHead->nVertex), nPolygons, nOffsetIndex=0;
		LPMESHHEADER	pMeshPos	= m_pMeshList;
		MATRIX	mtxTrans, mtxWorld;
		ns_d3d::MatrixMultiply(&mtxWorld, matrix, &m_matScale);

		if(m_pModelHead->nAnimationSet)
			this->Animation(0);
		for (size_t i=0; i<nMesh; ++i, ++pMeshPos)
		{
			if (!pMeshPos->dwMtrlIndex)
			{
				// Material
				TransMatrixCompleteTest(&mtxTrans, pMeshPos->pPrev);
				ns_d3d::MatrixMultiply(&mtx, &mtxTrans, &mtxWorld);
				device->SetTransform(D3DTS_WORLD, &mtx);
				device->SetMaterial(pMeshPos->pMtrl);
				pMeshPos->pTexture ? device->SetTexture(0, pMeshPos->pTexture->pTexture) : device->SetTexture(0, NULL);

				// Draw
				nPolygons	= static_cast<UINT>(pMeshPos->nPolygonNum);
				device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, nVertex, nOffsetIndex, nPolygons);
				nOffsetIndex += nPolygons*3;
			}
			else
			{
				size_t		nOffsetPolygons=0, nMtrl = pMeshPos->nMtrlNum-1;
				LPMATERIAL	pMtrlPos = pMeshPos->pMtrl;
				for (size_t j=0; j<nMtrl; ++j, ++pMtrlPos)
				{
					// Material
					TransMatrixCompleteTest(&mtxTrans, pMeshPos->pPrev);
					ns_d3d::MatrixMultiply(&mtx, &mtxTrans, &mtxWorld);
					device->SetTransform(D3DTS_WORLD, &mtx);
					device->SetMaterial(pMtrlPos);
					pMeshPos->pTexture ? device->SetTexture(0, pMeshPos->pTexture->pTexture) : device->SetTexture(0, NULL);

					// Draw
					nPolygons = static_cast<UINT>((pMeshPos->dwMtrlIndex >> (j*8)) & 0x000000ff);
					device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, nVertex, nOffsetIndex, nPolygons);
					nOffsetIndex	+= nPolygons*3;
					nOffsetPolygons	+= nPolygons;
				}
				// Material
				TransMatrixCompleteTest(&mtxTrans, pMeshPos->pPrev);
				ns_d3d::MatrixMultiply(&mtx, &mtxTrans, &mtxWorld);
				device->SetTransform(D3DTS_WORLD, &mtx);
				device->SetMaterial(pMtrlPos);
				pMeshPos->pTexture ? device->SetTexture(0, pMeshPos->pTexture->pTexture) : device->SetTexture(0, NULL);

				// Draw
				nPolygons = static_cast<UINT>(pMeshPos->nPolygonNum);
				device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, nVertex, nOffsetIndex, nPolygons-nOffsetPolygons);
				nOffsetIndex += (nPolygons-nOffsetPolygons)*3;
			}
		}
	}


//--------------------------------------------------------
//	Name:	CModel::Render
//	Desc:	Render the model
//--------------------------------------------------------
	void CModel::Render( const LPMATRIX matrix )
	{
		MATRIX mtx, mtxTrans;
		ns_d3d::Cd3d::Instance().SetVertexBuffer( m_pVB );
		ns_d3d::Cd3d::Instance().SetIndexBuffer( m_pIB );
		ns_d3d::Cd3d::Instance().SetShaderHandles();

		size_t	num_mesh = m_pModelHead->nMesh;
		UINT	num_vertex = static_cast<UINT>(m_pModelHead->nVertex), num_polygons, offset_index = 0;
		ns_model::LPMESHHEADER	mesh_pos	= m_pMeshList;
		LPDIRECT3DDEVICE9		device		= ns_d3d::Cd3d::Instance().Getd3dDevice();

		//if(m_pModelHead->nAnimationSet)
		//	this->Animation(0);
		for (size_t iMesh=0; iMesh<num_mesh; ++iMesh, ++mesh_pos)
		{
			D3DXMatrixMultiply(&mtx, matrix, &m_matScale);
			if (mesh_pos->pPrev) {
				TransMatrixComplete(&mtxTrans, mesh_pos->pPrev);	// added Test: not Animation
				D3DXMatrixMultiply(&mtx, &mtxTrans, &mtx);
			}

			LPEFFECTINSTANCE fxCont = mesh_pos->pEffect;
			HRESULT hEffect = E_FAIL;
			if (fxCont)
			{
				hEffect = ns_d3d::Cd3d::_Inst().BeginEffect(fxCont, &mtx);
				if (SUCCEEDED(hEffect)) {
					fxCont->d3dEffect->BeginPass(0);
				}
			}

			ns_d3d::Cd3d::Instance().SetWorld(&mtx);
			if (!mesh_pos->dwMtrlIndex)
			{
				num_polygons = static_cast<UINT>(mesh_pos->nPolygonNum);
				device->SetVertexShaderConstantF(12, reinterpret_cast<float*>(&mesh_pos->pMtrl->Diffuse), 1);
				device->SetVertexShaderConstantF(14, reinterpret_cast<float*>(&mesh_pos->pMtrl->Specular), 1);
				device->SetVertexShaderConstantF(16, reinterpret_cast<float*>(&mesh_pos->pMtrl->Ambient), 1);
				mesh_pos->pTexture ? device->SetTexture(0, mesh_pos->pTexture->pTexture) : device->SetTexture(0, NULL);
				device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_vertex, offset_index, num_polygons);
				offset_index += num_polygons * 3;
			}
			else
			{
				size_t		num_mtrl = mesh_pos->nMtrlNum-1, offset_polygons = 0;
				LPMATERIAL	mtrl_pos = mesh_pos->pMtrl;
				for (size_t jMtrl=0; jMtrl<num_mtrl; ++jMtrl, ++mtrl_pos)
				{
					num_polygons	= static_cast<UINT>((mesh_pos->dwMtrlIndex >> (jMtrl*8)) & 0x000000ff);
					device->SetVertexShaderConstantF(12, reinterpret_cast<float*>(&mtrl_pos->Diffuse), 1);
					device->SetVertexShaderConstantF(14, reinterpret_cast<float*>(&mtrl_pos->Specular), 1);
					device->SetVertexShaderConstantF(16, reinterpret_cast<float*>(&mtrl_pos->Ambient), 1);
					device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_vertex, offset_index, num_polygons);
					offset_index	+= num_polygons * 3;
					offset_polygons	+= num_polygons;
				}
				num_polygons = static_cast<UINT>(mesh_pos->nPolygonNum);
				device->SetVertexShaderConstantF(12, reinterpret_cast<float*>(&mtrl_pos->Diffuse), 1);
				device->SetVertexShaderConstantF(14, reinterpret_cast<float*>(&mtrl_pos->Specular), 1);
				device->SetVertexShaderConstantF(16, reinterpret_cast<float*>(&mtrl_pos->Ambient), 1);
				device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_vertex, offset_index, num_polygons-offset_polygons);
				offset_index	+= (num_polygons - offset_polygons)*3;
			}
			
			if (SUCCEEDED(hEffect)) {
				LPD3DXEFFECT fx = fxCont->d3dEffect;
				fx->EndPass();
				fx->End();
			}
		}
	};


//--------------------------------------------------------
//	Name:	CSkinedMeshModel::Render
//	Desc:	Render the Skined mesh model
//--------------------------------------------------------
	void CSkinedMeshModel::Render( const LPMATRIX matrix )
	{
		MATRIX mtx, mtxTrans;
		ns_d3d::Cd3d::Instance().SetVertexBuffer( m_pVB );
		ns_d3d::Cd3d::Instance().SetIndexBuffer( m_pIB );
		ns_d3d::Cd3d::Instance().SetShaderHandles();

		size_t	num_mesh = m_pModelHead->nMesh;
		UINT	num_vertex = static_cast<UINT>(m_pModelHead->nVertex), num_polygons, offset_index = 0;
		ns_model::LPMESHHEADER	mesh_pos	= m_pMeshList;
		LPDIRECT3DDEVICE9		device		= ns_d3d::Cd3d::Instance().Getd3dDevice();

		if(m_pModelHead->nAnimationSet)
			this->Animation(0);
		for (size_t iMesh=0; iMesh<num_mesh; ++iMesh, ++mesh_pos)
		{
			D3DXMatrixMultiply(&mtx, matrix, &m_matScale);
			if (mesh_pos->bSkinMesh) {
				Skining(mesh_pos->pSkinMesh);
			} else {
				TransMatrixComplete(&mtxTrans, mesh_pos->pPrev);
				D3DXMatrixMultiply(&mtx, &mtxTrans, &mtx);
			}
			ns_d3d::Cd3d::Instance().SetWorld(&mtx);
			if (!mesh_pos->dwMtrlIndex)
			{
				num_polygons = static_cast<UINT>(mesh_pos->nPolygonNum);
				device->SetVertexShaderConstantF(12, reinterpret_cast<float*>(&mesh_pos->pMtrl->Diffuse), 1);
				device->SetVertexShaderConstantF(14, reinterpret_cast<float*>(&mesh_pos->pMtrl->Specular), 1);
				device->SetVertexShaderConstantF(16, reinterpret_cast<float*>(&mesh_pos->pMtrl->Ambient), 1);
				mesh_pos->pTexture ? device->SetTexture(0, mesh_pos->pTexture->pTexture) : device->SetTexture(0, NULL);
				device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_vertex, offset_index, num_polygons);
				offset_index += num_polygons * 3;
			}
			else
			{
				size_t		num_mtrl = mesh_pos->nMtrlNum-1, offset_polygons = 0;
				LPMATERIAL	mtrl_pos = mesh_pos->pMtrl;
				for (size_t jMtrl=0; jMtrl<num_mtrl; ++jMtrl, ++mtrl_pos)
				{
					num_polygons	= static_cast<UINT>((mesh_pos->dwMtrlIndex >> (jMtrl*8)) & 0x000000ff);
					device->SetVertexShaderConstantF(12, reinterpret_cast<float*>(&mtrl_pos->Diffuse), 1);
					device->SetVertexShaderConstantF(14, reinterpret_cast<float*>(&mtrl_pos->Specular), 1);
					device->SetVertexShaderConstantF(16, reinterpret_cast<float*>(&mtrl_pos->Ambient), 1);
					device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_vertex, offset_index, num_polygons);
					offset_index	+= num_polygons * 3;
					offset_polygons	+= num_polygons;
				}
				num_polygons = static_cast<UINT>(mesh_pos->nPolygonNum);
				device->SetVertexShaderConstantF(12, reinterpret_cast<float*>(&mtrl_pos->Diffuse), 1);
				device->SetVertexShaderConstantF(14, reinterpret_cast<float*>(&mtrl_pos->Specular), 1);
				device->SetVertexShaderConstantF(16, reinterpret_cast<float*>(&mtrl_pos->Ambient), 1);
				device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_vertex, offset_index, num_polygons-offset_polygons);
				offset_index	+= (num_polygons - offset_polygons)*3;
			}
		}
	};


	void CModel::Animation(size_t _AnimeNo)
	{
		if (_AnimeNo >= m_pModelHead->nAnimationSet) {
			return;
		}
		LPANIMEHEADER animeHeader = this->m_pAnimeHead;
		LPANIMESET animeSet = animeHeader->pAnimeSetList + _AnimeNo;
		LPANIMENODE posAnime = animeSet->pAnimeList;
		float nowframe = animeHeader->fThisTime;
		for (size_t i=animeSet->nAnimation; i; --i, ++posAnime) {
			LPMATRIX mtx;
			mtx = nowframe ? posAnime->pTargetFrame->pmatAnim : &(*posAnime->pTargetFrame->pmatAnim = posAnime->pTargetFrame->matTrans);
			if (posAnime->pQuat) {
				LPANIMEQUATHEADER quat = posAnime->pQuat;
				size_t *postime = quat->pnTimeList;
				size_t limit = quat->nTimeNum, index = 0;
				// get frame
				if (*postime < nowframe) {
					for (;*postime<nowframe && index<limit; ++postime, ++index);
				}
				if (*postime > nowframe) {
					if (nowframe == .0f) { // 0フレームの情報無し + 現在0フレーム
					} else if (index == 0) { // 初期フレームが0より上
						float fraction = (float)(nowframe / *postime);
						float invers = 1.0f - fraction;
						LPMATRIX mtx_quat = quat->pmatQuatList;
						mtx->_11 = invers * mtx->_11 + fraction * mtx_quat->_11;
						mtx->_12 = invers * mtx->_12 + fraction * mtx_quat->_12;
						mtx->_13 = invers * mtx->_13 + fraction * mtx_quat->_13;
						mtx->_21 = invers * mtx->_21 + fraction * mtx_quat->_21;
						mtx->_22 = invers * mtx->_22 + fraction * mtx_quat->_22;
						mtx->_23 = invers * mtx->_23 + fraction * mtx_quat->_23;
						mtx->_31 = invers * mtx->_31 + fraction * mtx_quat->_31;
						mtx->_32 = invers * mtx->_32 + fraction * mtx_quat->_32;
						mtx->_33 = invers * mtx->_33 + fraction * mtx_quat->_33;
					} else {
						size_t time = *postime, prevtime = *(postime - 1);
						float fraction = (float)(nowframe - prevtime) / (time - prevtime);
						LPVECTOR4 vec2 = quat->pvQuatList + index, vec1 = vec2 - 1;
						float dot = VectorDot(vec1, vec2);
						if (dot < 0) {
							dot = VectorDot(vec1, &(*vec2 *= -1.0f));
						}
						if (dot < 1.0f) {
							float angle = acosf(dot);
							if (sinf(angle) != .0f) {
								VECTOR4 vec = (sinf(angle * (1.0f - fraction)) / sinf(angle)) * *vec1
											+ (sinf(angle * fraction) / sinf(angle)) * *vec2;
								mtx->_11 = 1.0f - 2.0f * (vec.y*vec.y + vec.z*vec.z);
								mtx->_12 = 2.0f * (vec.x*vec.y - vec.z*vec.w);
								mtx->_13 = 2.0f * (vec.x*vec.z + vec.y*vec.w);
								mtx->_21 = 2.0f * (vec.x*vec.y + vec.z*vec.w);
								mtx->_22 = 1.0f - 2.0f * (vec.x*vec.x + vec.z*vec.z);
								mtx->_23 = 2.0f * (vec.y*vec.z - vec.x*vec.w);
								mtx->_31 = 2.0f * (vec.x*vec.z - vec.y*vec.w);
								mtx->_32 = 2.0f * (vec.y*vec.z + vec.x*vec.w);
								mtx->_33 = 1.0f - 2.0f * (vec.x*vec.x + vec.y*vec.y);
							}
						}
					}
				} else { // time == nowframe
					LPMATRIX mtxQuat = quat->pmatQuatList + index;
					mtx->_11 = mtxQuat->_11;
					mtx->_12 = mtxQuat->_12;
					mtx->_13 = mtxQuat->_13;
					mtx->_21 = mtxQuat->_21;
					mtx->_22 = mtxQuat->_22;
					mtx->_23 = mtxQuat->_23;
					mtx->_31 = mtxQuat->_31;
					mtx->_32 = mtxQuat->_32;
					mtx->_33 = mtxQuat->_33;
				}
			}
			if (posAnime->pScale) {
				LPANIMESCALEHEADER scale = posAnime->pScale;
				size_t *postime = scale->pnTimeList;
				size_t limit = scale->nTimeNum, index = 0;
				if (*postime < nowframe) {
					for (;*postime<nowframe && index<limit; ++postime, ++index);
				}
				if (*postime > nowframe) {
					if (nowframe == .0f) {
					} else if (index == 0) {
						float fraction = (float)(nowframe / *postime);
						float invers = 1.0f - fraction;
						LPVECTOR3 vec = scale->pvScaleList;
						mtx->_11 = invers * mtx->_11 + fraction * vec->x;
						mtx->_22 = invers * mtx->_22 + fraction * vec->y;
						mtx->_33 = invers * mtx->_33 + fraction * vec->z;
					} else {
						size_t time = *postime, prevtime = *(postime - 1);
						float fraction = (nowframe - (float)prevtime) / (float)(time - prevtime);
						float invers = 1.0f - fraction;
						LPVECTOR3 vec2 = scale->pvScaleList + index, vec1 = vec2 - 1;
						mtx->_11 *= invers * vec1->x + fraction * vec2->x;
						mtx->_22 *= invers * vec1->y + fraction * vec2->y;
						mtx->_33 *= invers * vec1->z + fraction * vec2->z;
					}
				} else {
					LPVECTOR3 vec = scale->pvScaleList + index;
					mtx->_11 *= vec->x;
					mtx->_22 *= vec->y;
					mtx->_33 *= vec->z;
				}
			}
			if (posAnime->pSlide) {
				LPANIMESLIDEHEADER slide = posAnime->pSlide;
				size_t *postime = slide->pnTimeList;
				size_t limit = slide->nTimeNum, index = 0;
				if (*postime < nowframe) {
					for (;*postime<nowframe && index<limit; ++postime, ++index);
				}
				if (*postime > nowframe) {
					if (nowframe == .0f) {
					} else if (index == 0) {
						float fraction = (float)(nowframe / *postime);
						float invers = 1.0f - fraction;
						LPVECTOR3 vec = slide->pvSlideList;
						mtx->_41 = invers * mtx->_41 + fraction * vec->x;
						mtx->_42 = invers * mtx->_42 + fraction * vec->y;
						mtx->_43 = invers * mtx->_43 + fraction * vec->z;
					} else {
						size_t time = *postime, prevtime = *(postime - 1);
						float fraction = (float)(nowframe - prevtime) / (time - prevtime);
						float invers = 1.0f - fraction;
						LPVECTOR3 vec2 = slide->pvSlideList + index, vec1 = vec2 - 1;
						mtx->_41 = invers * vec1->x + fraction * vec2->x;
						mtx->_42 = invers * vec1->y + fraction * vec2->y;
						mtx->_43 = invers * vec1->z + fraction * vec2->z;
					}
				} else {
					LPVECTOR3 vec = slide->pvSlideList + index;
					mtx->_41 = vec->x;
					mtx->_42 = vec->y;
					mtx->_43 = vec->z;
				}
			}
			if (posAnime->pMat) {
				LPANIMEMATHEADER mat = posAnime->pMat;
				size_t *postime = mat->pnTimeList;
				size_t limit = mat->nTimeNum, index = 0;
				if (*postime < nowframe) {
					for (;*postime<nowframe && index<limit; ++postime, ++index);
				}
				if (*postime > nowframe) {
					if (nowframe == .0f) {
					} else if (index == 0) {
						float fraction = (float)(nowframe / *postime);
						LPMATRIX mtx_now = mat->pmatList;
						*mtx = (1.0f - fraction) * *mtx + fraction * *mtx_now;
					} else {
						size_t time = *postime, prevtime = *(postime - 1);
						float fraction = (float)(nowframe - prevtime) / (time - prevtime);
						LPMATRIX mtx2 = mat->pmatList + index, mtx1 = mtx2 - 1;
						*mtx = (1.0f - fraction) * *mtx1 + fraction * *mtx2;
					}
				} else {
					*mtx = *(mat->pmatList + index);
				}
			}
		}

		if ((animeHeader->fThisTime += 0.05f) > animeSet->nMaxTime) {
			animeHeader->fThisTime -= animeSet->nMaxTime;
		}
	}

	void CSkinedMeshModel::Skining(LPSKINEDMESHNODE _Skin)
	{
		if (FAILED(this->m_pVB->Lock(0, 0, (void**)this->m_pVertices, 0))) {
			NS_ASSERT_FAILURE(Failed Lock in CModel::Skining());
			return;
		}
		size_t bone = _Skin->nBones;
		LPSKINEDMESHBONE posBone = _Skin->pBoneList;
		for (size_t i=0; i<bone; ++i, ++posBone) {
			// ボーンの親子関係に沿った変換行列
			TransMatrixComplete(&posBone->matTrans, posBone->pLinkFrame);
			// フレーム変換行列 * ボーン変換行列
			D3DXMatrixMultiply(&posBone->matTrans, &posBone->matBone, &posBone->matTrans);
		}
		LPCUSTOMVERTEX vers = this->m_pVertices + _Skin->nVertexPos;
		LPSKINEDMESHNODEVERTEX posSkinVers = _Skin->pSkinVers;
		posBone = _Skin->pBoneList;
		for (size_t i=_Skin->nVertexNum; i; --i, ++posSkinVers, ++vers) {
			size_t blend = posSkinVers->nBlendNum-1;
			if (!blend) {
				VecxMat(&vers->position,
					&posSkinVers->vPosition,
					&(posBone+posSkinVers->dwBoneIndex)->matTrans);
			}
			else {
				float offset_weight = .0f;
				LPVECTOR3 pos = &vers->position;
				for (size_t j=0; j<=blend; ++j) {
					size_t index_bone = (size_t)((posSkinVers->dwBoneIndex >> (j*8)) & 0x000000ff);
					LPSKINEDMESHBONE targetBone = posBone + index_bone;
					VECTOR3 vec;
					// ローカル座標 * フレーム変換行列 * ボーン変換行列
					VecxMat(&vec, &posSkinVers->vPosition, &targetBone->matTrans);
					if (!j) { // first blending
						// LPSKINEDMESHNODEVERTEX + i ->dwWeightIndex = 
						size_t index_weight = (size_t)(posSkinVers->dwWeightIndex & 0x000000ff);
						offset_weight = *(targetBone->pfWeightList + index_weight);
						pos->x = vec.x * offset_weight;
						pos->y = vec.y * offset_weight;
						pos->z = vec.z * offset_weight;
					}
					else if (j == blend) { // last
						float weight = 1.0f - offset_weight;
						pos->x += vec.x * weight;
						pos->y += vec.y * weight;
						pos->z += vec.z * weight;
					}
					else { // 2st, 3st
						size_t index_weight = (size_t)((posSkinVers->dwWeightIndex >> (j*8)) & 0x00ff);
						float weight = *(targetBone->pfWeightList + index_weight);
						pos->x += vec.x * weight;
						pos->y += vec.y * weight;
						pos->z += vec.z * weight;
						offset_weight += weight;
					}
				}
			}
		}
		this->m_pVB->Unlock();
	}



//--------------------------------------------------------
//	Name:	CSkinedMeshModel::Release
//	Desc:	Release CModel
//--------------------------------------------------------
	void CSkinedMeshModel::Release()
	{
		delete this;
	};


//--------------------------------------------------------
//	Name:	CModel::CModel
//	Desc:	CModel Constracta
//--------------------------------------------------------
	CModel::CModel()
		: m_pModelHead()
		, m_matScale()
		, m_pVertices()
		, m_pwIndices()
		, m_pFrameList()
		, m_pMeshList()
		, m_pTexList()
		, m_pMtrlList()
		, m_pAnimeHead()
		, m_pVB()
		, m_pIB()
	{
		ns_d3d::MatrixIdentity( &m_matScale );	// m_matScaleがコンストラクタで正規化されていたらここはいらない
	}


//--------------------------------------------------------
//	Name:	CModel::CModel
//	Desc:	CModel Constracta
//--------------------------------------------------------
	CModel::CModel( LPMODELHEADER pModelHead, LPANIMEHEADER pAnimeHead )
		: m_pModelHead( pModelHead )
		, m_matScale()
		, m_pVertices()
		, m_pwIndices()
		, m_pFrameList()
		, m_pMeshList()
		, m_pTexList()
		, m_pMtrlList()
		, m_pAnimeHead( pAnimeHead )
		, m_pVB()
		, m_pIB()
	{
		D3DXMatrixIdentity( &m_matScale );
	}


//--------------------------------------------------------
//	Name:	CModel::CModel
//	Desc:	CModel Constracta 3 (Texture)
//--------------------------------------------------------
	CModel::CModel( LPMODELHEADER pModelHead, LPANIMEHEADER pAnimeHead, LPTEXTUREHEADER pTexHead )
		: m_pModelHead( pModelHead )
		, m_matScale()
		, m_pVertices()
		, m_pwIndices()
		, m_pFrameList()
		, m_pMeshList()
		, m_pTexList( pTexHead )
		, m_pMtrlList()
		, m_pAnimeHead( pAnimeHead )
		, m_pVB()
		, m_pIB()
	{
		D3DXMatrixIdentity( &m_matScale );
	}



	CModel::CModel( LPMODELHEADER pModelHead, LPANIMEHEADER pAnimeHead,
		LPTEXTUREHEADER pTexHead, LPEFFECTHEADER pEffect )
		: m_pModelHead( pModelHead )
		, m_matScale()
		, m_pVertices()
		, m_pwIndices()
		, m_pFrameList()
		, m_pMeshList()
		, m_pTexList( pTexHead )
		, m_pMtrlList()
		, m_pAnimeHead( pAnimeHead )
		, m_pVB()
		, m_pIB()
		, m_pEffectHead(pEffect)
	{
		D3DXMatrixIdentity( &m_matScale );
	}



//--------------------------------------------------------
//	Name:	CModel::~CModel
//	Desc:	CModel Destracta
//--------------------------------------------------------
	CModel::~CModel()
	{
		if ( m_pVB ) { m_pVB->Release(); m_pVB=NULL; }
		if ( m_pIB ) { m_pIB->Release(); m_pIB=NULL; }
		if ( m_pFrameList )	delete [] m_pFrameList;
		if ( m_pMeshList )	delete [] m_pMeshList;
		if ( m_pModelHead->nMaterial ) delete [] m_pMtrlList;
		if ( m_pModelHead->nEffect ) {
			LPEFFECTHEADER fx = m_pEffectHead;
			delete [] fx->fxInstance;
			delete [] fx->fxString;
			delete [] fx->fxFloats1;
			delete [] fx->fxFloats4;
		}

		size_t nTexture = m_pModelHead->nTexture;
		if ( 1==nTexture )
		{
			m_pTexList->pTexture->Release();
			m_pTexList->pTexture = NULL;
			delete m_pTexList;
		}
		else if ( nTexture )
		{
			LPTEXTUREHEADER tex = m_pTexList;
			for(size_t i=0; i<nTexture; ++i, ++tex)
				if(tex->pTexture) {tex->pTexture->Release(); tex->pTexture=NULL;}
			delete [] m_pTexList;
		}

		if ( m_pModelHead->nAnimationSet )
		{
			delete [] m_pAnimeHead->pAnimeList;
			delete [] m_pAnimeHead->pAnimeSetList;
			delete [] m_pAnimeHead->pMatAnime;
			delete [] m_pAnimeHead->pnTimeList;
			if ( m_pAnimeHead->pQuatList )
				delete [] m_pAnimeHead->pQuatList;
			if ( m_pAnimeHead->pScaleList )
				delete [] m_pAnimeHead->pScaleList;
			if ( m_pAnimeHead->pSlideList )
				delete [] m_pAnimeHead->pSlideList;
			if ( m_pAnimeHead->pMatList )
				delete [] m_pAnimeHead->pMatList;
			if ( m_pAnimeHead->pvQuatList )
				delete [] m_pAnimeHead->pvQuatList;
			if ( m_pAnimeHead->pmatList )
				delete [] m_pAnimeHead->pmatList;
			if ( m_pAnimeHead->pvList )
				delete [] m_pAnimeHead->pvList;
			delete	m_pAnimeHead;
		}
	};



//--------------------------------------------------------
//	Name:	CModel::Release
//	Desc:	Release CModel
//--------------------------------------------------------
	void CModel::Release()
	{
		delete this;
	};

}	// namespace: ns_model
