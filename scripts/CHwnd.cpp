//
//	CHwnd.cpp
//
//	Copyright (c) 2010 Naoki Shimoyamada
//

#include "CHwnd.h"

namespace ns_win {

	CHwnd::~CHwnd()
	{
		this->m_ProcList.RemoveAll();
		this->m_HwndList.RemoveAll();
	}

	bool CHwnd::GenerateWindow(const size_t id, const HINSTANCE hInst)
	{
		// Register the window class
		if ( !this->CreateWndClassEx( hInst, &ns_win::CHwnd::MsgProcMain, m_pMainClassName, 0x777777 ) )
			return	false;

		HWND hWnd		= CreateWindow( m_pMainClassName,
											m_pMainWindowName,
											WS_OVERLAPPEDWINDOW,
											eMainWindowX,
											eMainWindowY,
											eMainWindowWidth,
											eMainWindowHeight,
											GetDesktopWindow(),
											NULL,
											hInst,
											NULL );

		// Initialize OpenFileName
		//	nsOpenFileInit( hWndMain );

		// Show the window
		ShowWindow( hWnd, SW_SHOWDEFAULT );
		UpdateWindow( hWnd );

		HwndList::iterator& wnd = CallHandle( id );
		if (!wnd) {
			AddHandle(id);
			this->m_HwndList.end()->hWnd = hWnd;
		} else {
			wnd->hWnd = hWnd;
		}
		return	true;
	}


	CHwnd::HwndList::iterator CHwnd::CallHandle( const size_t id )
	{
		if ( !m_HwndList.Size() )
			return	NULL;
		HwndList::iterator& ite = this->m_HwndList.begin();
		while (!!ite) {
			if (ite->nID == id) {
				return ite;
			}
			++ ite;
		}
		return	NULL;
	}


	bool CHwnd::CreateWndClassEx( const HINSTANCE hInst, const WNDPROC WndProc, const char_t* ClassName, const dword_t color )
	{
		WNDCLASSEX	wc;
		wc.cbSize = sizeof(WNDCLASSEX);					// 構造体サイズ
		wc.style = CS_HREDRAW | CS_VREDRAW;				// ウィンドウスタイル
		wc.lpfnWndProc = WndProc;						// プロシージャ名
		wc.cbClsExtra = 0;								// メモリ領域の追加設定
		wc.cbWndExtra = 0;								// メモリ領域の追加設定
		wc.hInstance = hInst;							// インスタンス
		wc.hIcon = LoadIcon(0, IDI_APPLICATION);		// アイコン
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);		// カーソル
		wc.hbrBackground = CreateSolidBrush( color );	// 背景色
		wc.lpszMenuName = NULL;							// メニュー名
		wc.lpszClassName = ClassName;					// クラス名
		wc.hIconSm = LoadIcon(0, IDI_APPLICATION);

		return ( !RegisterClassEx(&wc) ? false : true );
	}


	LRESULT WINAPI CHwnd::MsgProcMain(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		ProcList::iterator& ite = CHwnd::_Inst().m_ProcList.begin();
		while (!!ite) {
			int ret = ite->WndProc(hWnd, msg, wParam, lParam);
			if (ret) return ret;
			++ ite;
		}

		switch(msg)
		{
		case WM_DESTROY:
			PostQuitMessage( 0 );
			return 0;
		}
		
		return DefWindowProc( hWnd, msg, wParam, lParam );
	}

} // ns_win