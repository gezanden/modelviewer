#pragma	once
#ifndef	_NSTYPELIST_H_
#define	_NSTYPELIST_H_

#include	"NSTypeTraits.h"

//--------------------------------------
//	Namespace
//--------------------------------------
namespace	ns_types
{

	template <class TList, template <class> class Unit>
	class GenScatterHierarchy;

	// GenScatterHierarchy の特殊化: Typelist から Unit へ
	template <class T1, class T2, template <class>class Unit>
	class GenScatterHierarchy<ns_types::Typelist<T1, T2>, Unit>
		: public GenScatterHierarchy<T1, Unit>
		, public GenScatterHierarchy<T2, Unit>
	{
	public:
		 typedef ns_types::Typelist<T1, T2>	TList;
		 typedef GenScatterHierarchy<T1, Unit>	LeftBase;
		 typedef GenScatterHierarchy<T2, Unit>	RightBase;
		 template <typename T> struct Rebind
		 {
			 typedef Unit<T> Result;
		 };
	};

	// Unit へ原始型（非タイプリスト）を引き渡す
	template <class AtomicType, template <class> class Unit>
	class GenScatterHierarchy : public Unit<AtomicType>
	{
		typedef Unit<AtomicType>	LeftBase;
		template <typename T> struct Rebind
		{
			typedef Unit<T> Result;
		};
	};

	// NullType の場合は何もしない
	template <template <class> class Unit>
	class GenScatterHierarchy<ns_types::NullType, Unit>
	{
		template <typename T> struct Rebind
		{
			typedef Unit<T> Result;
		};
	};

	// function template Field: Loki::HierarchyGenerators.h::L.110
	// Accesses Helpers
	template <class T, class H>
	typename H::template Rebind<T>::Result&
		Field(H& obj)
	{
		return	obj;
	}

	template <class T, class H>
	const typename H::template Rebind<T>::Result&
		Field(const H& obj)
	{
		return	obj;
	}

	//------ 活用方法
	//template <class T> struct Holder
	//{
	//	T value_;
	//};

	//typedef GenScatterHierarchy<
	//	ns_types::TYPELIST_3(int, string, Widget),
	//	Holder>
	//	WidgetInfo;
	//WidgetInfo obj;
	//string name = (static_cast<Holder<string>&>(obj)).value_;
	//-------------------
	//↑のキャストが分かりづらいので、容易にアクセスできるメンバ関数を提供

	// FieldTraits の定義については HierarchyGenerators.h を参照してください
	//template <class T, class H>
	//typename Loki::Private::FieldTraits<H>::Rebind<T>::Result&
	//	Field(H& obj)
	//{
	//	return obj;
	//}

	// 使用例: Field<Widget>(obj)

	// インデックスにに基づくフィールド・アクセス関数 P.74
	//template <class H, typename R>
	//inline R& FieldHelper(H& obj, Type2Type<R>, ns_types::Int2Type<0>)
	//{
	//	typename H::LeftBase&	subobj = obj;
	//	return	subobj;
	//}

	//template <class H, typename R, int i>
	//inline R& FieldHelper(H& obj, Type2Type<R> tt, Int2Type<i>)
	//{
	//	typename H::RightBase&	subobj = obj;
	//	return	FieldHelper(subobj, tt, Int2Type<i-1>());
	//}

	//template <int i, class H>
	//typename Private::FieldTraits<H>::At<i>::Result&
	//	Field(H& obj)
	//{
	//	typedef typename Private::FieldTraits<H>::At<i>::Result
	//		Result;
	//	return	FieldHelper(obj, Type2Type<Result>(), Int2Type<i>());
	//}

	////////////////////////////
	//	タプル（無名フィールドを保持した小さな構造を生成する）P.76
	//	Loki\HierarchyGenerators.h::L.127
	template <class T>
	struct TupleUnit
	{
		T m_value;
		operator T&() { return m_value; }
		operator const T&() const { return m_value; }
	};

	template <class TList>
	struct Tuple : public GenScatterHierarchy<TList, TupleUnit>
	{
	};

	//////////////////////////////////
	//	FieldHelper
	//	- LokiPort::HierarchyGenerators.h::FieldHelper
	template <unsigned int i>
	struct FieldHelper
	{
		template<class H>
		struct In
		{
		private:
			typedef typename ns_types::TypeAt<typename H::TList, i>::Result	ElementType;
			typedef typename H::template Rebind<ElementType>::Result		UnitType;

			enum { isConst = TypeTraits<H>::isConst };	//TypeTraits

			typedef typename ns_types::Select
			<
				isConst,
				const typename H::RightBase,
				typename H::RightBase
			>
			::Result	UnqualifiedResultType;

		public:
			typedef typename ns_types::Select
			<
				isConst,
				const UnqualifiedResultType,
				UnqualifiedResultType
			>
			::Result	ResultType;

			static ResultType& Do(H& obj)
			{
				RightBase& rightBase = obj;
				return	FieldHelper<i - 1>::template In<RightBase>::Do(rightBase);
			}
		};
	};

	template <>
	struct FieldHelper<0>
	{
		template<class H>
		struct In
		{
		private:
			typedef typename H::TList::Head ElementType;
			typedef typename H::template Rebind<ElementType>::Result UnitType;

			enum { isConst = TypeTraits<H>::isConst };

			typedef typename ns_types::Select
			<
				isConst,
				const typename H::LeftBase,
				typename H::LeftBase
			>
			::Result LeftBase;

			typedef typename ns_types::Select
			<
				ns_types::SameType<UnitType, TupleUnit<ElementType> >::value,
				ElementType,
				UnitType
			>
			::Result UnqualifiedResultType;

		public:
			typedef typename ns_types::Select
			<
				isConst,
				const UnqualifiedResultType,
				UnqualifiedResultType
			>
			::Result ResultType;

			static ResultType& Do(H& obj)
			{
				LeftBase& leftBase = obj;
				return	leftBase;
			}
		};
	};

	// Accesses Helper
	template <unsigned int i, class H>
	typename FieldHelper<i>::template In<H>::ResultType&
		Field(H& obj)
	{
		return	FieldHelper<i>::template In<H>::Do(obj);
	}

	////////////////////////////
	//	線形階層の生成 P.77
	template <class T> class EventHandler
	{
	public:
		virtual void OnEvent(const T&, int eventId) = 0;
		virtual ~EventHandler() {}
//		virtual void ~EventHandler() {}
	};

	// 使用例
	//typedef GenScatterHierarchy
	//<
	//	ns_types::TYPELIST_3(Window, Button, ScrollBar),
	//	EventHandler
	//>
	//WidgetEventHandler;

	// 多重継承を用いないバージョン
	template
	<
		class TList,
			template <class AtomicType, class Base> class Unit,
			class Root = EmptyType
	>
	class GenLinearHierarchy;

	template
	<
		class T1,
		class T2,
		template <class, class> class Unit,
		class Root
	>
	class GenLinearHierarchy<ns_types::Typelist<T1, T2>, Unit, Root>
		: public Unit< T1, GenLinearHierarchy<T2, Unit, Root> >
	{
	};

	template
	<
		class T,
		template <class, class> class Unit,
		class Root
	>
	class GenLinearHierarchy<TYPELIST_1(T), Unit, Root>
		: public Unit<T, Root>
	{
	};

	// 使用例
	//template <class T, class Base>
	//class EventHandler : public Base
	//{
	//public:
	//	virtual void OnEvent(T&, obj, int eventId);
	//};
	//typedef GenLinearHierarchy
	//<
	//	ns_types::TYPELIST_3(Window, Button, ScrollBar),
	//	EventHandler
	//>
	//MyEventHandler;

}

#endif	// _NS_TYPELIST_H_