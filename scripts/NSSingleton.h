//
//	NSSingleton.h
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once
#ifndef	_NSSINGLETON_H_
#define	_NSSINGLETON_H_

// Including std::new, std::bad_alloc;
#include <new>
// Including std::realloc;
#include <memory>
// Including std::upper_bound
#include <algorithm>
// Including std::copy_backword
#include <xutility>
// Including std::atexit
#include <stdexcept>
// Including assert();
#include <cassert>

namespace ns_lib
{

	//////////////////////////////////////////////////////////
	//	Template class CSingleton
	//
	//	member:	m_pSingleton	- シングルトンオブジェクトのポインタ
	//
	//	desc:	シングルトンパターンテンプレート
	//				・このクラスを継承して作ったクラスから GetSingleton/GetSingletonPtr
	//				　を呼び出すことで、シングルトンパターンを実装できる。
	//				　このテンプレートを使用するとき、派生クラスのコンストラクタは public
	//				　で実装しなければならない。
	//
	//	Reference documents:
	//			・著: Erich Gamma/Richard Helm/Ralph Johnson/John Vlissides 訳: 本位田 真一/吉田 和樹 (1999/10)
	//				『オブジェクト指向における再利用のためのデザインパターン 改訂版』 P137
	//			・編者: Mark DeLoura 監訳: 川西 裕幸 翻訳 : 狩野 智英 (2001/06)
	//				『Game Programming Gems』P36
	//
	template <typename T> class CSingleton
	{
	public:
		static T& _Inst()
		{
			return *m_pSingleton;
		}
		static T& Instance()	// 一回目の呼び出しでm_tSingletonに割り当てる
		{
			return m_pSingleton ? *m_pSingleton : *( m_pSingleton = CreateSingleton() );
		}
	protected:
		CSingleton()
		{
			//int offset = (int)(T*)1 - (int)(CSingleton <T>*)(T*)1;
			//m_tSingleton = (T*)((int)this + offset);
		}
		~CSingleton()
		{
			//return;
		}
		//static T& GetSingleton()	// 一回目の呼び出しでm_tSingletonに割り当てる
		//{
		//	return m_pSingleton ? *m_pSingleton : *( m_pSingleton = CreateSingleton() );
		//}
		//static T* GetSingletonPtr()
		//{
		//	return m_pSingleton ? m_pSingleton : CreateSingleton();
		//}
		static void ReleaseSingleton()
		{
			delete m_pSingleton;
			m_pSingleton = NULL;
		}

	private:
		CSingleton(const CSingleton&);
		CSingleton& operator=(const CSingleton&);

		static T* CreateSingleton()
		{
			try { m_pSingleton = new typename T; }
			catch ( std::bad_alloc ) { return NULL; }
			atexit(ReleaseSingleton);				// コンパイラ依存の破棄
			return	m_pSingleton;
		}

		static T* m_pSingleton;
	};	// CSingleton

	template <typename T> T* CSingleton<T>::m_pSingleton	= 0;


	/////////////////////////////////////
	//	Modern C++ Design, print 6
	namespace Private
	{

		///////////////////////////////
		//	Private::LifetimeTracker, page 151
		//	
		class LifetimeTracker
		{
		public:
			LifetimeTracker(unsigned int x) : m_longevity(x) {}
			virtual ~LifetimeTracker() = 0;
			friend inline bool Compare(unsigned int longevity, const LifetimeTracker* p)
			{ return p->m_longevity > longevity; }

		private:
			unsigned int m_longevity;
		};

		typedef LifetimeTracker** TrackerArray;
		extern TrackerArray pTrackerArray;
		extern unsigned int elements;

		////////////////////////////////
		//	Private::Deleter
		//	追跡されるオブジェクトの破棄を行うためにファンクタを使用
		//	破棄用のヘルパー関数
		template <typename T>
		struct Deleter
		{
			static void Delete(T* pObj)
			{ delete pObj; }
		};

		// T 型のオブジェクトに対する具体的な LifetimeTracker
		template <typename T, typename Destroyer>
		class ConcreteLifetimeTracker : public LifetimeTracker
		{
		public:
			ConcreteLifetimeTracker(T* p, unsigned int longevity, Destroyer d)
				: LifetimeTracker(longevity)
				, m_pTracked(p)
				, m_destroyer(d)
			{}
			~ConcreteLifetimeTracker()
			{
				m_destroyer(m_pTracked);
			}

		private:
			T* m_pTracked;
			Destroyer m_destroyer;
		};
		void AtExitFn();	// 以下で必要となる宣言

	///////////////////////////////
	//	SetLongevity page 149, LokiPort\Singleton.h line 100
	template <typename T, typename Destroyer>
	void SetLongevity(T* pDynObject, unsigned int longevity,
		Destroyer d = Private::Deleter<T>::Delete)
	{
		//using namespace Private;
		Private::TrackerArray		pNewArray = static_cast<Private::TrackerArray>(std::realloc(pTrackerArray, elements + 1));
		if (!pNewArray)				throw std::bad_alloc();
		Private::LifetimeTracker*	p = new Private::ConcreteLifetimeTracker<T, Destroyer>(pDynObject, longevity, d);
		pTrackerArray = pNewArray;
		Private::TrackerArray		pos = std::upper_bound(
											pTrackerArray,
											pTrackerArray + elements,
											longevity,
											Private::LifetimeTracker::Compare);
		std::copy_backward(pos, pTrackerArray + elements, pTrackerArray + elements + 1);
		*pos = p;
		++elements;
		std::atexit(Private::AtExitFn);
	}

	// Function AtExitFn(); page 154
	//static void AtExitFn()
	//{
	//	assert(elements > 0 && pTrackerArray != 0);
	//	// スタックの先頭にある要素を取得する
	//	Private::LifetimeTracker*	pTop = pTrackerArray[elements - 1];
	//	// 該当オブジェクトをスタックから除去する
	//	// メモリ不足に対する std::realloc のエラー･チェックは行わない
	//	pTrackerArray = (TrackerArray*)std::realloc(pTrackerArray, --elements);
	//	// 要素を破棄する
	//	delete	pTop;
	//}

	// class template CreateUsingNew: LokiPort\Singleton.h line 139
	// new-delete Allocation
	template <class T> struct CreateUsingNew
	{
		static T* Create()
		{ return new T; }

		static void Destroy(T* p)
		{ delete p; }
	};

	// class template DefaultLifetime
	// Use std::atexit
	template <class T>
	struct DefaultLifetime
	{
		static void ScheduleDestruction(T*, void (*pFun)())	// T*: exist
		{ std::atexit(pFun); }

		static void OnDeadReference()
		{ throw	std::logic_error("Dead Reference Detected"); }
	};

	// class template SingleThreaded: LokiPort\Threads.h line 26
	template <class Host>
	class SingleThreaded
	{
	public:
		struct Lock
		{
			Lock() {}
			explicit Lock(const SingleThreaded&) {}
		};
		typedef Host	VolatileType;
	};

	// class template SingletonHolderStaticData: LokiPort\Singleton.h line 323
	template
	<
		class ClientType,
		class ThreadingModelType,
		class PtrInstanceType
	>
	class SingletonHolderStaticData
	{
		friend ClientType;	// illegal (11.4/2) but works with VC
		static ThreadingModelType	s_ThreadingModelData;
		static PtrInstanceType		s_pInstance;
		static bool					s_destroyed;
	};

	template<class C, class M, class P>
	M SingletonHolderStaticData<C, M, P>::s_ThreadingModelData;

	template<class C, class M, class P>
	P SingletonHolderStaticData<C, M, P>::s_pInstance;

	template<class C, class M, class P>
	bool SingletonHolderStaticData<C, M, P>::s_destroyed;

	}	// ns_lib::Private

	///////////////////////////////
	//	SingletonHolder, page 160
	//	How to use:
	//		typedef ns_lib::CSingletonHolder<MyClass> MyHolder
	//		MyClass& c = MyHolder::Instance();
	//
	//		typedef ns_lib::CsingletonHolder<MyClass, MyCreator> MyHolder
	//		MyClass: not has a Default Constracta
	//		MyCreator: has a Default constracta that create MyClass
	template 
	<
		class T,
			template <class> class CreationPolicy = Private::CreateUsingNew,
			template <class> class LifetimePolicy = Private::DefaultLifetime,
			template <class> class ThreadingModel = Private::SingleThreaded
	>
	class CSingletonHolder
	{
		typedef typename ThreadingModel<T*>::VolatileType	PtrInstanceType;	// インスタンスの型（=T*）

		typedef Private::SingletonHolderStaticData
		<
			CSingletonHolder,
			ThreadingModel<T>,
			PtrInstanceType
		>
		MySingletonHolderStaticData;

	public:
		static T& Instance()
		{
			if (!pInstance_())
			{
				typename ThreadingModel<T>::Lock guard;
				if (!pInstance_())
				{
					if (destroyed_())
					{
						LifetimePolicy<T>::OnDeadReference();
						MySingletonHolderStaticData::s_destroyed = false;
						assert(destroyed_() == true);
						destroyed_() = false;
					}
					pInstance_() = CreationPolicy<T>::Create();
					LifetimePolicy<T>::ScheduleDestruction(pInstance_(), &DestroySingleton);
				}
			}
			return	*pInstance_();
		}
		static T& _Inst()
		{
			return *MySingletonHolderStaticData::s_pInstance;
		}

	private:
		// Helper
		static void DestroySingleton()
		{
			assert(!destroyed_());
			CreationPolicy<T>::Destroy(pInstance_());
			pInstance_() = 0;
			destroyed_() = true;
		}

		// Protect
		CSingletonHolder();
		CSingletonHolder(const CSingletonHolder&);
		CSingletonHolder& operator =(const CSingletonHolder&);
		~CSingletonHolder();
		CSingletonHolder* operator *();

		// Data
		//static InstanceType*	m_pInstance;
		//static bool				m_destroyed;

		static PtrInstanceType& pInstance_()
		{
			return MySingletonHolderStaticData::s_pInstance;
		}

		static bool& destroyed_()
		{
			return MySingletonHolderStaticData::s_destroyed;
		}
	};	// CSingletonHolder

}	// ns_lib


#endif	// _NSSINGLETON_H_