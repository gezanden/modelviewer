//
// File: CModelMgr.cpp
//
// Desc: モデルを管理するクラス
//
// Copyright (c) 2008 Naoki Shimoyamada. All rights reserved.
//

// including ns_model::CModelMgr;
#include	"CModelMgr.h"
// including Cd3d::CreateTexture();
#include	"Cd3d.h"
// including ns_model::MatrixIdentity();
#include	"XFileFuncs.h"
// including std::wifstream;
#include	<fstream>
// including std::string
#include	<string>
// including nslib::strictlystrstr();
#include	"nsfunc.h"

#include	"CFStream.h"
#include	"ModelStructures.h"
//#include	<time.h>
// including std::mem_fun;
#include	<functional>
// including nsgl::CSceneBuilder::CController::RegisterProtagonist();
#include	"SceneBuilder.h"

namespace ns_model
{
	using namespace ns_types;

	namespace Private
	{
		//--------------------------------------------------------
		//	Name:	GetModelInit()
		//	Desc:	CModel 用のモデル初期化クラスの生成関数
		//--------------------------------------------------------
		CModelInit* GetModelInit()
		{
			return new CModelInit;
		}


		//--------------------------------------------------------
		//	Name:	GetSkinedMeshInit()
		//	Desc:	CSkinedMeshModel 用のモデル初期化クラスの生成関数
		//--------------------------------------------------------
		CModelInit* GetSkinedMeshInit()
		{
			return new CSkinedMeshModelInit;
		}


	}


	//--------------------------------------------------------
	//	Name:	CModelMgr::CModelMgr()
	//	Desc:	モデルデータの読み込みとIDの関連付け。
	//--------------------------------------------------------
	CModelMgr::CModelMgr()
		: m_nModelation()
		, m_nCharactors()
		, m_Charactors()
		, m_ConcreteModels()
	{
		ModelPool::Instance().SetExecutor(&CModelMgr::Renderer);
		//FunctorModel _Functor(&CModelAllocator::GetModelFromXFile);
		//ModelFactory::Instance().Register( eStanderdInitFactory, Private::GetModelInit );
		//ModelFactory::Instance().Register( eSkinedMeshInitFactory, Private::GetSkinedMeshInit );
	}

	//--------------------------------------------------------
	//	Name:	CModelMgr::RegisterId
	//	Desc:	モデルデータの読み込みとIDの関連付け。
	//--------------------------------------------------------
	void CModelMgr::RegisterId(const size_t charId, const size_t modelId)
	{
		LPCHARACTORNODE Node = new CHARACTORNODE(m_ConcreteModels.At(modelId), charId);
		MatrixIdentity(&Node->matrix);
		m_Charactors.push_back(Node);
		++m_nCharactors;
	}


	//--------------------------------------------------------
	//	Name:	CModelMgr::RegisterModel
	//	Desc:	モデルデータの読み込みとIDの関連付け。
	//--------------------------------------------------------
	size_t CModelMgr::RegisterModel( const char_t* name )
	{
		////////////////////////////////// 読み込み&登録 ///////////////////////////////////////////
		CModel* pModel = GetModel(name);
		if (pModel == 0) {
			return 0;
		}
		m_ConcreteModels.push_back(pModel);
		return ++m_nModelation;
	}

	size_t CModelMgr::RegisterModel(const char* name)
	{
		CModel* __model = ModelAllocator::Instance()(name);
		if (__model == NULL) {
			return 0;
		}
		LPCHARACTORID __id = new CHARACTORID;
		ModelPool::Instance().RegisterFile(*__id, *__model);
		//CModel* __model = GetModel(name);
		//if (__model == 0) {
		//	return 0;
		//}
		//m_ConcreteModels.push_back(__model);
		return ++m_nModelation;
	}

	size_t CModelMgr::RegisterModelAsPlayer(const char* name, const float scale, const float yaxis)
	{
		CModel* __model = ModelAllocator::Instance()(name);
		if (__model == NULL) {
			return 0;
		}
		LPCHARACTORID __id = new CHARACTORID;
		LPMATRIX mtx = &__id->matrix;
		mtx->_11 = mtx->_22 = mtx->_33 = scale;
		mtx->_42 += yaxis;					// little bit
		ModelPool::Instance().RegisterFile(*__id, *__model);
		nsgl::CPlayerController::RegisterProtagonist(__id->nId, &__id->matrix);

		// update to lock charactor
		m_CharaLockedID = __id;
		m_CharaLocked = __model;
		return ++m_nModelation;
	}

	void CModelMgr::Delete()
	{
		if (m_nCharactors) {
			m_Charactors.RemoveAll();
			m_nCharactors = 0;
		}
		if (m_nModelation) {
			m_ConcreteModels.RemoveAll();
			m_nModelation = 0;
		}
	}

	//--------------------------------------------------------
	//	Name:	CModelMgr::Renderer
	//	Desc:	モデルのレンダリング命令。
	//--------------------------------------------------------

	void CModelMgr::RLZ()
	{
		LPCHARACTORNODE	node_char;
		for (size_t i=0; i<m_Charactors.Size(); ++i)
		{
			node_char = m_Charactors.At(i);
			if (node_char->bExist)
				node_char->pModel->Render(&node_char->matrix);
		}
	}


	void CModelMgr::RLZTest()
	{
		LPCHARACTORNODE	node_char;
		for (size_t i=0; i<m_Charactors.Size(); ++i)
		{
			node_char = m_Charactors.At(i);
			if (node_char->bExist)
				node_char->pModel->RenderTest(&node_char->matrix);
		}
	}


	
	//-------------------------------------------------------------------------
	//	Name:	CreateModel()
	//	Desc:	モデルインスタンス生成
	//-------------------------------------------------------------------------
	CModel* CModelMgr::GetModel( const char* _Name )
	{
		using namespace nslib;
		///////////////////////////////////// ファイルオープン /////////////////////////////////////
//#ifdef	_UNICODE
		//std::wifstream		f_in;		// ストリーム
//#else
//		std::ifstream		f_in;
//#endif

		// ファイルを開く
		//f_in.open( name, std::ios::binary );
		//if( !f_in.is_open() )
		//	return NULL;

		//char_t* line = new char_t[eLineLength+1];
		//char* data = new char[eMaximizeChunk+1];
		size_t __count, __vertex, __index, __square = 0;
		char __texnames[eSizeofDesc];
		__texnames[0] = eNull;
		__texnames[eDescLength] = eNull;
		char* __ptexnames = __texnames;

		const char __slash = '/';
		const char __header = 'H';
		const char __space = 0x20;
		const char __child = '{';
		const char __parent = '}';
		const char __temp = 't';
		const char __frame = 'F';
		const char __mesh = 'M';
		const char __animation = 'A';
		const char* __coords = "7MeshT";
		const char* __material = "MeshM";
		const char* __normals = "MeshN";
		const char* __transform = "FrameT";
		const char __dupli = 'V';
		const char __xskin = 'X';
		const char __quote = '"';

		LPMODELHEADER __modelHeader	= new MODELHEADER();
		LPANIMEHEADER __animeHeader = 0;
		LPTEXTUREHEADER __texHeader;
		LPSKINEDMESHHEADER __skinHeader;

		// C++のstreamの転送速度を調べる - 純粋関数型雑記帳
		// http://d.hatena.ne.jp/tanakh/20081223
		// ifstream vs fopen()/fread()/fclose() - GameDev.Net Discussion Forums
		// http://www.gamedev.net/community/forums/topic.asp?topic_id=121193
		// iostream メモ - IT戦記
		// http://d.hatena.ne.jp/amachang/20080928/1222604408

		//_tsetlocale(LC_CTYPE, L"");
		//FILE* fp;
		//_tfopen_s(&fp, name, L"r");
		//nslib::CIFStreamT ifs(STR(G:\\works\\leet\\nsXFile_S\\new_x_file\\Tiny\\tiny_4anim.x));
		CIFStream ifs(_Name);
		//clock_t start = clock();
		//while(f_in.read(data, eMaximizeChunk));	// 2750s, 2735s
		//while(f_in.getline(line, eLineLength));	// 3094s, 3094s
		//while(f_in.rdbuf()->sgetn(data, eMaximizeChunk));	// 2667s
		//while(_fgetts(line, eLineLength, fp));	// 46s, 47s, 579s, 594s
		//while(__src = ifs.getl());					// 609s, 610s
		//clock_t end = clock();

		// xof
		ifs._ignorel();
		char* __src = ifs.getl();
		// header
		if (*__src == __space) {
			ifs._ignorel(5);
		}
		// knot.x
		else {
			if (*__src == __slash) { // comment (compatible with triplane.x, heli.x)
				ifs.ignorelc(__slash);
				__src = ifs.getl();
			}
			// template
			if (*__src == __temp) {
				do {
					ifs.ignorel(__parent); // compatible with triplane.x
				} while (*(__src = ifs.getl()) == __temp); // getls: triplane.x ('A`)
				// header: lobby, tiger, triplane
				if (*__src == __header) {
					ifs.ignorels(__parent); // compatible with triplane.x
				}
			}
		}
		do {// while(__src = ifs.getl())
			if (*__src == __parent) {
			}
			else if (*__src == __frame) {
				++ __modelHeader->nFrame;
				__src = ifs.getls();
				if (!strictlystrstr(__src, __transform)) // triplane.x
					continue;
				// FrameTransformMatrix
				ifs.ignorels(__parent); // parent:Frame
			}
			else if (*__src == __mesh) {
				++ __modelHeader->nMesh;
				__modelHeader->nVertex += __vertex = stoi(ifs._getl());
				ifs._ignorel(__vertex);
				__modelHeader->nIndex += __index = stoi(ifs.getl());
				for (size_t i=__index; i; --i) {
					if (stoi(ifs._getl()) == 4) {
						++ __modelHeader->nSquare;
					}
				}

				// child, data, parent の間に空行が無いこと前提
				while (__src = ifs.getls()) { // parent:mesh, getls:triplane.x
					if (*__src == __parent) { // mesh
						break;
					}
					else if (nslib::strictlystrstr(__src, __material)) {
						__modelHeader->nMaterial += __count = stoi(ifs._getl());
						ifs._ignorel(__index + 1);
						// Material
						for (size_t i=0; i<__count; ++i) {
							++ __modelHeader->nMaterial;
							ifs.getl(); // child: Material
							ifs._ignorel(4);
							// TextureFilename
							__src = ifs.getls(); // child:Filename, parent:Material
							if (*__src != __parent) {
								if (!nslib::findstr(__texnames, __src = ifs.getls()+1, __quote)) {
									*__ptexnames = *__src;
									for (; *__src!=__quote; *++__ptexnames = *++__src); // 15step
									//while (*++__ptexnames = *++__src != __quote);	// 18step
									//while ((*++__ptexnames = *++__src) != __quote); // =↑*2
									*__ptexnames = __slash; // delimiter
									*(++ __ptexnames) = 0;	// terminated
									++ __modelHeader->nTexture;
								}
								ifs._ignorel(2);// parent:Material
							}
						}
						ifs._ignorel(); // parent:MaterialList -> while
					}
					else if (strictlystrstr(__src, __coords)) {
						ifs._ignorel(__vertex + 2);
					}
					else if (*__src == __dupli) {
						ifs._ignorel(__vertex + 3);
					}
					else if (strictlystrstr(__src, __normals)) {
						ifs._ignorel(stoi(ifs._getl()));	// furea.x
						ifs.getl();
						ifs._ignorel(__index + 1);
						//ifs._ignorel(__vertex + 1);
						//ifs.getl();
						//ifs._ignorel(__index + 1);
					}
					else if (*__src == __xskin) {
						++ __modelHeader->nSkinedMesh;
						__skinHeader = new SKINEDMESHHEADER();
						__skinHeader->nBoneAll += __count = stoi(ifs._getl(3));
						__skinHeader->nVertexAll += __vertex;
						ifs._ignorel();
						size_t total = 0;
						for (; __count; --__count) {
							size_t weight = stoi(ifs._getl(4));
							total += weight;
							ifs._ignorel(weight*2+2);
						}
						__skinHeader->nWeightAll += total;
					}
					else {
						ifs.ignorels(__parent);
					}
				}
				continue;
			}
			else if (*__src == __animation) {
				++ __modelHeader->nAnimationSet;
				if (!__animeHeader)
					__animeHeader = new ANIMEHEADER();

				while (*ifs.getls() != __parent) {	// child:Animation, parent:ASet
					++ __animeHeader->nAnimation;
					if (*(__src = ifs.getls()) == __child) {	// name, child:AnimationKey
						ifs._ignorel();	// child:AnimationKey
					}
					do {
						switch (stoi(ifs._getl())) {
							case eAnimationQuatanion:
								++ __animeHeader->nQuat;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nQuatList += __count;
								__animeHeader->nMatList += __count;
								break;
							case eAnimationScale:
								++ __animeHeader->nScale;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nVecList += __count;
								break;
							case eAnimationSlide:
								++ __animeHeader->nSlide;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nVecList += __count;
								break;
							case eAnimationMatrix:
								++ __animeHeader->nMat;
								__animeHeader->nTimeList += __count = stoi(ifs._getl());
								__animeHeader->nMatList += __count;
						}
						ifs._ignorel(__count+1); // parent:AnimetionKey
						if (*(__src = ifs.getls()) == __child) { // name, child:AKey, parent:Animation
							ifs._ignorel(); // parent:Animation
							break;
						}
					} while (*__src != __parent); // child:AKey, parent:Animation
				}
			}
		} while(__src = ifs.getls());
		//clock_t end = clock();	// char 46ms:250 296, wchar_t 625ms:281 906

		__modelHeader->nIndex += __modelHeader->nSquare;
		char_t *pass = __modelHeader->szPass;
		nslib::GetFilePass(pass, _Name);
		size_t __textures = __modelHeader->nTexture;
		if (__textures == 1) {
			__texHeader = new TEXTUREHEADER();
			*--__ptexnames = 0;
			mbstowcs(__texHeader->szTexName, __texnames, eSizeofName);
			ns_d3d::Cd3d::Instance().CreateTexture(__texHeader->szTexName, &__texHeader->pTexture, pass);
		}
		else if (__textures) {
			__texHeader = new TEXTUREHEADER[__textures]();
			LPTEXTUREHEADER __postex = __texHeader;
			char_t __temptexnames[eSizeofDesc],
				*__ptmp = __temptexnames, *__pout = __texHeader->szTexName;
			mbstowcs_s(NULL, __temptexnames, eSizeofDesc, __texnames, _TRUNCATE);
			for (size_t i=0; i<__textures; ++i, __pout = (++__postex)->szTexName) {
				*__pout = *__ptmp;
				for (; *__ptmp!=__slash; *++__pout = *++__ptmp);
				*__pout = 0;
				ns_d3d::Cd3d::Instance().CreateTexture(__postex->szTexName, &__postex->pTexture, pass);
			}
		}

		CModel* __model;
		if (__modelHeader->nSkinedMesh) {
			CSkinedMeshModel* __skinedModel = new CSkinedMeshModel(__modelHeader, __animeHeader, __skinHeader, __texHeader);
			CSkinedMeshModelInit* __initModel = new CSkinedMeshModelInit();
			if (!__initModel->CreateFromStream(__skinedModel, ifs, __texnames)) {
				__skinedModel->Release();
				__initModel->Release();
				return NULL;
			}
			__initModel->Release();
			__model = __skinedModel;
		}
		else {
			__model = new CModel(__modelHeader, __animeHeader, __texHeader);
			CModelInit* __initModel = new CModelInit();
			if (!__initModel->CreateFromStream(__model, ifs, __texnames)) {
				__model->Release();
				__initModel->Release();
				return NULL;
			}
			__initModel->Release();
		}

		return __model;
	}
/*
		while (_fgetts(line, eLineLength, fp))
		{
			if (!strchr_t(line, child)) {// || strchr_t(line, parent) || line==10)
				continue;
			}
			else if (strstr_t(line, temp)) {
				f_in.ignore(ns_types::eSizeofDesc, parent);
				f_in.ignore(2);	// to next line
				continue;
			}
			else if (strstr_t(line, frame)) {
				++ pModelHead->nFrame;
			}
			else if (strstr_t(line, mesh)) {
				++ pModelHead->nMesh;
				f_in.getline(line, eLineLength);
				pModelHead->nVertex += term = atoi_t(line);
				++ term;
				for (i=0; i<term; ++i) {
					f_in.getline(line, eLineLength);
				}
				pModelHead->nIndex += term = atoi_t(line);
				for (i=0; i<term; ++i) {
					f_in.getline(line, eLineLength);
					if (4 == atoi_t(line)) {
						++ pModelHead->nSquare;
					}
				}

				while (f_in.getline(line, eLineLength))
				{
					if (!strchr_t(line, child)) {
						continue;
					}
					//else if (strstr_t(line, coords)) {
					//}
					else if (strstr_t(line, material)) {
						f_in.getline(line, eLineLength);
						term = atoi_t(line);
						f_in.ignore(eSizeofDesc, child);
						f_in.ignore(2);
						for (i=0; i<term; ++i)
						++ pModelHead->nMaterial;
					}
					else if (strchr_t(line, parent)) {
						break;
					}
					f_in.ignore(ns_types::eSizeofDesc, parent);
					f_in.ignore(2);
				}
			}
			f_in.ignore(ns_types::eSizeofDesc, parent);
			f_in.ignore(2);
		}
	}

*/
	//--------------------------------------------------------
	//	Name:	CModelMgr::GetModel
	//	Desc:	モデルデータの読み込み。
	//--------------------------------------------------------
	CModel* CModelMgr::GetModel( const wchar_t* name )
	{
		///////////////////////////////////// ファイルオープン /////////////////////////////////////
#ifdef	_UNICODE
		std::wifstream		f_in;		// ストリーム
#else
		std::ifstream		f_in;
#endif

		// ファイルを開く
		f_in.open( name, std::ios::binary | std::ios::ate );
		if( !f_in )
			return NULL;

		char_t*		pData, *pBuf;	// データバッファ
		std::streamsize	s_size;		// ストリームサイズ

		// ファイルサイズ
		s_size	= f_in.tellg();

		// 先頭へシーク
		f_in.seekg( std::ios::beg );

		// メモリを確保
		pData	= new char_t[ s_size ];
		pBuf	= new char_t[ s_size ];

		// ファイル読み込み
		f_in.read( pData, s_size );
		memcpy_t( pBuf, s_size, pData, s_size );

		// ファイルを閉じる
		f_in.close();
		/////////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////// データ読み込み ///////////////////////////////////////
		const char_t	szParent[]		= STR({);
		const char_t	szChild[]		= STR(});
		const char_t	szCoron[]		= STR(;);
		const char_t	szLastToken[]	= STR(;;);
		char_t	szDoubleQuatation[2];
		char_t	szSubTokens[4];
		char_t*	pMainToken, *pSubToken, *pNextToken = pData;
		const char_t	szFrame[]		= STR(Frame);
		const char_t	szMesh[]		= STR(Mesh);
		const char_t	szCoords[]		= STR(Coords);
		const char_t	szMaterial[]	= STR(Material);
		const char_t	szFilename[]	= STR(Filename);
		const char_t	szSkin[]		= STR(Skin);
		const char_t	szAnimation[]	= STR(Animation);
		size_t	nSquare			= 0, nVertexNum=0;
		LPMODELHEADER		pModelHead	= new MODELHEADER();
		//memset( pModelHead, '\0', sizeof( MODELHEADER ) );
		LPANIMEHEADER		pAnimeHead	= NULL;
		LPSKINEDMESHHEADER	pSkinHead	= NULL;
		LPTEXTUREHEADER		pTexHeads	= NULL;
		std::wstring		textures;
		//std::vector<std::wstring> TexNames;

		szDoubleQuatation[0]	= 0x22;
		szDoubleQuatation[1]	= '\0';
		szSubTokens[0]	= 0x20;
		szSubTokens[1]	= 0x0d;
		szSubTokens[2]	= 0x0a;
		szSubTokens[3]	= '\0';

		strcpy_t( pModelHead->szName, name );

		// トークンループ
		while ( pMainToken = strtok_t( NULL, szChild, &pNextToken ) )
		{
			if ( !( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) ) )
			{
				// 親子関係処理
				continue;
			}

			if ( strstr_t( pSubToken, szFrame ) )
			{
				// Frame処理
				++ pModelHead->nFrame;
			}
			else if ( strstr_t( pSubToken, szMesh ) )
			{
				// Mesh処理
				++ pModelHead->nMesh;
				pMainToken	= strstr_t( pMainToken, szParent ) + 1;	// Mesh名
				pModelHead->nVertex	+= nVertexNum = atoi_t( pMainToken );	// 頂点数
				pMainToken	= strstr_t( pMainToken, szLastToken ) + 2;
				pModelHead->nIndex	+= atoi_t(strtok_t( NULL, szCoron, &pMainToken ));		// インデックス数

				// 四角形ポリゴンの数
				for ( size_t iIndex=0; iIndex<pModelHead->nIndex; ++iIndex )
				{
					if ( 4 == atoi_t(strtok_t( NULL, szCoron, &pMainToken )) )
						++ nSquare;
					strtok_t( NULL, szSubTokens, &pMainToken );
				}
				//pMainToken	= strstr_t( pMainToken, szLastToken ) + 2;

				do
				{
					if ( !( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) ) )
						break;
					else if ( strstr_t( pSubToken, szCoords ) )
					{
						// Texture coords処理
					}
					else if ( strstr_t( pSubToken, szMaterial ) )
					{
						size_t	nMtrl		= 0;

						// Material list処理
						pMainToken	= strstr_t( pMainToken, szParent ) + 1;	// Material名
						nMtrl		= atoi_t( pMainToken );
						pMainToken	= strstr_t( pMainToken, szParent ) + 1;

						for ( size_t iMtrl=0; iMtrl<nMtrl; ++iMtrl )
						{
							// Material処理
							++ pModelHead->nMaterial;
							if ( iMtrl > 0 )
								pMainToken	= strtok_t( NULL, szChild, &pNextToken );
							for ( size_t jMtrl=0; jMtrl<3; ++jMtrl )
								pMainToken	= strstr_t( pMainToken, szLastToken ) + 2;

							if ( ( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) )
								&& strstr_t( pSubToken, szFilename ) )
							{
								// Texture file name処理
								strtok_t( NULL, szDoubleQuatation, &pMainToken );
								pSubToken	= strtok_t( NULL, szDoubleQuatation, &pMainToken );
								if ( textures.empty()||(std::string::npos==textures.find(pSubToken)) )
								{
									textures.append(pSubToken);
									textures.push_back('\0');
									++pModelHead->nTexture;
								}
								//if ( TexNames.size() )
								//{
								//	size_t i=0;
								//	for ( ;i<TexNames.size(); ++i )
								//	{
								//		if ( strstr_t( TexNames[i].c_str(), pSubToken ) )
								//			break;
								//	}
								//	if ( i==TexNames.size() )
								//	{
								//		Name = pSubToken;
								//		TexNames.push_back( Name );
								//	}
								//}
								//else
								//{
								//	Name = pSubToken;
								//	TexNames.push_back( Name );
								//}

								strtok_t( NULL, szChild, &pNextToken );
							}
						}
						pNextToken	= strstr_t( pNextToken, szChild );
					}
					else if ( strstr_t( pSubToken, szSkin ) )
					{
						size_t		nBone	= 0;
						// Skin mesh処理
						++ pModelHead->nSkinedMesh;
						if ( !pSkinHead )
						{
							pSkinHead	= new SKINEDMESHHEADER();
							memset( pSkinHead, '\0', sizeof( SKINEDMESHHEADER ) );
						}
						pSkinHead->nVertexAll	+= nVertexNum;
						pMainToken			= strstr_t( pMainToken, szCoron ) + 1;
						pMainToken			= strstr_t( pMainToken, szCoron ) + 1;
						nBone				= atoi_t( pMainToken );	// ボーン数
						pSkinHead->nBoneAll	+= nBone;

						// Skin weightsループ
						for ( size_t iBone=0; iBone<nBone; ++iBone )
						{
							pMainToken				= strtok_t( NULL, szChild, &pNextToken );
							pMainToken				= strstr_t( pMainToken, szCoron ) + 1;	// 頂点数
							pSkinHead->nWeightAll	+= atoi_t( pMainToken );
						}
					}
				} while ( pMainToken = strtok_t( NULL, szChild, &pNextToken ) );
			}
			else if ( strstr_t( pSubToken, szAnimation ) )	// Animation set
			{
				size_t	nType, nTime;
				char_t	szSet[]	= STR(AnimationSet);
				char_t	szKey[]	= STR(Key);
				if (pAnimeHead) {
					pAnimeHead		= new ANIMEHEADER();
				}
				memset( pAnimeHead, '\0', sizeof( ANIMEHEADER ) );

				// Animation set処理
				++ pModelHead->nAnimationSet;
				pMainToken		= strstr_t( pMainToken, szParent ) + 1;
				strtok_t( NULL, szParent, &pMainToken );	// Animation

				// Animationループ
				do
				{
					++ pAnimeHead->nAnimation;
					if ( strstr_t( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ), szParent ) )
					{
						// Animation name処理
						pMainToken	= strtok_t( NULL, szChild, &pNextToken );
						pSubToken	= strtok_t( NULL, szSubTokens, &pMainToken );
					}

					// Animation keyループ
					while ( pSubToken && strstr_t( pSubToken, szKey ) )
					{
						// Animation key処理
						++ pMainToken;
						nType	= atoi_t( strtok_t( NULL, szSubTokens, &pMainToken ) );
						nTime	= atoi_t( strtok_t( NULL, szSubTokens, &pMainToken ) );
						pAnimeHead->nTimeList	+= nTime;
						switch ( nType )
						{
						case eAnimationQuatanion:
							{	// Quatanion処理
								++ pAnimeHead->nQuat;
								pAnimeHead->nQuatList	+= nTime;
								pAnimeHead->nMatList	+= nTime;
							} break;

						case eAnimationScale:
							{	// Scale処理
								++ pAnimeHead->nScale;
								pAnimeHead->nVecList	+= nTime;
							} break;

						case eAnimationSlide:
							{	// Slide処理
								++ pAnimeHead->nSlide;
								pAnimeHead->nVecList	+= nTime;
							} break;

						case eAnimationMatrix:
							{	// Matrix処理
								++ pAnimeHead->nMat;
								pAnimeHead->nMatList	+= nTime;
							} break;
						}

						pMainToken	= strtok_t( NULL, szChild, &pNextToken );
						pSubToken	= strtok_t( NULL, szSubTokens, &pMainToken );
					}

					if ( !pSubToken )
						pMainToken	= strtok_t( NULL, szParent, &pNextToken );
					else if ( strstr_t( pSubToken, szParent ) )
					{
						// Animation name処理
						pMainToken	= strtok_t( NULL, szSubTokens, &( pNextToken += 4 ) );
					}

					if ( !strstr_t( pMainToken, szAnimation ) )	// Animation
					{
						if ( ( pMainToken = strtok_t( NULL, szSubTokens, &pNextToken ) )
							&& strstr_t( pMainToken, szSet ) )
						{
							// Animation set処理
							++ pModelHead->nAnimationSet;
							strtok_t( NULL, szParent, &pNextToken );	// Animation
							strtok_t( NULL, szParent, &pNextToken );	// AnimationKey
						}
						else
							break;
					}
				} while ( pMainToken = strtok_t( NULL, szChild, &( ++pNextToken ) ) );
			}
		}

		//delete [] pData;
		pData	= pBuf;

		// 修正
		pModelHead->nIndex += nSquare;

		// テクスチャ
		//pModelHead->nTexture	= TexNames.size();
		size_t nTexture = pModelHead->nTexture;
		if ( 1==nTexture )
		{
			pTexHeads	= new TEXTUREHEADER;
			GetName(pTexHeads->szTexName, textures.c_str());
			ns_d3d::Cd3d::Instance().CreateTexture(pTexHeads->szTexName, &pTexHeads->pTexture, name);
		}
		else if ( nTexture )
		{
			pTexHeads	= new TEXTUREHEADER[ nTexture ];
			//memset( pTexHeads, '\0', sizeof( TEXTUREHEADER )*pModelHead->nTexture );
			LPTEXTUREHEADER	pTexPos = pTexHeads;
			const char_t* pName = textures.c_str();
			for ( size_t i=0; i<nTexture; ++i, ++pTexPos )
			{
				GetName(pTexPos->szTexName, pName);
				//strcpy_t( pTexPos->szTexName, eSizeofName, TexNames[i].c_str() );
				ns_d3d::Cd3d::Instance().CreateTexture( pTexPos->szTexName, &pTexPos->pTexture, name );
				pName = strchr_t(pName, '\0')+1;	// seek to the next name
			}
		}


		/////////////////////////////////// モデル生成 ///////////////////////////////////////

		CModel* pModel			= NULL;

		if ( pModelHead->nSkinedMesh )
		{
			CSkinedMeshModel* pSkinedMesh		= new CSkinedMeshModel( pModelHead, pAnimeHead, pSkinHead, pTexHeads );
			CSkinedMeshModelInit* pModelInit	= new CSkinedMeshModelInit();
			if( !pModelInit->Create( pSkinedMesh, pData, nSquare ) )
			{
				pSkinedMesh->Release();
				pModelInit->Release();
				return NULL;
			}
			pModelInit->Release();
			pModel	= pSkinedMesh;
		}
		else
		{
			CModel* pStanderdModel = new CModel( pModelHead, pAnimeHead, pTexHeads );
			CModelInit* pModelInit	= new CModelInit();
			if( !pModelInit->Create( pStanderdModel, pData, nSquare ) )
			{
				pStanderdModel->Release();
				pModelInit->Release();
				return NULL;
			}
			pModelInit->Release();
			pModel	= pStanderdModel;
		}

		return pModel;
	}

	void CModelMgr::Landing() {
		// 必要なもの
		// ローカル座標と当たり判定の半径
		//m_FloorModel->m_pModelHead->vRadius;	// 当たり判定ベクトル
		// 座標を与えると床の高さを返す関数
		//ModelPool::_Inst().
		//for (FloorObjects::iterator &ite = m_Floors.begin(); !!ite; ++ ite) {
		//	*ite;
		//}
		WorldOrg::Instance().Landing(&m_CharaLockedID->matrix, m_CharaLocked->GetRadiies());
	}

	void CWorldOrganizer::Landing(LPMATRIX mtx_pos, const LPVECTOR3 vec_point) {
		if (m_fallingout == 1) {
			float vy = Falling();
			float ty = mtx_pos->_42 - vy;
			if (m_scream == 0 && ty < -5.0f) {
				m_scream = 1;
				nsgl::CSceneBuilder::Instance().PlaySoundW();
			}
			if (ty < -50.0f) {
				nsgl::CPlayerController::ResetScene();
				mtx_pos->_42 = 20.0f;
				m_fallingout = 0;
				m_scream = 0;
				return;
			}
			mtx_pos->_42 = ty;
			return;
		}

		size_t polygons = m_Land->m_pModelHead->nIndex;
		LPCUSTOMVERTEX pos_vtx = m_Land->m_pVertices,
						vtx_begin = pos_vtx;
		LPVECTOR3 p0 = NULL, p1 = NULL, p2 = NULL;
		VECTOR3 tx, td, t0, t1, d0, d1, x(mtx_pos->_41, mtx_pos->_42 + vec_point->y, mtx_pos->_43), normal, xtop;
		word_t* pos_idx = m_Land->m_pwIndices;
		float c1, c2, c3, t0xt1, h;
		size_t i = 0;
		for (; i<polygons; i++) {
			if ((vtx_begin + *pos_idx)->normal.y <= 0) {
				pos_idx += 3;
				continue;
			}
			p0 = &(vtx_begin + *pos_idx)->position;
			tx = x - *p0;
			pos_idx++;
			p1 = &(vtx_begin + *pos_idx)->position;
			t0 = *p1 - *p0;
			pos_idx++;
			p2 = &(vtx_begin + *pos_idx)->position;
			t1 = *p2 - *p0;

			c1 = tx.z * t0.x - t0.z * tx.x;
			t0xt1 = t1.z * t0.x - t0.z * t1.x;	// normal
			pos_idx++;
			if ((t0xt1 == 0) || ((c1 * t0xt1) < 0)) { // d1xd0==0 verticly poly
				continue;
			}
			tx = x - *p1;
			td = *p2 - *p1;
			c2 = tx.z * td.x - td.z * tx.x;
			if ((c2 * t0xt1) < 0) {
				continue;
			}
			tx = x - *p2;
			td = *p0 - *p2;
			c3 = tx.z * td.x - td.z * tx.x;
			if ((c3 * t0xt1) < 0) {
				continue;
			}
			break;
		}
		if (i == polygons) {
			if (m_fallstart == 0) {
				m_fallstart = clock();
			}
			m_fallingout = 1;
			float vy = Falling();
			mtx_pos->_42 -= vy;
			return;
		}

		tx = x - *p0;
		if (tx.y < 0) {
			nsgl::CPlayerController::DeniedMove();
			return;
		}
		ns_model::VectorCross(&normal, &t1, &t0);
		h = (-1.0f * ((normal.x * tx.x) + (normal.z * tx.z)) + (normal.y * p0->y)) / normal.y;
		h += vec_point->y;
		if (m_vjump > 0) {
			float vy = Falling();
			m_vjump -= vy;
			float ty = mtx_pos->_42 + m_vjump;
			if (ty > h) {
				h = ty;
			}
		} else if (m_fallstart > 0) {
			float vy = Falling();
			float ty = mtx_pos->_42 - vy;
			if (ty > h) {
				h = ty;
			}
			else {
				m_fallstart = 0;
				if (m_vjump < 0) {
					m_vjump = .0f;
					nsgl::CPlayerController::Jumped();
				}
			}
		} else if (mtx_pos->_42 > h) {
			m_fallstart = clock();
			return;
		}
		mtx_pos->_42 = h;
		m_index = i;
	}

	float CWorldOrganizer::Falling()
	{
		m_falltime = clock();
		float t = static_cast<float>(m_falltime - m_fallstart) / CLOCKS_PER_SEC;
		float txt = t * t;
		return m_gravity * txt / 4.0f;	// 1/2gt^2
	}

	void CWorldOrganizer::RegisterLand(const char* name) {
		CModel* __model = ModelMgr::Instance().CallFactory(name);
		if (__model == NULL) {
			return;
		}
		m_LandID = new LANDID;
		m_Land = __model;
	}

}	// namespace ns_model