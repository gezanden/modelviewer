#pragma	once
#ifndef	_NSNULLTYPE_H_
#define	_NSNULLTYPE_H_


namespace ns_types
{

	/////////////////////////////////////////////////
	//	NullType, EmptyType
	//	Modern C++ Design - P.42
	class	NullType	// 生成できない、意味の無い型。
	{
	public:
		struct Head { private: Head(); };
		struct Tail { private: Tail(); };
	};
	struct	EmptyType {};	// 生成、継承できる。

}

#endif	// _NSNULLTYPE_H_