#include "nstypes.h"
#include <typeinfo.h>

namespace ns_types
{

	//////////////////////////////////////////////////////////
	//	CTypeInfo
	//	Modern C++ Design - P.40
	class TypeInfo
	{
	public:
		TypeInfo();
		TypeInfo( const std::type_info& );
		TypeInfo( const TypeInfo& );
		TypeInfo& operator=( const TypeInfo& );

		bool before( const TypeInfo& ) const;
		const ns_types::char_t* name() const;

	private:
		const std::type_info*	m_pInfo;
	};

	bool operator==( const TypeInfo&, const TypeInfo& );
	bool operator!=( const TypeInfo&, const TypeInfo& );
	bool operator<( const TypeInfo&, const TypeInfo& );
	bool operator<=( const TypeInfo&, const TypeInfo& );
	bool operator>( const TypeInfo&, const TypeInfo& );
	bool operator>=( const TypeInfo&, const TypeInfo& );

}