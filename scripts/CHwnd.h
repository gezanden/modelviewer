//
//  CHwnd.hpp
//
//  Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once
#ifndef	_CHWND_H_
#define	_CHWND_H_

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
// including ns_system::CSingleton;
#include	"Cd3d.h"
// including ns_lib::NSSingleLinkedList;
#include	"nslist.h"
#include	"NSFunctor.h"


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_win
{


	//--------------------------------------------------------
	//	Enum
	//--------------------------------------------------------
	// Window IDs
	enum
	{
		eMAIN_WINDOW,
	};



	//--------------------------------------------------------
	//	Structures
	//--------------------------------------------------------
	typedef struct _WINDOWHANDLENODE_
	{
		bool	bd3dDevice;
		size_t	nID;
		HWND	hWnd;
		RECT	rect;
		_WINDOWHANDLENODE_()
			: bd3dDevice( false )
			, nID( 0 )
			, hWnd()
			, rect() {}
		_WINDOWHANDLENODE_( size_t id )
			: bd3dDevice( false )
			, nID( id )
			, hWnd()
			, rect() {}
	} HWNDNODE, *LPHWNDNODE;



	//--------------------------------------------------------
	//	Classes
	//--------------------------------------------------------
	class CHwnd : public ns_singleton::CSingleton< CHwnd >
	{
		typedef struct _NS_WNDPROC_NODE_
		{
			LRESULT (__stdcall *WndProc)(HWND, UINT, WPARAM, LPARAM);
			_NS_WNDPROC_NODE_(LRESULT (__stdcall *Proc)(HWND, UINT, WPARAM, LPARAM))
				: WndProc(Proc) {}
		} WNDPROCNODE, *LPWNDPROCNODE;

		typedef ns_lib::CSinglyLinkedList<HWNDNODE> HwndList;
		// __w64が Type List で無効化される
		//typedef ns_lib::CSinglyLinkedList<ns_lib::CFunctor<int, TYPELIST_4(HWND, UINT, __w64 unsigned int, __w64 long)>> ProcList;
		typedef ns_lib::CSinglyLinkedList<WNDPROCNODE> ProcList;

	public:
		CHwnd()
			//: m_nCount( 0 )
			//, m_pNode( 0 )
			: m_pMainClassName( STR(D3D Tutorial) )
			, m_pMainWindowName( STR(NS X FILE VIEWER) )
			, m_HwndList()
			, m_ProcList()
		{}
		explicit CHwnd( const char*, const char* );
		~CHwnd();
		bool GenerateWindow( const size_t id, const HINSTANCE hInst );
		bool Generated3dDevice( const size_t id )
		{
			return	( ns_d3d::Cd3d::Instance().CreateDevice( CallHandle(id)->hWnd ) ) ? true : false;
		}
		size_t GetWindowWidth()
		{
			return	eMainWindowWidth;
		}
		size_t GetWindowHeight()
		{
			return	eMainWindowHeight;
		}
		void AddWndHook(LRESULT (__stdcall *WndProc)(HWND, UINT, WPARAM, LPARAM))
		{
			this->m_ProcList.push_back(WndProc);
		}
		HWND GetWindowHandle()
		{
			return m_HwndList.begin()->hWnd;
		}
		//void Release()
		//{
		//	ns_d3d::Cd3d::Instance().Release();
		//	CHwnd::Instance().ReleaseSingleton();
		//}
		//static CHwnd& Instance()
		//{
		//	return	CHwnd::GetSingleton();
		//}

	private:
		bool CreateWndClassEx( HINSTANCE, WNDPROC, const char_t*, dword_t );
		static LRESULT WINAPI MsgProcMain( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
		void AddHandle( const size_t id )
		{
			m_HwndList.push_back(id);
			//++ m_nCount;
		}
		HwndList::iterator CallHandle( const size_t id );

		// Main Window Size
		enum
		{
			eMainWindowX		= 100,
			eMainWindowY		= 100,
			eMainWindowWidth	= 640,
			eMainWindowHeight	= 480,
		};

		//size_t				m_nCount;
		//LPHWNDNODE			m_pNode;
		const char_t* const	m_pMainClassName;
		const char_t* const	m_pMainWindowName;
		HwndList			m_HwndList;
		ProcList			m_ProcList;
	};

}	// namespace: ns_win

#endif	// _CHWND_H_