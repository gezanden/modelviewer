#pragma once
#ifndef _NS_C_SOUND_H_
#define _NS_C_SOUND_H_

#pragma comment(lib, "winmm.lib")

#include "nswin.h"
#include <mmsystem.h>

namespace nsl {

	class CSound
	{
	public:
		CSound();
		~CSound();
		void LoadWave(const char* name);
		void PlayWave(HWND hWnd);

	private:
		CSound(CSound&);
		CSound& operator=(CSound&);

		enum eSound {
			eWaveBufferSize = 4,
		};

		unsigned char* _name;
		unsigned char* _wave;
		unsigned char* _pos_wave;
		long _len_wave;
		int _num_playbuf;

		unsigned char* _tbl_buf[eWaveBufferSize];
		WAVEHDR _tbl_wavehdr[eWaveBufferSize];
		WAVEFORMATEX _wave_fmtex;
		HWAVEOUT _wave_out;

		int _bufsize;
		unsigned char* _last_wave;
	};

}

#endif // _NS_C_SOUND_H_