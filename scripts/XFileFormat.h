#ifndef _NS_X_FILE_FORMAT_H_
#define _NS_X_FILE_FORMAT_H_

#include "nstypes.h"

// Tokens are written as little-endian WORDs.
// The following list of token values is divided
//  into record-bearing and stand-alone tokens.
// The record-bearing tokens are defined as follows.
#define TOKEN_NAME         1
#define TOKEN_STRING       2
#define TOKEN_INTEGER      3
#define TOKEN_GUID         5
#define TOKEN_INTEGER_LIST 6
#define TOKEN_FLOAT_LIST   7

// The stand-alone tokens are defined as follows.
#define TOKEN_OBRACE		10
#define TOKEN_CBRACE		11
#define TOKEN_OPAREN		12
#define TOKEN_CPAREN		13
#define TOKEN_OBRACKET		14
#define TOKEN_CBRACKET		15
#define TOKEN_OANGLE		16
#define TOKEN_CANGLE		17
#define TOKEN_DOT			18
#define TOKEN_COMMA			19
#define TOKEN_SEMICOLON		20
#define TOKEN_TEMPLATE		31
#define TOKEN_WORD			40
#define TOKEN_DWORD			41
#define TOKEN_FLOAT			42
#define TOKEN_DOUBLE		43
#define TOKEN_CHAR			44
#define TOKEN_UCHAR			45
#define TOKEN_SWORD			46
#define TOKEN_SDWORD		47
#define TOKEN_VOID			48
#define TOKEN_LPSTR			49
#define TOKEN_UNICODE		50
#define TOKEN_CSTRING		51
#define TOKEN_ARRAY			52


#define MAX_STRING			128
#define MAX_INT_LIST		128
#define MAX_FLOAT_LIST		128

namespace nsgl {
	
#pragma pack(1)
	struct stXHeader
	{										// size			desc
		char signature[4];		// 4		Magic Number (required) "xof "
		char minor[2];			// 2		Minor Version	03
		char major[2];			// 2		Major Version	02
		char type[4];			// 4		Format Type (required)
											// "txt " Text File
											// "bin " Binary File
											// "tzip" MSZip Compressed Text File
											// "bzip" MSZip Compressed Binary File
		char accuracy[4];		// 4		Float Accuracy "0032" 32 bit or "0064" 64 bit
	};
	
	struct stTokenName
	{
		unsigned short	token;				// token_name 
		unsigned int	count;				// Length of name field, in bytes 
		char			name[MAX_STRING];	// array count ASCII name 
	};

	struct stTokenString
	{
		unsigned short	token;				// WORD 2 token_string 
		unsigned int	count;				// DWORD 4 Length of string field in bytes 
		char			string[MAX_STRING];// BYTE array count ASCII string  
		unsigned int	terminator;			// DWORD 4 tOKEN_SEMICOLON or TOKEN_COMMA 
	};

	struct stTokenInt
	{
		unsigned short	token;				// WORD 2 tOKEN_INTEGER 
		unsigned int	value;				// DWORD 4 Single integer 
	};

	struct stTokenGUID
	{
		unsigned short token;				// WORD 2 tOKEN_GUID 
		unsigned short Data1;				// DWORD 4 UUID data field 1 
		unsigned short Data2;				// WORD 2 UUID data field 2 
		unsigned short Data3;				// WORD 2 UUID data field 3 
		unsigned char  Data4[4];			// BYTE array 8 UUID data field 4 
	};

	struct stTokenIntList
	{
		unsigned short token;				// WORD 2 tOKEN_INTEGER_LISt 
		unsigned int   count;				// DWORD 4 Number of integers in list field 
		unsigned int   list[MAX_INT_LIST];	// DWORD 4 x count Integer list  
	};

	struct stTokenFloatList
	{
		unsigned short token;				// WORD 2 tOKEN_FLOAT_LISt 
		unsigned int   count;				// DWORD 4 Number of floats or doubles in list field  
		float		   list[MAX_FLOAT_LIST];// float/double array 4 or 8 x count Float or double list  
											// determined from the header accuracy
	};

#pragma pack()


	class CXFileAsBinary
	{
	public:
#pragma warning(disable:4996)
		explicit CXFileAsBinary(const char* Name)
			: _fp(::fopen(Name, "rb")) {}
#pragma warning(default:4996)
		~CXFileAsBinary() {}
		int Road();

	private:
		CXFileAsBinary(CXFileAsBinary&);
		CXFileAsBinary& operator = (CXFileAsBinary&);

		bool ReadToTokens(FILE* fp);

		FILE* _fp;
	};

}


#endif