#pragma once
#ifndef _NS_FUNC_H_
#define _NS_FUNC_H_

#include "nstypes.h"

namespace nslib {
	using ns_types::char_t;
	using ns_types::err_t;

	bool chrtok(const char_t, const char_t*);
	// strcmptok(str1, str2, delimiter)
	char_t* strstrtok(char_t*, char_t*, const char_t*);
	char_t* strstr_e(char_t**, char_t*);
	char_t* strchr_e(char_t**, const char_t);
	err_t strictlystrstr(const char_t* _Str, const char_t* _Sub);
	err_t findstr(const char_t* _Str, const char_t* _Sub, const char_t _Delim = ns_types::eNull);
	err_t strictlystrstr(const char* _Str, const char* _Sub);
	err_t findstr(const char* _Str, const char* _Sub, const char _Delim = ns_types::eNull);
	//err_t findstr(const char* _Str, const char* _Sub);
	char* simplystrchr(char** _Str, const char _Delim);
	ns_types::err_t strictlystrcmp(const char_t* _Str, const char_t* _Sub);
	void simplymbstowcs(char_t *_Out, const char *_Src);
	void GetFilePass(char_t* _Out, const char* _Src);
	ns_types::word_t stow(char* _Str);
	ns_types::word_t stow(char** _Pstr);
	int stoi(char* _Str);
	int stoi(char** _Pstr);
	float stof32(char* _Str);
	float stof32(char** _Pstr);
	float stof(char* _Str);
	float stof(char** _Pstr);

}	// ns_types

#endif	// _NS_FUNC_H_