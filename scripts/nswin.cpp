//
//	nswin.cpp
//
//	Copyright (c) 2006 Naoki Shimoyamada
//

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include	"nswin.h"


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_win
{


	//------------------------------------------------------------------------------
	//	Name:	CreateWndClassEx()
	//	Desc:	Create the structure window class ex
	//------------------------------------------------------------------------------
	bool CreateWndClassEx( const HINSTANCE hInst, const WNDPROC WndProc, const char_t* ClassName, const dword_t color )
	{
		WNDCLASSEX	wc;
		wc.cbSize = sizeof(WNDCLASSEX);					// 構造体サイズ
		wc.style = CS_HREDRAW | CS_VREDRAW;				// ウィンドウスタイル
		wc.lpfnWndProc = WndProc;						// プロシージャ名
		wc.cbClsExtra = 0;								// メモリ領域の追加設定
		wc.cbWndExtra = 0;								// メモリ領域の追加設定
		wc.hInstance = hInst;							// インスタンス
		wc.hIcon = LoadIcon(0, IDI_APPLICATION);		// アイコン
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);		// カーソル
		wc.hbrBackground = CreateSolidBrush( color );	// 背景色
		wc.lpszMenuName = NULL;							// メニュー名
		wc.lpszClassName = ClassName;					// クラス名
		wc.hIconSm = LoadIcon(0, IDI_APPLICATION);

		return ( !RegisterClassEx(&wc) ? false : true );
	}


	//------------------------------------------------------------------------------
	//	Name:	StrCmp()
	//	Desc:	Create the structure window class ex
	//------------------------------------------------------------------------------
	bool StrCmp( const char_t* Str1, const char_t* Str2 )
	{
		if ( !strcmp_t( Str1, Str2 )
			&& strstr_t( Str1, Str2 ) )
			return	true;
		return	false;
	}

	//-----------------------------------------------------------------------------
	// Name: MsgProcMain()
	// Desc: The main window's message handler
	//-----------------------------------------------------------------------------
	LRESULT WINAPI MsgProcMain(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		switch(msg)
		{
		case WM_DESTROY:
			PostQuitMessage( 0 );
			return 0;
		}
		
		return DefWindowProc( hWnd, msg, wParam, lParam );
	}

}