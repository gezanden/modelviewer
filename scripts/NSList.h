//
//  nslist.hpp
//
//  Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once
#ifndef _NSLIST_H_
#define _NSLIST_H_

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
// Including ns_types::STR();
#include "NSTypes.h"
#include "nswin.h"
// Including TYPELIST Macros;
#include "NSTypeUnits.h"
// Including NSAssert();
#include "NSAssert.h"


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_lib
{
/*
	namespace Private
	{
		template <typename TList>
		struct ListNode;

		template <>
		struct ListNode<ns_types::NullType>
		{
			typedef ns_types::NullType TypeList;
		};

		template <typename P1>
		struct ListNode<TYPELIST_1(P1)>
		{
			typedef TYPELIST_1(P1)	TypeList;
			P1	parm1;
			ListNode(const ListNode& List)
				: parm1(List.parm1)
			{}
			ListNode(const P1& p1)
				: parm1(p1)
			{}
			ListNode& operator=(const ListNode& rhs)
			{
				parm1 = rhs.parm1;
				return	*this;
			}
		};

		template <typename P1, typename P2>
		struct ListNode<TYPELIST_2(P1, P2)>
		{
			typedef TYPELIST_2(P1, P2)	TypeList;
			P1	parm1;
			P2	parm2;
			ListNode(const ListNode& List)
				: parm1(List.parm1)
				, parm2(List.parm2)
			{}
			ListNode(const P1& p1, const P2& p2)
				: parm1(p1)
				, parm2(p2)
			{}
			ListNode& operator=(const ListNode& rhs)
			{
				parm1 = rhs.parm1;
				parm2 = rhs.parm2;
				return	*this;
			}
		};

		template <typename P1, typename P2, typename P3>
		struct ListNode<TYPELIST_3(P1, P2, P3)>
		{
			typedef TYPELIST_3(P1, P2, P3)	TypeList;
			P1	parm1;
			P2	parm2;
			P3	parm3;
			ListNode(const ListNode& List)
				: parm1(List.parm1)
				, parm2(List.parm2)
				, parm3(List.parm3)
			{}
			ListNode(const P1& p1, const P2& p2, const P3& p3, const P4& p4)
				: parm1(p1)
				, parm2(p2)
				, parm3(p3)
			{}
			ListNode& operator=(const ListNode& rhs)
			{
				parm1 = rhs.parm1;
				parm2 = rhs.parm2;
				parm3 = rhs.parm3;
				return	*this;
			}
		};

		template <typename P1, typename P2, typename P3, typename P4>
		struct ListNode<TYPELIST_4(P1, P2, P3, P4)>
		{
			typedef TYPELIST_4(P1, P2, P3, P4)	TypeList;
			P1	parm1;
			P2	parm2;
			P3	parm3;
			P4	parm4;
			ListNode(const ListNode& List)
				: parm1(List.parm1)
				, parm2(List.parm2)
				, parm3(List.parm3)
				, parm4(List.parm4)
			{}
			ListNode(const P1& p1, const P2& p2, const P3& p3, const P4& p4)
				: parm1(p1)
				, parm2(p2)
				, parm3(p3)
				, parm4(p4)
			{}
			ListNode& operator=(const ListNode& rhs)
			{
				parm1 = rhs.parm1;
				parm2 = rhs.parm2;
				parm3 = rhs.parm3;
				parm4 = rhs.parm4;
				return	*this;
			}
		};

		template < typename P1, typename P2, typename P3, typename P4, typename P5>
		struct ListNode<TYPELIST_5(P1, P2, P3, P4, P5)>
		{
			typedef TYPELIST_5(P1, P2, P3, P4, P5)	TypeList;
			P1	parm1;
			P2	parm2;
			P3	parm3;
			P4	parm4;
			P5	parm5;
			ListNode(const ListNode& List)
				: parm1(List.parm1)
				, parm2(List.parm2)
				, parm3(List.parm3)
				, parm4(List.parm4)
				, parm5(List.parm5)
			{}
			ListNode(const P1& p1, const P2& p2, const P3& p3, const P4& p4, const P5& p5)
				: parm1(p1)
				, parm2(p2)
				, parm3(p3)
				, parm4(p4)
				, parm5(p5)
			{}
			ListNode& operator=(const ListNode& rhs)
			{
				parm1 = rhs.parm1;
				parm2 = rhs.parm2;
				parm3 = rhs.parm3;
				parm4 = rhs.parm4;
				parm5 = rhs.parm5;
				return	*this;
			}
		};

		template <typename Node>
		struct SinglyLinkedType
		{
			Node	Node;
			Node*	pNext;
			SinglyLinkedType& operator()()
				return	*this;
			SinglyLinkedType& operator=(const SinglyLinkedType& rhs)
			{
				Node = rhs.Node;
				pNext = rhs.pNext;
				return	*this;
			}
		};

		template <typename Node>
		struct DoublyLinkedType
		{
			Node	Node;
			Node*	pNext;
			Node*	pPrev;
			DoublyLinkedType& operator()()
				return	*this;
			SinglyLinkedType& operator=(const SinglyLinkedType& rhs)
			{
				Node = rhs.Node;
				pNext = rhs.pNext;
				pPrev = rhs.pPrev;
				return	*this;
			}
		};


		template <typename Node>
		Node* GetNode()
		{ return new Node<Node::TypeList>; }


	//////////////////////////////////////////
	// template class CList
	// 任意のノードメンバを持つリスト
	// TList: Typelist型でノードメンバを指定
	// LinkedType: 単方向か双方向か
	template
	<
		class TList,
		class LinkedType = Private::SinglyLinkedType,
	>
	class CList
	{
		using namespace ns_basic;
		typedef Private::ListNode<TList>						NodeEntry;
		typedef LinkedType<NodeEntry >							NodeType;
		typedef Private::GetNode<NodeEntry>						GetFunc;
		typedef Factory<NodeType, int, Functor<NodeEntry, TList> >	NodeFactory;
		// NodeFactory factory
		// factory.Register(0, MyCreator);

	public:
		size_t Size() const
		{ return m_nCount; }
		bool Empty() const
		{ return m_nCount ? false : true; }
		void Clear();
		void Traverse( void( *visit )( NodeEntry& ) );
		bool Retrieve( size_t position, NodeEntry& x ) const;
		bool Replace( size_t position, const NodeEntry& x );
		bool Remove( size_t position, NodeEntry& x );
		void RemoveAll();
		NodeEntry* Insert( size_t position, const NodeEntry& x );
		NodeType* GetPosition( size_t position ) const;
	private:
		size_t		m_nCount;
		NodeType*	m_pNodeList;
	};
	}	// Private
*/

	namespace Private
	{

		//--------------------------------------------------------
		//	Template structure
		//--------------------------------------------------------
		//////////////////////////////////////////////////////////
		//	name:	SINGLYLINKEDLISTNODE
		//
		//	desc:	単方向リスト用構造体
		//
		template <typename Node_entry>
		struct SINGLYLINKEDLISTNODE
		{
			Node_entry*	List;
			SINGLYLINKEDLISTNODE<Node_entry>*	pNext;

			// Constracta
			SINGLYLINKEDLISTNODE()
				: List()
				, pNext( NULL ) {}
			SINGLYLINKEDLISTNODE( Node_entry* item, SINGLYLINKEDLISTNODE<Node_entry>* Link = NULL )
				: List( item )
				, pNext( Link ) {}
		};

	}

	//--------------------------------------------------------
	//	Template class
	//--------------------------------------------------------
	//////////////////////////////////////////////////////////
	//	name:	CSinglyLinkedList
	//
	//	member:	m_nCount			- リストのカウンタ
	//			m_nCurrentPosition	- 最後にアクセスしたリストの位置
	//			m_pCurrent			- 最後にアクセスしたリストのポインタ
	//			m_pEnd				- リストの終端
	//			m_pNodeList			- リストのポインタ
	//
	//	desc:	単方向リストテンプレートクラス
	//			- このクラスを使ってリストを設計する、またはリストにしたいオブジェクトを持つクラスに持たせる。
	//
	//	function:
	//			size		- リストの数を返す
	//			Empty		- リストが空かどうか
	//			Clear		- ※未実装
	//			Traverse	- リストのすべての要素に対して引数で受け取った関数を実行する
	//			Retrieve	- ※未実装
	//			Replace		- ※未実装
	//			Remove		- 指定された位置の要素を削除する
	//			RemoveAll	- リストを空にする
	//			Insert		- 指定された位置に要素を追加する
	//			GetPosition	- 指定された位置の要素を返す
	//
	//	Reference Documents:
	//		・I foget it.
	//
	template <typename List_entry>
	class CSinglyLinkedList
	{
		typedef List_entry									EntryType;
		typedef Private::SINGLYLINKEDLISTNODE<EntryType>	NodeType;
	public:
		CSinglyLinkedList()
			: m_nCount()
			, m_nCurrentPosition()
			, m_Node()
			, m_pNodeList(&m_Node)
			, m_pCurrent()
			, m_pEnd() {}
		~CSinglyLinkedList()
		{
			if ( m_pNodeList )	RemoveAll();
		}
		size_t Size() const
		{
			return	m_nCount;
		}
		bool Empty() const
		{
			return	m_nCount ? false : true;
		}
		void Clear();
		void Traverse( void( *visit )( EntryType& ) );
		bool Retrieve( size_t position, EntryType& x ) const;
		bool Replace( size_t position, const EntryType& x );
		bool Remove( size_t position );
		void RemoveAll();
		void push_back( EntryType* x );
		template<typename other> void push_back(other x) 
		{
			//List_entry* pEntry = x;
			NodeType*	pNode = new NodeType( new EntryType(x) );
			m_pEnd ? m_pEnd->pNext = pNode : m_pCurrent = m_pNodeList = pNode;
			m_pEnd = pNode;
			++ m_nCount;
		}
		EntryType* At( const size_t position );
		EntryType* Insert( size_t position, EntryType* x );
		const EntryType* front() const
		{
			return m_pCurrent;
		}
		const EntryType* back() const
		{
			return m_pEnd;
		}

		template<typename T> class NSIterator
			: public std::iterator<std::forward_iterator_tag, T>
		{
			typedef T* ValueType;
			typedef CSinglyLinkedList CurrentType;
			friend CurrentType;

		public:
			explicit NSIterator() {}
			NSIterator(ValueType vt) : _cont(vt) {}
			NSIterator(CurrentType& list) : _cont(list.m_pNodeList) {}
			~NSIterator() {}
			const EntryType& operator *() const { return *_cont->List; }
			EntryType* operator ->() const { return _cont->List; }
			NSIterator& operator ++() { _cont = _cont->pNext; return *this; }
			bool operator !=(const NSIterator& rhs);
			bool operator !() const { return _cont == NULL; }

		private:
			NSIterator& operator =(CurrentType& vec) {
				_cont = vec._base;
				return *this;
			}
			NSIterator& operator =(EntryType& value) {
				_cont = value;
				return *this;
			}
			NSIterator& operator =(NSIterator&);
			ValueType _cont;
		};
		typedef NSIterator<NodeType> iterator;

		iterator begin() {
			return m_pCurrent;
		}
		iterator end() {
			return m_pEnd;
		}

	private:
		NodeType* GetPosition( size_t position ) const;

		size_t				m_nCount;
		mutable size_t		m_nCurrentPosition;
		NodeType			m_Node;
		NodeType*			m_pNodeList;
		mutable NodeType*	m_pCurrent;
		mutable NodeType*	m_pEnd;
	};

	template <typename List_entry>
	void CSinglyLinkedList<List_entry>::Traverse( void (*visit)(List_entry&) )
	{
		for ( size_t i=0; i<m_nCount; ++i )
			(*visit)(m_pNodeList[i]);
	}

	template <typename List_entry>
	void CSinglyLinkedList<List_entry>::RemoveAll()
	{
		for ( size_t i=0; i<m_nCount; ++i )
		{
			m_pCurrent	= m_pNodeList;
			m_pNodeList	= m_pNodeList->pNext;
			delete m_pCurrent;
		}
		m_nCount = 0;
	}

	template <typename List_entry>
	List_entry* CSinglyLinkedList<List_entry>::Insert( size_t position, List_entry* x = new List_entry )
	{
		if ( (position < 0) || (position > m_nCount) )
			return	false;
		NodeType	*pNew, *pPrevious, *pFollowing;
		if ( !m_nCount )
		{
			pFollowing	= m_pNodeList;
		}
		else if	( position == m_nCount )
		{
			push_back( x );
			return	m_pEnd->List;
		}
		else
		{
			pPrevious	= GetPosition( position-1 );
			pFollowing	= pPrevious->pNext;
		}
		try
		{
			pNew	= new NodeType( x, pFollowing );
		}
		catch ( std::bad_alloc )
		{
			return	false;
		}
		if ( !position )
		{
			m_pNodeList	= pNew;
			m_pEnd		= pNew;
		}
		else
		{
			pPrevious->pNext	= pNew;
			if ( position == m_nCount )
				m_pEnd	= pNew;
		}
		++ m_nCount;
		return	pNew->List;
	}

	template <typename List_entry>
	void CSinglyLinkedList<List_entry>::push_back( EntryType* x )
	{
		//List_entry* pEntry = x;
		NodeType*	pNode = new NodeType( x );
		m_pEnd ? m_pEnd->pNext = pNode : m_pCurrent = m_pNodeList = pNode;
		m_pEnd = pNode;
		++ m_nCount;
	}

	//template <typename other>
	//template <typename List_entry>
	//void CSinglyLinkedList<List_entry>::push_back<other>( other x )
	//{
	//	//List_entry* pEntry = x;
	//	NodeType*	pNode = new NodeType( x );
	//	m_pEnd ? m_pEnd->pNext = pNode : m_pCurrent->pNext = pNode;
	//	m_pEnd = pNode;
	//	++ m_nCount;
	//}

	template <typename List_entry>
	List_entry* CSinglyLinkedList<List_entry>::At( const size_t position )
	{
		return	(position >= m_nCount) ? NULL : m_pCurrent[position].List;
	}

	template <typename List_entry>
	Private::SINGLYLINKEDLISTNODE<List_entry>*	CSinglyLinkedList<List_entry>::GetPosition( size_t position ) const
	{
		if ( position >= m_nCount )
			return	NULL;
		if ( position <= m_nCurrentPosition )
		{
			m_nCurrentPosition	= 0;
			m_pCurrent	= m_pNodeList;
		}
		for ( ; m_nCurrentPosition != position; ++m_nCurrentPosition )
			m_pCurrent	= m_pCurrent->pNext;
		return	m_pCurrent;
	}

	//--------------------------------------------------------
	//	Template structure
	//--------------------------------------------------------

	// 双方向リスト
	namespace Private
	{
		template <typename Node_entry>
		struct DOUBLYLINKEDLISTNODE
		{
			Node_entry	List;
			size_t		Id;
			DOUBLYLINKEDLISTNODE<Node_entry>* pNext, *pPrev;

			// Constracta
			DOUBLYLINKEDLISTNODE()
				: List()
				, Id()
				, pNext()
				, pPrev() {}
			DOUBLYLINKEDLISTNODE( Node_entry item, size_t id,
				DOUBLYLINKEDLISTNODE<Node_entry>* Link_next = 0,
				DOUBLYLINKEDLISTNODE<Node_entry>* Link_prev = 0 )
				: List( item )
				, Id(id)
				, pNext( Link_next )
				, pPrev( Link_prev ) {}
		};
	}


	//--------------------------------------------------------
	//	Template class
	//--------------------------------------------------------

	//////////////////////////////////////////////////////////
	//	name:	CDoublyLinkedList
	//
	//	member:	m_nCount			- リストのカウンタ
	//			m_nCurrentPosition	- 最後にアクセスしたリストの位置
	//			m_pCurrent			- 最後にアクセスしたリストのポインタ
	//			m_pNodeList			- リストのポインタ
	//
	//	desc:	双方向リストテンプレートクラス
	//			- このクラスを使ってリストを設計する、またはリストにしたいオブジェクトを持つクラスに持たせる。
	//
	//	function:
	//			・size		- リストの数を返す
	//			・Empty		- リストが空かどうか
	//			・Clear		- ※未実装
	//			・Traverse	- リストのすべての要素に対して引数で受け取った関数を実行する
	//			・Retrieve	- ※未実装
	//			・Replace	- ※未実装
	//			・Remove	- 指定された位置の要素を削除する
	//			・RemoveAll	- リストを空にする
	//			・Insert	- 指定された位置に要素を追加する
	//			・GetPosition	- 指定された位置の要素を返す
	//
	//	Reference Documents:
	//		・I foget it.
	//
	template <typename List_entry>
	class CDoublyLinkedList
	{
		typedef List_entry									EntryType;
		typedef Private::DOUBLYLINKEDLISTNODE<EntryType>	NodeType;

	public:
		CDoublyLinkedList()
			: m_nCount( 0 )
			, m_nCurrentPosition( 0 )
			, m_pCurrent( 0 )
			, m_pNodeList( 0 ) {}
		~CDoublyLinkedList() {}
		size_t Size() const
		{ return m_nCount; }
		bool Empty() const
		{ return m_nCount ? false: true; }
		void Clear();
		void Traverse( void( *visit )( EntryType& ) );
		bool Retrieve( size_t position, EntryType& x ) const;
		bool Replace( size_t position, const EntryType& x );
		bool Remove( size_t position, EntryType& x );
		void RemoveAll();
		void Append(const EntryType& x, const size_t id = m_nCount + 1);
		EntryType* Insert( size_t position, const EntryType& x, const size_t id = m_nCount+1 );
		NodeType* GetPosition( size_t position ) const;

	private:

		size_t				m_nCount;
		mutable size_t		m_nCurrentPosition;
		mutable NodeType*	m_pCurrent;
		NodeType*			m_pNodeList;
	};

	template <typename List_entry>
	void CDoublyLinkedList<List_entry>::RemoveAll()
	{
		for (size_t i=0; i<m_nCount; +ii)
		{
			m_pCurrent = m_pNodeList;
			m_pNodeList = m_pNodeList->pNext;
			delete	m_pCurrent;
		}
	}

	template <typename List_entry>
	void Append(const List_entry& x, const size_t id = m_nCount +1)
	{
		NodeType*	pNew;
		try
		{
			if ( m_nCount )
			{
				pNew = new NodeType( x, id, NULL, m_pLast );
				m_pLast->pNext = pNew;
			}
			else
				pNew = new NodeType( x, id );
		}
		catch ( std::bad_alloc )
		{
			NSAssert(0, STR(Bad alloc.));
		}
		m_pLast = pNew;
	}

	template <typename List_entry>
	List_entry* CDoublyLinkedList<List_entry>::Insert( const size_t position, const List_entry& x, const size_t id = m_nCount+1 )
	{
		if ( (position < 0) || (position > m_nCount) )
			return	false;
		NodeType	*pNew, *pPrevious, *pFollowing;
		if ( position > 0 )
		{
			pPrevious	= GetPosition( position-1 );
			pFollowing	= pPrevious->pNext;
		}
		else
		{
			if ( m_nCount )
				pFollowing	= GetPosition( 0 );
			else
				pFollowing	= 0;
			pPreceding	= 0;
		}
		try
		{
			pNew	= new NodeType( x, id, pFollowing );
		}
		catch ( std::bad_alloc )
		{
			return	false;
		}
		if ( pPreceding )
			pPreceding->pNext	= pNew;
		if ( !pFollowing )
			pFollowing->pPrev	= pNew;
		m_pCurrent	= pNew;
		m_nCurrentPosition	= position;
		++ m_nCount;
		return	&pNew->List;
	}

	template <typename List_entry>
	Private::DOUBLYLINKEDLISTNODE<List_entry>*
		CDoublyLinkedList<List_entry>::GetPosition( size_t position ) const
	{
		if ( m_nCurrentPosition <= position )
			for ( ; m_nCurrentPosition != position; ++m_nCurrentPosition )
				m_pCurrent	= m_pCurrent->pNext;
		else
			for ( ; m_nCurrentPosition != position; --m_nCurrentPosition )
				m_pCurrent	= m_pCurrent->pPrev;
		return	m_pCurrent;
	}
}

#endif	// _NSLIST_H_