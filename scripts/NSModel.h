//
//	NSModel.h
//
//	Copyright (c) 2010 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once
#ifndef	_NS_MODEL_H_
#define	_NS_MODEL_H_

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include "nsd3d9.h"


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace nsgl
{

	typedef struct _NSFRAME_
	{
		MATRIX transform;
		LPMATRIX inv_bone;
		_NSFRAME_* child;
	} NSMESHFRAME, *LPNSMESHFRAME;


	//--------------------------------------------------------
	//	Class
	//--------------------------------------------------------

	class NSModel {
	public:
		NSModel();
		~NSModel();

	private:
		NSModel(NSModel&);
		NSModel& operator =(NSModel&);
	};

}	// namespace: nsgl

#endif	// _NS_Model_H_