

#include "nsfunc.h"

namespace nslib {

	// chrtok(chr, tok)
	bool chrtok(const char_t chr, const char_t* tok)
	{
		while(*(++tok)==NULL)
			if(chr==*tok)
				return true;
		return false;
	}

	char_t* strstrtok(char_t* str1, char_t* str2, const char_t* delim)
	{
		// 始めのdelimiterを飛ばす
		//while(chrtok(*str1, delim))
		//	++str1;
		while(*str1==NULL&&!chrtok(*str1, delim))
			for(;*str1==*str2;++str1)
				if(*(++str2)==NULL)
					return ++str1;
		return NULL;
	}

	// tokのどれかと一致したら、其の次のpointerを返す
	char_t* strstr_e(char_t** str, char_t* tok)
	{
		char_t* ary = *str,*tkn;
		for(;*ary==NULL;++ary)
			for(tkn=tok;*tkn==NULL;++tkn)
				if(*ary==*tkn)
					return *str=(++ary);
		return NULL;
	}

	char_t* strchr_e(char_t** str, const char_t chr)
	{
		if(*(*str=strchr_t(*str, chr)+1)==chr)
			for(;**str==chr;++*str);
		return *str;
	}


	ns_types::err_t strictlystrstr(const char_t* _Str, const char_t* _Sub)
	{
		if (*_Str == ns_types::eSpace) {
			while (*++_Str == ns_types::eSpace);
		}
		//while (*_Str++ == *_Sub++);	// 18 steps
		if (*_Str == *_Sub) {
			while (*++_Str == *++_Sub); // 13 steps
		}
		return *_Sub == ns_types::eNull ? true : false;
	}
	ns_types::err_t findstr(const char_t* _Str, const char_t* _Sub, const char_t _Delim)
	{
		if (*_Str == ns_types::eNull)
			return false;
		const char_t* __s = _Sub;
		do {
			if (*_Str == *__s) {
				while (*++_Str == *++__s) {
					if (*__s == _Delim)
						return true;
				}
				__s = _Sub;
			}
		} while (*++_Str != ns_types::eNull);
		return false;
	}

	ns_types::err_t strictlystrstr(const char* _Str, const char* _Sub)
	{
		if (*_Str == ns_types::eSpace) {
			while (*++_Str == ns_types::eSpace);
		}
		//while (*_Str++ == *_Sub++);	// 18 steps
		if (*_Str == *_Sub) {
			while (*++_Str == *++_Sub); // 13 steps
		}
		return *_Sub == ns_types::eNull ? true : false;
	}

	ns_types::err_t findstr(const char* _Str, const char* _Sub, const char _Delim)
	{
		if (*_Str == ns_types::eNull)
			return false;
		const char* __s = _Sub;
		do {
			if (*_Str == *__s) {
				while (*++_Str == *++__s);
				if (*__s == _Delim) {
					return true;
				}
				__s = _Sub;
			}
		} while (*++_Str != ns_types::eNull);
		return false;
	}

	//ns_types::err_t findstr(const char* _Str, const char* _Sub)
	//{
	//	if (*_Str == ns_types::eNull)
	//		return false;
	//	const char* __s = _Sub;
	//	do {
	//		if (*_Str == *__s) {
	//			while (*++_Str == *++__s);
	//			if (*__s == ns_types::eNull) {
	//				return true;
	//			}
	//			__s = _Sub;
	//		}
	//	} while (*++_Str != ns_types::eNull);
	//	return false;
	//}

	ns_types::err_t strictlystrcmp(const char_t* _Str, const char_t* _Sub)
	{
		if (*_Str == *_Sub) {
			while (*++_Str == *++_Sub && *_Sub != ns_types::eNull);
		}
		return !(*_Str | *_Sub) ? true : false;
	}


	char* simplystrchr(char** _Str, const char _Delim)
	{
		if (**_Str != _Delim) {
			while (*++*_Str != _Delim);
		}
		return *_Str;
	}


	void simplymbstowcs(char_t* _Out, const char* _Src)
	{
		*_Out = (wchar_t)(unsigned char)*_Src;
		while (*++_Out && (*_Out = (wchar_t)(unsigned char)*++_Src));
	}


	//////////////////////////
	//	_Out の最後の文字が 0x00 であること前提
	void GetFilePass(char_t* _Out, const char* _Src)
	{
		*_Out = (wchar_t)(unsigned char)*_Src;
		if (*++_Src != '.' && *_Src != '\\' && *_Src != ':') {
			*_Out = NULL;
			return;
		}
		*++_Out = *_Src;
		while (*++_Out && (*_Out = (wchar_t)(unsigned char)*++_Src));
		while (*--_Out != ns_types::eBackSlash);
		*++_Out = NULL;
	}

	namespace Private {
		int HasSign(char** _Str)
		{
			char* __s = *_Str;
			if (*__s == ns_types::eSpace) {
				while (*++__s == ns_types::eSpace);
			}
			else if (*__s == ns_types::eTab) {
				while (*++__s == ns_types::eTab);
			}
			if (*__s == '-') {
				*_Str = ++__s;
				return true;
			}
			*_Str = __s;
			return false;
		}
	}

	ns_types::word_t stow(char* _Str)
	{
		int __sign = Private::HasSign(&_Str);
		ns_types::word_t __numeric = 0;
		do {
			__numeric = __numeric*10 + ((ns_types::word_t)*_Str - ns_types::eZero);
		} while (*++_Str > 0x2f && *_Str < 0x3a);
		return __sign ? -__numeric : __numeric;
	}

	ns_types::word_t stow(char** _Pstr)
	{
		int __sign = Private::HasSign(_Pstr);
		ns_types::word_t __numeric = 0;
		char* __s = *_Pstr;
		do {
			__numeric = __numeric*10 + ((ns_types::word_t)*__s - ns_types::eZero);
		} while (*++__s > 0x2f && *__s < 0x3a);
		*_Pstr = ++__s;
		return __sign ? -__numeric : __numeric;
	}

	int stoi(char* _Str)
	{
		int __sign = Private::HasSign(&_Str);
		int __numeric = 0;
		do {
			__numeric = __numeric*10 + ((int)*_Str - ns_types::eZero);
		} while (*++_Str > 0x2f && *_Str < 0x3a);
		return __sign ? -__numeric : __numeric;
	}

	int stoi(char** _Pstr)
	{
		int __sign = Private::HasSign(_Pstr);
		int __numeric = 0;
		char* __s = *_Pstr;
		do {
			__numeric = __numeric*10 + ((int)*__s - ns_types::eZero);
		} while (*++__s > 0x2f && *__s < 0x3a);
		*_Pstr = ++__s;
		return __sign ? -__numeric : __numeric;
	}

	float stof32(char* _Str)
	{
		int __sign = Private::HasSign(&_Str);
		int __numeric = 0, __digit = 1;
		do {
			__numeric = __numeric*10 + ((int)*_Str - ns_types::eZero);
		} while (*++_Str != '.');
		while (*++_Str > 0x2f && *_Str < 0x3a) {
			__numeric = __numeric*10 + ((int)*_Str - ns_types::eZero);
			__digit *= 10;
		}
		return __sign ? (float)-__numeric / __digit : (float)__numeric / (float)__digit;
	}

	float stof32(char** _Pstr)
	{
		int __sign = Private::HasSign(_Pstr);
		int __numeric = 0, __decimal = 1;
		char* __s = *_Pstr;
		do {
			__numeric = __numeric*10 + ((int)*__s - ns_types::eZero);
		} while (*++__s != '.');
		while (*++__s > 0x2f && *__s < 0x3a) {
			__numeric = __numeric*10 + ((int)*__s - ns_types::eZero);
			__decimal *= 10;
		}
		*_Pstr = ++__s;
		return __sign ? (float)-__numeric / __decimal : (float)__numeric / __decimal;
	}

	float stof(char* _Str)
	{
		int __sign = Private::HasSign(&_Str);
		int __numeric = 0, __figure = 1;
		float __decimal = .0f;
		do {
			__numeric = __numeric*10 + ((int)*_Str - ns_types::eZero);
		} while (*++_Str != '.');
		while (*++_Str > 0x2f && *_Str < 0x3a) {
			__figure *= 10;
			__decimal += ((float)*_Str - ns_types::eZero) / __figure;
		}
		return __sign ? (float)-__numeric + __decimal : (float)__numeric + __decimal;
	}

	float stof(char** _Pstr)
	{
		int __sign = Private::HasSign(_Pstr);
		int __numeric = 0, __figure = 1;
		float __decimal = .0f;
		char* __s = *_Pstr;
		do {
			__numeric = __numeric*10 + ((int)*__s - ns_types::eZero);
		} while (*++__s != '.');
		while (*++__s > 0x2f && *__s < 0x3a) {
			__figure *= 10;
			__decimal += ((float)*__s - ns_types::eZero) / __figure;
		}
		*_Pstr = ++__s;
		return __sign ? (float)-__numeric + __decimal : (float)__numeric + __decimal;
	}
}