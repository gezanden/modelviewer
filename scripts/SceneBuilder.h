//
//	SceneBuilder.h
//
//	Copyright (c) 2010 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once
#ifndef	_NS_SCENE_BUILDER_H_
#define	_NS_SCENE_BUILDER_H_

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include "NSSingleton.h"
#include "CHwnd.h"
#include "CSound.h"


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace nsgl
{

	//--------------------------------------------------------
	//	Class
	//--------------------------------------------------------
	class CPlayerController
	{
		//friend ns_d3d::Cd3d;

	public:
		CPlayerController() {}
		~CPlayerController() {}

		static LRESULT WINAPI ControlProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		static void RegisterProtagonist(size_t id, LPMATRIX mtx) {
			_my_id = id;
			_my_mtx = mtx;
			_start_vec.x = mtx->_41;
			_start_vec.y = mtx->_42;
			_start_vec.z = mtx->_43;
		}
		static void Move() {
			_my_mtx->_41 += _input_vec.x;
			_my_mtx->_43 += _input_vec.z;
		}
		static void MoveWithCam() {
			_my_mtx->_41 += _input_vec.x * _my_mtx->_11 + _input_vec.z * _my_mtx->_31;
			_my_mtx->_43 += _input_vec.x * _my_mtx->_13 + _input_vec.z * _my_mtx->_33;
			ns_d3d::Cd3d::_Inst().m_cam_trg.x += _input_vec.x * _my_mtx->_11 + _input_vec.z * _my_mtx->_31;
			ns_d3d::Cd3d::_Inst().m_cam_trg.z += _input_vec.x * _my_mtx->_13 + _input_vec.z * _my_mtx->_33;
		}
		static void DeniedMove() {
			_my_mtx->_41 -= _input_vec.x * _my_mtx->_11 + _input_vec.z * _my_mtx->_31;
			_my_mtx->_43 -= _input_vec.x * _my_mtx->_13 + _input_vec.z * _my_mtx->_33;
			ns_d3d::Cd3d::_Inst().m_cam_trg.x -= _input_vec.x * _my_mtx->_11 + _input_vec.z * _my_mtx->_31;
			ns_d3d::Cd3d::_Inst().m_cam_trg.z -= _input_vec.x * _my_mtx->_13 + _input_vec.z * _my_mtx->_33;
		}
		static void SetCamToword(float yaw) {
			_my_mtx->_11 = cosf(yaw);
			_my_mtx->_13 = -sinf(yaw);
			_my_mtx->_31 = sinf(yaw);
			_my_mtx->_33 = cosf(yaw);
		}
		static void ResetScene() {
			_my_mtx->_41 = _start_vec.x * _my_mtx->_11 + _start_vec.z * _my_mtx->_31;
			_my_mtx->_43 = _start_vec.x * _my_mtx->_13 + _start_vec.z * _my_mtx->_33;
			ns_d3d::Cd3d::_Inst().m_cam_trg.x = _start_vec.x * _my_mtx->_11 + _start_vec.z * _my_mtx->_31;
			ns_d3d::Cd3d::_Inst().m_cam_trg.z = _start_vec.x * _my_mtx->_13 + _start_vec.z * _my_mtx->_33;
		}
		static void Jumped() {
			_jumping = 0;
		}


	private:
		CPlayerController(CPlayerController&);
		CPlayerController& operator = (CPlayerController&);

		static size_t _my_id;
		static LPMATRIX _my_mtx;
		static VECTOR3 _input_vec;
		static VECTOR3 _start_vec;
		static size_t _is_inputting;
		static size_t _jumping;
		static float _camspeed;
	};

	class CSceneBuilder : public ns_lib::CSingleton<CSceneBuilder>
	{
	public:

		CSceneBuilder() {}
		~CSceneBuilder() {
		}
		void EnableControl() {
			ns_win::CHwnd::_Inst().AddWndHook(&CPlayerController::ControlProc);
		}
		void LoadSound(const char *name) {
			_Sound.LoadWave(name);
		}
		void PlaySound() {
			_Sound.PlayWave(ns_win::CHwnd::Instance().GetWindowHandle());
		}

	private:
		CSceneBuilder(CSceneBuilder&);
		CSceneBuilder& operator =(CSceneBuilder&);

		nsl::CSound _Sound;
	};

}	// namespace: nsgl

#endif	// _NS_SCENE_BUILDER_H_