
#pragma once
#ifndef _NS_VECTOR_H_
#define _NS_VECTOR_H_

namespace nsl {
	//////////////////////////////////////////////
	// �ǉ��A�폜�FO(N)
	// �T���FO(log N)
	template<typename T> class NSVector
	{
		typedef T* PtrType;
		typedef T** ContType;

	public:
		explicit NSVector() : _size(eDefaultSize), _count(0) { AllocBase(_size); }
		NSVector(size_t size) : _size(size), _count(0) { AllocBase(_size); }
		explicit NSVector(const NSVector &from) : _base(from._base), _size(from._size),
			_count(from._count), _called(from._called) { ++ *_called; }
		~NSVector() {
			if (*_called == 0) {
				delete [] _base;
				delete _called;
			} else {
				-- *_called;
			}
		}
		size_t size() { return _count; }
		size_t capacity() { return _size; }
		size_t max_size() { return eMaxSize; }
		void push_back(T& value) { *(_base+_count) = &value; ++_count; }
		bool empty() { return _count == 0; }
		ns_types::result_t find();
		void clear();
		void reserve(size_t s) {
			if (s<=_size) return;
			size_t remainder = s%DefaultSize;
			_size = s + remainder;
			ContType newb = new PtrType[_size];
			ContType pos = newb;
			for (ContType i=_base; *i; ++i, ++pos) *pos = *i;
			memset(pos, NULL, _ptrsize * remainder);
			delete [] _base;
			_base = newb;
		}

		template<typename T> class NSIterator
			: public std::iterator<std::forward_iterator_tag, T>
		{
			typedef T* ResultType;
			typedef T** ValueType;
			friend NSVector;
		public:
			explicit NSIterator() {}
			NSIterator(ValueType vt) : _cont(vt) {}
			NSIterator(NSVector& vec) : _cont(vec._base) {}
			~NSIterator() {}
			const ValueType operator *() const { return _cont; }
			const ResultType operator ->() const { return *_cont; }
			NSIterator& operator ++() { ++_cont; return *this; }
			bool operator !=(const NSIterator& rhs) const {
				return _cont != *rhs ? true : false;
			}
			NSIterator& operator =(NSVector& vec) {
				_cont = vec._base;
				return *this;
			}
		private:
			NSIterator& operator =(NSIterator&);
			ValueType _cont;
		};
		typedef NSIterator<T> iterator;

		iterator begin() const {
			return _base;
		}
		iterator end() const {
			return _base + _count;
		}
		//void for_each(void* (*exe)(ContType)) {
		//	exe(_base);
		//}
		const ContType front() const {
			return _base;
		}
		const ContType back() const {
			return _base + _count;
		}

	private:
		void AllocBase(size_t size) {
			_ptrsize = sizeof(PtrType);
			_base = new PtrType[size];
			memset(_base, NULL, _ptrsize * size);
			_called = new size_t(0);
		}
		enum eVector {
			eDefaultSize = 16,
			eMaxSize = 100,
		};

		ContType _base;
		size_t _size;
		size_t _count;
		size_t _ptrsize;
		size_t* _called; // count referenced
	};
}

#endif // _NS_VECTOR_H_