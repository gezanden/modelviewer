// Excerpts from Modern C++ Design, 6th Printing, by Andrei Alexandrescu
// http://www.moderncppdesign.com
//
// Main function and suggested fix by Naoki Shimoyamada
// http://megamanimania.blogspot.com/
// E-mail (no spam please): "megamani22" AT "gmail" DOT "com"
// 2007-Oct-27

#pragma	once
#ifndef	_NSTYPETRAITS_H_
#define	_NSTYPETRAITS_H_

// Includeing ns_types::NullType;
#include "NSTypeUnits.h"
// Including std::memcpy;
#include <cstring>

namespace ns_types
{

	// A typical BitBlast function...
	// http://www.xs4all.nl/~nd/dekkerware/moderncppdesign/CopyIntegersToFloats.cpp
	// Main function and suggested fix by Niels Dekker
	// http://www.xs4all.nl/~nd/dekkerware/moderncppdesign/index.htm
	// Modern C++ Design, pring 6, page 43;
	void BitBlast(const void* src, void* dest, unsigned int bytes);


	////////////////////////////////
	//	TypeTraits	P.44
	//	---------------------
	//	1.指定した方が属するカテゴリを明らかにする定数を定義している。
	template <typename T>
	class TypeTraits
	{
		// 型 T がポインタであるかどうかの判定
	private:
		template <class U> struct PointerTraits
		{
			enum { result = false };
			typedef NullType PointeeType;
		};
		template <class U> struct PointerTraits<U*>
		{
			enum { result = true };
			typedef U PointeeType;
		};
	public:
		enum { isPointer = PointerTraits<T>::result };
		typedef typename PointerTraits<T>::PointeeType
			PointeeType;	// P.44

		// isReference, ReferredType :Loki::TypeTraits.h::TypeTraits
		// Tが参照型: ReferredType は T が参照する型
		// Tがストレートな型: ReferencedTYpe は T 自身
	private:
		template<class U> struct ReferenceTraits
		{
			enum { result = false };
			typedef U ReferredType;
		};
		template<class U> struct ReferenceTraits<U&>
		{
			enum { result = true };
			typedef U ReferredType;
		};

		typedef typename ReferenceTraits<T>::ReferredType
			ReferredType;

		// メンバへのポインタ(第 5 章)の検出
	private:
		template <class U> struct PToMTraits
		{
			enum { result = false };
		};
		template <class U, class V>
		struct PToMTraits<U V::*>
		{
			enum { result = true };
		};
	public:
		enum { isMemberPointer = PToMTraits<T>::result };

		// isStdFundamental: T が標準の基本形かどうかを現すコンパイル時定数

		// プリミティブな型に特化した部分の定義
	public:
		typedef TYPELIST_4(unsigned char, unsigned short int, unsigned int, unsigned long int)
			UnsignedInts;
		typedef TYPELIST_4(signed char, short int, int, long int)
			SignedInts;
		typedef TYPELIST_3(bool, char, wchar_t)			OtherInts;
		typedef TYPELIST_3(float, double, long double)	Floats;
		enum { isStdUnsignedInt = IndexOf<UnsignedInts, T>::value >= 0 };
		enum { isStdSignedInt = IndexOf<SignedInts, T>::value >= 0 };
		enum { isStdIntegral = isStdUnsignedInt || isStdSignedInt || IndexOf<OtherInts, T>::value >= 0 };
		enum { isStdFloat = IndexOf<Floats, T>::value >= 0 };
		enum { isStdArith = isStdIntegral || isStdFloat };
		enum { isStdFundamental = isStdArith || isStdFloat || Conversion<T, void>::sameType };

		// Select P.47
		// Tが参照: ParameterType は T と同じ		// 参照への参照は許されないため
		// T がスカラ型: ParameterType は T となる	// プリミティブ型は値で引き渡すのが最適
		// それ以外: ParameterType は T&となる		// 非プリミティブ(POD)型は参照で引き渡すのが最適
	public:
		typedef typename ns_types::Select<isStdArith || isPointer || isMemberPointer, T, ReferredType&>::Result
			ParameterType;

		// const の除去
	private:
		template <class U> struct UnConst
		{
			typedef U	Result;
			enum { isConst = 1 };
		};
		template <class U> struct UnConst<const U>
		{
			typedef U	Result;
			enum { isConst = 1 };
		};
	public:
		typedef typename UnConst<T>::Result
			NonConstType;	// warning C4346:依存名は型ではありません。-> typename

		//...
	};

	////////////////////////////////
	// TypeTraitsの使用 P.48
	// BitBlastの一般化
	enum CopyAlgoSelector { Conservative, Fast };

	// 全ての型に対して使用される従来のルーチン
	template <typename InIt, typename OutIt>
	OutIt CopyImpl(InIt first, InIt last, OutIt result, Int2Type<Conservative>)
	{
		for (; first != last; ++first, ++result)
			*result = *first;
		return	result;
	}
	// 生データへのポインタに対してのみ使用される高速ルーチン
	template <typename InIt, typename OutIt>
	OutIt CopyImpl(InIt first, InIt last, OutIt result, Int2Type<Fast>)
	{
		const unsigned int n = last - first;
		BitBlast(first, result, n * sizeof(*first));
		return	result + n;
	}
	template <typename InIt, typename OutIt>
	OutIt Copy(InIt first, InIt last, OutIt result)
	{
		typedef TypeTraits<InIt>::PointeeType SrcPointee;
		typedef TypeTraits<OutIt>::PointeeType DestPointee;
		enum { copyAlgo =
			TypeTraits<InIt>::isPointer &&
			TypeTraits<OutIt>::isPointer &&
			TypeTraits<SrcPointee>::isStdFundamental &&
			TypeTraits<DestPointee>::isStdFundamental &&
			sizeof(SrcPointee) == sizeof(DestPointee) ? Fast :
			Conservative };
		// 2 つのイテレータがポインタであり、ポインタが指す型が双方とも基本型、かつ同じサイズであれば、BitBlast
		return	CopyImpl(first, last, result, Int2Type<copyAlgo>);
	}

	// PODを検出
	template <typename T> struct SupportsBitwiseCopy
	{
		enum { result = TypeTraits<T>::isStdFundamental };
	};
	// MyType: 任意の POD 型
	//template<> struct SupportsBitwiseCopy<MyType>
	//{
	//	enum { result = true };
	//};
	template <typename InIt, typename OutIt>
	OutIt Copy(InIt first, InIt last, OutIt result, Int2Type<true>)
	{
		typedef TypeTraits<InIt>::PointeeType SrcPointee;
		typedef TypeTraits<OutIt>::PointeeType DestPointee;
		enum { useBitBlast =
			TypeTraits<InIt>::isPointer &&
			TypeTraits<OutIt>::isPointer &&
			SupportsBitwiseCopy<SrcPointee>::result &&
			SupportsBitwiseCopy<DestPointee>::result &&
			sizeof(SrcPointee) == sizeof(DestPointee) };
		return	CopyImpl(first, last, Int2Type<useBitBlast>);
	}

}	// ns_types

#endif	// _NSTYPETRAITS_H_