#pragma once
#ifndef	_CFUNCTOR_H_
#define	_CFUNCTOR_H_

// Including std::auto_ptr;
#include <memory>
// Including std::copy;
#include <algorithm>
// Including ns_types::NullType;
#include "NSTypeList.h"

namespace ns_lib
{

	namespace Private
	{
		template <typename R>
		struct IFunctorImplBase
		{
			typedef R ResultType;
			virtual IFunctorImplBase* DoClone() const =0;
			template <class U>
			static U* Clone(U* pObj)
			{
				if (!pObj) return 0;
				U* pClone = static_cast<U*>(pObj->DoClone());
				assert(typeid(*pClone) == typeid(*pObj));
				return	pClone;
			}
		};
	}

	/////////////////////////////////////
	//	関数呼び出しを抽象化する、ポリモフィズムに則ったインタフェースを定義する P.113
	//	R:		登録する関数
	//	TList:	関数の引数
	template <typename R, class TList>
	class IFunctorImpl;

	template <typename R>
	class IFunctorImpl<R, ns_types::NullType>
		: public Private::IFunctorImplBase<R>
	{
	public:
		virtual R operator()() = 0;
		virtual IFunctorImpl* DoClone() const = 0;
		virtual ~IFunctorImpl() {}
	};

	template <typename R, typename P1>
	class IFunctorImpl<R, TYPELIST_1(P1)>
		: public Private::IFunctorImplBase<R>
	{
	public:
		virtual R operator()(P1) = 0;
		virtual IFunctorImpl* DoClone() const = 0;
		virtual ~IFunctorImpl() {}
	};

	template <typename R, typename P1, typename P2>
	class IFunctorImpl<R, TYPELIST_2(P1, P2)>
		: public Private::IFunctorImplBase<R>
	{
	public:
		virtual R operator()(P1, P2) = 0;
		virtual IFunctorImpl* DoClone() const = 0;
		virtual ~IFunctorImpl() {}
	};

	template <typename R, typename P1, typename P2, typename P3>
	class IFunctorImpl<R, TYPELIST_3(P1, P2, P3)>
		: public Private::IFunctorImplBase<R>
	{
	public:
		virtual R operator()(P1, P2, P3) = 0;
		virtual IFunctorImpl* DoClone() const = 0;
		virtual ~IFunctorImpl() {}
	};

	template <typename R, typename P1, typename P2, typename P3, typename P4>
	class IFunctorImpl<R, TYPELIST_4(P1, P2, P3, P4)>
		: public Private::IFunctorImplBase<R>
	{
	public:
		virtual R operator()(P1, P2, P3, P4) = 0;
		virtual IFunctorImpl* DoClone() const = 0;
		virtual ~IFunctorImpl() {}
	};

	///////////////////////////////////
	// Fun 型のオブジェクトを格納し、operator() をそれに転送する
	template <class ParentFunctor, typename Fun>
	class CFunctorHandler
		: public IFunctorImpl
			<
				typename ParentFunctor::ResultType,
				typename ParentFunctor::ParmList
			>
	{
	public:
		typedef typename ParentFunctor::ResultType	ResultType;

		CFunctorHandler(const Fun& fun) : m_fun(fun) {}
		CFunctorHandler* DoClone() const
		{
			return	new CFunctorHandler(*this);
		}

		ResultType operator()()
		{
			return	m_fun();
		}
		ResultType operator()(typename ParentFunctor::Parm1 p1)
		{
			return	m_fun(p1);
		}
		ResultType operator()(typename ParentFunctor::Parm1 p1, typename ParentFunctor::Parm2 p2)
		{
			return	m_fun(p1, p2);
		}
		ResultType operator()(typename ParentFunctor::Parm1 p1, typename ParentFunctor::Parm2 p2,
			typename ParentFunctor::Parm3 p3)
		{
			return	m_fun(p1, p2, p3);
		}
		ResultType operator()(typename ParentFunctor::Parm1 p1, typename ParentFunctor::Parm2 p2,
			typename ParentFunctor::Parm3 p3, typename ParentFunctor::Parm4 p4)
		{
			return	m_fun(p1, p2, p3, p4);
		}
	private:
		Fun		m_fun;
	};

	/////////////////////////////////////
	//	Functorクラスの実装 P.110
	//	------------------------
	//	1.明示的なデストラクタは auto_ptr が自動的にリソースのクリーン･アップを行うため必要無い。
	//	2.拡張コンストラクタが auto_ptr を受け取ることによって、CFunctor が CFunctorImpl オブジェクトの
	//	  所有権を獲得することを外部に対して明確に表明することになる。
	//
	//	TypeAtNonStrict:	タイプリスト中の指定した位置にある型にアクセスするためのテンプレート。
	template <typename R, class TList = ns_types::NullType>
	class CFunctor
	{
	public:
		typedef IFunctorImpl<R, TList>	Impl;
		// パラメータ型の定義 P.115
		typedef R		ResultType;
		typedef TList	ParmList;
		typedef typename ns_types::TypeAtNonStrict<TList, 0, ns_types::EmptyType>::Result
			Parm1;
		typedef typename ns_types::TypeAtNonStrict<TList, 1, ns_types::EmptyType>::Result
			Parm2;
		typedef typename ns_types::TypeAtNonStrict<TList, 2, ns_types::EmptyType>::Result
			Parm3;
		typedef typename ns_types::TypeAtNonStrict<TList, 3, ns_types::EmptyType>::Result
			Parm4;

	private:
		// VC7 can't see the defined ctor when the template version in present
		struct Helper
		{
			Helper() : spImpl_(0)
			{}

			Helper(const Helper &rhs)
				: spImpl_(Impl::Clone(rhs.spImpl_.get()))
			{}

			explicit Helper(std::auto_ptr<Impl> spImpl)
				: spImpl_(spImpl)
			{}

			template<typename U>
			explicit Helper(U *ptr)
				: spImpl_(ptr)
			{}

			Impl& operator*() const
			{ return *spImpl_; }

		public:
			std::auto_ptr<Impl>	spImpl_;
		};
		// 本体の型のための便利な型の定義
		//std::auto_ptr<Impl>	m_spImpl; //VC7 bug
		Helper	m_spImpl;

	public:
		CFunctor() : m_spImpl()
		{}
		// VC7 bug
		//CFunctor(const CFunctor& rhs) : m_spImpl(Impl::Clone(rhs.m_spImpl.get()))
		//{}
		//explicit CFunctor(std::auto_ptr<Impl> spImpl) : m_spImpl(spImpl)
		//{}
		CFunctor& operator=(const CFunctor& rhs)
		{
			// loki\Functor.h line 899
			CFunctor copy(rhs);
			//Impl* p = m_spImpl.release();
			//m_spImpl.reset(copy.m_spImpl.release());
			//copy.m_spImpl.reset(p);
			// VC7
			//Impl* p = m_spImpl.spImpl_.release();
			//m_spImpl.spImpl_.reset(copy.m_spImpl.spImpl_.release());
			//copy.m_spImpl.spImpl_.reset(p);
			Impl* p = m_spImpl.spImpl_.release();
			m_spImpl.spImpl_.reset(copy.m_spImpl.spImpl_.release());
			return	*this;
		}

		// FunctorImpl::operator() への転送を行う P.115, 131
		ResultType operator()()
		{
			return	(*m_spImpl)();
		}
		ResultType operator()(typename ns_types::TypeTraits<Parm1>::ParameterType p1)
		{
			return	(*m_spImpl)(p1);
		}
		ResultType operator()(typename ns_types::TypeTraits<Parm1>::ParameterType p1,
			typename ns_types::TypeTraits<Parm2>::ParameterType p2)
		{
			return	(*m_spImpl)(p1, p2);
		}
		ResultType operator()(typename ns_types::TypeTraits<Parm1>::ParameterType p1,
			typename ns_types::TypeTraits<Parm2>::ParameterType p2,
			typename ns_types::TypeTraits<Parm3>::ParameterType p3)
		{
			return	(*m_spImpl)(p1, p2, p3);
		}
		ResultType operator()(typename ns_types::TypeTraits<Parm1>::ParameterType p1,
			typename ns_types::TypeTraits<Parm2>::ParameterType p2,
			typename ns_types::TypeTraits<Parm3>::ParameterType p3,
			typename ns_types::TypeTraits<Parm4>::ParameterType p4)
		{
			return	(*m_spImpl)(p1, p2, p3, p4);
		}

		// Functor オブジェクトを受け取るコンストラクタ P.117
		template <class Fun>
		CFunctor(const Fun fun)	// (const Fun& fun) VC7 bug: LokiPort\Functor.h line 890
			: m_spImpl(new CFunctorHandler<CFunctor, Fun>(fun))
		{}

	};

	// MemFunHandler, page 122

}

#endif	// _C_FUNCTOR_H_