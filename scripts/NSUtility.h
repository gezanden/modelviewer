
#pragma once
#ifndef _NS_UTILITY_H_
#define _NS_UTILITY_H_

namespace nsl {
	template<class _Ty1,
	class _Ty2> struct pair
	{	// store a pair of values
	typedef pair<_Ty1*, _Ty2*> _Myt;
	typedef _Ty1 first_type;
	typedef _Ty2 second_type;

	pair()
		: first(NULL), second(NULL)
		{	// construct from defaults
		}

	pair(_Ty1& _Val1, _Ty2& _Val2)
		: first(&_Val1), second(&_Val2)
		{	// construct from specified values
		}

	template<class _Other1, class _Other2>
	pair(const pair<_Other1, _Other2>& _Right)
		: first(_Right.first), second(_Right.second)
		{	// construct from compatible pair
		}

	//void swap(_Myt& _Right)
	//	{	// exchange contents with _Right
	//	std::swap(first, _Right.first);
	//	std::swap(second, _Right.second);
	//	}

	_Ty1* first;	// the first stored value
	_Ty2* second;	// the second stored value
	};
}

#endif // _NS_UTILITY_H_