#ifndef _NS_MESSAGE_SERVER_H_
#define _NS_MESSAGE_SERVER_H_

namespace nsl
{
	class CMessageServer
	{
	public:
		CMessageServer(){}
		~CMessageServer(){}
		void setup();
		void Open();
		void Close();
		void Terminate();
		char_t* Receive(char_t* rec);

	private:
		CMessageServer(CMessageServer&);
		CMessageServer& operator = (CMessageServer&);

		char_t* data;
	};
}

#endif