//
//	nsd3d9.hpp
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once

//--------------------------------------------------
//	Link liblary
//--------------------------------------------------
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")



//--------------------------------------------------
//	Includes
//--------------------------------------------------
// warning C4819: ファイルは、現在のコード ページ (932) で表示できない文字を含んでいます。データの損失を防ぐために、ファイルを Unicode 形式で保存してください。
#pragma	warning( push )		// このヘッダーのみ、警告を無効化する
 #pragma	warning( disable : 4819 )
 #include	<d3dx9.h>
#pragma	warning( pop )		// 元の警告レベルに戻す

#include	"nstypes.h"


typedef D3DXMATRIX	MATRIX, *LPMATRIX;
typedef D3DMATERIAL9	MATERIAL, *LPMATERIAL;
typedef D3DXVECTOR2 VECTOR2, *LPVECTOR2;
typedef D3DXVECTOR3	VECTOR3, *LPVECTOR3;
typedef D3DXVECTOR4	VECTOR4, *LPVECTOR4;


//--------------------------------------------------
//	Namespace
//--------------------------------------------------
namespace ns_d3d
{


	//--------------------------------------------------
	//	Variables
	//--------------------------------------------------
	extern const bool	WINDOW_MODE;
	extern const float	OBJECT_SIZE;
	extern const float	PI;



	//--------------------------------------------------
	//	Structures
	//--------------------------------------------------
//#define MATRIX D3DXMATRIX
//#define LPMATRIX D3DXMATRIX*
//#define MATERIAL D3DMATERIAL9
//#define LPMATERIAL D3DMATERIAL9*
//#define VECTOR3 D3DXVECTOR3
//#define LPVECTOR3 D3DXVECTOR3*
//#define VECTOR4 D3DXVECTOR4
//#define LPVECTOR4 D3DXVECTOR4*
	//typedef ::D3DXMATRIX	MATRIX, *LPMATRIX;
	//typedef ::D3DMATERIAL9	MATERIAL, *LPMATERIAL;
	//typedef ::D3DXVECTOR3	VECTOR3, *LPVECTOR3;
	//typedef ::D3DXVECTOR4	VECTOR4, *LPVECTOR4;
	//typedef struct _VECTOR3 {
	//	float x;
	//	float y;
	//	float z;
	//	explicit _VECTOR3()	: x(), y(), z() {
	//	}
	//	_VECTOR3(float _f) : x(_f), y(_f), z(_f) {
	//	}
	//	_VECTOR3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {
	//	}
	//	_VECTOR3& operator = (const _VECTOR3 &_v) {
	//		x = _v.x; y = _v.y; z = _v.z;
	//	}
	//} VECTOR3, LPVECTOR3;

	// 頂点構造体
	typedef struct _VERTEX3DPCT_
	{
		D3DXVECTOR3		pos;
		size_t			color;
		D3DXVECTOR2		texcoord;
	} VERTEX3DPCT, *LPVERTEX3DPCT;



	//--------------------------------------------------
	//	Functions
	//--------------------------------------------------
	LPMATRIX MatrixIdentity( LPMATRIX );
	LPMATRIX MatrixMultiply( LPMATRIX, const LPMATRIX, const LPMATRIX );

}	// namespace: ns_d3d
