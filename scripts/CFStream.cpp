#include "CFStream.h"

namespace nslib {

	char* CIFStream::getl() {
		FILE* __fp = this->_fp;
		//char_t* __src = _line;
		char* __p;
		while (*(__p = ::fgets(_line, eLineLength, __fp)) == 0x0a);
		return __p;
	}

	char* CIFStream::getls() {
		FILE* __fp = this->_fp;
		char* __p;
		while (__p = ::fgets(_line, eLineLength, __fp)) {
			if (*__p == 0x20) {
				while (*(++__p) == 0x20);
				if (*__p == 0x0a)
					continue;
				return __p;
			}
			else if (*__p == 0x09) {
				while (*(++__p) == 0x09);
				//if (*__p == 0x0a)
				//	continue;
				return __p;
			}
			else if (*__p != 0x0a) {
				return __p;
			}
		}
		return __p;
	}

	char* CIFStream::read() {
		FILE* __fp = this->_fp;
		char* __p = _line;
		size_t result = ::fread(__p, sizeof(char), ns_types::eSizeofLine, __fp);
		return result ? __p : NULL;
	}

	void CIFStream::write() {
		FILE* __fp = this->_fp, *__fpw;
		char* __p = _line;
		::fopen_s(&__fpw, "test.txt", "w");
		::fwrite(__p, sizeof(char), ns_types::eSizeofLine, __fpw);
		::fclose(__fpw);
	}

	char* CIFStream::getls(size_t _Row) {
		FILE* __fp = this->_fp;
		-- _Row;
		for (; _Row; --_Row, ::fgets(_line, eLineLength, __fp));
		char* __p;
		while (__p = ::fgets(_line, eLineLength, __fp)) {
			if (*__p == ns_types::eSpace) {
				while (*(++__p) == ns_types::eSpace);
				if (*__p == ns_types::eLineFeed)
					continue;
				return __p;
			}
			else if (*__p == ns_types::eTab) {
				while (*(++__p) == ns_types::eTab);
				return __p;
			}
			else if (*__p != ns_types::eLineFeed) {
				return __p;
			}
		}
		return __p;
	}

	char* CIFStream::ignore(char _Delimiter) {
		FILE* __fp = this->_fp;
		char* __p;
		while (__p = ::fgets(_line, eLineLength, __fp)) {
			if (*__p == 0x0a || !(__p = strchr(_line, _Delimiter)))
				continue;
			break;
		}
		return __p;
	}

	void CIFStream::ignorel() {
		FILE* __fp = this->_fp;
		char* __p;
		while (__p = ::fgets(_line, eLineLength, __fp)) {
			if (*__p == 0x0a)
				continue;
			break;
		}
		return;
	}

	void CIFStream::ignorel(char _Delimiter) {
		FILE* __fp = this->_fp;
		char* __p;
		while (__p = ::fgets(_line, eLineLength, __fp)) {
			if (*__p == _Delimiter)
				break;
		}
		return;
	}

	void CIFStream::ignorels(const char _Delimiter) {
		FILE* __fp = this->_fp;
		char* __p;
		while (__p = ::fgets(_line, eLineLength, __fp)) {
			if (*__p == 0x0a) {
				continue;
			}
			else if (*__p == 0x20) {
				for (;*__p == 0x20; ++__p);
				if (*__p == _Delimiter)
					break;
			}
			else if (*__p == _Delimiter) {
				break;
			}
		}
		return;
	}

	void CIFStream::ignorelc(const char _Ignore) {
		FILE* __fp = _fp;
		char* __p = 0;
		while (__p = ::fgets(_line, eLineLength, __fp)) {
			if (*__p != _Ignore)
				break;
		}
		return;
	}

}