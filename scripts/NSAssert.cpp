//
//	NSTypeTraits.cpp
//
//	Copyright (c) 2008 Naoki Shimoyamada, All Rights Reserved.
//

#include	"NSAssert.h"
#include	<windows.h>
#include	<stdarg.h>

namespace ns_dbg
{
	void CustomAssertFunction(const ns_types::char_t* format, ...)
	{
		ns_types::char_t description[ns_types::eSizeofDesc];
		va_list args;
		MessageBox(NULL, STR(殿中でござる), STR(assertion), MB_OK);
		va_start(args, format);
		wvsprintf(description, format, args);
		OutputDebugString(description);
		va_end(args);


		//assert(exp && assertion && description && line && file);

		//if (OpenClipboard(NULL))
		//{
		//	HGLOBAL	hMem;
		//	char_t	szAssert[eMaxSize];
		//	char_t	*pMem;

		//	sprintf_t(szAssert, "%s", description);
		//	hMem = GlobalAlloc(GHND|GMEM_DDESHARE, strlen_t(szAssert)+1);

		//	if (hMem)
		//	{
		//		pMem = static_cast<char_t*>GlobalLock(hMem);
		//		strcpy_t(pMem, szAssert);
		//		GlobalUnlock(hMem);
		//		EmptyClipboard();
		//		SetClipboardData(CF_TEXT, hMem);
		//	}
		//	CloseClipboard();
		//}
	}

	wchar_t* CharToWChar(const char* str)
	{
		int length = MultiByteToWideChar(CP_THREAD_ACP, 0, str, -1, NULL, 0);
		wchar_t* buf = new wchar_t[length];
		MultiByteToWideChar(CP_THREAD_ACP, 0, str, (int)wcslen(buf)+1, buf, length);
		return buf;
	}
}