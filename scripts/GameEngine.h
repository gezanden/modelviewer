#ifndef _NS_GAME_ENGINE_H_
#define _NS_GAME_ENGINE_H_

#include "NSSingleton.h"
#include "CModelMgr.h"
#include "SceneBuilder.h"

namespace nsgl
{
	class CGameEngine : public ns_lib::CSingleton<CGameEngine>
	{
	public:
		CGameEngine() : execute(NULL) {}
		~CGameEngine(){}
		void Refresh() {
			if(execute) execute();	// input
			DecidePointCharactor();
			ns_d3d::Cd3d::Instance().RefreshScene();
			//ModelMgr::Instance().Render();
		}
		void EnableControl() {
			ns_win::CHwnd::_Inst().AddWndHook(&CPlayerController::ControlProc);
		}
		void SetExecute(void (*exe)()) {
			execute = exe;
		}
		void EmptyExecute() {
			execute = NULL;
		}
		void DecidePointCharactor() {
			// キャラが衝突しているか総当り
			// Y: 境界値に設定
			// N: そのまま
			// 必要なもの
			// ・キャラの中心点
			// ・現在地座標
			// 座標とフロアをどう同期するか
			// 1.着地判定（Y軸）
			// 2.障害物判定（X, Z軸）
			ModelMgr::_Inst().Landing();
		}

	private:
		CGameEngine(CGameEngine&);
		CGameEngine& operator = (CGameEngine&);
		void (*execute)();
	};
}

#endif