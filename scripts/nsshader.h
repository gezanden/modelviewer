#pragma once

#include "nsd3d9.h"

// 新しいピクセル シェーダ
// http://msdn.microsoft.com/ja-jp/library/dd188511.aspx

namespace ns_d3d {

	// 頂点型
	const D3DVERTEXELEMENT9 g_decl[4] =
	{
		{ 0,  0,	D3DDECLTYPE_FLOAT3,		D3DDECLMETHOD_DEFAULT,	D3DDECLUSAGE_POSITION,	0 },
		{ 0, 12,	D3DDECLTYPE_FLOAT3,		D3DDECLMETHOD_DEFAULT,	D3DDECLUSAGE_NORMAL,	0 },
		{ 0, 24,	D3DDECLTYPE_FLOAT2,		D3DDECLMETHOD_DEFAULT,	D3DDECLUSAGE_TEXCOORD,	0 },
		D3DDECL_END()
	};

	// 頂点シェーダ
	static const char* g_vs =
	{
		"vs_1_1														\n"

		"dcl_position0		v0										\n"
		"dcl_normal0		v3										\n"
		"dcl_texcoord0		v7										\n"

		// Position
		"dp4	oPos.x,		v0,			c[0]						\n"
		"dp4	oPos.y,		v0,			c[1]						\n"
		"dp4	oPos.z,		v0,			c[2]						\n"
		"dp4	oPos.w,		v0,			c[3]						\n"
		"mov	oT0.xy,		v7.xy									\n"

		// diffuse & ambient
		"dp3	r1.x,		v3.xyz,		c[10].xyz					\n"	// |n|・|l|
		"max	r1.x,		r1.x,		c[9].w						\n"	// clamp
		"mov	r2,			c[12]									\n"	// material diffuse color
		"mul	r3,			r2,			c[11]						\n"	// light diffuse color
		"mov	r4,			c[16]									\n"	// material ambient color
		"mul	r5,			r4,			c[15]						\n"	// light ambient color
		"mad	oD0,		r3,			r1.xxxx,		r5			\n"	// diffuse + abmient

		// specular
		"sub	r3,			c[18],		v0							\n"	// light vector
		"dp3	r3.w,		r3,			r3							\n"
		"rsq	r3.w,		r3.w									\n"
		"mul	r3,			r3,			r3.w						\n"

		"sub	r4,			c[17],		v0							\n"	// eye vector
		"dp3	r4.w,		r4,			r4							\n"
		"rsq	r4.w,		r4.w									\n"
		"mul	r4,			r4,			r4.w						\n"

		"add	r5,			r4,			r3							\n"	// half vector
		"mul	r5,			r5,			c[9].xxxx					\n"

		"dp3	r6.x,		r3,			v3							\n"
		"max	r6.x,		r6.x,		c[9].w						\n"
		"dp3	r6.y,		r5,			v3							\n"
		"max	r6.y,		r6.y,		c[9].w						\n"
		"dp3	r6.z,		r4,			v3							\n"
		"max	r6.z,		r6.z,		c[9].w						\n"
		"mov	r6.w,		c[14].w									\n"
		"lit	r7.z,		r6										\n"

		"mov	r8,			c[14]									\n"
		"mul	r8,			r8,			c[13]						\n"
		"mul	oD1,		r8,			r7.zzzz						\n"
	};


	// 頂点シェーダ
	static const char* g_vs_skin =
	{
		"vs_1_1														\n"

		"dcl_position0		v0										\n"
		"dcl_normal0		v3										\n"
		"dcl_texcoord0		v7										\n"

		// Position
		"dp4	oPos.x,		v0,			c[0]						\n"
		"dp4	oPos.y,		v0,			c[1]						\n"
		"dp4	oPos.z,		v0,			c[2]						\n"
		"dp4	oPos.w,		v0,			c[3]						\n"
		"mov	oT0.xy,		v7.xy									\n"

		// diffuse & ambient
		"dp3	r1.x,		v3.xyz,		c[10].xyz					\n"	// |n|・|l|
		"max	r1.x,		r1.x,		c[9].w						\n"	// clamp
		"mov	r2,			c[12]									\n"	// material diffuse color
		"mul	r3,			r2,			c[11]						\n"	// light diffuse color
		"mov	r4,			c[16]									\n"	// material ambient color
		"mul	r5,			r4,			c[15]						\n"	// light ambient color
		"mad	oD0,		r3,			r1.xxxx,		r5			\n"	// diffuse + abmient

		// specular
		"sub	r3,			c[18],		v0							\n"	// light vector
		"dp3	r3.w,		r3,			r3							\n"
		"rsq	r3.w,		r3.w									\n"
		"mul	r3,			r3,			r3.w						\n"

		"sub	r4,			c[17],		v0							\n"	// eye vector
		"dp3	r4.w,		r4,			r4							\n"
		"rsq	r4.w,		r4.w									\n"
		"mul	r4,			r4,			r4.w						\n"

		"add	r5,			r4,			r3							\n"	// half vector
		"mul	r5,			r5,			c[9].xxxx					\n"

		"dp3	r6.x,		r3,			v3							\n"
		"max	r6.x,		r6.x,		c[9].w						\n"
		"dp3	r6.y,		r5,			v3							\n"
		"max	r6.y,		r6.y,		c[9].w						\n"
		"dp3	r6.z,		r4,			v3							\n"
		"max	r6.z,		r6.z,		c[9].w						\n"
		"mov	r6.w,		c[14].w									\n"
		"lit	r7.z,		r6										\n"

		"mov	r8,			c[14]									\n"
		"mul	r8,			r8,			c[13]						\n"
		"mul	oD1,		r8,			r7.zzzz						\n"
	};
	// ピクセルシェーダ
	static const char* g_ps =
	{
		"ps_1_1														\n"
		"tex	t0													\n"
		"add	r0,			v0,			v1							\n"
		"mul	r0,			r0,			t0							\n"
		//"mul		r0,			t0,			v0						\n"
		//"mad_sat	r0.rgb,		t0,			v1,			r0			\n"
		//"+mov		r0.a,		v0.a								\n"
	};

	static const char* g_ps_mono =
	{
		"ps_1_1														\n"
		"tex	t0													\n"
		"mul	r0,			t0,			v0							\n"
		"dp3	r0.rgb,		r0,			c0							\n"
		"+mul	r0.a,		t0,			v0							\n"
	};

}	// ns_d3d