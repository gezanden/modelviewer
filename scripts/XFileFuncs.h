//
//	XFileFuncsh
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include	"CModel.h"
#include	"CFStream.h"

//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_model
{


	//--------------------------------------------------------
	//	Functions
	//--------------------------------------------------------
	void VectorIdiot( LPVECTOR3 );
	void VectorCross( LPVECTOR3, LPVECTOR3, LPVECTOR3 );
	void VectorCross( float, LPVECTOR2, LPVECTOR2 );
	float VectorDot(LPVECTOR3, LPVECTOR3);
	float VectorDot(LPVECTOR4, LPVECTOR4);
	void MatrixIdentity( LPMATRIX );
	void ConvertQuatToMat( LPMATRIX, LPVECTOR4 );
	void GetName( char_t*, const char_t* );
	void GetName( char_t*, size_t );
	void TransMatrixComplete( LPMATRIX, LPFRAMEHEADER );
	void TransMatrixCompleteTest( LPMATRIX, LPFRAMEHEADER );
	void FileLoadMatrix( LPMATRIX, char_t* );
	void VecxMat( LPVECTOR3, LPVECTOR3, LPMATRIX );
	u_long FileLoadPass( char_t*, char_t* );
	void LinkFrame(char_t *_Name, LPFRAMEHEADER *_Link, LPFRAMEHEADER _FirstFrame, size_t _NumFrame);

	void BrendingIndex( dword_t*, size_t, size_t );
	void BubbleSort( word_t*, word_t*, size_t );

	void GetMatrixFromStream(nslib::CIFStream& _Ifs, D3DXMATRIX* _Mtx);
	void GetMaterialFromStream(nslib::CIFStream& _Ifs, D3DMATERIAL9* _Mtrl);
	void GetNameWithMBS(char *_Dest, char *_Src, char _Delim);
	void GetNameWithMBSToWCS(wchar_t *_Dest, char *_Src, char _Delim);
	void GetNameWithWCS(char_t *_Dest, char_t *_Src, char_t _Delim);
#ifdef _UNICODE
#define GetNameT GetNameWithMBSToWCS
#else
#define GetNameT GetNameWithMBS
#endif
	void JoinFilePass(char_t *_Dest, const char_t *_Path, const char_t *_Name);

	CModel* GetModel(const char* _XFile);
	CModel* CreateModel( const char_t* );

}
