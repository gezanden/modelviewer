//
//	CModelInit.cpp
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include	"CModelInit.h"
#include	"XFileFuncs.h"
#include	"Cd3d.h"
#include	"NSAssert.h"
// including strchr_e();
#include	"nsfunc.h"



//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_model
{
	using namespace ns_types;
	using namespace nslib;


	err_t CModelInit::CreateFromStream(CModel* _Model, nslib::CIFStream& _Ifs, char* _Texnames )
	{
		if ( !Allocate( _Model ) || !SetFromStream( _Model, _Ifs, _Texnames ) )
			return	false;
		Scaling( _Model );
		return	true;
	}

	err_t CSkinedMeshModelInit::CreateFromStream(CSkinedMeshModel* _Model, nslib::CIFStream& _Ifs, char* _Texnames)
	{
		if (!Allocate(_Model) || !SetFromStream(_Model, _Ifs, _Texnames))
			return false;
		Scaling(_Model);
		return true;
	}


	//--------------------------------------------------------
	//	Name:	CModelInit::Allocate
	//	Desc:	Allocate CModel member
	//--------------------------------------------------------
	bool CModelInit::Allocate( CModel* Model )
	{
		MODELHEADER*	pModelHead	= Model->m_pModelHead;
		ANIMEHEADER*	pAnimeHead	= Model->m_pAnimeHead;
		// Create the vertex buffer.
		if ( FAILED( ns_d3d::Cd3d::Instance().Getd3dDevice()->CreateVertexBuffer( static_cast<UINT>(pModelHead->nVertex * sizeof( CUSTOMVERTEX )), 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &Model->m_pVB, NULL )) )
			return	false;

		// Fill the index buffer.
		if ( FAILED( ns_d3d::Cd3d::Instance().Getd3dDevice()->CreateIndexBuffer( static_cast<UINT>(pModelHead->nIndex * 3 * sizeof( word_t )), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &Model->m_pIB, NULL )) )
			return	false;

		try
		{
			// フレーム領域
			if ( pModelHead->nFrame )
			{
				Model->m_pFrameList	= new FRAMEHEADER[ pModelHead->nFrame ];
				//memset( Model->m_pFrameList,	'\0', sizeof( FRAMEHEADER ) * pModelHead->nFrame );
			}

			// メッシュ領域
			Model->m_pMeshList		= new MESHHEADER[ pModelHead->nMesh ];
			//memset( Model->m_pMeshList,	'\0', sizeof( MESHHEADER ) * pModelHead->nMesh );

			// マテリアル領域
			if ( pModelHead->nMaterial )
			{
				Model->m_pMtrlList		= new MATERIAL[ pModelHead->nMaterial ];
				//memset( Model->m_pMtrlList,	'\0', sizeof( MATERIAL ) * pModelHead->nMaterial );
			}

			//////////////////////////////////////
			//	アニメ領域

			if ( pModelHead->nAnimationSet )
			{
				pAnimeHead->pAnimeSetList	= new ANIMESET[ pModelHead->nAnimationSet ];
				//memset( pAnimeHead->pAnimeSetList,	'\0', sizeof( ANIMESET ) * pModelHead->nAnimationSet );
				pAnimeHead->pAnimeList	= new ANIMENODE[ pAnimeHead->nAnimation ];
				//memset( pAnimeHead->pAnimeList,		'\0', sizeof( ANIMENODE ) * pAnimeHead->nAnimation );
				pAnimeHead->pMatAnime		= new MATRIX[ pAnimeHead->nAnimation ];
				//memset( pAnimeHead->pMatAnime,			'\0', sizeof( MATRIX ) * pAnimeHead->nAnimation );
				pAnimeHead->pnTimeList	= new size_t[ pAnimeHead->nTimeList ];
				//memset( pAnimeHead->pnTimeList,		'\0', sizeof( size_t ) * pAnimeHead->nTimeList );

				if ( pAnimeHead->nQuat )
				{
					pAnimeHead->pQuatList	= new ANIMEQUATHEADER[ pAnimeHead->nQuat ];
					//memset( pAnimeHead->pQuatList,	'\0', sizeof( ANIMEQUATHEADER ) * pAnimeHead->nQuat );
				}
				if ( pAnimeHead->nScale )
				{
					pAnimeHead->pScaleList	= new ANIMESCALEHEADER[ pAnimeHead->nScale ];
					//memset( pAnimeHead->pScaleList,	'\0', sizeof( ANIMESCALEHEADER ) * pAnimeHead->nScale );
				}
				if ( pAnimeHead->nSlide )
				{
					pAnimeHead->pSlideList	= new ANIMESLIDEHEADER[ pAnimeHead->nSlide ];
					//memset( pAnimeHead->pSlideList,	'\0', sizeof( ANIMESLIDEHEADER ) * pAnimeHead->nSlide );
				}
				if ( pAnimeHead->nMat )
				{
					pAnimeHead->pMatList	= new ANIMEMATHEADER[ pAnimeHead->nMat ];
					//memset( pAnimeHead->pMatList,		'\0', sizeof( ANIMEMATHEADER ) * pAnimeHead->nMat );
				}

				if ( pAnimeHead->nQuatList )
				{
					pAnimeHead->pvQuatList	= new VECTOR4[ pAnimeHead->nQuatList ];
					//memset( pAnimeHead->pvQuatList,	'\0', sizeof( VECTOR4 ) * pAnimeHead->nQuatList );
				}
				if ( pAnimeHead->nMatList )
				{
					pAnimeHead->pmatList	= new MATRIX[ pAnimeHead->nMatList ];
					memset( pAnimeHead->pmatList,		'\0', sizeof( MATRIX ) * pAnimeHead->nMatList );
				}
				if ( pAnimeHead->nVecList )
				{
					pAnimeHead->pvList		= new VECTOR3[ pAnimeHead->nVecList ];
					//memset( pAnimeHead->pvList,		'\0', sizeof( VECTOR3 ) * pAnimeHead->nVecList );
				}
			}
		}
		catch ( std::bad_alloc )
		{
			return false;
		}

		return	true;
	}



	ns_types::err_t CModelInit::SetFromStream(CModel* _Model, nslib::CIFStream& ifs, char* _Texnames)
	{
		// Lock vertex buffer
		if( _Model->m_pVB->Lock( 0, 0, reinterpret_cast<void**>( &_Model->m_pVertices ), 0 ) < 0 )
			return	false;

		// Lock index buffer
		if( _Model->m_pIB->Lock( 0, 0, reinterpret_cast<void**>( &_Model->m_pwIndices ), 0 ) < 0 )
			return	false;

		size_t __count, __vertex, __index;
		char* __ptexnames = _Texnames;

		const char __header = 'H';
		const char __temp = 't';
		const char __frame = 'F';
		const char __mesh = 'M';
		const char __animation = 'A';
		const char* __coords = "MeshT";
		const char* __material = "MeshM";
		const char* __normals = "MeshN";
		const char* __transform = "FrameT";
		const char __dupli = 'V';
		const char __xskin = 'X';

		LPMODELHEADER __modelHeader = _Model->m_pModelHead;
		LPFRAMEHEADER __posFrame = _Model->m_pFrameList, __prevFrame = NULL,
			__firstFrame = __posFrame;
		LPMESHHEADER __posMesh = _Model->m_pMeshList;
		LPTEXTUREHEADER __posTex = _Model->m_pTexList;
		LPCUSTOMVERTEX __customVers = _Model->m_pVertices,
			__firstVtx = __customVers, __polygon[3], __customVersSub;
		word_t* __wordIndices = _Model->m_pwIndices;
		LPMATERIAL __posMtrl = _Model->m_pMtrlList;
		word_t __offsetvtx = 0;
		size_t __textures = __modelHeader->nTexture, __frames = __modelHeader->nFrame;

		LPANIMEHEADER __animeHeader = _Model->m_pAnimeHead;
		LPANIMENODE __animeNodes;
		LPANIMESET __animeSets;
		LPMATRIX __posMtx;
		LPANIMEQUATHEADER __animeQuats;
		LPANIMESCALEHEADER __animeScales;
		LPANIMESLIDEHEADER __animeSlides;
		LPANIMEMATHEADER __animeMats;
		LPVECTOR4 __posQuat;
		LPVECTOR3 __posVec;
		size_t *__posTime;
		if (__animeHeader != NULL) {
			__animeNodes = __animeHeader->pAnimeList;
			__animeSets = __animeHeader->pAnimeSetList;
			__posMtx = __animeHeader->pmatList;
			__animeQuats = __animeHeader->pQuatList;
			__posQuat = __animeHeader->pvQuatList;
			__animeScales = __animeHeader->pScaleList;
			__posVec = __animeHeader->pvList;
			__animeSlides = __animeHeader->pSlideList;
			__animeMats = __animeHeader->pMatList;
			__posTime = __animeHeader->pnTimeList;
		}
		ifs.seekbegin();
		// xof
		ifs._ignorel();
		char* __src = ifs.getl();
		// header
		if (*__src == eSpace) {
			ifs._getl(5);
		}
		// knot.x
		else {
			if (*__src == eSlash) { // comment (compatible with triplane.x, heli.x)
				ifs.ignorelc(eSlash);
				__src = ifs.getl();
			}
			// template
			if (*__src == __temp) {
				do {
					ifs.ignorel(eParent); // compatible with triplane.x
				} while (*(__src = ifs.getl()) == __temp); // getls: triplane.x ('A`)
				// header: lobby, tiger, triplane
				if (*__src == __header) {
					ifs.ignorels(eParent); // compatible with triplane.x
				}
			}
		}
		do {// while(__src = ifs.getl())
			if (*__src == eParent) {
				__prevFrame = __prevFrame->pPrev;
			}
			else if (*__src == __frame) {
				__src = ifs.getls();
				if (!nslib::strictlystrstr(__src, __transform)) // triplane.x
					continue;
				// FrameTransformMatrix
				D3DXMATRIX* mat = &__posFrame->matTrans;
				GetMatrixFromStream(ifs, mat);
				ifs._ignorel();
				//ifs.ignorels(__parent); // parent:Matrix

				__posFrame->pPrev = __prevFrame;
				__prevFrame = __posFrame;
				++__posFrame;
			}
			else if (*__src == __mesh) {
				__posMesh->nVertexNum = __vertex = stoi(ifs._getl());
				__customVersSub = __customVers;	// for TextureCoords
				VECTOR3* vec = &__customVers->position;
				for (size_t i=__vertex; i; --i, vec=&(++__customVers)->position) {
					__src = ifs._getl();
					vec->x = stof32(&__src);
					vec->y = stof32(&__src);
					vec->z = stof32(__src);
					__customVers->normal = VECTOR3(.0f, .0f, .0f);
				}
				__index = stoi(ifs.getl());
				size_t offsetindex = 0;
				for (size_t i=__index; i; --i, ++__wordIndices) {
					VECTOR3 vec;
					switch (stoi(&(__src = ifs.getls())))
					{
					case 4:
						{
							word_t* buf = __wordIndices;
							*__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
							VectorCross(&vec,
								&(__polygon[1]->position - __polygon[0]->position),
								&(__polygon[2]->position - __polygon[0]->position));
							__polygon[0]->normal += vec;
							__polygon[1]->normal += vec;
							__polygon[2]->normal += vec;
							*++__wordIndices = *buf;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = *(buf+2);
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
							++ offsetindex;
							break;
						}
					case 3:
						{
							*__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
						}
					}
					VectorCross(&vec,
						&(__polygon[1]->position - __polygon[0]->position),
						&(__polygon[2]->position - __polygon[0]->position));
					__polygon[0]->normal += vec;
					__polygon[1]->normal += vec;
					__polygon[2]->normal += vec;
				}
				__posMesh->nPolygonNum = __index + offsetindex;
				__offsetvtx += (word_t)__posMesh->nVertexNum;

				// child, data, parent の間に空行が無いこと前提
				while (__src = ifs.getls()) { // parent:mesh, getls:triplane.x
					if (*__src == eParent) { // mesh
						break;
					}
					else if (nslib::strictlystrstr(__src, __material)) {
						__posMesh->pMtrl = __posMtrl;
						__posMesh->nMtrlNum = __count = atoi(ifs._getl());
						ifs._ignorel(__index + 1);
						// Material
						for (; __count; --__count, ++__posMtrl) {
							ifs.getl(); // child: Material
							GetMaterialFromStream(ifs, __posMtrl);
							// TextureFilename
							__src = ifs.getls(); // child:Filename, parent:Material
							if (*__src != eParent) {
								if (__textures == 1) {
									__posMesh->pTexture = __posTex;
									ifs._ignorel(3);
								} else {
									__src = ifs.getls()+1;
									char* __s = __src;
									size_t tex = 0;
									do {
										if (*__ptexnames != *__s) {
											while (*++__ptexnames != *__s) {
												if (*__ptexnames == eSlash) {
													++ tex;
													continue;
												}
											}
										}
										while (*++__ptexnames == *++__s && *__s == eQuote) {
											if (*__ptexnames == eSlash) {
												++ tex;
												continue;
											}
										}
										break;
									} while (*++__ptexnames);
									__ptexnames = _Texnames;
									__posMesh->pTexture = __posTex + tex;
									ifs._ignorel(2);// parent:Material
								}
								//if (!nslib::findstr(__texnames, __src = ifs.getls()+1, eQuote)) {
								//	*__ptexnames = *__src;
								//	for (; *__src!=eQuote; *++__ptexnames = *++__src); // 15step
								//	*__ptexnames = eSlash; // delimiter
								//	*(++ __ptexnames) = 0;	// terminated
								//	++ __modelHeader->nTexture;
								//}
							}
						}
						ifs._ignorel(); // parent:MaterialList -> while
					}
					else if (nslib::strictlystrstr(__src, __coords)) {
						ifs._ignorel();
						for (size_t i=__vertex; i; --i, ++__customVersSub) {
							__src = ifs._getl();
							__customVersSub->texcoord.x = stof32(&__src);
							__customVersSub->texcoord.y = stof32(__src);
						}
						ifs._ignorel();
					}
					else if (*__src == __dupli) {
						ifs._ignorel(__vertex + 3);
					}
					else if (nslib::strictlystrstr(__src, __normals)) {
						ifs._ignorel(stoi(ifs._getl()));
						ifs.getl();
						ifs._ignorel(__index+1);
					}
					else {
						ifs.ignorels(eParent);
					}
				}

				if (__frames) {
					__posMesh->pPrev = __prevFrame;
				}
				++ __posMesh;
				continue;
			}
			else if (*__src == __animation) {
				size_t animation = 0;
				__animeSets->pAnimeList = __animeNodes;
				while (*ifs.getls() != eParent) {	// child:Animation, parent:ASet
					if (*(__src = ifs.getls()) == eChild) {	// name, child:AnimationKey
						ns_model::GetNameT(__animeNodes->szAnimeName, __src+1, eParent);
						ifs._ignorel();	// child:AnimationKey
					}
					do {
						switch (stoi(ifs._getl())) {
							case eAnimationQuatanion:
								__animeNodes->pQuat = __animeQuats;
								__animeQuats->nTimeNum = __count = stoi(ifs._getl());
								__animeQuats->pnTimeList = __posTime;
								__animeQuats->pvQuatList = __posQuat;
								__animeQuats->pmatQuatList = __posMtx;
								for (; __count; --__count, ++__posQuat, ++__posTime, ++__posMtx) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posQuat->w = stof32(&++__src);
									__posQuat->x = stof32(&__src);
									__posQuat->y = stof32(&__src);
									__posQuat->z = stof32(__src);
									ConvertQuatToMat(__posMtx, __posQuat);
								}
								++ __animeQuats;
								break;
							case eAnimationScale:
								__animeNodes->pScale = __animeScales;
								__animeScales->nTimeNum = __count = stoi(ifs._getl());
								__animeScales->pnTimeList = __posTime;
								__animeScales->pvScaleList = __posVec;
								for (; __count; --__count, ++__posTime, ++__posVec) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posVec->x = stof32(&++__src);
									__posVec->y = stof32(&__src);
									__posVec->z = stof32(__src);
								}
								++ __animeScales;
								break;
							case eAnimationSlide:
								__animeNodes->pSlide = __animeSlides;
								__animeSlides->nTimeNum = __count = stoi(ifs._getl());
								__animeSlides->pnTimeList = __posTime;
								__animeSlides->pvSlideList = __posVec;
								for (; __count; --__count, ++__posTime, ++__posVec) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posVec->x = stof32(&++__src);
									__posVec->y = stof32(&__src);
									__posVec->z = stof32(__src);
								}
								++ __animeSlides;
								break;
							case eAnimationMatrix:
								__animeNodes->pMat = __animeMats;
								__animeMats->nTimeNum = __count = stoi(ifs._getl());
								__animeMats->pnTimeList = __posTime;
								__animeMats->pmatList = __posMtx;
								for (; __count; --__count, ++__posTime, ++__posMtx) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posMtx->_11 = stof32(&++__src);
									__posMtx->_12 = stof32(&__src);
									__posMtx->_13 = stof32(&__src);
									__posMtx->_14 = stof32(&__src);
									__posMtx->_21 = stof32(&__src);
									__posMtx->_22 = stof32(&__src);
									__posMtx->_23 = stof32(&__src);
									__posMtx->_24 = stof32(&__src);
									__posMtx->_31 = stof32(&__src);
									__posMtx->_32 = stof32(&__src);
									__posMtx->_33 = stof32(&__src);
									__posMtx->_34 = stof32(&__src);
									__posMtx->_41 = stof32(&__src);
									__posMtx->_42 = stof32(&__src);
									__posMtx->_43 = stof32(&__src);
									__posMtx->_44 = stof32(__src);
								}
								++ __animeMats;
						}
						ifs._ignorel();
						if (*(__src = ifs.getls()) == eChild) { // name, child:AKey, parent:Animation
							ns_model::GetNameT(__animeNodes->szAnimeName, ++__src, eParent);
							ifs._ignorel(); // parent:Animation
							break;
						}
					} while (*__src != eParent); // child:AKey, parent:Animation
					LinkFrame(__animeNodes->szTargetFrame, &__animeNodes->pTargetFrame, __firstFrame, __frames);
					++ __animeNodes;
					++ animation;
				}
				__animeSets->nMaxTime = *--__posTime;	// add last frame
				__animeSets->nAnimation = animation;
				++ __animeSets;
			}
		} while(__src = ifs.getls());

		ifs.close();
		return true;
	}



	ns_types::err_t CSkinedMeshModelInit::SetFromStream(CSkinedMeshModel* _Model, nslib::CIFStream& ifs, char* _Texnames)
	{
		// Lock vertex buffer
		if( _Model->m_pVB->Lock( 0, 0, reinterpret_cast<void**>( &_Model->m_pVertices ), 0 ) < 0 )
			return	false;

		// Lock index buffer
		if( _Model->m_pIB->Lock( 0, 0, reinterpret_cast<void**>( &_Model->m_pwIndices ), 0 ) < 0 )
			return	false;

		size_t __count, __vertex, __index;
		char* __ptexnames = _Texnames;

		const char __header = 'H';
		const char __temp = 't';
		const char __frame = 'F';
		const char __mesh = 'M';
		const char __animation = 'A';
		const char* __coords = "MeshT";
		const char* __material = "MeshM";
		const char* __normals = "MeshN";
		const char* __transform = "FrameT";
		const char __dupli = 'V';
		const char __xskin = 'X';

		LPMODELHEADER __modelHeader = _Model->m_pModelHead;
		LPFRAMEHEADER __posFrame = _Model->m_pFrameList, __prevFrame = NULL,
			__firstFrame = __posFrame;
		LPMESHHEADER __posMesh = _Model->m_pMeshList;
		LPTEXTUREHEADER __posTex = _Model->m_pTexList;
		LPCUSTOMVERTEX __customVers = _Model->m_pVertices,
			__firstVtx = __customVers, __polygon[3], __customVersSub;
		word_t* __wordIndices = _Model->m_pwIndices;
		LPMATERIAL __posMtrl = _Model->m_pMtrlList;
		word_t __offsetvtx = 0;
		size_t __textures = __modelHeader->nTexture, __frames = __modelHeader->nFrame;

		LPANIMEHEADER __animeHeader = _Model->m_pAnimeHead;
		LPANIMENODE __animeNodes;
		LPANIMESET __animeSets;
		LPMATRIX __posMtx;
		LPANIMEQUATHEADER __animeQuats;
		LPANIMESCALEHEADER __animeScales;
		LPANIMESLIDEHEADER __animeSlides;
		LPANIMEMATHEADER __animeMats;
		LPVECTOR4 __posQuat;
		LPVECTOR3 __posVec;
		size_t *__posTime;
		if (__animeHeader != NULL) {
			__animeNodes = __animeHeader->pAnimeList;
			__animeSets = __animeHeader->pAnimeSetList;
			__posMtx = __animeHeader->pmatList;
			__animeQuats = __animeHeader->pQuatList;
			__posQuat = __animeHeader->pvQuatList;
			__animeScales = __animeHeader->pScaleList;
			__posVec = __animeHeader->pvList;
			__animeSlides = __animeHeader->pSlideList;
			__animeMats = __animeHeader->pMatList;
			__posTime = __animeHeader->pnTimeList;
		}

		size_t __offsetBoneVtx = 0, __offsetSkinVtx = 0;
		LPSKINEDMESHHEADER __skinHeader = _Model->m_pSkinHead;
		LPSKINEDMESHNODE __posSkin = _Model->m_pSkinList;
		LPSKINEDMESHBONE __posBone = __skinHeader->pBoneList;

		ifs.seekbegin();
		// xof
		ifs._ignorel();
		char* __src = ifs.getl();
		// header
		if (*__src == eSpace) {
			ifs._ignorel(5);
		}
		// knot.x
		else {
			if (*__src == eSlash) { // comment (compatible with triplane.x, heli.x)
				ifs.ignorelc(eSlash);
				__src = ifs.getl();
			}
			// template
			if (*__src == __temp) {
				do {
					ifs.ignorel(eParent); // compatible with triplane.x
				} while (*(__src = ifs.getl()) == __temp); // getls: triplane.x ('A`)
				// header: lobby, tiger, triplane
				if (*__src == __header) {
					ifs.ignorels(eParent); // compatible with triplane.x
				}
			}
		}
		do {// while(__src = ifs.getl())
			if (*__src == eParent) {
				__prevFrame = __prevFrame->pPrev;
			}
			else if (*__src == __frame) {
				GetNameT(__posFrame->szFrameName, __src+6, eSpace);
				__src = ifs.getls();
				if (!nslib::strictlystrstr(__src, __transform)) // triplane.x
					continue;
				// FrameTransformMatrix
				D3DXMATRIX* mat = &__posFrame->matTrans;
				GetMatrixFromStream(ifs, mat);
				ifs._ignorel();
				//ifs.ignorels(__parent); // parent:Matrix

				__posFrame->pPrev = __prevFrame;
				__prevFrame = __posFrame;
				++__posFrame;
			}
			else if (*__src == __mesh) {
				__posMesh->nVertexNum = __vertex = stoi(ifs._getl());
				__customVersSub = __customVers;	// for TextureCoords
				VECTOR3* vec = &__customVers->position;
				for (size_t i=__vertex; i; --i, vec=&(++__customVers)->position) {
					__src = ifs._getl();
					vec->x = stof32(&__src);
					vec->y = stof32(&__src);
					vec->z = stof32(__src);
					__customVers->normal = VECTOR3(.0f, .0f, .0f);
				}
				__index = stoi(ifs.getl());
				size_t offsetpolygon = 0;
				for (size_t i=__index; i; --i, ++__wordIndices) {
					VECTOR3 vec;
					switch (stoi(&(__src = ifs.getls())))
					{
					case 4:
						{
							word_t* buf = __wordIndices;
							*__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
							VectorCross(&vec,
								&(__polygon[1]->position - __polygon[0]->position),
								&(__polygon[2]->position - __polygon[0]->position));
							__polygon[0]->normal += vec;
							__polygon[1]->normal += vec;
							__polygon[2]->normal += vec;
							*++__wordIndices = *buf;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = *(buf+2);
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
							++ offsetpolygon;
							break;
						}
					case 3:
						{
							*__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[0] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(&__src) + __offsetvtx;
							__polygon[1] = __firstVtx + *__wordIndices;
							*++__wordIndices = stow(__src) + __offsetvtx;
							__polygon[2] = __firstVtx + *__wordIndices;
						}
					}
					VectorCross(&vec,
						&(__polygon[1]->position - __polygon[0]->position),
						&(__polygon[2]->position - __polygon[0]->position));
					__polygon[0]->normal += vec;
					__polygon[1]->normal += vec;
					__polygon[2]->normal += vec;
				}
				__offsetvtx += (word_t)__posMesh->nVertexNum;
				__posMesh->nPolygonNum = __index + offsetpolygon;

				// child, data, parent の間に空行が無いこと前提
				while (__src = ifs.getls()) { // parent:mesh, getls:triplane.x
					if (*__src == eParent) { // mesh
						break;
					}
					else if (nslib::strictlystrstr(__src, __material)) {
						__posMesh->pMtrl = __posMtrl;
						__posMesh->nMtrlNum = __count = atoi(ifs._getl());
						ifs._ignorel(__index + 1);
						// Material
						for (; __count; --__count, ++__posMtrl) {
							ifs.getl(); // child: Material
							GetMaterialFromStream(ifs, __posMtrl);
							// TextureFilename
							__src = ifs.getls(); // child:Filename, parent:Material
							if (*__src != eParent) {
								if (__textures == 1) {
									__posMesh->pTexture = __posTex;
									ifs._ignorel(3);
								} else {
									__src = ifs.getls()+1;
									char* __s = __src;
									size_t tex = 0;
									do {
										if (*__ptexnames != *__s) {
											while (*++__ptexnames != *__s) {
												if (*__ptexnames == eSlash) {
													++ tex;
													continue;
												}
											}
										}
										while (*++__ptexnames == *++__s && *__s == eQuote) {
											if (*__ptexnames == eSlash) {
												++ tex;
												continue;
											}
										}
										break;
									} while (*++__ptexnames);
									__ptexnames = _Texnames;
									__posMesh->pTexture = __posTex + tex;
									ifs._ignorel(2);// parent:Material
								}
							}
						}
						ifs._ignorel(); // parent:MaterialList -> while
					}
					else if (nslib::strictlystrstr(__src, __coords)) {
						ifs._ignorel();
						__customVers = __customVersSub;
						for (size_t i=__vertex; i; --i, ++__customVers) {
							__src = ifs._getl();
							__customVers->texcoord.x = stof32(&__src);
							__customVers->texcoord.y = stof32(__src);
						}
						ifs._ignorel();
					}
					else if (*__src == __dupli) {
						ifs._ignorel(__vertex + 3);
					}
					else if (nslib::strictlystrstr(__src, __normals)) {
						__src = ifs._getl();
						//NS_ASSERT(__vertex != stoi(__src), STR(Mesh Normals));
						ifs._ignorel(__vertex);
						ifs.getl();
						ifs._ignorel(__index + 1);
					}
					else if (*__src == __xskin) {
						__posMesh->bSkinMesh = true;
						__posMesh->pSkinMesh = __posSkin;
						__posSkin->nVertexNum = __vertex;
						__posSkin->pnVertexList = __skinHeader->pnVertexList + __offsetBoneVtx;
						__posSkin->pfWeightList = __skinHeader->pfWeightList + __offsetBoneVtx;
						__posSkin->pBoneList = __posBone;
						__posSkin->nVertexPos = static_cast<size_t>(__offsetvtx - __posMesh->nVertexNum);
						__posSkin->pSkinVers = __skinHeader->pSkinVers + __offsetSkinVtx;
						__posSkin->pbWeight = __skinHeader->pbWeight + __offsetSkinVtx;
						__posSkin->nMaxWeightPerVertex = stoi(ifs._getl());
						__posSkin->nMaxWeightPerFace = stoi(ifs._getl());
						__posSkin->nBones = __count = stoi(ifs._getl());

						LPSKINEDMESHNODEVERTEX nodevtx = __posSkin->pSkinVers, firstnode = nodevtx;
						__customVers = __customVersSub;
						for (size_t i=__vertex; i; --i, ++nodevtx, ++__customVers) {
							nodevtx->vPosition = __customVers->position;
							nodevtx->vNormal = __customVers->normal;
							nodevtx->vTexture = __customVers->texcoord;
						}

						size_t offset = 0;
						for (size_t i=0; i<__count; ++i, ++__posBone) {
							__src = ifs.getls(4);
							GetNameT(__posBone->szBoneName, ++__src, eQuote);

							// vertex
							size_t vtx = atoi(ifs._getl());
							__posSkin->nBoneVertexAll += __posBone->nVertex = vtx;
							size_t *posv = __posBone->pnVertexList = __posSkin->pnVertexList + offset,
								*subv = posv;
							float *posw = __posBone->pfWeightList = __posSkin->pfWeightList + offset;
							offset += vtx;
							for (size_t j=0; j<vtx; ++j, ++posv) {
								*posv = stoi(ifs._getl());
							}

							// weight
							for (size_t j=0; j<vtx; ++j, ++subv, ++posw) {
								nodevtx = firstnode + *subv;
								// bone no.
								BrendingIndex(&nodevtx->dwBoneIndex, i, nodevtx->nBlendNum);
								// weight no.
								BrendingIndex(&nodevtx->dwWeightIndex, j, nodevtx->nBlendNum);
								++ nodevtx->nBlendNum;
								*posw = stof32(ifs._getl());
							}
							LPMATRIX mtx = &__posBone->matBone;
							__src = ifs._getl();
							mtx->_11 = stof32(&__src);
							mtx->_12 = stof32(&__src);
							mtx->_13 = stof32(&__src);
							mtx->_14 = stof32(&__src);
							mtx->_21 = stof32(&__src);
							mtx->_22 = stof32(&__src);
							mtx->_23 = stof32(&__src);
							mtx->_24 = stof32(&__src);
							mtx->_31 = stof32(&__src);
							mtx->_32 = stof32(&__src);
							mtx->_33 = stof32(&__src);
							mtx->_34 = stof32(&__src);
							mtx->_41 = stof32(&__src);
							mtx->_42 = stof32(&__src);
							mtx->_43 = stof32(&__src);
							mtx->_44 = stof32(__src);
							LinkFrame(__posBone->szBoneName, &__posBone->pLinkFrame, __firstFrame, __frames);
						}
						++ __posSkin;
						__offsetBoneVtx += __posSkin->nBoneVertexAll;
						__offsetSkinVtx += __vertex;
						ifs._ignorel();
					}
					else {
						ifs.ignorels(eParent);
					}
				}

				if (__frames) {
					__posMesh->pPrev = __prevFrame;
				}
				++ __posMesh;
				continue;
			}
			else if (*__src == __animation) {
				size_t animation = 0;
				__animeSets->pAnimeList = __animeNodes;
				while (*ifs.getls() != eParent) {	// child:Animation, parent:ASet
					if (*(__src = ifs.getls()) == eChild) {	// name, child:AnimationKey
						ns_model::GetNameT(__animeNodes->szAnimeName, ++__src, eParent);
						ifs._ignorel();	// child:AnimationKey
					}
					do {
						switch (stoi(ifs._getl())) {
							case eAnimationQuatanion:
								__animeNodes->pQuat = __animeQuats;
								__animeQuats->nTimeNum = __count = stoi(ifs._getl());
								__animeQuats->pnTimeList = __posTime;
								__animeQuats->pvQuatList = __posQuat;
								__animeQuats->pmatQuatList = __posMtx;
								for (; __count; --__count, ++__posQuat, ++__posTime, ++__posMtx) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posQuat->w = stof32(&++__src);
									__posQuat->x = stof32(&__src);
									__posQuat->y = stof32(&__src);
									__posQuat->z = stof32(__src);
									ConvertQuatToMat(__posMtx, __posQuat);
								}
								++ __animeQuats;
								break;
							case eAnimationScale:
								__animeNodes->pScale = __animeScales;
								__animeScales->nTimeNum = __count = stoi(ifs._getl());
								__animeScales->pnTimeList = __posTime;
								__animeScales->pvScaleList = __posVec;
								for (; __count; --__count, ++__posTime, ++__posVec) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posVec->x = stof32(&++__src);
									__posVec->y = stof32(&__src);
									__posVec->z = stof32(__src);
								}
								++ __animeScales;
								break;
							case eAnimationSlide:
								__animeNodes->pSlide = __animeSlides;
								__animeSlides->nTimeNum = __count = stoi(ifs._getl());
								__animeSlides->pnTimeList = __posTime;
								__animeSlides->pvSlideList = __posVec;
								for (; __count; --__count, ++__posTime, ++__posVec) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posVec->x = stof32(&++__src);
									__posVec->y = stof32(&__src);
									__posVec->z = stof32(__src);
								}
								++ __animeSlides;
								break;
							case eAnimationMatrix:
								__animeNodes->pMat = __animeMats;
								__animeMats->nTimeNum = __count = stoi(ifs._getl());
								__animeMats->pnTimeList = __posTime;
								__animeMats->pmatList = __posMtx;
								for (; __count; --__count, ++__posTime, ++__posMtx) {
									__src = ifs._getl();
									*__posTime = stoi(&__src);
									nslib::simplystrchr(&__src, eSemi);
									__posMtx->_11 = stof32(&++__src);
									__posMtx->_12 = stof32(&__src);
									__posMtx->_13 = stof32(&__src);
									__posMtx->_14 = stof32(&__src);
									__posMtx->_21 = stof32(&__src);
									__posMtx->_22 = stof32(&__src);
									__posMtx->_23 = stof32(&__src);
									__posMtx->_24 = stof32(&__src);
									__posMtx->_31 = stof32(&__src);
									__posMtx->_32 = stof32(&__src);
									__posMtx->_33 = stof32(&__src);
									__posMtx->_34 = stof32(&__src);
									__posMtx->_41 = stof32(&__src);
									__posMtx->_42 = stof32(&__src);
									__posMtx->_43 = stof32(&__src);
									__posMtx->_44 = stof32(__src);
								}
								++ __animeMats;
						}
						ifs._ignorel();
						if (*(__src = ifs.getls()) == eChild) { // name, child:AKey, parent:Animation
							ns_model::GetNameT(__animeNodes->szTargetFrame, __src+2, eSpace); // tiny
							ifs._ignorel(); // parent:Animation
							break;
						}
					} while (*__src != eParent); // child:AKey, parent:Animation
					LinkFrame(__animeNodes->szTargetFrame, &__animeNodes->pTargetFrame, __firstFrame, __frames);
					++ __animeNodes;
					++ animation;
				}
				__animeSets->nMaxTime = *--__posTime;	// add last frame
				__animeSets->nAnimation = animation;
				++ __animeSets;
			}
		} while(__src = ifs.getls());

		ifs.close();
		return true;
	}



//--------------------------------------------------------
//	Name:	CModelInit::Set
//	Desc:	Set model members
//--------------------------------------------------------
	bool CModelInit::Set( CModel* Model, char_t* Data, size_t square )
	{
		// Lock vertex buffer
		if( Model->m_pVB->Lock( 0, 0, reinterpret_cast<void**>( &Model->m_pVertices ), 0 ) < 0 )
			return	false;

		// Lock index buffer
		if( Model->m_pIB->Lock( 0, 0, reinterpret_cast<void**>( &Model->m_pwIndices ), 0 ) < 0 )
			return	false;

		// tokens
		const char_t	szParent[]		= STR({);
		const char_t	szChild[]		= STR(});
		const char_t	szCoron[]		= STR(;);
		const char_t	szCanma[]		= L",";
		const char_t	szLastToken[]	= L";,";
		const char_t	szNameToken[]	= STR({ });
		const char_t	szFrame[]		= STR(Frame);
		const char_t	szMesh[]		= STR(Mesh);
		const char_t	szCoords[]		= STR(Coords);
		const char_t	szMaterial[]	= STR(Material);
		const char_t	szFilename[]	= STR(Filename);
		const char_t	szSkin[]		= STR(Skin);
		const char_t	szAnimation[]	= STR(Animation);
		char_t	szDoubleQuatation[2];
		char_t	szSubTokens[4];
		char_t*	pMainToken, *pSubToken, *pNextToken = Data;

		// Mesh
		LPMODELHEADER	pModelHead	= Model->m_pModelHead;
		LPFRAMEHEADER	pFramePrev	= NULL, pFramePos = Model->m_pFrameList;
		LPMESHHEADER	pMeshPos	= Model->m_pMeshList;
		LPCUSTOMVERTEX	pVertexPos	= Model->m_pVertices, pVertexPosSub = pVertexPos, pTriangleTbl[3];
		VECTOR3			vBuf;
		word_t*			pwIndexPos	= Model->m_pwIndices, *pwIndexBuf;
		LPMATERIAL		pMtrlPos	= Model->m_pMtrlList;
		LPTEXTUREHEADER	pTexPos;
		SQUAREPOLYGONHEADER Square;

		// アニメーション
		LPANIMEHEADER	pAnimeHead	= Model->m_pAnimeHead;
		LPANIMENODE		pAnimeNodePos;
		LPANIMESET		pAnimeSetPos;
		LPMATRIX		pMatAnimePos;
		LPANIMEQUATHEADER	pQuatPos;
		LPANIMESCALEHEADER	pScalePos;
		LPANIMESLIDEHEADER	pSlidePos;
		LPANIMEMATHEADER	pMatPos;
		size_t			nOffsetTime	= 0, nOffsetQuat = 0, nOffsetMat = 0, nOffsetVec = 0, nOffsetAnime = 0,
						nTexture = pModelHead->nTexture;
		if ( pAnimeHead )
		{
			pAnimeNodePos	= pAnimeHead->pAnimeList;
			pAnimeSetPos	= pAnimeHead->pAnimeSetList;
			pMatAnimePos	= pAnimeHead->pMatAnime;
			pQuatPos	= pAnimeHead->pQuatList;
			pScalePos	= pAnimeHead->pScaleList;
			pSlidePos	= pAnimeHead->pSlideList;
			pMatPos		= pAnimeHead->pMatList;
		}

#if square
		Square.nSquare	= square;
		Square.pwSquare	= new dword_t[ square ];
		Square.pwSquare[0] = '\0';
#endif

		size_t	nNoName			= 0;
		size_t	nVertex			= 0;
		size_t	nOffsetVertex	= 0;
		size_t	nIndex			= 0;
		const size_t	nBackSlash		= '\\';

		szDoubleQuatation[0]	= 0x22;
		szDoubleQuatation[1]	= '\0';
		szSubTokens[0]	= 0x20;
		szSubTokens[1]	= 0x0d;
		szSubTokens[2]	= 0x0a;
		szSubTokens[3]	= '\0';

		// トークンループ
		while ( pMainToken = strtok_t( NULL, szChild, &pNextToken ) )
		{
			if ( !( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) ) )
			{	// 親子関係処理
				if ( pFramePrev )
					pFramePrev	= pFramePrev->pPrev;
				continue;
			}

			if ( strstr_t( pSubToken, szFrame ) )
			{
				// Frame処理
				if ( (pSubToken = strtok_t( NULL, szNameToken, &pMainToken )) && !(*pSubToken==0x0d) )
					GetName( pFramePos->szFrameName, pSubToken );	// Frame name
				else
					GetName( pFramePos->szFrameName, nNoName );	// No name

				// Matrix
				strtok_t( NULL, szParent, &pMainToken );
				FileLoadMatrix( &pFramePos->matTrans, pMainToken );

				// Frame list
				if ( pFramePrev )
					pFramePos->pPrev	= pFramePrev;
				pFramePrev	= pFramePos;
				++ pFramePos;
			}
			else if ( strstr_t( pSubToken, szMesh ) )
			{
				// Mesh処理
				if ( (pSubToken = strtok_t( NULL, szParent, &pMainToken )) && !(*pSubToken==0x0d) )
					GetName( pMeshPos->szMeshName, pSubToken );
				else
					GetName( pMeshPos->szMeshName, nNoName );

				// Vertices
				pMeshPos->nVertexNum = nVertex = atoi_t( strtok_t( NULL, szCoron, &pMainToken ) );
				for ( size_t iVertex=0; iVertex<nVertex; ++iVertex, ++pVertexPos )
				{
					pVertexPos->position.x	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken )) );
					pVertexPos->position.y	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken )) );
					pVertexPos->position.z	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken )) );
					pVertexPos->normal		= VECTOR3(.0f, .0f, .0f);
				}

				// Indices
				pMeshPos->nPolygonNum = nIndex = atoi_t( strtok_t( NULL, szCoron, &pMainToken ) );
				for ( size_t iIndex=0; iIndex<nIndex; ++iIndex )
				{
					switch ( atoi_t( strtok_t( NULL, szLastToken, &pMainToken ) ) )
					{
					case 3:
						{
							for ( size_t iIndex=0; iIndex<3; ++iIndex, ++pwIndexPos )
							{
								*pwIndexPos				= static_cast<word_t>( atoi_t( strtok_t( NULL, szLastToken, &pMainToken )) + nOffsetVertex );
								pTriangleTbl[ iIndex ]	= Model->m_pVertices + *pwIndexPos;
							}
						} break;

					case 4:	// 未完成
						{
							pwIndexBuf	= pwIndexPos;
							// 三角形ポリゴン１枚目
							for ( size_t iIndex=0; iIndex<3; ++iIndex, ++pwIndexPos )
							{
								*pwIndexPos		= static_cast<word_t>( atoi_t( strtok_t( NULL, szLastToken, &pMainToken )) + nOffsetVertex );
								pTriangleTbl[ iIndex ]	= Model->m_pVertices + *pwIndexPos;
							}
							VectorCross( &vBuf,
								&(pTriangleTbl[1]->position - pTriangleTbl[0]->position),
								&(pTriangleTbl[2]->position - pTriangleTbl[0]->position) );
							for ( size_t iTriangle=0; iTriangle<3; ++iTriangle )
								pTriangleTbl[ iTriangle ]->normal	+= vBuf;

							// ２枚目
							*pwIndexPos			= *( pwIndexBuf ) + static_cast<word_t>(nOffsetVertex);
							pTriangleTbl[0]		= Model->m_pVertices + *pwIndexPos;
							*( ++pwIndexPos )	= *( pwIndexBuf + 2 ) + static_cast<word_t>(nOffsetVertex);	// ←あやしい
							pTriangleTbl[1]		= Model->m_pVertices + *pwIndexPos;
							*( ++pwIndexPos )	= static_cast<word_t>( atoi_t( strtok_t( NULL, szLastToken, &pMainToken )) + nOffsetVertex );
							pTriangleTbl[2]		= Model->m_pVertices + *pwIndexPos;

							// 変数更新
							++ pMeshPos->nPolygonNum;
							++ pwIndexPos;
						} break;
					}
					VectorCross( &vBuf,
						&(pTriangleTbl[1]->position - pTriangleTbl[0]->position),
						&(pTriangleTbl[2]->position - pTriangleTbl[0]->position) );
					for ( size_t iTriangle=0; iTriangle<3; ++iTriangle )
						pTriangleTbl[ iTriangle ]->normal += vBuf;
				}
				++ pMainToken;

				// Mesh内ループ
				do
				{
					if ( !( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) ) )
						break;
					else if ( strstr_t( pSubToken, szCoords ) )
					{
						// Texture coords処理
						strtok_t(NULL, szSubTokens, &pMainToken);
						if (nVertex != atoi_t(strtok_t(NULL, szCoron, &pMainToken)))
							//NS_ASSERT(true, STR(Dont Mach the Texture coords Number));

						for (size_t i=0; i<nVertex; ++i, ++pVertexPosSub)
						{
							pVertexPosSub->texcoord.x = static_cast<float>(atof_t(strtok_t(NULL, szLastToken, &pMainToken)));
							pVertexPosSub->texcoord.y = static_cast<float>(atof_t(strtok_t(NULL, szLastToken, &pMainToken)));
						}
					}
					else if ( strstr_t( pSubToken, szMaterial ) )
					{
						size_t	nMtrl	= 0;
						pMeshPos->pMtrl	= pMtrlPos;

						// Material list処理
						pMainToken	= strstr_t( pMainToken, szParent ) + 1;	// Material名
						pMeshPos->nMtrlNum = nMtrl = atoi_t( pMainToken );
						pMainToken	= strstr_t( pMainToken, szParent ) + 1;

						for ( size_t iMtrl=0; iMtrl<nMtrl; ++iMtrl, ++pMtrlPos )
						{
							// Material処理
							//++ pModelHead->nMaterialNum;
							if ( iMtrl > 0 )
								pMainToken	= strtok_t( NULL, szChild, &pNextToken );
							pMeshPos->pMtrl->Ambient.r	= static_cast<float>( atof_t( strtok_t( NULL, szCoron, &pMainToken )) );
							pMeshPos->pMtrl->Ambient.g	= static_cast<float>( atof_t( strtok_t( NULL, szCoron, &pMainToken )) );
							pMeshPos->pMtrl->Ambient.b	= static_cast<float>( atof_t( strtok_t( NULL, szCoron, &pMainToken )) );
							pMeshPos->pMtrl->Ambient.a	= static_cast<float>( atof_t( strtok_t( NULL, szCoron, &pMainToken )) );
							pMeshPos->pMtrl->Power		= static_cast<float>( atof_t( strtok_t( NULL, szCoron, &pMainToken )) );
							pMeshPos->pMtrl->Specular.r	= static_cast<float>( atof_t( strtok_t( NULL, szCoron, &pMainToken )) );
							pMeshPos->pMtrl->Specular.g	= static_cast<float>( atof_t( strtok_t( NULL, szCoron, &pMainToken )) );
							pMeshPos->pMtrl->Specular.b	= static_cast<float>( atof_t( strtok_t( NULL, szCoron, &pMainToken )) );
							pMeshPos->pMtrl->Emissive.r	= static_cast<float>( atof_t( strtok_t( NULL, szCoron, &pMainToken )) );
							pMeshPos->pMtrl->Emissive.g	= static_cast<float>( atof_t( strtok_t( NULL, szCoron, &pMainToken )) );
							pMeshPos->pMtrl->Emissive.b	= static_cast<float>( atof_t( strtok_t( NULL, szCoron, &pMainToken )) );
							++ pMainToken;

							if ( ( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) )
								&& strstr_t( pSubToken, szFilename ) )
							{
								// Texture file name処理
								strtok_t( NULL, szDoubleQuatation, &pMainToken );
								pSubToken	= strtok_t( NULL, szDoubleQuatation, &pMainToken );
								pTexPos=Model->m_pTexList;
								for ( size_t i=0; i<nTexture; ++i, ++pTexPos )
								{
									if ( strstr_t( pTexPos->szTexName, pSubToken ) )
									{
										pMeshPos->pTexture = pTexPos;
										break;
									}
								}
								strtok_t( NULL, szChild, &pNextToken );
							}
						}
						pNextToken	= strstr_t( pNextToken, szChild );
					}
					else if ( strstr_t( pSubToken, szSkin ) )
					{
						//NS_ASSERT(true, STR(Mesh Roop Error));
					}
				} while ( pMainToken = strtok_t( NULL, szChild, &pNextToken ) );

				++pMeshPos;
			}
			else if ( strstr_t( pSubToken, szAnimation ) )	// Animation set
			{
				size_t		nType, nTime, nAnime(0), nFrame = Model->m_pModelHead->nFrame;
				char_t	szSet[]	= STR(AnimationSet);
				char_t	szKey[]	= STR(Key);
				size_t	nAnimationSet	= Model->m_pModelHead->nAnimationSet, nParent = 0;

				// Animation set処理
				//++ pModelHead->nAnimeSetNum;
				pMainToken	= strstr_t( pMainToken, szParent ) + 1;
				pSubToken	= strtok_t( NULL, szParent, &pMainToken );	// Animation Name

				// Animationループ
				do
				{
					if ( strstr_t( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ), szParent ) )
					{
						// frame name処理
						if ( (pSubToken = strtok_t( NULL, szNameToken, &pMainToken )) && !(*pSubToken==0x0d) )
							GetName( pAnimeNodePos->szTargetFrame, pSubToken );	// Frame name
						else
							GetName( pAnimeNodePos->szTargetFrame, nNoName );	// No name
						// frameの関連付け
						for ( size_t iFrame=0; iFrame<nFrame; ++iFrame )
						{
							if ( ns_win::StrCmp( pAnimeNodePos->szTargetFrame, Model->m_pFrameList[iFrame].szFrameName ) )
							{
								pAnimeNodePos->pTargetFrame	= &Model->m_pFrameList[iFrame];
								Model->m_pFrameList[iFrame].pmatAnim	= pMatAnimePos;
								++ pMatAnimePos;
								break;
							}
						}

						pMainToken	= strtok_t( NULL, szChild, &pNextToken );
						pSubToken	= strtok_t( NULL, szSubTokens, &pMainToken );
					}

					// Animation keyループ
					while ( pSubToken && strstr_t( pSubToken, szKey ) )
					{
						// Animation key処理
						++ pMainToken;
						nType	= atoi_t( strtok_t( NULL, szSubTokens, &pMainToken ) );
						nTime	= atoi_t( strtok_t( NULL, szSubTokens, &pMainToken ) );
						//pAnimeHead->nTimeList	+= nTime;
						switch ( nType )
						{
						case eAnimationQuatanion:
							{	// Quatanion処理
								pAnimeNodePos->pQuat	= pQuatPos;
								pQuatPos->nTimeNum		= nTime;

								pQuatPos->pnTimeList	= &pAnimeHead->pnTimeList[ nOffsetTime ];
								pQuatPos->pvQuatList	= &pAnimeHead->pvQuatList[ nOffsetQuat ];
								pQuatPos->pmatQuatList	= &pAnimeHead->pmatList[ nOffsetMat ];

								for ( size_t i=0; i<nTime; ++i )
								{
									pQuatPos->pnTimeList[i]		= atoi_t(strtok_t(NULL, szCoron, &pMainToken));
									strtok_t(NULL, szCoron, &pMainToken);
									pQuatPos->pvQuatList[i].w	= static_cast<float>(atof_t(strtok_t(NULL, szCanma, &pMainToken)));
									pQuatPos->pvQuatList[i].x	= static_cast<float>(atof_t(strtok_t(NULL, szCanma, &pMainToken)));
									pQuatPos->pvQuatList[i].y	= static_cast<float>(atof_t(strtok_t(NULL, szCanma, &pMainToken)));
									pQuatPos->pvQuatList[i].z	= static_cast<float>(atof_t(strtok_t(NULL, szCanma, &pMainToken)));
									ConvertQuatToMat(&pQuatPos->pmatQuatList[i], &pQuatPos->pvQuatList[i]);
								}

								// 最終フレームタイム更新
								if ( pAnimeSetPos->nMaxTime < pQuatPos->pnTimeList[pQuatPos->nTimeNum-1] )
									pAnimeSetPos->nMaxTime = pQuatPos->pnTimeList[pQuatPos->nTimeNum-1];

								// 変数更新
								nOffsetTime += pQuatPos->nTimeNum;
								nOffsetQuat	+= pQuatPos->nTimeNum;
								nOffsetMat	+= pQuatPos->nTimeNum;
								++ pQuatPos;
							} break;

						case eAnimationScale:
							{	// Scale処理
								//++ pAnimeHead->nScale;
								//pAnimeHead->nVecList	+= nTime;
							} break;

						case eAnimationSlide:
							{	// Slide処理
								//++ pAnimeHead->nSlide;
								//pAnimeHead->nVecList	+= nTime;
							} break;

						case eAnimationMatrix:
							{	// Matrix処理
								//++ pAnimeHead->nMat;
								//pAnimeHead->nMatList	+= nTime;
							} break;
						}

						pMainToken	= strtok_t( NULL, szChild, &pNextToken );
						pSubToken	= strtok_t( NULL, szSubTokens, &pMainToken );
					}	//<- AnimationKey ループ

					if ( !pSubToken )
						pMainToken	= strtok_t( NULL, szParent, &pNextToken );
					else if ( strstr_t( pSubToken, szParent ) )
					{
						// Animation name処理(Frame Name?)
						pMainToken	= strtok_t( NULL, szSubTokens, &( pNextToken += 4 ) );
					}

					if ( !strstr_t( pMainToken, szAnimation ) )	// Animation
					{
						if ( ( pMainToken = strtok_t( NULL, szSubTokens, &pNextToken ) )
							&& strstr_t( pMainToken, szSet ) )
						{
							// Animation set処理
							//++ pModelHead->nAnimeSetNum;
							strtok_t( NULL, szParent, &pNextToken );	// Animation
							strtok_t( NULL, szParent, &pNextToken );	// AnimationKey
						}
						else
							break;
					}

					++ pAnimeNodePos;
					++ nAnime;
				} while ( pMainToken = strtok_t( NULL, szChild, &( ++pNextToken ) ) );	// Animation ループ

				// AnimationSet の処理の最後
				pAnimeSetPos->nAnimation = nAnime - nOffsetAnime;
				nOffsetAnime = nAnime;
				++ pAnimeSetPos;
			}
		}

		Model->m_pVB->Unlock();
		Model->m_pIB->Unlock();
		delete [] Data;
		return	true;
	}



	//--------------------------------------------------------
	//	Name:	CModelInit::Create
	//	Desc:	Create model data
	//--------------------------------------------------------
	bool CModelInit::Create(  CModel* Model, char_t* Data, size_t square )
	{
		if ( !Allocate( Model ) || !Set( Model, Data, square ) )
			return	false;
		Scaling( Model );
		return	true;
	}



	//--------------------------------------------------------
	//	Name:	CModelInit::Scaling
	//	Desc:	Scaling any Model
	//--------------------------------------------------------
	void CModelInit::Scaling( CModel* model )
	{
		size_t	nVertex,
			nMesh	= model->m_pModelHead->nMesh,
			nFrame	= model->m_pModelHead->nFrame;
		float	fMaximumVertex	= 0, fRatio = 0;
		VECTOR3	vTemp;
		MATRIX	mtx;
		LPCUSTOMVERTEX	pVertexPos	= model->m_pVertices;
		LPMESHHEADER	pMeshPos	= model->m_pMeshList;

		for (size_t iMesh=0; iMesh<nMesh; ++iMesh, ++pMeshPos)
		{
			// メッシュの変換行列の構築
			if (nFrame)
			{
				TransMatrixCompleteTest(&mtx, pMeshPos->pPrev);
			}

			// 実空間での頂点の最大ベクトルを求める
			//nOffsetVertex += pMeshPos->nVertexNum;
			nVertex	= pMeshPos->nVertexNum;
			for (size_t jVers=0; jVers<nVertex; ++jVers, ++pVertexPos)
			{
				VectorIdiot(&pVertexPos->normal);
				if (nFrame)
				{
					VecxMat(&vTemp, &pVertexPos->position, &mtx);
					vTemp.x	= sqrtf(vTemp.x * vTemp.x);
					vTemp.y	= sqrtf(vTemp.y * vTemp.y);
					vTemp.z	= sqrtf(vTemp.z * vTemp.z);

					if (vTemp.x > fMaximumVertex)
						fMaximumVertex = vTemp.x;
					if (vTemp.y > fMaximumVertex)
						fMaximumVertex = vTemp.y;
					if (vTemp.z > fMaximumVertex)
						fMaximumVertex = vTemp.z;
				}
				else
				{
					if ((vTemp.x = sqrtf(pVertexPos->position.x * pVertexPos->position.x)) > fMaximumVertex)
						fMaximumVertex = vTemp.x;
					if ((vTemp.y = sqrtf(pVertexPos->position.y * pVertexPos->position.y)) > fMaximumVertex)
						fMaximumVertex = vTemp.y;
					if ((vTemp.z = sqrtf(pVertexPos->position.z * pVertexPos->position.z)) > fMaximumVertex)
						fMaximumVertex = vTemp.z;
				}
			}
		}

		fRatio	= ns_d3d::OBJECT_SIZE / fMaximumVertex;
		//MatrixIdentity( &model->m_matScale );
		model->m_matScale._11 = fRatio;
		model->m_matScale._22 = fRatio;
		model->m_matScale._33 = fRatio;
	}

	
	//--------------------------------------------------------
	//	Name:	CSkinedMeshModelInit::Allocate
	//	Desc:	Allocate CModel member
	//--------------------------------------------------------
	bool CSkinedMeshModelInit::Allocate( CSkinedMeshModel* Model )
	{
		MODELHEADER*		pModelHead	= Model->m_pModelHead;
		ANIMEHEADER*		pAnimeHead	= Model->m_pAnimeHead;
		SKINEDMESHHEADER*	pSkinHead	= Model->m_pSkinHead;

		// Create the vertex buffer.
		if ( FAILED( ns_d3d::Cd3d::Instance().Getd3dDevice()->CreateVertexBuffer( static_cast<UINT>(pModelHead->nVertex * sizeof( CUSTOMVERTEX )), 0, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &Model->m_pVB, NULL )) )
			return	false;

		// Fill the index buffer.
		if ( FAILED( ns_d3d::Cd3d::Instance().Getd3dDevice()->CreateIndexBuffer( static_cast<UINT>(pModelHead->nIndex * 3 * sizeof( word_t )), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, &Model->m_pIB, NULL )) )
			return	false;

		try
		{
			// フレーム領域
			if ( pModelHead->nFrame )
			{
				Model->m_pFrameList	= new FRAMEHEADER[ pModelHead->nFrame ];
				//memset( Model->m_pFrameList,	'\0', sizeof( FRAMEHEADER ) * pModelHead->nFrame );
			}

			// メッシュ領域
			Model->m_pMeshList		= new MESHHEADER[ pModelHead->nMesh ];
			//memset( Model->m_pMeshList,	'\0', sizeof( MESHHEADER ) * pModelHead->nMesh );

			// マテリアル領域
			if ( pModelHead->nMaterial )
			{
				Model->m_pMtrlList		= new MATERIAL[ pModelHead->nMaterial ];
				//memset( Model->m_pMtrlList,	'\0', sizeof( MATERIAL ) * pModelHead->nMaterial );
			}

			//////////////////////////////////////
			//	アニメ領域

			if ( pModelHead->nAnimationSet )
			{
				pAnimeHead->pAnimeSetList	= new ANIMESET[ pModelHead->nAnimationSet ];
				//memset( pAnimeHead->pAnimeSetList,	'\0', sizeof( ANIMESET ) * pModelHead->nAnimationSet );
				pAnimeHead->pAnimeList	= new ANIMENODE[ pAnimeHead->nAnimation ];
				//memset( pAnimeHead->pAnimeList,		'\0', sizeof( ANIMENODE ) * pAnimeHead->nAnimation );
				pAnimeHead->pMatAnime		= new MATRIX[ pAnimeHead->nAnimation ];
				//memset( pAnimeHead->pMatAnime,		'\0', sizeof( MATRIX ) * pAnimeHead->nAnimation );
				pAnimeHead->pnTimeList	= new size_t[ pAnimeHead->nTimeList ];
				//memset( pAnimeHead->pnTimeList,		'\0', sizeof( size_t ) * pAnimeHead->nTimeList );

				if ( pAnimeHead->nQuat )
				{
					pAnimeHead->pQuatList	= new ANIMEQUATHEADER[ pAnimeHead->nQuat ];
					//memset( pAnimeHead->pQuatList,	'\0', sizeof( ANIMEQUATHEADER ) * pAnimeHead->nQuat );
				}
				if ( pAnimeHead->nScale )
				{
					pAnimeHead->pScaleList	= new ANIMESCALEHEADER[ pAnimeHead->nScale ];
					//memset( pAnimeHead->pScaleList,	'\0', sizeof( ANIMESCALEHEADER ) * pAnimeHead->nScale );
				}
				if ( pAnimeHead->nSlide )
				{
					pAnimeHead->pSlideList	= new ANIMESLIDEHEADER[ pAnimeHead->nSlide ];
					//memset( pAnimeHead->pSlideList,	'\0', sizeof( ANIMESLIDEHEADER ) * pAnimeHead->nSlide );
				}
				if ( pAnimeHead->nMat )
				{
					pAnimeHead->pMatList	= new ANIMEMATHEADER[ pAnimeHead->nMat ];
					//memset( pAnimeHead->pMatList,	'\0', sizeof( ANIMEMATHEADER ) * pAnimeHead->nMat );
				}

				if ( pAnimeHead->nQuatList )
				{
					pAnimeHead->pvQuatList	= new VECTOR4[ pAnimeHead->nQuatList ];
					//memset( pAnimeHead->pvQuatList,	'\0', sizeof( VECTOR4 ) * pAnimeHead->nQuatList );
				}
				if ( pAnimeHead->nMatList )
				{
					pAnimeHead->pmatList	= new MATRIX[ pAnimeHead->nMatList ];
					//memset( pAnimeHead->pmatList,	'\0', sizeof( MATRIX ) * pAnimeHead->nMatList );
				}
				if ( pAnimeHead->nVecList )
				{
					pAnimeHead->pvList		= new VECTOR3[ pAnimeHead->nVecList ];
					//memset( pAnimeHead->pvList,		'\0', sizeof( VECTOR3 ) * pAnimeHead->nVecList );
				}
			}

			/////////////////////////////////////
			//	Allocate Skined Mesh Pool.
			Model->m_pSkinList			= new SKINEDMESHNODE[ pModelHead->nSkinedMesh ];
			//memset( Model->m_pSkinList,		'\0',	sizeof( SKINEDMESHNODE ) * pModelHead->nSkinedMesh );

			pSkinHead->pbWeight			= new bool[ pSkinHead->nVertexAll ];
			pSkinHead->pnVertexList		= new size_t[ pSkinHead->nWeightAll ];
			pSkinHead->pfWeightList		= new float[ pSkinHead->nWeightAll ];
			pSkinHead->pSkinVers		= new SKINEDMESHNODEVERTEX[ pSkinHead->nVertexAll ];
			pSkinHead->pBoneList		= new SKINEDMESHBONE[ pSkinHead->nBoneAll ];
			//memset( pSkinHead->pbWeight,	'\0',	sizeof( bool ) * pSkinHead->nVertexAll );
			//memset( pSkinHead->pnVertexList,'\0',	sizeof( size_t ) * pSkinHead->nWeightAll );
			//memset( pSkinHead->pfWeightList,'\0',	sizeof( float ) * pSkinHead->nWeightAll );
			//memset( pSkinHead->pSkinVers,	'\0',	sizeof( SKINEDMESHNODEVERTEX ) * pSkinHead->nVertexAll );
			//memset( pSkinHead->pBoneList,	'\0',	sizeof( SKINEDMESHBONE ) * pSkinHead->nBoneAll );
		}
		catch ( std::bad_alloc )
		{
			return false;
		}

		return	true;
	};



//--------------------------------------------------------
//	Name:	CSkinedMeshModelInit::Set
//	Desc:	Set Skined mesh model members
//--------------------------------------------------------
	bool CSkinedMeshModelInit::Set( CSkinedMeshModel* Model, char_t* Data, size_t square )
	{
		// Lock vertex buffer
		if( Model->m_pVB->Lock( 0, 0, reinterpret_cast<void**>( &Model->m_pVertices ), 0 ) < 0 )
			return	false;

		// Lock index buffer
		if( Model->m_pIB->Lock( 0, 0, reinterpret_cast<void**>( &Model->m_pwIndices ), 0 ) < 0 )
			return	false;

		// tokens
		const char_t	szParent[]		= STR({);
		const char_t	szChild[]		= STR(});
		const char_t*	pCoron			= STR(;);
		const char_t*	pCanma			= L",";
		const char_t	szLastToken[]	= L";,";
		const char_t	szNameToken[]	= STR({ });
		const char_t	szFrame[]		= STR(Frame);
		const char_t	szMesh[]		= STR(Mesh);
		const char_t	szCoords[]		= STR(Coords);
		const char_t	szMaterial[]	= STR(Material);
		const char_t	szFilename[]	= STR(Filename);
		const char_t	szSkin[]		= STR(Skin);
		const char_t	szAnimation[]	= STR(Animation);
		char_t	szDoubleQuatation[2];
		char_t	szSubTokens[4];
		char_t*	pMainToken, *pSubToken, *pNextToken = Data;

		// Mesh
		LPMODELHEADER	pModelHead	= Model->m_pModelHead;
		LPFRAMEHEADER	pFramePrev	= NULL, pFramePos = Model->m_pFrameList;
		LPMESHHEADER	pMeshPos	= Model->m_pMeshList;
		LPCUSTOMVERTEX	pVertexPos	= Model->m_pVertices, pVertexPosSub = pVertexPos, pTriangleTbl[3];
		VECTOR3			vBuf;
		word_t*			pwIndexPos	= Model->m_pwIndices, *pwIndexBuf;
		LPMATERIAL		pMtrlPos	= Model->m_pMtrlList;
		LPTEXTUREHEADER	pTexPos;
		SQUAREPOLYGONHEADER Square;

		// SkinedMesh
		LPSKINEDMESHNODE	pSkinPos	= Model->m_pSkinList;
		LPSKINEDMESHHEADER	pSkinHead	= Model->m_pSkinHead;
		LPSKINEDMESHBONE	pBonePos	= pSkinHead->pBoneList;
		size_t			nOffsetSkinVertex=0, nOffsetBoneVertex=0, nOffsetBone=0;

		// アニメーション
		LPANIMEHEADER	pAnimeHead	= Model->m_pAnimeHead;
		LPANIMENODE		pAnimeNodePos;
		LPANIMESET		pAnimeSetPos;
		LPMATRIX		pMatAnimePos;
		LPANIMEQUATHEADER	pQuatPos;
		LPANIMESCALEHEADER	pScalePos;
		LPANIMESLIDEHEADER	pSlidePos;
		LPANIMEMATHEADER	pMatPos;
		size_t			nOffsetTime	= 0, nOffsetQuat = 0, nOffsetMat = 0, nOffsetVec = 0, nOffsetAnime = 0;
		if ( pAnimeHead )
		{
			pAnimeNodePos	= pAnimeHead->pAnimeList;
			pAnimeSetPos	= pAnimeHead->pAnimeSetList;
			pMatAnimePos	= pAnimeHead->pMatAnime;
			pQuatPos	= pAnimeHead->pQuatList;
			pScalePos	= pAnimeHead->pScaleList;
			pSlidePos	= pAnimeHead->pSlideList;
			pMatPos		= pAnimeHead->pMatList;
		}

#if square
		Square.nSquare	= square;
		Square.pwSquare	= new dword_t[ square ];
		Square.pwSquare[0] = '\0';
#endif

		size_t	nNoName			= 0;
		size_t	nVertex			= 0;
		size_t	nOffsetVertex	= 0;
		size_t	nIndex			= 0;
		const size_t	nBackSlash		= '\\';

		szDoubleQuatation[0]	= 0x22;
		szDoubleQuatation[1]	= '\0';
		szSubTokens[0]	= 0x20;
		szSubTokens[1]	= 0x0d;
		szSubTokens[2]	= 0x0a;
		szSubTokens[3]	= '\0';

		// トークンループ
		while ( pMainToken = strtok_t( NULL, szChild, &pNextToken ) )
		{
			if ( !( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) ) )
			{	// 親子関係処理
				if ( pFramePrev )
					pFramePrev	= pFramePrev->pPrev;
				continue;
			}

			if ( strstr_t( pSubToken, szFrame ) )
			{
				// Frame処理
				if ( (pSubToken = strtok_t( NULL, szNameToken, &pMainToken )) && !(*pSubToken==0x0d) )
					GetName( pFramePos->szFrameName, pSubToken );	// Frame name
				else
					GetName( pFramePos->szFrameName, nNoName );		// No name

				// Matrix
				strtok_t( NULL, szParent, &pMainToken );
				FileLoadMatrix( &pFramePos->matTrans, pMainToken );

				// Frame list
				if ( pFramePrev )
					pFramePos->pPrev	= pFramePrev;
				pFramePrev	= pFramePos;
				++ pFramePos;
			}
			else if ( strstr_t( pSubToken, szMesh ) )
			{
				// 親子関係
				if ( pModelHead->nFrame )
				{
					pMeshPos->pPrev = pFramePrev;
				}
				else
				{
					//NS_ASSERT( true, STR(NO FRAME) );
				}

				// Mesh処理
				if ( *pMainToken == szParent[0] )
				{
					GetName( pMeshPos->szMeshName, nNoName );
					++ pMainToken;
				}
				else if ( (pSubToken = strtok_t( NULL, szParent, &pMainToken )) && !(*pSubToken==0x0d) )
					GetName( pMeshPos->szMeshName, pSubToken );
				else
					GetName( pMeshPos->szMeshName, nNoName );

				// Vertices
				pMeshPos->nVertexNum = nVertex = atoi_t( strtok_t( NULL, pCoron, &pMainToken ) );
				for ( size_t iVertex=0; iVertex<nVertex; ++iVertex, ++pVertexPos )
				{
					pVertexPos->position.x	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken )) );
					pVertexPos->position.y	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken )) );
					pVertexPos->position.z	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken )) );
					pVertexPos->normal = VECTOR3( .0f, .0f, .0f );	// normalの値のバグ対策
				}

				// Indices
				pMeshPos->nPolygonNum = nIndex = atoi_t( strtok_t( NULL, pCoron, &pMainToken ) );
				for ( size_t iIndex=0; iIndex<nIndex; ++iIndex )
				{
					switch ( atoi_t( strtok_t( NULL, szLastToken, &pMainToken ) ) )
					{
					case 3:
						{
							for ( size_t iIndex=0; iIndex<3; ++iIndex, ++pwIndexPos )
							{
								*pwIndexPos				= static_cast<word_t>( atoi_t( strtok_t( NULL, szLastToken, &pMainToken )) + nOffsetVertex );
								pTriangleTbl[ iIndex ]	= Model->m_pVertices + *pwIndexPos;
							}
						} break;

					case 4:	// 未完成
						{
							pwIndexBuf	= pwIndexPos;
							// 三角形ポリゴン１枚目
							for ( size_t iIndex=0; iIndex<3; ++iIndex, ++pwIndexPos )
							{
								*pwIndexPos		= static_cast<word_t>( atoi_t( strtok_t( NULL, szLastToken, &pMainToken )) + nOffsetVertex );
								pTriangleTbl[ iIndex ]	= Model->m_pVertices + *pwIndexPos;
							}
							VectorCross( &vBuf,
								&(pTriangleTbl[1]->position - pTriangleTbl[0]->position),
								&(pTriangleTbl[2]->position - pTriangleTbl[0]->position) );
							for ( size_t iTriangle=0; iTriangle<3; ++iTriangle )
								pTriangleTbl[ iTriangle ]->normal	+= vBuf;

							// ２枚目
							*pwIndexPos			= *( pwIndexBuf ) + static_cast<word_t>(nOffsetVertex);
							pTriangleTbl[0]		= Model->m_pVertices + *pwIndexPos;
							*( ++pwIndexPos )	= *( pwIndexBuf + 2 ) + static_cast<word_t>(nOffsetVertex);	// ←あやしい
							pTriangleTbl[1]		= Model->m_pVertices + *pwIndexPos;
							*( ++pwIndexPos )	= static_cast<word_t>( atoi_t( strtok_t( NULL, szLastToken, &pMainToken )) + nOffsetVertex );
							pTriangleTbl[2]		= Model->m_pVertices + *pwIndexPos;

							// 変数更新
							++ pMeshPos->nPolygonNum;
							++ pwIndexPos;
						} break;
					}
					VectorCross( &vBuf,
						&(pTriangleTbl[1]->position - pTriangleTbl[0]->position),
						&(pTriangleTbl[2]->position - pTriangleTbl[0]->position) );
					for ( size_t iTriangle=0; iTriangle<3; ++iTriangle )
						pTriangleTbl[ iTriangle ]->normal += vBuf;
				}
				++ pMainToken;

				// Mesh内ループ
				do
				{
					if ( !( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) ) )
						break;
					else if ( strstr_t( pSubToken, szCoords ) )
					{
						// Texture coords処理
						strtok_t(NULL, szSubTokens, &pMainToken);
						if (nVertex != atoi_t(strtok_t(NULL, pCoron, &pMainToken)))
							//NS_ASSERT(true, STR(Dont Mach the Texture coords Number));

						for (size_t i=0; i<nVertex; ++i, ++pVertexPosSub)
						{
							pVertexPosSub->texcoord.x = static_cast<float>(atof_t(strtok_t(NULL, szLastToken, &pMainToken)));
							pVertexPosSub->texcoord.y = static_cast<float>(atof_t(strtok_t(NULL, szLastToken, &pMainToken)));
						}

					}
					else if ( strstr_t( pSubToken, szMaterial ) )
					{
						size_t	nMtrl	= 0;
						pMeshPos->pMtrl	= pMtrlPos;

						// Material list処理
						pMainToken	= strstr_t( pMainToken, szParent ) + 1;	// Material名
						pMeshPos->nMtrlNum = nMtrl = atoi_t( pMainToken );
						pMainToken	= strstr_t( pMainToken, szParent ) + 1;

						for ( size_t iMtrl=0; iMtrl<nMtrl; ++iMtrl, ++pMtrlPos )
						{
							// Material処理
							//++ pModelHead->nMaterialNum;
							if ( iMtrl > 0 )
								pMainToken	= strtok_t( NULL, szChild, &pNextToken );
							pMeshPos->pMtrl->Ambient.r	= static_cast<float>( atof_t( strtok_t( NULL, pCoron, &pMainToken )) );
							pMeshPos->pMtrl->Ambient.g	= static_cast<float>( atof_t( strtok_t( NULL, pCoron, &pMainToken )) );
							pMeshPos->pMtrl->Ambient.b	= static_cast<float>( atof_t( strtok_t( NULL, pCoron, &pMainToken )) );
							pMeshPos->pMtrl->Ambient.a	= static_cast<float>( atof_t( strtok_t( NULL, pCoron, &pMainToken )) );
							pMeshPos->pMtrl->Power		= static_cast<float>( atof_t( strtok_t( NULL, pCoron, &pMainToken )) );
							pMeshPos->pMtrl->Specular.r	= static_cast<float>( atof_t( strtok_t( NULL, pCoron, &pMainToken )) );
							pMeshPos->pMtrl->Specular.g	= static_cast<float>( atof_t( strtok_t( NULL, pCoron, &pMainToken )) );
							pMeshPos->pMtrl->Specular.b	= static_cast<float>( atof_t( strtok_t( NULL, pCoron, &pMainToken )) );
							pMeshPos->pMtrl->Emissive.r	= static_cast<float>( atof_t( strtok_t( NULL, pCoron, &pMainToken )) );
							pMeshPos->pMtrl->Emissive.g	= static_cast<float>( atof_t( strtok_t( NULL, pCoron, &pMainToken )) );
							pMeshPos->pMtrl->Emissive.b	= static_cast<float>( atof_t( strtok_t( NULL, pCoron, &pMainToken )) );
							++ pMainToken;

							if ( ( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ) )
								&& strstr_t( pSubToken, szFilename ) )
							{
								// Texture file name処理
								strtok_t( NULL, szDoubleQuatation, &pMainToken );
								pSubToken	= strtok_t( NULL, szDoubleQuatation, &pMainToken );
								pTexPos=Model->m_pTexList;
								for ( size_t i=0; i<pModelHead->nTexture; ++i, ++pTexPos )
								{
									if ( strstr_t( pTexPos->szTexName, pSubToken ) )
									{
										pMeshPos->pTexture = pTexPos;
										break;
									}
								}
								strtok_t( NULL, szChild, &pNextToken );
							}
						}
						pNextToken	= strstr_t( pNextToken, szChild );
					}
					else if ( strstr_t( pSubToken, szSkin ) )
					{
						size_t		nBone=0, nOffset=0, nVers=0, jVers=0, *pBoneVersPos;
						LPSKINEDMESHNODEVERTEX	pSkinVerNodePos;
						LPCUSTOMVERTEX			pVersPos;
						LPFRAMEHEADER			pFrameBuf;

						// モデルのスキンメッシュ設定
						pMeshPos->bSkinMesh	= true;
						pMeshPos->pSkinMesh	= pSkinPos;

						strtok_t( NULL, szSubTokens, &pMainToken );
						pSkinPos->nMaxWeightPerVertex	= atoi_t( strtok_t( NULL, pCoron, &pMainToken ) );
						pSkinPos->nMaxWeightPerFace		= atoi_t( strtok_t( NULL, pCoron, &pMainToken ) );
						pSkinPos->nBones				= nBone	= atoi_t( pMainToken );	// ボーン数

						pSkinPos->nVertexNum			= pMeshPos->nVertexNum;
						pSkinPos->pnVertexList			= &pSkinHead->pnVertexList[ nOffsetBoneVertex ];
						pSkinPos->pfWeightList			= &pSkinHead->pfWeightList[ nOffsetBoneVertex ];
						pSkinPos->pBoneList				= pBonePos;
						pSkinPos->nVertexPos			= nOffsetVertex;

						pSkinPos->pSkinVers				= &pSkinHead->pSkinVers[ nOffsetSkinVertex ];
						pSkinPos->pbWeight				= &pSkinHead->pbWeight[ nOffsetSkinVertex ];

						// メッシュで使う頂点リストを格納
						pSkinVerNodePos	= pSkinPos->pSkinVers;
						pVersPos		= &Model->m_pVertices[ pSkinPos->nVertexPos ];
						for ( size_t i=0; i<pSkinPos->nVertexNum; ++i )
							pSkinVerNodePos->vPosition	= pVersPos[ i ].position;

						// Skin weightsループ
						for ( size_t iBone=0; iBone<nBone; ++iBone, ++pBonePos )
						{
							pMainToken	= strtok_t( NULL, szChild, &pNextToken );
							pSubToken	= strtok_t( NULL, pCoron, &pMainToken );
							strtok_t( NULL, szDoubleQuatation, &pSubToken );

							GetName( pBonePos->szBoneName, strtok_t( NULL, szDoubleQuatation, &pSubToken ) );	// ボーン名
							pBonePos->nVertex			= nVers = atoi_t( strtok_t( NULL, pCoron, &pMainToken ) );	// 頂点数
							pSkinPos->nBoneVertexAll	+= pBonePos->nVertex;						// 作用する頂点の計数
							pBonePos->pnVertexList		= &pSkinPos->pnVertexList[ nOffset ];
							pBonePos->pfWeightList		= &pSkinPos->pfWeightList[ nOffset ];

							nOffset	+= pBonePos->nVertex;	// オフセット更新

							// 頂点リスト
							pBoneVersPos	= pBonePos->pnVertexList;
							for ( jVers=0; jVers<nVers; ++jVers, ++pBoneVersPos )
								*pBoneVersPos	= atoi_t( strtok_t( NULL, szLastToken, &pMainToken ) );

							// ウェイトリスト
							pBoneVersPos	= pBonePos->pnVertexList;
							for ( jVers=0; jVers<nVers; ++jVers, ++pBoneVersPos )
							{
								pSkinVerNodePos	= &pSkinPos->pSkinVers[ *pBoneVersPos ];
								BrendingIndex( &pSkinVerNodePos->dwBoneIndex, iBone, pSkinVerNodePos->nBlendNum );
								BrendingIndex( &pSkinVerNodePos->dwWeightIndex, jVers, pSkinVerNodePos->nBlendNum );
								++ pSkinVerNodePos->nBlendNum;
								pBonePos->pfWeightList[ jVers ]	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							}

							pBonePos->matBone._11	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._12	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._13	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._14	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._21	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._22	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._23	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._24	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._31	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._32	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._33	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._34	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._41	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._42	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._43	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );
							pBonePos->matBone._44	= static_cast<float>( atof_t( strtok_t( NULL, szLastToken, &pMainToken ) ) );

							// フレームとリンク
							pFrameBuf	= Model->m_pFrameList;
							for ( jVers=0; jVers<pModelHead->nFrame; ++jVers, ++pFrameBuf )
							{
								if ( strstr_t( pBonePos->szBoneName, pFrameBuf->szFrameName ) )
								{
									pBonePos->pLinkFrame	= pFrameBuf;
									break;
								}
							}
						}

						// オフセット更新
						nOffsetBone			+= pSkinPos->nBones;
						nOffsetBoneVertex	+= pSkinPos->nBoneVertexAll;
						nOffsetSkinVertex	+= pSkinPos->nVertexNum;
						++ pSkinPos;
					}
				} while ( pMainToken = strtok_t( NULL, szChild, &pNextToken ) );

				++pMeshPos;
				nOffsetVertex += nVertex;
			}
			else if ( strstr_t( pSubToken, szAnimation ) )	// Animation set
			{
				size_t		nType, nTime, nAnime(0), nFrame = Model->m_pModelHead->nFrame;
				char_t	szSet[]	= STR(AnimationSet);
				char_t	szKey[]	= STR(Key);
				size_t	nAnimationSet	= Model->m_pModelHead->nAnimationSet, nParent = 0;
				size_t* time;
				LPVECTOR3 vec3;
				LPVECTOR4 vec4;
				LPMATRIX mtx;

				// Animation set処理
				//++ pModelHead->nAnimeSetNum;
				pMainToken	= strstr_t( pMainToken, szParent ) + 1;
				pSubToken	= strtok_t( NULL, szParent, &pMainToken );	// Animation Name

				// Animationループ
				do
				{
					if ( strstr_t( pSubToken = strtok_t( NULL, szSubTokens, &pMainToken ), szParent ) )
					{
						// frame name処理
						if ( (pSubToken = strtok_t( NULL, szNameToken, &pMainToken )) && !(*pSubToken==0x0d) )
							GetName( pAnimeNodePos->szTargetFrame, pSubToken );	// Frame name
						else
							GetName( pAnimeNodePos->szTargetFrame, nNoName );	// No name
						// frameの関連付け
						for ( size_t iFrame=0; iFrame<nFrame; ++iFrame )
						{
							if ( ns_win::StrCmp( pAnimeNodePos->szTargetFrame, Model->m_pFrameList[iFrame].szFrameName ) )
							{
								pAnimeNodePos->pTargetFrame	= &Model->m_pFrameList[iFrame];
								Model->m_pFrameList[iFrame].pmatAnim	= pMatAnimePos;
								++ pMatAnimePos;
								break;
							}
						}

						pMainToken	= strtok_t( NULL, szChild, &pNextToken );
						pSubToken	= strtok_t( NULL, szSubTokens, &pMainToken );
					}

					// Animation keyループ
					while ( pSubToken && strstr_t( pSubToken, szKey ) )
					{
						// Animation key処理
						++ pMainToken;
						nType	= atoi_t( strtok_t( NULL, szSubTokens, &pMainToken ) );
						nTime	= atoi_t( strtok_t( NULL, szSubTokens, &pMainToken ) );
						//pAnimeHead->nTimeList	+= nTime;
						switch ( nType )
						{
						case eAnimationQuatanion:
							{	// Quatanion処理
								pAnimeNodePos->pQuat	= pQuatPos;
								pQuatPos->nTimeNum		= nTime;

								time = pQuatPos->pnTimeList		= pAnimeHead->pnTimeList + nOffsetTime;
								vec4 = pQuatPos->pvQuatList		= pAnimeHead->pvQuatList + nOffsetQuat;
								mtx = pQuatPos->pmatQuatList	= pAnimeHead->pmatList + nOffsetMat;
								for ( size_t i=0; i<nTime; ++i, ++time, ++vec4, ++mtx )
								{
									*time = atoi_t(strtok_t(NULL, pCoron, &pMainToken));
									strtok_t(NULL, pCoron, &pMainToken);
									vec4->w = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									vec4->x = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									vec4->y = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									vec4->z = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									ConvertQuatToMat(mtx, vec4);
								}

								// 最終フレームタイム更新
								if ( pAnimeSetPos->nMaxTime < *(--time) )
									pAnimeSetPos->nMaxTime = *time;

								// 変数更新
								nOffsetTime += nTime;
								nOffsetQuat	+= nTime;
								nOffsetMat	+= nTime;
								++ pQuatPos;
							} break;

						case eAnimationScale:
							{	// Scale処理
								pAnimeNodePos->pScale	= pScalePos;
								pScalePos->nTimeNum		= nTime;

								time = pScalePos->pnTimeList	= pAnimeHead->pnTimeList + nOffsetTime;
								vec3 = pScalePos->pvScaleList	= pAnimeHead->pvList + nOffsetVec;
								for ( size_t i=0; i<nTime; ++i, ++time, ++vec3 )
								{
									*time = atoi_t(strtok_t(NULL, pCoron, &pMainToken));
									strtok_t(NULL, pCoron, &pMainToken);
									vec3->x = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									vec3->y = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									vec3->z = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
								}

								if ( pAnimeSetPos->nMaxTime < *(--time) )
									pAnimeSetPos->nMaxTime = *time;

								nOffsetTime += nTime;
								nOffsetVec	+= nTime;
								++pScalePos;
							} break;

						case eAnimationSlide:
							{	// Slide
								pAnimeNodePos->pSlide	= pSlidePos;
								pSlidePos->nTimeNum		= nTime;

								time = pSlidePos->pnTimeList	= pAnimeHead->pnTimeList + nOffsetTime;
								vec3 = pSlidePos->pvSlideList	= pAnimeHead->pvList + nOffsetVec;
								for ( size_t i=0; i<nTime; ++i, ++time, ++vec3 )
								{
									*time = atoi_t(strtok_t(NULL, pCoron, &pMainToken));
									strtok_t(NULL, pCoron, &pMainToken);
									vec3->x = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									vec3->y = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									vec3->z = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
								}

								if ( pAnimeSetPos->nMaxTime < *(--time) )
									pAnimeSetPos->nMaxTime = *time;

								nOffsetTime += nTime;
								nOffsetVec	+= nTime;
								++pSlidePos;
							} break;

						case eAnimationMatrix:
							{	// Matrix処理
								pAnimeNodePos->pMat	= pMatPos;
								pMatPos->nTimeNum	= nTime;

								time = pMatPos->pnTimeList	= pAnimeHead->pnTimeList + nOffsetTime;
								mtx	= pMatPos->pmatList		= pAnimeHead->pmatList + nOffsetMat;

								for ( size_t i=0; i<nTime; ++i, ++time, ++mtx )
								{
									*time = atoi_t(strtok_t(NULL, pCoron, &pMainToken));
									strtok_t(NULL, pCoron, &pMainToken);
									mtx->_11 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_12 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_13 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_14 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_21 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_22 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_23 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_24 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_31 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_32 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_33 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_34 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_41 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_42 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_43 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
									mtx->_44 = static_cast<float>(atof_t(strtok_t(NULL, pCanma, &pMainToken)));
								}

								if ( pAnimeSetPos->nMaxTime < *(--time) )
									pAnimeSetPos->nMaxTime = *time;

								nOffsetTime += nTime;
								nOffsetMat	+= nTime;
								++pMatPos;
							} break;
						}

						pMainToken	= strtok_t( NULL, szChild, &pNextToken );
						pSubToken	= strtok_t( NULL, szSubTokens, &pMainToken );
					}	//<- AnimationKey ループ

					if ( !pSubToken )
						pMainToken	= strtok_t( NULL, szParent, &pNextToken );
					else if ( strstr_t( pSubToken, szParent ) )
					{
						// Animation name処理(Frame Name?)
						pMainToken	= strtok_t( NULL, szSubTokens, &( pNextToken += 4 ) );
					}

					if ( !strstr_t( pMainToken, szAnimation ) )	// Animation
					{
						if ( ( pMainToken = strtok_t( NULL, szSubTokens, &pNextToken ) )
							&& strstr_t( pMainToken, szSet ) )
						{
							// Animation set処理
							//++ pModelHead->nAnimeSetNum;
							strtok_t( NULL, szParent, &pNextToken );	// Animation
							strtok_t( NULL, szParent, &pNextToken );	// AnimationKey
						}
						else
							break;
					}

					++ pAnimeNodePos;
					++ nAnime;
				} while ( pMainToken = strtok_t( NULL, szChild, &( ++pNextToken ) ) );	// Animation ループ

				// AnimationSet の処理の最後
				pAnimeSetPos->nAnimation = nAnime - nOffsetAnime;
				nOffsetAnime = nAnime;
				++ pAnimeSetPos;
			}
		}

		Model->m_pVB->Unlock();
		Model->m_pIB->Unlock();
		delete [] Data;
		return	true;
	}



	//--------------------------------------------------------
	//	Name:	CSkinedMeshModelInit::Create
	//	Desc:	Create skined mesh model data
	//--------------------------------------------------------
	bool CSkinedMeshModelInit::Create(  CSkinedMeshModel* Model, char_t* Data, size_t square )
	{
		if ( !Allocate( Model ) || !Set( Model, Data, square ) )
			return	false;
		Scaling( Model );
		return	true;
	}

}	// namespace: ns_model
