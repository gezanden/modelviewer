//
//	CModel.h
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once
#ifndef	_CMODEL_H_
#define	_CMODEL_H_

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include	"ModelStructures.h"

//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_model
{
	class CModelInit;
	class CSkinedMeshModelInit;
	class CModelAllocator;
	class CWorldOrganizer;


//--------------------------------------------------------
//	Enum
//--------------------------------------------------------
	enum eANIMATIONNUMBER
	{
		eAnimationQuatanion,
		eAnimationScale,
		eAnimationSlide,
		eAnimationMatrix	= 4,
	};


//--------------------------------------------------------
//	name:	CModel
//	desc:	Model class
//--------------------------------------------------------

	class CModel
	{
		friend CModelInit;
		friend CModelAllocator;
		friend CWorldOrganizer;

	public:
		explicit CModel( LPMODELHEADER pModelHead, LPANIMEHEADER pAnimeHead );
		explicit CModel( LPMODELHEADER pModelHead, LPANIMEHEADER pAnimeHead, LPTEXTUREHEADER pTexHead );
		explicit CModel( LPMODELHEADER pModelHead, LPANIMEHEADER pAnimeHead, LPTEXTUREHEADER pTexHead, LPEFFECTHEADER pEffect );
		virtual ~CModel();
		virtual void Render( const LPMATRIX matrix );
		void RenderTest( const LPMATRIX matrix );
		virtual void Release();
		void Animation(size_t _AnimeNo);
		const LPVECTOR3 GetRadiies() {
			return &m_pModelHead->vRadius;
		}

	protected:
		explicit CModel();
		CModel( const CModel& );
		CModel& operator =( const CModel& );

		LPMODELHEADER	m_pModelHead;
		MATRIX			m_matScale;				// モデルのスケーリング用マトリクス
		LPCUSTOMVERTEX	m_pVertices;			// 頂点情報
		word_t*			m_pwIndices;			// 頂点バッファ
		LPFRAMEHEADER	m_pFrameList;			// フレーム情報
		LPMESHHEADER	m_pMeshList;			// メッシュ情報
		LPTEXTUREHEADER	m_pTexList;				// テクスチャ情報
		LPMATERIAL		m_pMtrlList;			// マテリアル情報
		LPANIMEHEADER	m_pAnimeHead;			// アニメーション情報格納メモリ領域群
		LPEFFECTHEADER	m_pEffectHead;
		LPDIRECT3DVERTEXBUFFER9 m_pVB;			// Buffer to hold vertices
		LPDIRECT3DINDEXBUFFER9	m_pIB;			// Buffer to hold indices
	};

	class CSkinedMeshModel : public CModel
	{
		friend	CSkinedMeshModelInit;
		friend CModelAllocator;

	public:
		explicit CSkinedMeshModel( LPMODELHEADER pModelHead, LPANIMEHEADER pAnimeHead, LPSKINEDMESHHEADER pSkinHead );
		explicit CSkinedMeshModel( LPMODELHEADER pModelHead, LPANIMEHEADER pAnimeHead, LPSKINEDMESHHEADER pSkinHead, LPTEXTUREHEADER pTexHead );
		explicit CSkinedMeshModel( LPMODELHEADER pModelHead, LPANIMEHEADER pAnimeHead, LPSKINEDMESHHEADER pSkinHead, LPTEXTUREHEADER pTexHead, LPEFFECTHEADER pEffect );
		virtual ~CSkinedMeshModel();
		void Render( const LPMATRIX matrix );
		void Release();

	private:
		explicit CSkinedMeshModel();
		CSkinedMeshModel( const CSkinedMeshModel& );
		CSkinedMeshModel& operator =( const CSkinedMeshModel& );
		void Skining(LPSKINEDMESHNODE);

		size_t					m_nSkinedMesh;		// スキンメッシュ数
		LPSKINEDMESHNODE		m_pSkinList;		// スキンメッシュ情報
		LPSKINEDMESHHEADER		m_pSkinHead;		// スキンメッシュ情報格納メモリ領域群
//		LPSKINEDMESHNODEVERTEX	m_pVertices;		// 頂点情報
//		word_t*					m_pwIndices;		// 頂点バッファ
//		LPDIRECT3DVERTEXBUFFER9 m_pVB;				// Buffer to hold vertices
//		LPDIRECT3DINDEXBUFFER9	m_pIB;				// Buffer to hold indices
	};

}	// namespace: ns_model

#endif	// _CMODEL_H_