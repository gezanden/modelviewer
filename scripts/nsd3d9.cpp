//
//	nsd3d9.cpp
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include	"nsd3d9.h"


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_d3d
{


	//--------------------------------------------------------
	//	Variables
	//--------------------------------------------------------
	const bool	WINDOW_MODE	= true;
	const float	OBJECT_SIZE	= 10.0f;		// オブジェクトサイズ比率
	const float PI			= 3.141592654f;


	//------------------------------------------------------------------------------
	//	Name:	MatrixIdentity
	//	Desc:	Matrix identity
	//------------------------------------------------------------------------------
	LPMATRIX MatrixIdentity( LPMATRIX Out )
	{
		return	D3DXMatrixIdentity( Out );
	}


	//------------------------------------------------------------------------------
	//	Name:	MatrixMultiply
	//	Desc:	Multiply matrix 
	//------------------------------------------------------------------------------
	LPMATRIX MatrixMultiply( LPMATRIX Out, const LPMATRIX M1, const LPMATRIX M2 )
	{
		return	D3DXMatrixMultiply( Out, M1, M2 );
	}

}	// namespace: ns_d3d
