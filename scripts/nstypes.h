//
//	nstypes.h
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once
#ifndef	_NSTYPES_H_
#define	_NSTYPES_H_

//-------------------------------------------------
//	Include
//-------------------------------------------------
#include	<stdio.h>
#include	<wchar.h>
#include	<tchar.h>
#include	<string>


//#define JOIN(X,Y) X##Y

namespace ns_types
{

	//-------------------------------------------------
	//	Variables
	//-------------------------------------------------
	enum eNSTYPES
	{
		eNull = 0x0000,
		eTab = 0x0009,
		eLineFeed,// = 0x000a,
		eReturn = 0x000d,
		eSpace = 0x0020,
		eQuote = 0x0022,
		eComma = 0x002c,
		eSlash = 0x002f,
		eZero,// = 0x0030,
		eSemi = 0x003b,
		eBackSlash = 0x005c,
		eChild = 0x007b,
		eParent = 0x007d,
		eNameLength	= 127,
		eSizeofName,// = 0x0080,
		eLineLength = 255,
		eSizeofLine,// = 0x0100,
		eDescLength = 511,
		eSizeofDesc,// = 0x0200,
		eChunkLength = 1023,
		eMaximizeChunk,// = 0x0400, // Maximize chunk size
		eTerminate = 0xffff,
		eSizeofWord = 2,
	};

	//-------------------------------------------------
	//	Typedefine
	//-------------------------------------------------
	typedef unsigned char		byte_t;
	typedef	unsigned short		word_t;
	typedef unsigned long		dword_t;
	typedef	unsigned int		uint_t;
	typedef int					err_t;
	typedef int					result_t;
	typedef int					code_t;

#ifdef _UNICODE
	typedef	wchar_t				char_t;
	typedef	std::wstring		string_t;
#define	strlen_t			wcsnlen
#define strstr_t			wcsstr
#define strtok_t			wcstok_s
#define strchr_t			wcschr
#define strcmp_t			wcscmp
#define strcpy_t			wcscpy_s
#define atoi_t				_wtoi
#define atof_t				_wtof
#define sprintf_t			swprintf_s
#define memcpy_t			wmemcpy_s
#define	STR(str)			L###str
#define MBS(str)			#str
#define CHR(chr)			L##chr
#else	// _UNICODE
	typedef char				char_t;
	typedef std::string			string_t;
#define	strlen_t			strlen
#define strstr_t			strstr
#define strtok_t			strtok_s
#define strchr_t			strchr
#define strcmp_t			strcmp
#define strcpy_t			strcpy
#define atoi_t				atoi
#define atof_t				atof
#define sprintf_t			sprintf_s
#define memcpy_t			memcpy_s
#define STR(str)			#str
#define MBS(str)			#str
#define CHR(chr)			chr
#endif	// _UNICODE


#ifdef _WIN64
	typedef unsigned __int64	w64_t;
#else	// _WIN64
	typedef __w64 unsigned int	w64_t;
#endif	// _WIN64

}	// ns_types

#endif	// _NSTYPES_H_
