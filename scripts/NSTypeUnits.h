#pragma	once
#ifndef	_NSTYPEUNITS_H_
#define	_NSTYPEUNITS_H_

#include "NSNullType.h"

namespace ns_types
{

	/////////////////////////////////////////////////
	//	型導出 P.31,35
	template <int v>
	struct Int2Type
	{
		enum { value = v };
	};

	template <typename T>
	struct Type2Type
	{
		typedef T	OriginalType;
//		Type2Type(){}	// VC7
	};

	/////////////////////
	// 型の選択 P.36
	template <bool flag, typename T, typename U>
	struct Select
	{
		typedef T	Result;
	};
	template <typename T, typename U>
	struct Select<false, T, U>
	{
		typedef U	Result;
	};

	// SameType: LokiPort\TypeManip.h::L.87
	// Result evaluates to true iff U == T (types equal)
	template <typename T, typename U>
	struct SameType
	{
	private:
		template<typename>
		struct In
		{ enum { value = false }; };

		template<>
		struct In<T>
		{ enum { value = true }; };

	public:
		enum { value = In<U>::value };
	};

	///////////////////////////////////
	// テンプレート指定式型保存コンテナ P34
	template <typename T, bool isPolymorphic>
	struct NiftyContainerValueTraits
	{
		typedef T*	ValueType;
	};
	template <typename T>
	struct NiftyContainerValueTraits<T, false>
	{
		typedef T	ValueType;
	};
	template <typename T, bool isPolymorphic>
	class NiftyContainer
	{
	private:
		void DoSomething(T* pObj, Int2Type<true>)
		{
			T* pNewObj = pObj->Clone();
			// モリモフィズム版アルゴリズム
		}
		void DoSomething(T* pObj, Int2Type<false>)
		{
			T* pNewObj = new T(*pObj);
			// 非ポリモフィズム版アルゴリズム
		}
	public:
		void DoSomething(T* pObj)
		{
			DoSomething(pObj, Int2Type<isPolymorphic>());
		}
		typedef typename Select<isPolymorphic, T*, T>::Result
			ValueType;
		typedef NiftyContainerValueTraits<T, isPolymorphic>
			Traits;
		//typedef typename Traits::ValueType	ValueType;
	};

	//////////////////////////////////
	// 型 T が型 U への自動変換をサポートしている（TはUを継承している）かの検出 P.39
	template <class T, class U>
	class Conversion
	{
		typedef char	Small;
		class Big { char dummy[2]; };
		static Small	Test(U);
		static Big		Test(...);	// 自動変換が存在しない場合にのみ起動される
		static T		MakeT();
	public:
		enum { exists = sizeof(Test(MakeT())) == sizeof(Small) };	// サポートしていれば true
		enum { sameType = false	};	// T と U が同じ型を表現している場合に true を返す
	};
	// sameType P.39
	template <class T>
	class Conversion<T, T>
	{
	public:
		enum { exists = 1, sameType = 1 };
	};

	// 継承関係の判定
	// const Sub*が暗黙のうちに const Super*へと変換される場合(true)
	// 1. T が U と同じ型である
	// 2. T が U の public かつ曖昧でない基底型である
	// 3. T が void である
#define	NS_SUPERSUBCLASS(Super, Sub)	\
	(Conversion<const Sub*, const Super*>::exists &&	\
	!Conversion<const Super*, const void*>::sameType)

	// より厳密な判定
#define NS_SUPERSUBCLASS_STRICT(T, U)	\
	(NS_SUPERSUBCLASS(T, U) &&	\
	!Conversion<const T*, const U*>::sameType)


	/////////////////////////////////////////////////
	//	Type List
	//	型情報を取り扱う
	//
	//	Modern C++ Design - P.55
	template <class T, class U>
	struct Typelist
	{
		typedef	T	Head;
		typedef	U	Tail;
	};

// TYPELISTマクロ
#define	TYPELIST_1(T1)	ns_types::Typelist<T1, ns_types::NullType >
#define	TYPELIST_2(T1, T2)	ns_types::Typelist<T1, TYPELIST_1(T2) >
#define	TYPELIST_3(T1, T2, T3)	ns_types::Typelist<T1, TYPELIST_2(T2, T3) >
#define	TYPELIST_4(T1, T2, T3, T4)	ns_types::Typelist<T1, TYPELIST_3(T2, T3, T4) >
#define	TYPELIST_5(T1, T2, T3, T4, T5)	ns_types::Typelist<T1, TYPELIST_4(T2, T3, T4, T5) >
#define	TYPELIST_6(T1, T2, T3, T4, T5, T6)	ns_types::Typelist<T1, TYPELIST_5(T2, T3, T4, T5, T6) >
#define	TYPELIST_7(T1, T2, T3, T4, T5, T6, T7)	ns_types::Typelist<T1, TYPELIST_6(T2, T3, T4, T5, T6, T7) >
#define	TYPELIST_8(T1, T2, T3, T4, T5, T6, T7, T8)	ns_types::Typelist<T1, TYPELIST_7(T2, T3, T4, T5, T6, T7, T8) >
#define	TYPELIST_9(T1, T2, T3, T4, T5, T6, T7, T8, T9)	ns_types::Typelist<T1, TYPELIST_8(T2, T3, T4, T5, T6, T7, T8, T9) >

	// typedef TPELIST_4(signed char, short int, int, long int) SignedIntegrals;

	/////////////////////////////////////////////////
	//	Length
	//	TYPELISTの長さを測る
	template <class TList> struct Length;
	template <> struct Length<NullType>
	{
		enum { value = 0 };
	};
	template <class T, class U>
	struct Length< Typelist<T, U> >		// Typelistの深さだけ再帰し、NullTypeが来たら終了。
	{
		enum { value = 1 + ns_types::Length<U>::value };
	};

	// std::type_info*	intsRtti[ Length<SignedIntegrals>::value ];

	// IndexOf P.60
	// タイプリストから型を検索し、そのインデックスを求める
	template <class TList, class T> struct IndexOf;

	template <class T>
	struct IndexOf<NullType, T>
	{
		enum { value = -1 };
	};

	template <class T, class Tail>
	struct IndexOf<Typelist<T, Tail>, T>
	{
		enum { value = 0 };
	};

	template <class Head, class Tail, class T>
	struct IndexOf<Typelist<Head, Tail>, T>
	{
	private:
		enum { temp = IndexOf<Tail, T>::value };
	public:
		enum { value = temp == -1 ? -1 : 1 + temp };
	};

	//P.62

	////////////////////////////////////////////////////
	//	TYPEAT
	//	タイプリストへのインデックスによるアクセス

	template <class TList, unsigned int index> struct TypeAt;
	template <class Head, class Tail>
	struct TypeAt<Typelist<Head, Tail>, 0>
	{
		typedef	Head	Result;
	};
	template <class Head, class Tail, unsigned int i>
	struct TypeAt<Typelist<Head, Tail>, i>
	{
		typedef	typename TypeAt<Tail, i-1>::Result	Result;
	};

	template <class TList, unsigned int index, typename DefaultType = NullType>
	struct TypeAtNonStrict
	{
		typedef typename TList::Head	Head;
		typedef typename TList::Tail	Tail;

	private:
		template <class TList1, unsigned int i>
		struct In
		{
			typedef typename TypeAtNonStrict
			<
				typename TList1::Tail,
				i - 1,
				DefaultType
			>
			::Result	Result;
		};

		template <>
		struct In<Typelist<Head, Tail>, 0>
		{
			typedef Head	Result;
		};

		template <>
		struct In<NullType, index>
		{
			typedef DefaultType	Result;
		};

	public:
		typedef typename In<TList, index>::Result	Result;
	};
}

#endif	// _NSTYPESTRUCTS_H_