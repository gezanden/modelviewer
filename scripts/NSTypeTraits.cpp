//
//	NSTypeTraits.cpp
//
//	Copyright (c) 2008 Naoki Shimoyamada, All Rights Reserved.
//

#include	"NSTypeTraits.h"

namespace ns_types
{
	void BitBlast(const void* src, void* dest, unsigned int bytes)
	{
		std::memcpy(dest, src, bytes);
	}
}