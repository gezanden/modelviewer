//
//	Cd3d.hpp
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once
#ifndef	_CD3D_H_
#define	_CD3D_H_

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
#include	"CModel.h"
#include	"nswin.h"
#include	"CSingleton.h"


//--------------------------------------------------------
//	Define
//--------------------------------------------------------
// Our custom FVF, which describes our custom vertex structure
#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1)


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace nsgl
{
	class CPlayerController;
}

namespace ns_d3d
{

	//--------------------------------------------------------
	//	Class
	//--------------------------------------------------------

	//////////////////////////////////////////////////////////
	//	name:	Cd3d
	//
	//	member:	m_pd3d			- Direct 3D device
	//			m_pd3dDevice	- Other device
	//
	//	desc:	DirectX device 管理クラス（シングルトン）
	//			- このクラスは DirectX device をラッパして、DirectX device の処理をクラス内で行う。
	//
	//	function:
	//			Instance		- クラスのオブジェクトを返す
	//			Release			- クラスのオブジェクトを破棄する
	//			Getd3dDevice	- LPDIRECT3DDEVICE9 型を返す
	//			CreateDevice	- Direct 3D device の初期化
	//
	class Cd3d : public ns_singleton::CSingleton< Cd3d >
	{
		friend nsgl::CPlayerController;
	public:
		Cd3d();
		~Cd3d()
		{
			if ( m_pd3d )		m_pd3d->Release();
			if ( m_pd3dDevice )	m_pd3dDevice->Release();
		}
		//static Cd3d& Instance() { return Cd3d::GetSingleton(); }
		//void Release() { Cd3d::Instance().ReleaseSingleton(); }
		const LPDIRECT3DDEVICE9 Getd3dDevice() { return m_pd3dDevice; }
		bool CreateDevice( const HWND hWnd );
		void CreateTexture( const ns_types::char_t* name, LPDIRECT3DTEXTURE9* texture, const ns_types::char_t* model );
		void XFileRenderer( ns_model::CModel* model );
		void SetVertexBuffer( LPDIRECT3DVERTEXBUFFER9 vb ) { m_pd3dDevice->SetStreamSource(0, vb, 0, sizeof(ns_model::CUSTOMVERTEX)); }
		void SetIndexBuffer( LPDIRECT3DINDEXBUFFER9 ib ) { m_pd3dDevice->SetIndices(ib); }
		void SetWorld( const D3DXMATRIX* mtx ) const;
		void MatrixWorldToViewPort(LPMATRIX mtx);
		void ViewMatrix();
		void SetShaderHandles();
		void RefreshScene();
		void ExecuteTest();
		static LRESULT WINAPI Cd3d::CamMsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
		HRESULT CreateEffect(const ns_types::char_t* pass, const ns_types::char_t* name, LPD3DXEFFECT *effect);
		HRESULT BeginEffect(ns_model::LPEFFECTINSTANCE fxCont, LPMATRIX world);

	private:
		const ns_types::char_t*			m_texture_pass;
		LPDIRECT3D9			m_pd3d;			// Used to create the D3DDevice
		LPDIRECT3DDEVICE9	m_pd3dDevice;	// Our rendering device
		LPDIRECT3DVERTEXDECLARATION9	m_handle_decl;	// 頂点型ハンドル
		IDirect3DVertexShader9*			m_handle_vs;	// 頂点シェーダハンドル
		IDirect3DPixelShader9*			m_handle_ps;	// ピクセルシェーダハンドル
		MATRIX							m_mtx_view;		// 視点変換行列
		MATRIX							m_mtx_proj;		// プロジェクション行列

		VECTOR3							m_cam_pos;		// カメラ座標
		VECTOR3							m_cam_trg;		// カメラ注視座標
		float							m_pitch;		// カメラピッチ
		float							m_yaw;			// カメラヨー
		float							m_dist;			// カメラ距離

		VECTOR3							m_light_pos;	// ディレクショナルライト座標
		MATRIX							m_mtx_world;	// ワールド変換行列
	};
}


#endif	// _CD3D_H_
