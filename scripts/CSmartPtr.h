//
//	CSmatrPtr.h
//
//	Copyright (c) 2006 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once

//--------------------------------------------------------
//	Include
//--------------------------------------------------------
//#include	"nsrefcount.h"
#include	"CReferenceCount.h"


//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_smartptr
{


	//--------------------------------------------------------
	//	Template class
	//--------------------------------------------------------

	///////////////////////////////////////////////////////
	//	name:	CSmartPtr
	//
	//	member:	m_pRef		- スマートポインタが参照カウントを調べるために浸入するオブジェクト
	//			m_pPoint	- CSmartPtr がラップするオブジェクトのポインタ
	//
	//	desc:	進入型参照カウント方式スマートポインタ
	//			・演算子 * -> = をオーバーロード。
	//			・if ( CSmartPtr ) に対応。
	//				- CSmartPtrがラップしているオブジェクトの有無。
	//			・CSmartPtrがラップしているオブジェクトの型が引数の関数にそのまま渡す。
	//				- Func( const T* Obj ) という関数に Func( CSmartPtr ) と書いてオブジェクトを渡せる。
	//				- ※しかし、Func の中で delete Obj をすると動作未定義になる。
	//				- スマートポインタを使うシステムで、オブジェクトをそのまま delete することはしないと
	//				  思うので使いやすさ（生ポインタと同じような動作）優先。（クライアントを信頼して）
	//			・↑の機能を実装しつつ delete CSmartPtr にはエラー。
	//			・CSmartPtr::Release でデストラクタを意図的に呼び出せる。
	//			・T* 型を参照にとるコンストラクタは、中で new しないとならないので、安全性優先で private。
	//
	//	Reference documents:
	//			・Andrei Alexandrescu (2001/12) 『Modern C++ Design』 Peason Education Japan P167
	//			・Scott Meyers (2006/4/29) 『Effective C++ 原著第3版』 Peason Education Japan P27,31,50,234
	//
	template <typename T>
	class CSmartPtr
	{
		typedef	ns_refcount::CReferenceCount<T>	CReferenceCount;

	public:
		CSmartPtr( const CSmartPtr& rhs )
			: m_pRef( rhs.m_pRef )
			, m_pPoint( m_pRef ? m_pRef->Get() : 0 )
		{}

		template <typename U>
		CSmartPtr( const CSmartPtr<U>& other )
			: m_pRef( other.m_pRef )
			, m_pPoint( m_pRef ? other.Get() : 0 )
		{}

		CSmartPtr()
			: m_pRef( 0 )
			, m_pPoint( 0 )
		{}

		~CSmartPtr();

		void Release() const
		{
			delete this;
		}

		// 各種演算子オーバーロード
		const T& operator*() const
		{
			return	m_pPoint ? *m_pPoint : false;
		}

		const T* operator->() const
		{
			return	m_pPoint ? m_pPoint : false;
		}

		CSmartPtr& operator =( const CSmartPtr& rhs );

		template <typename U>
		CSmartPtr& operator =( const CSmartPtr<U>& other );

		// if ( CSmartPtr ) への対応
		operator const T*() const
		{
			return	m_pPoint ? m_pPoint : false;
		}

		operator const void*() const
		{
			return	m_pPoint ? m_pPoint : false;
		}

	private:
		CSmartPtr( T* );

		T* Get() const
		{
			return	m_pPoint;
		}

		CReferenceCount*	m_pRef;
		const T*			m_pPoint;
	};

	template <typename T> CSmartPtr<T>::~CSmartPtr()
	{
		m_pRef->Erase();
	}

	template <typename T> CSmartPtr<T>& CSmartPtr<T>::operator =( const CSmartPtr& rhs )
	{
		if ( this == &rhs )	return *this;		// 自己代入時
		m_pPoint	= m_pRef->Insert( rhs );
		return	*this;
	}

}	// namespace: ns_memory
