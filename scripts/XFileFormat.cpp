#include "XFileFormat.h"

namespace nsgl
{
	//struct mszipd_stream {
	//	struct mspack_system	*sys;		// I/O routines
	//	struct mspack_file		*input;		// input file handle
	//	struct mspack_file		*output;	// output file handle
	//	unsigned int window_posn;			// offset within window

	//	// inflate() will call this whenever the window should be emptied.
	//	int (*flush_window)(struct mszipd_stream *, unsigned int);

	//	int error, repair_mode, bytes_output, input_end;

	//	// I/O buffering
	//	unsigned char *inbuf, *i_ptr, *i_end, *o_ptr, *o_end;
	//	unsigned int bit_buffer, bits_left, inbuf_size;

	//	// hufffman code lengths
	//	unsigned char LITERAL_len[MSZIP_LITERAL_MAXSYMBOLS];
	//	unsigned char DISTANCE_len[MSZIP_DISTANCE_MAXSYMBOLS];

	//	// huffman decoding tables
	//	unsigned char LITERAL_table[MSZIP_LITERAL_TABLESIZE];
	//	unsigned char DISTANCE_table[MSZIP_DISTANCE_TABLESIZE];

	//	// 32kb history window
	//	unsigned char window[MSZIP_FRAME_SIZE];
	//};

	bool CXFileAsBinary::ReadToTokens(FILE *fp)
	{
		unsigned char tokenType = 0;
		size_t r = ::fread(&tokenType, sizeof(unsigned char), 1, fp);
		if (r == 0) {
			return false;
		}

		unsigned char bl_count[8] = {0};
		unsigned char next_code[8] = {0};
		for (size_t bits = 1; bits <= 8; bits++) {
			tokenType = (tokenType + bl_count[bits-1]) << 1;
			next_code[bits] = tokenType;
		}

		switch (tokenType)
		{
		case TOKEN_NAME:
			{
				stTokenName tokName;
				tokName.token = TOKEN_NAME;
				::fread(&tokName.count, sizeof(size_t), 1, fp);
				::fread(&tokName.name, sizeof(char), tokName.count, fp);
				break;
			}
		}

		return true;
	}

	int CXFileAsBinary::Road()
	{
		if (_fp == NULL) {
			return 0;
		}

		stXHeader h;
		::fread(&h, sizeof(h), 1, this->_fp);

		ReadToTokens(this->_fp);

		return 1;
	}
}