#pragma	once
#ifndef	_NSFACTORY_H_
#define	_NSFACTORY_H_

// Including std::map;
#include <map>
// Including std::list;
#include <list>
// std::input_iterator;
#include <iterator>
// Including ns_types::TypeInfo;
#include "NSTypeInfo.h"
// Including Loki::AssocVector;
#include "AssocVector.h"

namespace ns_lib
{

	template <class IdentifierType, class ProductType>
	class ThrowFactoryError
	{

	public:
		class Exception : public std::exception		//std::exceptionを継承しているので
		{
			public:
			Exception(const IdentifierType& unknownId)
				: m_unknownId(unknownId)
			{
			}
			virtual const ns_types::char_t* what()
			{
				return	STR(Unknown object type passed to Factory.);
			}
			const IdentifierType GetId()
			{
				return	m_unknownId;
			};

		private:
			IdentifierType	m_unknownId;
		};

	protected:
		static ProductType* OnUnknownType(const IdentifierType& id)
		{
			throw	Exception(id);
		}
	};


	//////////////////////////////////////////////
	//	Object Factory, Modern C++ Design: page 209
	//	--------------------------
	//	template members:
	//	1. AbstractProduct	生成する型
	//	2. IdentifierType	実体生成関数に対応する ID, 初期値は size_t
	//	3. ProductCreator	実体生成関数, Functor<AbstractProduct*> を代入すれば柔軟に型に対応できる
	//	4. FactoryErrorPolicy	例外の定義クラス, 初期値は ThrowFactoryError (例外を投げる)
	//	--------------------------
	//	desc:
	//	実体生成関数を保持するクラスは Loki::AssocVector
	//	--------------------------
	//	Loki::AssocVector:
	//	std::map を単純に交換できるもの（同じメンバカンスウが提供されている）であり、
	//	std::vector を用いて実装されている。map との違いは、
	//		1. erase 関数の動作（ AssocVector::erase によってオブジェクト内の全てのイテレータが破棄される。）
	//		2. insert と erase における動作の複雑さ（定数(map)ではなく線形(AssocVector)に比例する。
	//	参考文献:
	//	たぶん究極のC++ライブラリ、Lokiを使う - AssocVector
	//	http://shinh.skr.jp/loki/AssocVector.html
	//	--------------------------
	//	Use SingletonHolder (recommendation)
	template
	<
		class AbstractProduct,								// 生成する型
		typename IdentifierType = size_t,					// 実体生成関数に対応するID
		typename ProductCreator = AbstractProduct* (*)(),	// 実体生成関数
		template<typename, class>							// 生成関数が登録されていなかった場合のポリシー
			class FactoryErrorPolicy = ThrowFactoryError	//  -標準は例外を投げる
	>
	class CFactory
		: public FactoryErrorPolicy<IdentifierType, AbstractProduct>
	{
	public:
		bool Register(const IdentifierType& id, ProductCreator creator)
		{
			return	m_associations.insert(
				AssocMap::value_type(id, creator)).second;
		}

		bool Unregister(const IdentifierType& id)
		{
			return	m_associations.erase(id) == true;
		}

		AbstractProduct* GenerateObject(const IdentifierType& id)
		{
			typename AssocMap::iterator	i = m_associations.find(id);
			if (i != m_associations.end())
				return	(i->second)();	// 生成関数の登録有り
			return	OnUnknownType(id);	// 無し
		}

	private:
		typedef std::map<IdentifierType, ProductCreator>	AssocMap;
		AssocMap	m_associations;
	};

	//////////////////////////
	//	CloneFactory, page 227
	//	---------------------
	//	AssocVector: erase オブジェクト中の全てのイテレータを破壊
	//				 insert, erase の速度が定数ではなく線形に比例
	template
	<
		class AbstractProduct,
		class ProductCreator = AbstractProduct* (*)(AbstractProduct*),
		template <typename, class>
			class FactoryErrorPolicy = ThrowFactoryError
	>
	class CloneFactory
	{
	public:
		AbstractProduct* CreateObject(const AbstractProduct* model);
		bool Register(const ns_types::TypeInfo&, ProductCreator creator);
		bool Unregister(const ns_types::TypeInfo&);
	private:
		typedef Loki::AssocVector<ns_types::TypeInfo, ProductCreator>
			IdToProductMap;
		IdToProductMap m_associations;
	};

}


#endif	// _NSFACTORY_H_