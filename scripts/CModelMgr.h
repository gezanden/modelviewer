//
//	CModelMgr.h
//
//	Copyright (c) 2006, 2008 Naoki Shimoyamada, All Rights Reserved.
//

#pragma	once
#ifndef	_CMODELMGR_H_
#define	_CMODELMGR_H_

//--------------------------------------------------------
//	Includes
//--------------------------------------------------------
// including ns_lib::CSingleton, ns_lib::CSingletonHolder;
#include	"NSSingleton.h"
// including ns_lib::CFactory;
#include	"NSFactory.h"
// including ns_lib::CFunctorHandler;
#include	"NSFunctor.h"
// including ns_lib::CSinglyLinkedList;
#include	"NSList.h"
// including ns_model::CModel;
#include	"CModelInit.h"
// including ns_model::GetModel;
#include	"XFileFuncs.h"
#include	"CModelAllocator.h"
#include	"CObjectPool.h"
// including time_t, clock();
#include	<time.h>
//#include	"nsd3d9.h"



//--------------------------------------------------------
//	Namespace
//--------------------------------------------------------
namespace ns_model
{

	namespace Private
	{
		CModelInit* GetModelInit();
		CModelInit* GetSkinedMeshInit();

	//--------------------------------------------------------
	//	Structure
	//--------------------------------------------------------
	typedef struct _MODELNODE_
	{
		CModel*	pModel;
		_MODELNODE_() : pModel() {}
		_MODELNODE_(CModel* lpModel)
			: pModel(lpModel)
		{}
	} MODELNODE, *LPMODELNODE;


	}




	//--------------------------------------------------------
	//	Class
	//--------------------------------------------------------

	//////////////////////////////////////////////////////////
	//	Name:	CModelMgr
	//	Desc:	モデルを管理するクラス。
	//			モデルにIDをつけて、一つのモデルデータの使いまわしを
	//			可能にする。
	//			- Abstract Factory
	//	Reference documents:
	//			・著: Erich Gamma/Richard Helm/Ralph Johnson/John Vlissides 訳: 本位田 真一/吉田 和樹 (1999/10)
	//				『オブジェクト指向における再利用のためのデザインパターン 改訂版』 P95
	//
	class CModelMgr// : public ns_lib::CSingleton<CModelMgr>
	{

		typedef struct _CHARACTORNODE_
		{
			CModel*	pModel;		// モデルクラス
			size_t	nId;		// モデルID
			MATRIX	matrix;		// IDごとの変換行列
			bool	bExist;		// レンダリングフラグ
			_CHARACTORNODE_() : pModel(), nId(), matrix(), bExist() {
				MatrixIdentity(&matrix);
			}
			_CHARACTORNODE_(CModel* lpModel, size_t id)
				: pModel(lpModel)
				, nId(id)
				, matrix()
				, bExist(true) {
					MatrixIdentity(&matrix);
			}
		} CHARACTORNODE, *LPCHARACTORNODE;

		typedef struct _CHARACTORID_
		{
			size_t	nId;		// モデルID
			MATRIX	matrix;		// IDごとの変換行列
			//bool	bExist;		// レンダリングフラグ
			_CHARACTORID_() : nId(), matrix() {
				MatrixIdentity(&matrix);
			}
			_CHARACTORID_(size_t id)
				: nId(id)
				, matrix() {
					MatrixIdentity(&matrix);
			}
		} CHARACTORID, *LPCHARACTORID;

		// Factory: 生成関数を保持
		// Pool: 実体モデルとIDごとの情報を保持
		// Mgr: 
		typedef ns_lib::CSingletonHolder<nsl::CObjectPool<CHARACTORID, CModel, ns_lib::CFunctor<void, TYPELIST_2(LPCHARACTORID, CModel*)>>> ModelPool;
		//typedef ns_lib::CSingletonHolder<ns_lib::CFactory<CModel>> ModelFactory;	// 生成関数が多くなったとき用
		typedef ns_lib::CFunctor<CModel*, TYPELIST_1(const char*)> FunctorFactory;

		template <class T> struct CreatePolicy
		{
			static T* Create()
			{
				return new T(&CModelAllocator::GetModelFromXFile); // Functor生成時に登録
			}

			static void Destroy(T* p)
			{ delete p; }
		};

		typedef ns_lib::CSingletonHolder<FunctorFactory, CreatePolicy> ModelAllocator; //GetModel()

	public:
		CModelMgr();
		~CModelMgr() {}
		//static CModelMgr& Instance() { return CModelMgr::GetSingleton(); }
		//void Release() { CModelMgr::Instance().ReleaseSingleton(); }

		size_t RegisterModel( const char_t* name );						// モデルデータリストに指定されたモデルを読み込んで保持させる 戻り値（ID）
		size_t RegisterModel( const char* name );					// モデルデータリストに指定されたモデルを読み込んで保持させる 戻り値（ID）
		size_t RegisterModelAsPlayer( const char* name, const float scale, const float yaxis );
		//size_t RegisterModelAsFloor(const char* name);
		void RegisterId( const size_t charId, const size_t modelId );	// キャラクタID生成→モデルIDリストに登録
		//void RegisterId( const size_t charId, const char_t* modelName );
		void UnregisterId( const size_t id );
		void UnRegisterModel( const size_t id );
		void RegisterProtagonist( const size_t id );
		void Delete();
		void RLZ();														// 描画フラグONのモデルを全て描画
		void RLZTest();
		void Render() {
			if (m_nModelation) {
				ModelPool::_Inst().Execute();
			}
		}
		static void CModelMgr::Renderer(LPCHARACTORID __id, CModel* __model) {
			__model->Render(&__id->matrix);
		}
		void Execute() {
			ModelPool::Instance().Execute();
		}
		void Landing();
		CModel* CallFactory(const char* name) {
			return ModelAllocator::Instance()(name);
		}

	private:

		CModelMgr( const CModelMgr& );
		CModelMgr& operator=( const CModelMgr& );

		bool SetModel( const char_t* name, size_t id );
		CModel* GetModel( const char* name );
		CModel* GetModel( const wchar_t* name );

		enum FactoryId
		{
			eXFileFactory,
			eStanderdInitFactory,
			eSkinedMeshInitFactory,
		};

		size_t	m_nModelation;	// 登録モデル数
		size_t	m_nCharactors;	// キャラ数
		LPCHARACTORID m_CharaLockedID;
		CModel* m_CharaLocked;
		ns_lib::CSinglyLinkedList<CHARACTORNODE>	m_Charactors;		// キャラ(ID)リスト
		ns_lib::CSinglyLinkedList<CModel>			m_ConcreteModels;	// モデルデータリスト
	};

}	// namespace: ns_model

typedef ns_lib::CSingletonHolder<ns_model::CModelMgr> ModelMgr;

namespace ns_model {

	class CWorldOrganizer {

		typedef struct _LANDID_
		{
			size_t	nId;
			MATRIX	matrix;
			_LANDID_() : nId(), matrix() {
				MatrixIdentity(&matrix);
			}
			_LANDID_(size_t id)
				: nId(id)
				, matrix() {
					MatrixIdentity(&matrix);
			}
		} LANDID, *LPLANDID;


		// typedef ns_lib::CSingletonHolder<nsl::CObjectPool<CHARACTORID, CModel, ns_lib::CFunctor<void, TYPELIST_2(LPCHARACTORID, CModel*)>>> ObjPool;

	public:
		CWorldOrganizer() : m_Land(NULL), m_LandID(NULL), m_index(), m_fallingout(), m_scream(), m_specular(.0f), m_gravity(9.8f), m_vjump(.0f), m_fallstart(), m_falltime() {};
		~CWorldOrganizer() {
			if (m_Land != NULL) {
				delete m_Land;
			}
		}
		void RegisterLand(const char* name);
		void UnRegisterObj();
		void Landing(LPMATRIX mtx_pos, const LPVECTOR3 vec_point);
		void Render() {
			m_Land->Render(&m_LandID->matrix);
		}
		void Jumping() {
			m_vjump = .3f;
			m_fallstart = clock();
		}
		//void DebuggingRender() {
		//	m_Land->Render(&m_LandID->matrix);
		//	LPDIRECT3DDEVICE9 device = ns_d3d::Cd3d::Instance().Getd3dDevice();
		//	device->SetVertexShaderConstantF(14, m_specular, 1);
		//	device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, num_vertex, m_index*3, 1);


	private:
		CWorldOrganizer(const CWorldOrganizer&);
		CWorldOrganizer& operator=(const CWorldOrganizer&);
		float Falling();

		CModel*	m_Land;
		LPLANDID m_LandID;
		size_t m_index;
		size_t m_fallingout;
		size_t m_scream;
		float m_specular;
		float m_gravity;
		float m_vjump;
		time_t m_fallstart;
		time_t m_falltime;
	};
}

typedef ns_lib::CSingletonHolder<ns_model::CWorldOrganizer> WorldOrg;

#endif	// _CMODELMGR_H_